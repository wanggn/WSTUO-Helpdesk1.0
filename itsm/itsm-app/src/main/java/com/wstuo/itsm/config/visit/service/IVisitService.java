package com.wstuo.itsm.config.visit.service;

import com.wstuo.itsm.config.visit.dto.VisitDTO;
import com.wstuo.common.dto.PageDTO;

/**
 * 请求回访服务层接口
 * @author QXY
 *
 */
public interface IVisitService {
	/**
	 * 分页查询回访项
	 * @param start
	 * @param limit
	 * @param sord
	 * @param sidx
	 * @return PageDTO
	 */
	PageDTO findPagerVisit(VisitDTO visitDTO, int start, int limit,String sord, String sidx,boolean useStatus);
	/**
	 * 回访项保存
	 * @param visitDto
	 */
	void visitSave(VisitDTO visitDto);
	/**
	 * 回访项更新
	 * @param visitDto
	 */
	void visitUpdate(VisitDTO visitDto);
	/**
	 * 回访项删除
	 * @param visitNo
	 */
	void visitDelete(Long[] visitNo);
	/**
	 * 查询所有回访项
	 * @param start
	 * @param limit
	 * @param sord
	 * @param sidx
	 * @return PageDTO
	 */
	PageDTO findAllVisit(VisitDTO visitDTO,int start, int limit,String sord, String sidx,boolean useStatus);
	
	/**
	 * 根据ID查找回访项
	 * @param visitNo
	 */
	VisitDTO findById(Long visitNo);
}
