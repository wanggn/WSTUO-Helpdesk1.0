package com.wstuo.itsm.config.visit.dao;

import com.wstuo.itsm.config.visit.dto.VisitDTO;
import com.wstuo.itsm.config.visit.entity.Visit;
import com.wstuo.common.dao.IEntityDAO;
import com.wstuo.common.dto.PageDTO;
/**
 * 请求回访DAO接口类
 * @author QXY
 *
 */
public interface IVisitDAO extends IEntityDAO<Visit> {
	/**
	 * 分页查询请求回访事项
	 * @param start
	 * @param limit
	 * @param sord
	 * @param sidx
	 * @return PageDTO
	 */
	PageDTO findPager(VisitDTO visitDTO, int start, int limit,String sord, String sidx,boolean useStatus);
}
