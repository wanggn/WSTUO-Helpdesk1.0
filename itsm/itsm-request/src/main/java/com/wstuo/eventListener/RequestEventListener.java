package com.wstuo.eventListener;

import org.jbpm.api.listener.EventListener;
import org.jbpm.api.listener.EventListenerExecution;
import org.springframework.transaction.annotation.Transactional;



/**
 * 请求事件监听
 * 
 * @author WSTUO_QXY
 * 
 */
@SuppressWarnings("serial")
@Deprecated
public class RequestEventListener implements EventListener {
	@Deprecated
	String type;
	/**
	 * 通知
	 */
	@Transactional
	public void notify(EventListenerExecution execution) {
		
	}
	
}
