package com.wstuo.itsm.request.dto;

import java.util.Date;

/**
 * 请求详细DTO
 * @author QXY
 *
 */
public class RequestInfoDTO {
	private Long eno;
    private String ecategoryName;
    private String requestCode;
    private String etitle;
    private String edesc;
    private String effectRangeName;
    private String effectRemark;
    private String seriousnessName;
    private String priorityName;
    private String statusName;
    private String address;
    private Date createdOn;
    private String createdByName;
    private String technicianName;
    private String imodeName;
    private String levelName;
    private String assigneeGroupName;
    private String assigneeName;
    private String solutions;
    private Date closeTime;
    private Date responsesTime;
    private String companyName;//客户名称
    private Long companyNo;//客户No
    private Boolean hang=false;//挂起(20110831-QXY)
	public Long getEno() {
		return eno;
	}
	public void setEno(Long eno) {
		this.eno = eno;
	}
	public String getEcategoryName() {
		return ecategoryName;
	}
	public void setEcategoryName(String ecategoryName) {
		this.ecategoryName = ecategoryName;
	}
	public String getEtitle() {
		return etitle;
	}
	public void setEtitle(String etitle) {
		this.etitle = etitle;
	}
	public String getEdesc() {
		return edesc;
	}
	public void setEdesc(String edesc) {
		this.edesc = edesc;
	}
	public String getEffectRangeName() {
		return effectRangeName;
	}
	public void setEffectRangeName(String effectRangeName) {
		this.effectRangeName = effectRangeName;
	}
	public String getEffectRemark() {
		return effectRemark;
	}
	public void setEffectRemark(String effectRemark) {
		this.effectRemark = effectRemark;
	}
	public String getSeriousnessName() {
		return seriousnessName;
	}
	public void setSeriousnessName(String seriousnessName) {
		this.seriousnessName = seriousnessName;
	}
	public String getPriorityName() {
		return priorityName;
	}
	public void setPriorityName(String priorityName) {
		this.priorityName = priorityName;
	}
	public String getStatusName() {
		return statusName;
	}
	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public String getCreatedByName() {
		return createdByName;
	}
	public void setCreatedByName(String createdByName) {
		this.createdByName = createdByName;
	}
	public String getTechnicianName() {
		return technicianName;
	}
	public void setTechnicianName(String technicianName) {
		this.technicianName = technicianName;
	}
	public String getImodeName() {
		return imodeName;
	}
	public void setImodeName(String imodeName) {
		this.imodeName = imodeName;
	}
	public String getLevelName() {
		return levelName;
	}
	public void setLevelName(String levelName) {
		this.levelName = levelName;
	}
	public String getAssigneeGroupName() {
		return assigneeGroupName;
	}
	public void setAssigneeGroupName(String assigneeGroupName) {
		this.assigneeGroupName = assigneeGroupName;
	}
	public String getAssigneeName() {
		return assigneeName;
	}
	public void setAssigneeName(String assigneeName) {
		this.assigneeName = assigneeName;
	}
	public String getSolutions() {
		return solutions;
	}
	public void setSolutions(String solutions) {
		this.solutions = solutions;
	}
	public Date getCloseTime() {
		return closeTime;
	}
	public void setCloseTime(Date closeTime) {
		this.closeTime = closeTime;
	}
	public Date getResponsesTime() {
		return responsesTime;
	}
	public void setResponsesTime(Date responsesTime) {
		this.responsesTime = responsesTime;
	}
	public String getRequestCode() {
		return requestCode;
	}
	public void setRequestCode(String requestCode) {
		this.requestCode = requestCode;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public Long getCompanyNo() {
		return companyNo;
	}
	public void setCompanyNo(Long companyNo) {
		this.companyNo = companyNo;
	}
	public Boolean getHang() {
		return hang;
	}
	public void setHang(Boolean hang) {
		this.hang = hang;
	}
    
}
