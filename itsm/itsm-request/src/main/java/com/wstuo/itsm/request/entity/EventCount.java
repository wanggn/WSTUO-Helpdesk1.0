package com.wstuo.itsm.request.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * 事件创建统计器
 * @author WSTUO_QXY
 *
 */
@Entity
public class EventCount {
	@Id
    @GeneratedValue( strategy = GenerationType.AUTO )
	private Long eventCountId;
	/**创建日期*/
	private Date eventCreateDate = new Date();
	/**事件类型*/
	private String type;
	/**事件编号*/
	private Long eno;
	public EventCount(){
		super();
	}
	public EventCount(Long eno , String type){
		this.eno = eno;
		this.type = type;
	}
	public Long getEventCountId() {
		return eventCountId;
	}
	public void setEventCountId(Long eventCountId) {
		this.eventCountId = eventCountId;
	}
	public Date getEventCreateDate() {
		return eventCreateDate;
	}
	public void setEventCreateDate(Date eventCreateDate) {
		this.eventCreateDate = eventCreateDate;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Long getEno() {
		return eno;
	}
	public void setEno(Long eno) {
		this.eno = eno;
	}

	
	
}
