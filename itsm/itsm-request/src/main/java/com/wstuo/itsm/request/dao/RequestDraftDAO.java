package com.wstuo.itsm.request.dao;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.wstuo.itsm.request.dto.RequestDraftQueryDTO;
import com.wstuo.itsm.request.entity.RequestDraft;
import com.wstuo.common.dao.BaseDAOImplHibernate;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.util.StringUtils;

/**
 * 事件统计DAO类
 * @author WSTUO_QXY
 *
 */
public class RequestDraftDAO extends BaseDAOImplHibernate<RequestDraft> implements IRequestDraftDAO {
	
	public PageDTO findPageRequestDraft(RequestDraftQueryDTO queryDTO){
		final DetachedCriteria dc = DetachedCriteria.forClass( RequestDraft.class );
        int start = 0;
        int limit = 0;
        if(queryDTO!=null){
        	start=queryDTO.getStart();
        	limit=queryDTO.getLimit();
        	//dj.add(Restrictions.eq("scheduledTaskId", queryDTO.getScheduledTaskId()));
        	if(queryDTO.getCompanyNo()!=null)
        		dc.add(Restrictions.eq("companyNo", queryDTO.getCompanyNo()));
            if(StringUtils.hasText(queryDTO.getCreator()))
            	dc.add(Restrictions.eq("creator",queryDTO.getCreator()));
            if(StringUtils.hasText(queryDTO.getRequestDraftName()))
            	dc.add(Restrictions.eq("requestDraftName",queryDTO.getRequestDraftName()));
            if(queryDTO.getRequestDraftID()!=null)
            	dc.add(Restrictions.eq("requestDraftID", queryDTO.getRequestDraftID()));
            if(StringUtils.hasText(queryDTO.getLoginName()))
            	dc.add(Restrictions.eq("loginName", queryDTO.getLoginName()));
        
        //排序
        if(queryDTO.getSord()!=null&&!queryDTO.getSord().equals("")&&queryDTO.getSidx()!=null&&!queryDTO.getSidx().equals("")){
            if(queryDTO.getSord().equals("desc"))
            	dc.addOrder(Order.desc(queryDTO.getSidx()));
            else
            	dc.addOrder(Order.asc(queryDTO.getSidx()));
        }

        }
        return super.findPageByCriteria(dc, start,limit);

	}
}
