package com.wstuo.itsm.request.dao;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;

import com.wstuo.itsm.request.dto.EventCountDTO;
import com.wstuo.itsm.request.entity.EventCount;
import com.wstuo.common.dao.BaseDAOImplHibernate;
import com.wstuo.common.util.StringUtils;

/**
 * 事件统计DAO类
 * @author WSTUO_QXY
 *
 */
public class EventCountDAO extends BaseDAOImplHibernate<EventCount> implements IEventCountDAO {
	/**
	 * 统计当天创建的事件
	 * @param dto
	 * @return Integer
	 */
	public Integer eventCount(EventCountDTO dto){
		final DetachedCriteria dc = DetachedCriteria.forClass(EventCount.class);
		if(dto!=null && StringUtils.hasText(dto.getType())){
		dc.add(Restrictions.eq("type", dto.getType()));
		
		Calendar timeStartCl = new GregorianCalendar();
		timeStartCl.setTime(new Date());
		timeStartCl.set(Calendar.HOUR_OF_DAY, 0);
		timeStartCl.set(Calendar.MINUTE, 0);
		timeStartCl.set(Calendar.SECOND, 0);
    	
		Calendar timeEndCl = new GregorianCalendar();
		timeEndCl.setTime(new Date());
		timeEndCl.set(Calendar.HOUR_OF_DAY, 23);
		timeEndCl.set(Calendar.MINUTE, 59);
		timeEndCl.set(Calendar.SECOND, 59);
		
		dc.add(Restrictions.between("eventCreateDate", timeStartCl.getTime(), timeEndCl.getTime()));
		}
		return super.statCriteria(dc);
	
	}
}
