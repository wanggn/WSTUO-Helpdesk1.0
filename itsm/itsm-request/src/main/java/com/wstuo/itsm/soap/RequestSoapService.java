package com.wstuo.itsm.soap;

import javax.jws.WebService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.wstuo.itsm.request.dto.RequestDTO;
import com.wstuo.itsm.request.service.IRequestService;
import com.wstuo.common.security.dao.IUserDAO;
import com.wstuo.common.security.entity.User;

/**
 * 请求SOAP协议实现类
 * @author QXY
 *
 */
@WebService(endpointInterface="com.wstuo.itsm.soap.IRequestSoapService")//表示是要发布的web 服务
public class RequestSoapService implements IRequestSoapService {
	@Autowired
	private IRequestService requestService;
	@Autowired
	private IUserDAO userDAO;
	
	/**
	 * 保存请求
	 * @param endto
	 * @param message
	 * @return String
	 */
	@Transactional
	public String soapSaveRequest(String endto,String message) {
		String result="FAIL";
		RequestDTO dto=new RequestDTO();
		dto.setEtitle("监听");
		dto.setEdesc(message);
		dto.setCompanyNo(1L);
		User user=userDAO.findUniqueBy("moblie", endto);
		if(user!=null){
			dto.setCreatedByName(user.getLoginName());
			dto.setAssigneeNo(user.getUserId());
			requestService.saveRequest(dto);
			result="SUCCESS";
		}
		return result;
	}

}
