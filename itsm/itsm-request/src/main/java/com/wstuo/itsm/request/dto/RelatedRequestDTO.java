package com.wstuo.itsm.request.dto;

/**
 * 关联请求DTO
 * @author WSTUO_QXY
 *
 */
public class RelatedRequestDTO {
	/**
	 * 关联ID
	 */
	private Long relatedId;
	/**
	 * 关联的请求Nos
	 */
	private Long[] requestNo;
	/**
	 * 变更ID
	 */
	private Long changeId;
	public Long getRelatedId() {
		return relatedId;
	}
	public void setRelatedId(Long relatedId) {
		this.relatedId = relatedId;
	}
	public Long[] getRequestNo() {
		return requestNo;
	}
	public void setRequestNo(Long[] requestNo) {
		this.requestNo = requestNo;
	}
	public Long getChangeId() {
		return changeId;
	}
	public void setChangeId(Long changeId) {
		this.changeId = changeId;
	}
	
}
