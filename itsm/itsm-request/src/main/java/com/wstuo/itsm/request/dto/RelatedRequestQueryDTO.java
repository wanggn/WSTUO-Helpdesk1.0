package com.wstuo.itsm.request.dto;

import com.wstuo.common.dto.BaseDTO;

/**
 * 关联请求查询DTO
 * @author WSTUO_QXY
 *
 */
@SuppressWarnings("serial")
public class RelatedRequestQueryDTO extends BaseDTO {
	/**
	 * 关联ID
	 */
	private Long relatedId;
	/**
	 * 关联请求NO
	 */
	private Long requestNo;
	/**
	 * 变更ID
	 */
	private Long changeId;
	public Long getRelatedId() {
		return relatedId;
	}
	public void setRelatedId(Long relatedId) {
		this.relatedId = relatedId;
	}
	public Long getRequestNo() {
		return requestNo;
	}
	public void setRequestNo(Long requestNo) {
		this.requestNo = requestNo;
	}
	public Long getChangeId() {
		return changeId;
	}
	public void setChangeId(Long changeId) {
		this.changeId = changeId;
	}
	
}
