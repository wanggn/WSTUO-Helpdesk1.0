package com.wstuo.itsm.request.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.ForeignKey;

import com.wstuo.common.config.category.entity.EventCategory;
import com.wstuo.common.config.comment.entity.Comment;
import com.wstuo.common.config.dictionary.entity.DataDictionaryItems;
import com.wstuo.common.sla.entity.SLARule;
import com.wstuo.common.security.entity.Organization;
import com.wstuo.common.security.entity.OrganizationServices;
import com.wstuo.common.security.entity.TechnicalGroup;
import com.wstuo.common.security.entity.User;
import com.wstuo.itsm.cim.entity.CI;
import com.wstuo.itsm.domain.dao.Event;


/**
 * 请求信息实体类
 * @author QXY
 *
 */

@SuppressWarnings("serial")
@Entity
@Table(name="cu_request")
public class Request extends Event{
	@ManyToOne
    @JoinColumn( name = "imodeNo" )
    /** 来源 */
    private DataDictionaryItems imode;
	@ManyToOne
    @JoinColumn( name = "levelNo" )
    private DataDictionaryItems level;//级别
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn( name = "groupNo" )
    private Organization assigneeGroup;//指派组
    @Column(nullable=true)
    private String pid;//PID
    private Boolean isFCR=false;//是否FCR
    private String requestCode;//请求编号
    @Temporal(TemporalType.TIMESTAMP) 
    private Date responsesTime;//请求实际响应时间
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn( name = "serviceOrgNo" )
	private OrganizationServices servicesOrg;//服务机构
    private Long maxResponseTime;//最大响应时间
    private Long maxCompleteTime;//最大完成时间
    @Temporal(TemporalType.TIMESTAMP) 
    private Date closeTime;//关闭时间（请求实际完成时间）
    @Temporal(TemporalType.TIMESTAMP) 
    private Date autoUpdateTime;//自动更新时间
    @ManyToMany
    @Fetch(FetchMode.SUBSELECT)
    private List<EventCategory> serviceDirectory;   //服务目录
    @ManyToMany
    private List<CI> relatedConfigureItems;//关联配置项
    @ManyToOne
    @JoinColumn( name = "ownerNo" )
    private User owner;//负责人
    @ManyToOne
    @JoinColumn( name = "remarkStatusNo" )
    private DataDictionaryItems remarkStatus;//备注状态
    @ManyToOne
    @JoinColumn( name = "requestResolvedUserNo" )
    private User requestResolvedUser;//请求处理完成用户
    @Temporal(TemporalType.TIMESTAMP)
    private Date requestResolvedTime;//请求处理完成时间
    private Long upgradeApplySign;//请求申请升级标记:1、要求升级标记 ，2、升级完毕
    @Deprecated
    private Long maxCompleteTimeBack;//SLA最迟完成时间
    @Deprecated
    private Long maxResponseTimeBack;//SLA最迟响应时间
    @Column(nullable=true)
    private String approvalState;//审批状态    
    @Column(nullable=true)
    private String visitRecord;//回访记录
    private Long visitState;//回访状态:1：回访成功,2:邮件回访中,0:未回访
	@Deprecated
    @ManyToOne
    @JoinColumn( name = "categoryNo" )
    private EventCategory requestCategory; //请求分类
    @ManyToOne
    @JoinColumn( name = "slaNo" )
	private SLARule slaRule;//SLA
    private String visitSign;
    private Boolean submitChange=false;//是否已提交变更
    private Boolean deleteSign=false;
    private String approvalName;
    private Long approvalNo;
    private Boolean hang=false;//挂起(20110831-QXY)
    private Boolean createdProblem=false;//标识是否自动转为问题
    @ManyToOne
    private DataDictionaryItems slaState;//SLA状态(20120218:QXY)
    /*** 挂起时间*/
    private Date hangTime;
    /**已经转换为问题*/
    private Boolean isConvertdToProblem=false;
    /**已经转换为变更*/
    private Boolean isConvertdToChange=false;
    /**是否优化后的流程处理*/
    @Deprecated
    private String newFlowHandle;
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn( name = "tcGroupNo" )
    private TechnicalGroup technicalGroup;
    private Boolean slaExpired=false;//SLA是否已过期
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn( name = "requestServiceNo" )
    private EventCategory requestServiceDirectory;//请求匹配规则服务目录
    @ManyToOne
    @JoinColumn( name = "offmodeNo" )
	private DataDictionaryItems offmode;//级别
    
    @OneToOne(fetch=FetchType.LAZY)
    @ForeignKey(name="reqsolution")
    private Comment requestSolutions;//解决方案
    //实际处理时长
    private Long handleTimeLong=0L;
    private Long formId;

    private String isShowBorder;
    private Boolean isNewForm;
    
	public Long getApprovalNo() {
		return approvalNo;
	}
	public void setApprovalNo(Long approvalNo) {
		this.approvalNo = approvalNo;
	}
	public Boolean getIsNewForm() {
		return isNewForm;
	}
	public void setIsNewForm(Boolean isNewForm) {
		this.isNewForm = isNewForm;
	}
	public String getIsShowBorder() {
		return isShowBorder;
	}
	public void setIsShowBorder(String isShowBorder) {
		this.isShowBorder = isShowBorder;
	}
	public Long getFormId() {
		return formId;
	}
	public void setFormId(Long formId) {
		this.formId = formId;
	}
	public List<CI> getRelatedConfigureItems() {
		return relatedConfigureItems;
	}
	public void setRelatedConfigureItems(List<CI> relatedConfigureItems) {
		this.relatedConfigureItems = relatedConfigureItems;
	}
	public Comment getRequestSolutions() {
		return requestSolutions;
	}
	public void setRequestSolutions(Comment requestSolutions) {
		this.requestSolutions = requestSolutions;
	}
	public EventCategory getRequestServiceDirectory() {
		return requestServiceDirectory;
	}
	public void setRequestServiceDirectory(EventCategory requestServiceDirectory) {
		this.requestServiceDirectory = requestServiceDirectory;
	}
	public List<EventCategory> getServiceDirectory() {
		return serviceDirectory;
	}
	public void setServiceDirectory(List<EventCategory> serviceDirectory) {
		this.serviceDirectory = serviceDirectory;
	}
	public TechnicalGroup getTechnicalGroup() {
		return technicalGroup;
	}
	public void setTechnicalGroup(TechnicalGroup technicalGroup) {
		this.technicalGroup = technicalGroup;
	}
	public Request() {
		super();
	}
    public String getApprovalState() {
		return approvalState;
	}
	public void setApprovalState(String approvalState) {
		this.approvalState = approvalState;
	}

	public Long getMaxCompleteTimeBack() {
		return maxCompleteTimeBack;
	}

	public void setMaxCompleteTimeBack(Long maxCompleteTimeBack) {
		this.maxCompleteTimeBack = maxCompleteTimeBack;
	}

	public String getRequestCode() {
		return requestCode;
	}

	public void setRequestCode(String requestCode) {
		this.requestCode = requestCode;
	}

	public Date getAutoUpdateTime() {
		return autoUpdateTime;
	}

	public void setAutoUpdateTime(Date autoUpdateTime) {
		this.autoUpdateTime = autoUpdateTime;
	}
    public Long getMaxResponseTime() {
		return maxResponseTime;
	}

	public void setMaxResponseTime(Long maxResponseTime) {
		this.maxResponseTime = maxResponseTime;
	}

	public Long getMaxCompleteTime() {
		return maxCompleteTime;
	}

	public void setMaxCompleteTime(Long maxCompleteTime) {
		this.maxCompleteTime = maxCompleteTime;
	}
	public String getPid() {
		return pid;
	}
	public void setPid(String pid) {
		this.pid = pid;
	}
    public DataDictionaryItems getImode(  )
    {
        return imode;
    }
    public void setImode( DataDictionaryItems imode )
    {
        this.imode = imode;
    }
    public DataDictionaryItems getLevel(  )
    {
        return level;
    }
    public void setLevel( DataDictionaryItems level )
    {
        this.level = level;
    }
    public Organization getAssigneeGroup() {
		return assigneeGroup;
	}
	public void setAssigneeGroup(Organization assigneeGroup) {
		this.assigneeGroup = assigneeGroup;
	}
	public OrganizationServices getServicesOrg() {
		return servicesOrg;
	}
	public void setServicesOrg(OrganizationServices servicesOrg) {
		this.servicesOrg = servicesOrg;
	}
	public Date getCloseTime() {
		return closeTime;
	}
	public void setCloseTime(Date closeTime) {
		this.closeTime = closeTime;
	}
	public Date getResponsesTime() {
		return responsesTime;
	}
	public void setResponsesTime(Date responsesTime) {
		this.responsesTime = responsesTime;
	}
	public User getOwner() {
		return owner;
	}
	public void setOwner(User owner) {
		this.owner = owner;
	}
	public DataDictionaryItems getRemarkStatus() {
		return remarkStatus;
	}
	public void setRemarkStatus(DataDictionaryItems remarkStatus) {
		this.remarkStatus = remarkStatus;
	}
	public User getRequestResolvedUser() {
		return requestResolvedUser;
	}
	public void setRequestResolvedUser(User requestResolvedUser) {
		this.requestResolvedUser = requestResolvedUser;
	}
	public Date getRequestResolvedTime() {
		return requestResolvedTime;
	}
	public void setRequestResolvedTime(Date requestResolvedTime) {
		this.requestResolvedTime = requestResolvedTime;
	}
	public Long getUpgradeApplySign() {
		return upgradeApplySign;
	}
	public void setUpgradeApplySign(Long upgradeApplySign) {
		this.upgradeApplySign = upgradeApplySign;
	}
	public EventCategory getRequestCategory() {
		return requestCategory;
	}
	public void setRequestCategory(EventCategory requestCategory) {
		this.requestCategory = requestCategory;
	}
	public String getVisitRecord() {
		return visitRecord;
	}
	public void setVisitRecord(String visitRecord) {
		this.visitRecord = visitRecord;
	}
	public Long getVisitState() {
		return visitState;
	}
	public void setVisitState(Long visitState) {
		this.visitState = visitState;
	}
	public String getVisitSign() {
		return visitSign;
	}
	public void setVisitSign(String visitSign) {
		this.visitSign = visitSign;
	}
	public String getApprovalName() {
		return approvalName;
	}
	public void setApprovalName(String approvalName) {
		this.approvalName = approvalName;
	}
	public Long getMaxResponseTimeBack() {
		return maxResponseTimeBack;
	}
	public void setMaxResponseTimeBack(Long maxResponseTimeBack) {
		this.maxResponseTimeBack = maxResponseTimeBack;
	}
	public DataDictionaryItems getSlaState() {
		return slaState;
	}
	public void setSlaState(DataDictionaryItems slaState) {
		this.slaState = slaState;
	}
	public SLARule getSlaRule() {
		return slaRule;
	}
	public void setSlaRule(SLARule slaRule) {
		this.slaRule = slaRule;
	}
	public Date getHangTime() {
		return hangTime;
	}
	public void setHangTime(Date hangTime) {
		this.hangTime = hangTime;
	}
	public Boolean getIsConvertdToProblem() {
		return isConvertdToProblem;
	}
	public void setIsConvertdToProblem(Boolean isConvertdToProblem) {
		this.isConvertdToProblem = isConvertdToProblem;
	}
	public Boolean getIsConvertdToChange() {
		return isConvertdToChange;
	}
	public void setIsConvertdToChange(Boolean isConvertdToChange) {
		this.isConvertdToChange = isConvertdToChange;
	}
	public String getNewFlowHandle() {
		return newFlowHandle;
	}
	public void setNewFlowHandle(String newFlowHandle) {
		this.newFlowHandle = newFlowHandle;
	}
	public DataDictionaryItems getOffmode() {
		return offmode;
	}
	public void setOffmode(DataDictionaryItems offmode) {
		this.offmode = offmode;
	}
	public Boolean getIsFCR() {
		return isFCR;
	}
	public void setIsFCR(Boolean isFCR) {
		this.isFCR = isFCR;
	}
	public Boolean getSubmitChange() {
		return submitChange;
	}
	public void setSubmitChange(Boolean submitChange) {
		this.submitChange = submitChange;
	}
	public Boolean getDeleteSign() {
		return deleteSign;
	}
	public void setDeleteSign(Boolean deleteSign) {
		this.deleteSign = deleteSign;
	}
	public Boolean getHang() {
		return hang;
	}
	public void setHang(Boolean hang) {
		this.hang = hang;
	}
	public Boolean getCreatedProblem() {
		return createdProblem;
	}
	public void setCreatedProblem(Boolean createdProblem) {
		this.createdProblem = createdProblem;
	}
	public Boolean getSlaExpired() {
		return slaExpired;
	}
	public void setSlaExpired(Boolean slaExpired) {
		this.slaExpired = slaExpired;
	}
	public Long getHandleTimeLong() {
		return handleTimeLong;
	}
	public void setHandleTimeLong(Long handleTimeLong) {
		this.handleTimeLong = handleTimeLong;
	}
	public Long getSLANo(){
		return slaRule.getRuleNo();
	}
	@Override
	public void updateProperty() {
		super.updateProperty();
	}
}
