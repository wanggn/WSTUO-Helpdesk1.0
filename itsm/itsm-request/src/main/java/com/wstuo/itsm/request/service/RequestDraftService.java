package com.wstuo.itsm.request.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts2.json.JSONException;
import org.apache.struts2.json.JSONUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.wstuo.itsm.request.dao.IRequestDraftDAO;
import com.wstuo.itsm.request.dto.RequestDTO;
import com.wstuo.itsm.request.dto.RequestDraftGridDTO;
import com.wstuo.itsm.request.dto.RequestDraftQueryDTO;
import com.wstuo.itsm.request.entity.RequestDraft;
import com.wstuo.common.dto.PageDTO;

/**
 * 流程事件监听处理Service类
 * @author WSTUO_QXY
 *
 */
public class RequestDraftService implements IRequestDraftService {

	@Autowired 
	private IRequestDraftDAO requestDraftDAO;
	/**
	 * 添加
	 */
	@Transactional
	public void addRequestDraft(RequestDraft requestDraft) {
		requestDraftDAO.save(requestDraft);
	}

	/**
	 * 分页查询请求草稿
	 */
	@Transactional
	public PageDTO findPageRequestDraft(RequestDraftQueryDTO queryDTO) {
		PageDTO p = requestDraftDAO.findPageRequestDraft(queryDTO);
		List<RequestDraft> entityList=(List<RequestDraft>)p.getData();
		List<RequestDraftGridDTO> dtoList=new ArrayList<RequestDraftGridDTO>(entityList.size());
		for(RequestDraft entity:entityList){
			RequestDraftGridDTO dto=new RequestDraftGridDTO();
			if(null != entity.getRequestDraftID()){
				dto.setRequestDraftID(entity.getRequestDraftID());
			}
			if(null != entity.getCreateTime()){
				dto.setCreateTime(entity.getCreateTime());
			}
			if(null != entity.getLastUpdateTime()){
				dto.setLastUpdateTime(entity.getLastUpdateTime());
			}
			if(null != entity.getRequestJson()){
				dto.setRequestJson(entity.getRequestJson());
			}
			if(null != entity.getRequestDraftName()){
				dto.setRequestDraftName(entity.getRequestDraftName());
			}
			dtoList.add(dto);
		}
		p.setData(dtoList);
		return p;
		
	}

	public RequestDTO findRequestDraftById(Long ids) {
		RequestDraft requestDraft= requestDraftDAO.findById(ids);
		RequestDTO requestDTO =new RequestDTO();
		requestDTO.setEdesc(requestDraft.getEdesc());
		if( null != requestDraft && null != requestDraft.getRequestDraftName()){
			String[] splits=requestDraft.getRequestJson().split("&");
			for (int i = 0; i < splits.length; i++) {
				//names[0]属性names[1]值
				String[] names=splits[i].split("=");
				if(names.length == 2){
					if( null != names[0] && null != names[1] && names[0].contains("companyNo") && "" != names[1]){
						requestDTO.setCompanyNo(Long.parseLong(names[1]));
					}
					if( null != names[0] && null != names[1] && names[0].contains("companyName") && "" != names[1]){
						requestDTO.setCompanyName(names[1]);
					}
					if( null != names[0] && null != names[1] && names[0].contains("etitle") && "" != names[1]){
						requestDTO.setEtitle(names[1]);
					}
					if( null != names[0] && null != names[1] && names[0].contains("ecategoryNo") && "" != names[1]){
						requestDTO.setEcategoryNo(Long.parseLong(names[1]));
					}
					if( null != names[0] && null != names[1] && names[0].contains("imodeNo") && "" != names[1]){
						requestDTO.setImodeNo(Long.parseLong(names[1]));
					}
					if( null != names[0] && null != names[1] && names[0].contains("priorityNo") && "" != names[1]){
						requestDTO.setPriorityNo(Long.parseLong(names[1]));
					}
					if( null != names[0] && null != names[1] && names[0].contains("levelNo") && "" != names[1]){
						requestDTO.setLevelNo(Long.parseLong(names[1]));
					}
					if( null != names[0] && null != names[1] && names[0].contains("seriousnessNo") && "" != names[1]){
						requestDTO.setSeriousnessNo(Long.parseLong(names[1]));
					}
					if( null != names[0] && null != names[1] && names[0].contains("createdByNo") && "" != names[1]){
						requestDTO.setCreatedByNo(Long.parseLong(names[1]));
					}
					if( null != names[0] && null != names[1] && names[0].contains("createdByName") && "" != names[1]){
						requestDTO.setCreatedByName(names[1]);
					}
					if( null != names[0] && null != names[1] && names[0].contains("creator") && "" != names[1]){
						requestDTO.setCreator(names[1]);
					}
					if( null != names[0] && null != names[1] && names[0].contains("attachmentStr") && "" != names[1]){
						requestDTO.setAttachmentStr(names[1]);
					}
					if( null != names[0] && null != names[1] && names[0].contains("requestCategoryName") && "" != names[1]){
						requestDTO.setRequestCategoryName(names[1]);
					}
					if( null != names[0] && null != names[1] && names[0].contains("requestCategoryNo") && "" != names[1]){
						requestDTO.setRequestCategoryNo(Long.parseLong(names[1]));
					}
					if( null != names[0] && null != names[1] && names[0].contains("effectRangeNo") && "" != names[1]){
						requestDTO.setEffectRangeNo(Long.parseLong(names[1]));
					}
				}
			}
		}
		return requestDTO;
	}
	/**
	 * 修改请求草稿
	 */
	@Transactional
	public boolean editRequestDraft(RequestDraft draft) {
		RequestDraft res=requestDraftDAO.findById(draft.getRequestDraftID());
		res.setRequestJson(draft.getRequestJson());
		res.setEdesc(draft.getEdesc());
		res.setRequestDraftName(draft.getRequestDraftName());
		requestDraftDAO.merge(res);
		return true;
	}


	/**
	 * 删除请求草稿
	 */
	@Transactional
	public boolean deleteRequestDraftById(Long ids) {
		RequestDraft draft=requestDraftDAO.findById(ids);
		requestDraftDAO.delete(draft);
		return true;
	}

	@Transactional
	public void saveRequestDraft(RequestDraft requestDraft,RequestDTO requestDTO) {
		try {
			if(requestDraft==null)
				requestDraft=new RequestDraft();
			requestDraft.setRequestJson(JSONUtil.serialize(requestDTO));
			requestDraft.setEdesc(requestDTO.getEdesc());
			requestDraft.setCreator(requestDTO.getLoginUserName());
			requestDraft.setLoginName(requestDTO.getLoginUserName());
			requestDraft.setRequestDraftName(requestDTO.getEtitle());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(requestDraft.getRequestDraftID()!=null && requestDraft.getRequestDraftID()>0){
			editRequestDraft(requestDraft);
		}else
			addRequestDraft(requestDraft);
		
	}

}
