package com.wstuo.itsm.request.service;
import com.wstuo.itsm.request.dto.RequestDTO;


/**
 * 请求动作Service接口类
 * @author WSTUO_QXY
 *
 */
public interface IRequestActionService {

	/**
	 * 请求指派
	 * @param requestDTO
	 */
	void requestAssgin(RequestDTO requestDTO);
	/**
	 * 请求提取
	 * @param requestDTO
	 */
	void requestGet(RequestDTO requestDTO);
	/**
	 * 请求升级
	 * @param requestDTO
	 */
	void requestUpgrade(RequestDTO requestDTO);
	/**
	 * 请求回访
	 * @param requestDTO
	 */
	void requestVisit(RequestDTO requestDTO);
	
	/**
	 * 请求再指派
	 * @param requestDTO
	 */
	void requestAgainAssgin(RequestDTO requestDTO);
	/**
	 * 请求直接关闭,不管处于流程那一步，请求直接关闭
	 * @param requestDTO
	 */
	void requestDirectClose(RequestDTO requestDTO);
	
	/**
	 * 请求挂起与解挂
	 * @param requestDTO
	 */
	void requestHangRemove(RequestDTO requestDTO);
	/**
	 * 修改SLA响应与完成时间
	 * @param requestDTO
	 */
	void editRequestSlaTime(RequestDTO requestDTO);
	
}

