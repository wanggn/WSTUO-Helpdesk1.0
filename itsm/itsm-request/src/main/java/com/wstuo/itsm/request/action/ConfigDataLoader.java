package com.wstuo.itsm.request.action;

import java.io.File;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;
import com.wstuo.common.bpm.dto.ProcessUseDTO;
import com.wstuo.common.bpm.service.IProcessUseService;
import com.wstuo.common.config.category.service.IEventCategoryService;
import com.wstuo.common.config.dictionary.service.IDataDictionaryGroupService;
import com.wstuo.common.config.dictionary.service.IDataDictionaryItemsService;
import com.wstuo.common.rules.service.IRulePackageService;
import com.wstuo.common.rules.service.IRuleService;
import com.wstuo.common.sla.service.ISLAContractService;
import com.wstuo.common.sla.service.ISLARuleService;
import com.wstuo.common.config.category.service.ICICategoryService;
import com.wstuo.common.jbpm.IJbpmFacade;
import com.wstuo.common.security.service.ICompanyService;
import com.wstuo.common.security.service.IFunctionService;
import com.wstuo.common.security.service.IOperationService;
import com.wstuo.common.security.service.IOrganizationService;
import com.wstuo.common.security.service.IRoleService;
import com.wstuo.common.security.service.IUserInfoService;
import com.wstuo.common.security.utils.AppConfigUtils;

/**
 * 加载配置数据.
 * @author QXY
 */
@SuppressWarnings("serial")
public class ConfigDataLoader extends ActionSupport{

	private static final Logger LOGGER = Logger.getLogger(ConfigDataLoader.class);
	private static final String CONFIG_DATA_FILE_PATH=AppConfigUtils.getInstance().getConfigDataPath();
	@Autowired
	private IFunctionService functionService;//功能
	@Autowired
	private IOperationService operationService;//操作
	@Autowired
	private IEventCategoryService eventCategoryService;//常用分类
	@Autowired
	private ICICategoryService cicategoryService;//配置项分类
	@Autowired
	private IDataDictionaryGroupService dataDictionaryGroupService;//数据字典分组
	@Autowired
	private IDataDictionaryItemsService dataDictionaryItemsService;//数据字典项
	@Autowired
	private ICompanyService companyService;//公司信息
	@Autowired
	private IOrganizationService organizationService;//机构信息（内部、服务机构）
	@Autowired
	private IRoleService roleService;//角色信息
	@Autowired
	private IUserInfoService userInfoService;//用户
	@Autowired
	private IRuleService ruleService;//请求规则
	@Autowired
	private ISLAContractService slaContractService;//SLA
	@Autowired
	private ISLARuleService slaRuleService;//SLA规则
	@Autowired
	private IJbpmFacade jbpmFacade;//流程
	@Autowired
	private IProcessUseService processUseService;//流程选用
	@Autowired
	private IRulePackageService rulePackageService;//流程选用
	private String effect;
	public String getEffect() {
		return effect;
	}
	public void setEffect(String effect) {
		this.effect = effect;
	}
	
	/**
	 * 导入数据
	 */
	public String importConfigData(){
		if(functionService.findAllFunction().size()==0){
			effect=importAllConfigData();
		}else{
			effect="configed";
		}
		return SUCCESS;
	}
	
	/**
	 * 导入资源数据.
	 */
	public String importAllConfigData(){
		try{
			functionService.importFunction(getFilePath("function.csv"));//功能
			operationService.importOperation(getFilePath("operation.csv"));//操作
			eventCategoryService.importEventCategory(getImportFile("category.csv"));//常用分类
			cicategoryService.importCiCategory(getImportFile("cicategory.csv"));//配置项分类
			dataDictionaryGroupService.importDataDictionaryGroup(getImportFile("dictionary_group.csv"));//数据字典分组
			dataDictionaryItemsService.importDataDictionaryItems(getImportFile("dictionary_items.csv"));//数据字典项
			companyService.importCompanyInfo(getFilePath("company.csv"));//导入公司信息
			organizationService.importOrganization(getFilePath("organizations.csv"));//导入机构信息
			roleService.importRoleItems(getImportFile("roles.csv"));//导入角色
			userInfoService.importUserItems(getImportFile("users.csv"));//导入用户
			ruleService.importRules(getFilePath("request_rules.drl"), null);//导入默认规则
			slaContractService.importSLA(getFilePath("sla.csv"));//导入SLA
			slaRuleService.importSLARule(getFilePath("sla_rule.csv"));//导入SLA规则
			slaRuleService.importSLARule_drl(getFilePath("sla_rule.drl"), 1L);//导入匹配规则
			jbpmFacade.deployJpdl(getImportFile("request.zip"));//请求流程
			jbpmFacade.deployJpdl(getImportFile("problem.zip"));//问题流程
			jbpmFacade.deployJpdl(getImportFile("change.zip"));//变更流程
			processUser();//使用请求流程
			rulePackageService.printAllRuleFiles();//生成所有规则文件
			return "success";
		}catch(Exception ex){
			LOGGER.error(ex);
			return "failure";
		}
		
	}
	
	  /**
	   * 公共方法，取得CSV文件路径.
	   * @param fileName 
	   * @return csvFilePath String
	   */
	  public String getFilePath(String fileName){
		  return CONFIG_DATA_FILE_PATH+"/"+fileName;
	  }
	  
	  /**
	   * 公共方法，取得CSV文件.
	   * @param fileName
	   * @return csvFile File
	   */
	  public File getImportFile(String fileName){
		  return new File(getFilePath(fileName));
	  }
	  
	  /**
	   * 默认流程使用
	   */
	  public void processUser(){
		  ProcessUseDTO dto=new ProcessUseDTO();
		  dto.setRequestProcessKey("Request");
		  dto.setChangeProcessKey("Change");
		  dto.setProblemProcessKey("Problem");
		  dto.setUseName("processUse");
		  processUseService.processUseSave(dto);
	  }
	  
}
