package com.wstuo.itsm.itsop.itsopuser.dto;

/**
 * 外包客户的用户数据传递类
 * @author QXY
 *
 */
public class ITSOPUserQueryDTO {
	
	private Integer start;
	private Integer limit;
	private String sidx;
	private String sord;
	
	private String orgName;
	
	private String loginName;//登录帐号(QXY)
	
	private String email;
	private String officePhone;

	private Long typeNo;
	private Long typeName;
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getOfficePhone() {
		return officePhone;
	}
	public void setOfficePhone(String officePhone) {
		this.officePhone = officePhone;
	}
	public Integer getStart() {
		return start;
	}
	public void setStart(Integer start) {
		this.start = start;
	}
	public Integer getLimit() {
		return limit;
	}
	public void setLimit(Integer limit) {
		this.limit = limit;
	}
	public String getSidx() {
		return sidx;
	}
	public void setSidx(String sidx) {
		this.sidx = sidx;
	}
	public String getSord() {
		return sord;
	}
	public void setSord(String sord) {
		this.sord = sord;
	}
	public String getOrgName() {
		return orgName;
	}
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public Long getTypeNo() {
		return typeNo;
	}
	public void setTypeNo(Long typeNo) {
		this.typeNo = typeNo;
	}
	public Long getTypeName() {
		return typeName;
	}
	public void setTypeName(Long typeName) {
		this.typeName = typeName;
	}
	public ITSOPUserQueryDTO(){
		super();
	}
	public ITSOPUserQueryDTO(String loginName){
		this.loginName = loginName;
	}
}
