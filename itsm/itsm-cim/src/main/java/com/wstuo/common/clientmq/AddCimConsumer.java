package com.wstuo.common.clientmq;

import org.springframework.beans.factory.annotation.Autowired;

import com.wstuo.itsm.cim.dto.CIDTO;
import com.wstuo.itsm.cim.service.ICIService;
/**
 * MQ配置项导出
 * @author will
 *
 */
public class AddCimConsumer{

    @Autowired
    private ICIService ciService;
	public void messageConsumer(Object obj) {
		CIDTO ciDto=(CIDTO) obj;
		ciService.addConfigureItem(ciDto, ciDto.getHardwareDTOs());
	}

}
