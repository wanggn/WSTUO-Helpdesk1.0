package com.wstuo.common.exportmq;

import org.springframework.beans.factory.annotation.Autowired;

import com.wstuo.common.activemq.consumer.IMessageConsumer;
import com.wstuo.common.tools.dto.ExportQueryDTO;
import com.wstuo.itsm.cim.service.ICIService;
/**
 * MQ配置项导出
 * @author will
 *
 */
public class CIExportConsumer implements IMessageConsumer{

    @Autowired
    private ICIService ciService;
	@Override
	public void messageConsumer(Object obj) {
		ExportQueryDTO dto=(ExportQueryDTO) obj;
		ciService.commonExportCIData(dto);
	}

}
