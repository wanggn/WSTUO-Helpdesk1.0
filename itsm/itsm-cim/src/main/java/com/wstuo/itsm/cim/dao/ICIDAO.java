package com.wstuo.itsm.cim.dao;

import java.util.List;

import com.wstuo.common.dao.IEntityDAO;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.security.dto.UserDTO;
import com.wstuo.common.tools.dto.StatResultDTO;
import com.wstuo.itsm.cim.dto.CIQueryDTO;
import com.wstuo.itsm.cim.entity.CI;

/**
 * 配置项DAO接口类
 * 
 * @author QXY
 */
public interface ICIDAO extends IEntityDAO<CI> {
	/***
	 * 配置项分页查询接口方法
	 * 
	 * @return PageDTO
	 */
	PageDTO findPager(final CIQueryDTO ciQueryDTO, int start, int limit, String sidx, String sord);

	/**
	 * 软件使用许可统计
	 * 
	 * @param softwareName
	 * @return String
	 */
	Integer softwareLicenseUseCount(String softwareName);

	/**
	 * 根据获取某一用户关联的配置项
	 * 
	 * @param start
	 * @param limit
	 * @return String
	 */
	PageDTO findPageConfigureItemByUser(UserDTO userDTO, int start, int limit, String sidx, String sord, CIQueryDTO ciQueryDTO);

	/**
	 * 根据硬件中的计算机名进行搜索查询
	 * 
	 * @param companyName
	 * @return String
	 */
	List<CI> findAllByCompanyName(String companyName);
	/**
	 * 查询生命周期到期的配置项
	 */
	List<CI> calcCiLifeCycle();
	
	/**
	 * 查询报修期到期的配置项
	 */
	List<CI> calcCiWarranty();
	
	/**
	 * 查询预警期到期的配置项
	 */
	List<CI> calcWarningDate();
	
	/**
	 * 分组统计配置项
	 * @param qdto 统计条件CIQueryDTO
	 * @return
	 */
	List<StatResultDTO> groupStatCiByCompanyNo(CIQueryDTO qdto);
	/**
	 * 配置项分页查询
	 * 
	 * @return PageDTO：
	 */
	Integer findAllCICount(CIQueryDTO queryDTO);
}
