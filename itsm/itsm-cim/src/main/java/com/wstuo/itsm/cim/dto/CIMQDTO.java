package com.wstuo.itsm.cim.dto;

public class CIMQDTO {
	
	private CIDTO ciDto;
	private HardwareDTO hardwareDTOs;
	public CIDTO getCiDto() {
		return ciDto;
	}
	public void setCiDto(CIDTO ciDto) {
		this.ciDto = ciDto;
	}
	public HardwareDTO getHardwareDTOs() {
		return hardwareDTOs;
	}
	public void setHardwareDTOs(HardwareDTO hardwareDTOs) {
		this.hardwareDTOs = hardwareDTOs;
	}
	
}
