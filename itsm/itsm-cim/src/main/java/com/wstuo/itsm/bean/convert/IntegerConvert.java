package com.wstuo.itsm.bean.convert;

import org.apache.commons.beanutils.Converter;

public class IntegerConvert implements Converter {

	@Override
	public Object convert(Class class1, Object object) {
		if( object != null && object instanceof String ){
			String temp = null;
			//超过9位数的就不能转换成Integer
			if( (temp = (String) object).matches("[\\d]{1,9}")){
				return Integer.parseInt(temp);
			}else{
				return 0;
			}
		}
		return object;
	}

}
