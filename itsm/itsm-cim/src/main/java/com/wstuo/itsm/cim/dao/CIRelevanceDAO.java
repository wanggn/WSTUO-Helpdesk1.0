package com.wstuo.itsm.cim.dao;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Restrictions;

import com.wstuo.common.dao.BaseDAOImplHibernate;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.util.DaoUtils;
import com.wstuo.itsm.cim.dto.CIRelevanceDTO;
import com.wstuo.itsm.cim.entity.CIRelevance;

/**
 * 配置项关联配置项DAO类
 * @author QXY
 */
public class CIRelevanceDAO
    extends BaseDAOImplHibernate<CIRelevance>
    implements ICIRelevanceDAO
{
	/***
     * 配置项关联配置分页查询
     * @return PageDTO
     */
    public PageDTO findPager( Long ciRelevanceId, int start, int limit,String sord,String sidx)
    {
        DetachedCriteria dc = DetachedCriteria.forClass( CIRelevance.class );
        Disjunction disjunction = Restrictions.disjunction(  );
        dc.add( disjunction );

        dc.createAlias( "ciRelevanceId", "cir" );
        dc.createAlias( "unCiRelevanceId", "unc" );

        disjunction.add( Restrictions.eq( "cir.ciId", ciRelevanceId ) );
        disjunction.add( Restrictions.eq( "unc.ciId", ciRelevanceId ) );

        //排序
        dc = DaoUtils.orderBy(sidx, sord, dc);
        
        return super.findPageByCriteria( dc, start, limit );
    }
    /**
     * 关联配置项
     * @param ciId
     * @return List<CIRelevance>
     */
    @SuppressWarnings("unchecked")
    public List<CIRelevance> findCiRelevanceList(Long ciId){
    	String hql=" from CIRelevance cire where cire.ciRelevanceId.ciId=? and cire.dataFlag not in(99)";
    	return super.getHibernateTemplate().find(hql,ciId);
    };
    /**
     * 被关联配置项
     * @param ciId
     * @return List<CIRelevance>
     */
    @SuppressWarnings("unchecked")
    public List<CIRelevance> findNuCiRelevanceList(Long ciId){
    	String hql=" from CIRelevance cire where cire.unCiRelevanceId.ciId=? and cire.dataFlag not in(99)";
    	return super.getHibernateTemplate().find(hql,ciId);
    };
    
    /**
     * 判断当前关联配置项是否已关联
     * @param ciReleDto
     * @return boolean
     */
    @SuppressWarnings("unchecked")
    public boolean isCIRelevance(CIRelevanceDTO ciReleDto){
    	boolean result=false;
    	if(ciReleDto!=null){
    		String hql=" from CIRelevance cire where ((cire.unCiRelevanceId.ciId=? and cire.ciRelevanceId.ciId=? ) or (cire.unCiRelevanceId.ciId=? and cire.ciRelevanceId.ciId=? )) and cire.dataFlag not in(99)";
    		List<CIRelevance> list=super.getHibernateTemplate().find(hql,ciReleDto.getCiRelevanceId(),ciReleDto.getUnCiRelevanceId(),ciReleDto.getUnCiRelevanceId(),ciReleDto.getCiRelevanceId());
    		
    		if(list.size()==0){
    			result=false;
    		}else
    		{
    			result=true;
    		}
    	}
    	return result;
    };

}
