package com.wstuo.itsm.cim.service;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.context.ApplicationContext;

import com.wstuo.common.util.AppliactionBaseListener;

/**
 * 配置项过期检测
 * @author QXY
 *
 */
public class CiExpiredJob implements Job{
	/**
	 * 配置项过期检测方法
	 */
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		ApplicationContext ctx =AppliactionBaseListener.ctx;
		ICiExpiredService ciExpiredService=(ICiExpiredService)ctx.getBean("ciExpiredService");
		ciExpiredService.configureItemDetectExpiredNotice();
	}
}
