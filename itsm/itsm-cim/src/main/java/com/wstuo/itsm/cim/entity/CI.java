package com.wstuo.itsm.cim.entity;

import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.wstuo.common.config.attachment.entity.Attachment;
import com.wstuo.common.config.category.entity.CICategory;
import com.wstuo.common.config.category.entity.EventCategory;
import com.wstuo.common.config.dictionary.entity.DataDictionaryItems;
import com.wstuo.common.entity.BaseEntity;

/***
 * 配置项实体类
 * @author QXY
 */
@SuppressWarnings({ "rawtypes", "serial" })
@Entity
public class CI extends BaseEntity{
    @Id
    @GeneratedValue( strategy = GenerationType.AUTO )
    private Long ciId;//ID
    @Column(nullable=false,unique=true)
    private String cino;//配置项编号
    @Column(nullable=false)
    private String ciname;//配置项名称
    @Column(nullable=true)
    private String model;//型号
    @Column(nullable=true)
    private String serialNumber;//序列号
    @Column(nullable=true)
    private String barcode;//条形码
    @Temporal(TemporalType.TIMESTAMP) 
    private Date buyDate;//购买日期
    @Temporal(TemporalType.TIMESTAMP) 
    private Date arrivalDate;//到货日期
    @Column(nullable=true)
    private String poNo;//订单号
    @Temporal(TemporalType.TIMESTAMP) 
    private Date warningDate;//警报日期
    @ManyToOne
    @JoinColumn( name = "statusDcode" )
    private DataDictionaryItems status;
    @ManyToOne
    @JoinColumn( name = "brandDcode" )
    private DataDictionaryItems brand;
    @ManyToOne
    @JoinColumn( name = "locDcode" )
    private EventCategory loc;
    @ManyToOne
    @JoinColumn( name = "categoryNo" )
    private CICategory category;
    @ManyToOne
    @JoinColumn( name = "providerDcode" )
    private DataDictionaryItems provider;
    @Column(nullable=true)
    private Integer lifeCycle;//生命周期
    @Column(nullable=true)
    private Integer warranty;//保修期
    @Column(nullable=true)
    private String userName;//使用人
    @Column(nullable=true)
    private String owner;//负责人
    @Column(nullable=true)
    private String computerName;//计算机名
    @ManyToOne
    private DataDictionaryItems systemPlatform;//所属系统、系统平台
    
    /**
     * 附件
     */
    @ManyToMany
    private List<Attachment> ciAttachements;
    
    /**
     * 安装的软件信息
     */
    @ManyToMany
    private List<CiSoftware> ciSoftwares;
    
    
  //20110709-QXY
    //部门
    @Column(nullable=true)
    private String department;
    //CDI
    @Column(nullable=true)
    private String CDI;
    //Work Number
    @Column(nullable=true)
    private String workNumber;
    //Project
    @Column(nullable=true)
    private String project;
    //来源单位
    @Column(nullable=true)
    private String sourceUnits;
    //与财务对应
    @Column(nullable=true)
    private Boolean financeCorrespond=false;
    //资产原值
    @Column(nullable=true)
    private Double assetsOriginalValue; 
    //报废时间
    @Temporal(TemporalType.TIMESTAMP) 
    private Date wasteTime;
    //借出时间
    @Temporal(TemporalType.TIMESTAMP) 
    private Date borrowedTime;
    //原使用者
    @Column(nullable=true)
    private String originalUser;
    //回收时间
    @Temporal(TemporalType.TIMESTAMP)
    private Date recoverTime;
    //预计回收时间
    @Temporal(TemporalType.TIMESTAMP)
    private Date expectedRecoverTime;
    //使用权限
    @Column(nullable=true)
    private String usePermissions;
    @ManyToMany(fetch=FetchType.EAGER)
    @Column(name="o_cisdty")
    private List<EventCategory> ServiceDirectory;//受影响的服务
    
    private Long originalUserId;
    private Long userNameId;
    private Long ownerId;
    
    private Long formId;

    private String isShowBorder;
    private Boolean isNewForm;
    
    //20130706 ciel
    private Integer depreciationIsZeroYears = 0; //多少年折旧率为0
    
    private Double ciDepreciation =0.0; //折旧率
    
    
	public Boolean getIsNewForm() {
		return isNewForm;
	}

	public void setIsNewForm(Boolean isNewForm) {
		this.isNewForm = isNewForm;
	}

	public Long getFormId() {
		return formId;
	}

	public void setFormId(Long formId) {
		this.formId = formId;
	}

	public String getIsShowBorder() {
		return isShowBorder;
	}

	public void setIsShowBorder(String isShowBorder) {
		this.isShowBorder = isShowBorder;
	}

	public Long getOriginalUserId() {
		return originalUserId;
	}

	public void setOriginalUserId(Long originalUserId) {
		this.originalUserId = originalUserId;
	}

	public Long getUserNameId() {
		return userNameId;
	}

	public void setUserNameId(Long userNameId) {
		this.userNameId = userNameId;
	}

	public Long getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(Long ownerId) {
		this.ownerId = ownerId;
	}
    
    
	public List<EventCategory> getServiceDirectory() {
		return ServiceDirectory;
	}

	public void setServiceDirectory(List<EventCategory> serviceDirectory) {
		ServiceDirectory = serviceDirectory;
	}

	public Long getCiId(  )
    {
        return ciId;
    }

    public void setCiId( Long ciId )
    {
        this.ciId = ciId;
    }

    public String getCino(  )
    {
        return cino;
    }

    public void setCino( String cino )
    {
        this.cino = cino;
    }
    
    public String getComputerName() {
		return computerName;
	}

	public void setComputerName(String computerName) {
		this.computerName = computerName;
	}

	public String getCiname(  )
    {
        return ciname;
    }

    public void setCiname( String ciname )
    {
        this.ciname = ciname;
    }

    public String getModel(  )
    {
        return model;
    }

    public void setModel( String model )
    {
        this.model = model;
    }

    public String getSerialNumber(  )
    {
        return serialNumber;
    }

    public void setSerialNumber( String serialNumber )
    {
        this.serialNumber = serialNumber;
    }

    public String getBarcode(  )
    {
        return barcode;
    }

    public void setBarcode( String barcode )
    {
        this.barcode = barcode;
    }

    public Date getBuyDate(  )
    {
        return buyDate;
    }

    public void setBuyDate( Date buyDate )
    {
        this.buyDate = buyDate;
    }

    public Date getArrivalDate(  )
    {
        return arrivalDate;
    }

    public void setArrivalDate( Date arrivalDate )
    {
        this.arrivalDate = arrivalDate;
    }

    public String getPoNo(  )
    {
        return poNo;
    }

    public void setPoNo( String poNo )
    {
        this.poNo = poNo;
    }

    public Date getWarningDate(  )
    {
        return warningDate;
    }

    public void setWarningDate( Date warningDate )
    {
        this.warningDate = warningDate;
    }

    public DataDictionaryItems getStatus(  )
    {
        return status;
    }

    public void setStatus( DataDictionaryItems status )
    {
        this.status = status;
    }

    public DataDictionaryItems getBrand(  )
    {
        return brand;
    }

    public void setBrand( DataDictionaryItems brand )
    {
        this.brand = brand;
    }

    public EventCategory getLoc(  )
    {
        return loc;
    }

    public void setLoc( EventCategory loc )
    {
        this.loc = loc;
    }

    public DataDictionaryItems getProvider(  )
    {
        return provider;
    }

    public void setProvider( DataDictionaryItems provider )
    {
        this.provider = provider;
    }

    public Integer getLifeCycle(  )
    {
        return lifeCycle;
    }

    public void setLifeCycle( Integer lifeCycle )
    {
        this.lifeCycle = lifeCycle;
    }

    public Integer getWarranty(  )
    {
        return warranty;
    }

    public void setWarranty( Integer warranty )
    {
        this.warranty = warranty;
    }

    public String getUserName(  )
    {
        return userName;
    }

    public void setUserName( String userName )
    {
        this.userName = userName;
    }

    public String getOwner(  )
    {
        return owner;
    }

    public void setOwner( String owner )
    {
        this.owner = owner;
    }


	public CICategory getCategory() {
		return category;
	}

	public void setCategory(CICategory category) {
		this.category = category;
	}
	public List<Attachment> getCiAttachements() {
		return ciAttachements;
	}

	public void setCiAttachements(List<Attachment> ciAttachements) {
		this.ciAttachements = ciAttachements;
	}

	public List<CiSoftware> getCiSoftwares() {
		return ciSoftwares;
	}

	public void setCiSoftwares(List<CiSoftware> ciSoftwares) {
		this.ciSoftwares = ciSoftwares;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getCDI() {
		return CDI;
	}

	public void setCDI(String cDI) {
		CDI = cDI;
	}

	public String getWorkNumber() {
		return workNumber;
	}

	public void setWorkNumber(String workNumber) {
		this.workNumber = workNumber;
	}

	public String getProject() {
		return project;
	}

	public void setProject(String project) {
		this.project = project;
	}

	public String getSourceUnits() {
		return sourceUnits;
	}

	public void setSourceUnits(String sourceUnits) {
		this.sourceUnits = sourceUnits;
	}
	public Boolean getFinanceCorrespond() {
		return financeCorrespond;
	}

	public void setFinanceCorrespond(Boolean financeCorrespond) {
		this.financeCorrespond = financeCorrespond;
	}

	public Double getAssetsOriginalValue() {
		return assetsOriginalValue;
	}

	public void setAssetsOriginalValue(Double assetsOriginalValue) {
		this.assetsOriginalValue = assetsOriginalValue;
	}

	public Date getWasteTime() {
		return wasteTime;
	}

	public void setWasteTime(Date wasteTime) {
		this.wasteTime = wasteTime;
	}

	public Date getBorrowedTime() {
		return borrowedTime;
	}

	public void setBorrowedTime(Date borrowedTime) {
		this.borrowedTime = borrowedTime;
	}

	public String getOriginalUser() {
		return originalUser;
	}

	public void setOriginalUser(String originalUser) {
		this.originalUser = originalUser;
	}

	public Date getRecoverTime() {
		return recoverTime;
	}

	public void setRecoverTime(Date recoverTime) {
		this.recoverTime = recoverTime;
	}

	public Date getExpectedRecoverTime() {
		return expectedRecoverTime;
	}

	public void setExpectedRecoverTime(Date expectedRecoverTime) {
		this.expectedRecoverTime = expectedRecoverTime;
	}

	public String getUsePermissions() {
		return usePermissions;
	}

	public void setUsePermissions(String usePermissions) {
		this.usePermissions = usePermissions;
	}


	public DataDictionaryItems getSystemPlatform() {
		return systemPlatform;
	}

	public void setSystemPlatform(DataDictionaryItems systemPlatform) {
		this.systemPlatform = systemPlatform;
	}

	public Integer getDepreciationIsZeroYears() {
		return depreciationIsZeroYears;
	}

	public void setDepreciationIsZeroYears(Integer depreciationIsZeroYears) {
		this.depreciationIsZeroYears = depreciationIsZeroYears;
	}

	public Double getCiDepreciation() {
		return ciDepreciation;
	}

	public void setCiDepreciation(Double ciDepreciation) {
		this.ciDepreciation = ciDepreciation;
	}

}
