package com.wstuo.itsm.cim.dto;

public class CIDictionaryitems {
	private Long dcode;
	private String dname;
	private String color;
	
	public Long getDcode() {
		return dcode;
	}
	public void setDcode(Long dcode) {
		this.dcode = dcode;
	}
	public String getDname() {
		return dname;
	}
	public void setDname(String dname) {
		this.dname = dname;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
}
