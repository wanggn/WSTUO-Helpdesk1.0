package com.wstuo.itsm.cim.dao;

import java.util.List;

import com.wstuo.common.dao.IEntityDAO;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.itsm.cim.dto.CIRelevanceDTO;
import com.wstuo.itsm.cim.entity.CIRelevance;

/**
 * 配置项关联配置项DAO接口类
 * @author QXY
 */
public interface ICIRelevanceDAO
    extends IEntityDAO<CIRelevance>
{
	/**
	 * 关联配置项分页查询
	 * @return PageDTO:分页数据集
	 */
    PageDTO findPager( Long ciRelevanceId, int start, int limit,String sord,String sidx);
    /**
     * 关联配置项
     * @param ciId
     * @return List<CIRelevanceDTO>
     */
    List<CIRelevance> findCiRelevanceList(Long ciId);
    /**
     * 被关联配置项
     * @param ciId
     * @return List<CIRelevanceDTO>
     */
    List<CIRelevance> findNuCiRelevanceList(Long ciId);
    /**
     * 判断当前关联配置项是否已关联
     * @param ciReleDto
     * @return boolean
     */
    boolean isCIRelevance(CIRelevanceDTO ciReleDto);

}
