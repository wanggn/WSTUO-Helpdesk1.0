package com.wstuo.bugfix;

import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

public class RequestWrapper extends HttpServletRequestWrapper {

    public RequestWrapper(HttpServletRequest request) {
        super(request);
    }
    
    public String getParameter(String name) {
        String value = super.getParameter(name);
        return XssClean.xssClean(value);
    }
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public Map<String,String[]> getParameterMap(){
        Map<String,String[]> request_map = super.getParameterMap();
        Iterator iterator = request_map.entrySet().iterator();
        while(iterator.hasNext()){
            Map.Entry me = (Map.Entry)iterator.next();
            String[] values = (String[])me.getValue();
            for(int i = 0 ; i < values.length ; i++){
                values[i] = XssClean.xssClean(values[i]);
            }
        }
        return request_map;
    }
    
}