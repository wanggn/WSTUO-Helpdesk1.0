package com.wstuo.itsm;

import javax.servlet.ServletContextEvent;

import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.wstuo.common.scheduledTask.service.IScheduledTaskService;

public class ITSMContextLoaderListener extends ContextLoaderListener{ //implements ServletContextListener {
	public void contextInitialized(ServletContextEvent event) {
		//super.contextInitialized(event);
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(event.getServletContext());
		//AppliactionBaseListener.ctx = ctx;
		
		new AppliactionDataListener().contextInitialized(event);
		
		//生成全部定期任务
		IScheduledTaskService scheduledTaskService=(IScheduledTaskService)ctx.getBean("scheduledTaskService");
		scheduledTaskService.generateScheduledTask();
		
		
	}

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		
	}
}
