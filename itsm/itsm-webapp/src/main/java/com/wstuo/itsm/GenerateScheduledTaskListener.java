package com.wstuo.itsm;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.springframework.context.ApplicationContext;

import com.wstuo.common.scheduledTask.service.IScheduledTaskService;
import com.wstuo.common.util.AppliactionBaseListener;

/**
 * 定期任务监听
 * @author Ciel
 *
 */
public class GenerateScheduledTaskListener implements ServletContextListener{
	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
	}

	@Override
	public void contextInitialized(ServletContextEvent event) {
		ApplicationContext ctx = AppliactionBaseListener.ctx;
		//生成全部定期任务
		IScheduledTaskService scheduledTaskService=(IScheduledTaskService)ctx.getBean("scheduledTaskService");
		scheduledTaskService.generateScheduledTask();
	}
}
