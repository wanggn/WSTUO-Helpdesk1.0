package com.wstuo.itsm;

import org.hibernate.dialect.MySQL5InnoDBDialect;

/** 
 * 
 * Extends MySQL5InnoDBDialect and sets the default charset to be UTF-8 
 * @author will
 */
public class MySQL5InnoDBDialectUTF8 extends MySQL5InnoDBDialect {
    public String getTableTypeString() {
        return " ENGINE=InnoDB DEFAULT CHARSET=utf8";
    }
}