<style>
    .table_border{width:100%}.table_border{ border-collapse:collapse; border:solid
    black; border-width:1px 0 0 1px;}.table_border caption {font-size:14px;font-weight:bolder;}.table_border
    th,.table_border td {border:solid black;border-width:0 1px 1px 0;padding-left:5px;}
    .table_border a{text-decoration:none}
</style>
<table width="550">
    <#escape x as x!>
        <tr>
            <td colspan="2">
                您好:
                </b>
                </font>
            </td>
        </tr>
        <#if (variables.outcomes?exists && variables.outcomes?size>
            0) >
            <tr>
                <td colspan="2">
                     編號為：${variables.ecode}【標題：${variables.etitle}】的事件指派給您，您可以登陸系統進行查看，也可以點擊下列按鈕對事件進行操作
                </td>
            </tr>
            <tr>
                <td colspan="2">
                     <#list variables.outcomes as oc><img alt="" src="${variables.visitPath}/images/icons/bullet_blue.gif">
                        <a href="${variables.visitPath}/pages/common/jbpm/mailReturn.jsp?eno=${variables.eno}&random=${variables.randomParam}&ocIndex=${oc_index}&pid=${variables.pid}&moduleType=${variables.moduleType}&taskId=${variables.taskId}&tenantId=${tenantId}">
                            ${oc}
                        </a>
                    </#list>
                    <br />
                    <br />
                </td>
            </tr>
            <#else>
                <tr>
                    <td colspan="2">
                        流程已結束!
                    </td>
                </tr>
        </#if>
        <#if (variables.historyRecordDTO?exists && variables.historyRecordDTO?size>
            0) >
            <tr>
                <td colspan="2">
                    歷史記錄
                    <table class="table_border">
                        <tr>
                            <td>
                                動作
                            </td>
                            <td>
                                詳細
                            </td>
                            <td>
                                操作者
                            </td>
                            <td>
                                操作時間
                            </td>
                        </tr>
                        <#list variables.historyRecordDTO as historyRecord>
                            <tr>
                                <td width="20%">
                                    ${historyRecord.logTitle}
                                </td>
                                <td style="word-wrap:break-word;word-break:break-all;" width="40%">
                                    ${historyRecord.logDetails}
                                </td>
                                <td width="20%">
                                    ${historyRecord.operator}
                                </td>
                                <td width="20%">
                                    ${historyRecord.createdTime?string("yyyy-MM-dd HH:mm:ss")}
                                </td>
                            </tr>
                        </#list>
                    </table>
                </td>
            </tr>
        </#if>
        <tr>
            <td colspan="2">
                <br>
                謝謝！
            </td>
        </tr>
        <tr>
            <td colspan="2">
                Wstuo Team
            </td>
        </tr>
    </#escape>
</table>
<br>
-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~
<br>
這是壹封系統的郵件，請勿直接回復！
<br>
-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~