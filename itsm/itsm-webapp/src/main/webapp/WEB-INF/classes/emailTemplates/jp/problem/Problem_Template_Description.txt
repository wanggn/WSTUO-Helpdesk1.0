﻿序号，规则编号，描述
1、problemCareteSuccessNotice：问题创建成功通知
2、problemSignKnownErrorNotice：问题标记为已知错误通知
3、problemCloseNotice：问题关闭通知
4、problemFlowActionNotice：问题流程动作通知
5、problemRemoveSignKnownErrorNotice：问题取消已知错误标记通知
6、problemAssignNotice：问题指派通知