<%@page import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@page import="org.springframework.context.ApplicationContext"%>
<%@page import="com.wstuo.common.tools.entity.PagesSet"%>
<%@page import="com.wstuo.common.tools.service.IPagesSetSerice"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%
response.setHeader("Cache-Control","no-store");
response.setHeader("Pragrma","no-cache");
response.setDateHeader("Expires",0);
String version="1.0";

request.setAttribute("version",version);

ApplicationContext ac = WebApplicationContextUtils.getWebApplicationContext(getServletContext());
IPagesSetSerice systemPage = (IPagesSetSerice)ac.getBean("pagesSetSerice");
PagesSet pagesSet=systemPage.showPagesSet();
request.setAttribute("pagesSet",pagesSet);

%>

<!DOCTYPE html>
<html>
<head lang="en">
  <meta charset="UTF-8">
  <title>WSTUO-企业一体化解决方案</title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="format-detection" content="telephone=no">
  <meta name="format-detlogin.htmlection" content="telephone=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
  <meta name="description" content="wstuo,企业一体化解决方案">
<meta name="author" content="WSTUO">
<link rel="icon" type="image/png" href="../images/logo2.png">
<link rel="stylesheet" href="assets/css/amazeui.min.css"/>
<link rel="stylesheet" href="assets/css/app.css">

  <style>
    .header {
      text-align: center;
    }
    .header h1 {
      font-size: 200%;
      color: #333;
      margin-top: 30px;
    }
    .header p {
      font-size: 14px;
    }
  </style>

</head>
<c:if test="${empty pagesSet.paName}">
	<title>企业一体化解决方案</title>
</c:if>
<c:if test="${!empty pagesSet.paName}">
<title>${pagesSet.paName}</title>
</c:if>
<c:if test="${!empty pagesSet.paName}">
<c:set var="paname" value="${pagesSet.paName}"></c:set>
</c:if>

<body>
<div class="header">
	<c:if test="${!empty pagesSet.imgname}">
	     <img style="height:60px;min-width:150px;max-width:320px;" src="logo.jsp?picName=${pagesSet.imgname}" />
	     </c:if>
	     <c:if test="${empty pagesSet.imgname}">
	     <img style="height:60px" src="../images/logo.png" />
     </c:if> 
  	<hr />
</div>
<div class="am-g">
  <div class="am-u-lg-6 am-u-md-8 am-u-sm-centered">
    
    <br>
     <form id="userLogin" class="am-form" name="userLogin" method="POST" action="${pageContext.request.contextPath}/j_spring_security_check" >
      
    
      <label for="text">用户名:</label>
      <input type="text" name="j_username" id="j_username" value="" required="true">
     
      <br/>
      <label for="password">密码:</label>
      <input type="password" name="j_password" id="j_password" value="" required="true">
    
      <br/>

       <div style="text-align: center;">
       	<div>
       	 <input type="hidden" name="j_loginModel" id="j_loginModel" value="wap">
       	<input type="submit" style="width: 100%;padding: 11px" value="登 录" class="am-btn am-btn-primary am-btn-sm">
        </div>
       </div><br/> 
       <div style="text-align: center;">
       	<div>
       	<a href="javascript:getPassword()" style="width: 40%;padding: 11px">忘记密码</a><a  href="reg.jsp" style="width: 40%;padding: 11px">注册试用</a>
        </div>
       </div>
        <br/> 
         <div style="text-align: center;">Helpdesk<%=version%> &copy; <a href="http://wstuo.com" target="_blank" >维思拓</a> 2016  &nbsp; WSTUO<sup>TM</sup></div>
    </form>
    <hr>
  </div>
</div>

<div class="am-modal am-modal-prompt" tabindex="-1" id="my-prompt">
  <div class="am-modal-dialog">
    <div class="am-modal-hd">找回密码</div>
    <div class="am-modal-bd">
      请输入手机号
      <input type="text" id="phone" class="am-modal-prompt-input">
    </div>
    <div class="am-modal-footer">
      <span class="am-modal-btn" data-am-modal-cancel>取消</span>
      <span class="am-modal-btn" data-am-modal-confirm>提交</span>
    </div>
  </div>
</div>
<div class="am-modal am-modal-prompt" tabindex="-1" id="my-validate">
  <div class="am-modal-dialog">
    <div class="am-modal-hd">找回密码</div>
    <div class="am-modal-bd">
      <span id="code_tip">请输入验证码</span>
      <input type="text" class="am-modal-prompt-input">
    </div>
    <div class="am-modal-footer">
      <span class="am-modal-btn" data-am-modal-cancel>取消</span>
      <span class="am-modal-btn" data-am-modal-confirm>提交</span>
    </div>
  </div>
</div>
<%@ include file="include_tip.jsp"%>                   	
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/amazeui.min.js"></script>
<script src="../js/i18n/i18n_zh_CN.js"></script>
<script src="js/common.js"></script>

<script type="text/javascript">
var num=60;
var myVar;
function getPassword() {
    $('#my-prompt').modal({
      relatedTarget: this,
      onConfirm: function(e) {
	       var phone=e.data || '';
	       if(!checkPhone(phone)){
	    	   msgAlert("手机格式错误！");
		   		return false;
		   	}
		   	$.ajax({
		           type:"post",
		           url: "sms!sendSMS.action",
		           data: {mobiles:phone,content:'validate'},
		           dataType: "json",
		           success: function(res){
		          		if(res.sendCount>0){
		          			$('#my-validate').modal({
		          			      relatedTarget: this,
		          			      onConfirm: function(e) {
		          				     var data=e.data || '';
		          				     clearInterval(myVar); 
		          				   	 $.post('validateCode.jsp',function(code){
		          				   		if(code.indexOf(data)>-1){
			          						$.ajax({
			          					        type:"post",
			          					        url: "user!resetPassword.action",
			          					        data: "editUserPasswordDTO.loginName="+phone+"&editUserPasswordDTO.resetPassword=resetPassword",
			          					        dataType: "json",
			          					        success: function(res){
			          					        	if(res || res=='true')
			          					        		msgAlert('密码重置成功！密码为：itsmbest');
			          					        	else{
			          					        		msgAlert('密码重置失败！');
				          					        	location.href="login.jsp";
			          					        	}
			          					        }
			          					    });
			          					}else
			          						msgAlert('验证码错误！');
		          				   	 });
		          			      },
			          		      onCancel: function(e) {
			          		    	clearInterval(myVar); 
			          		      }
		          			});
		          			num=60;
		          			$("#code_tip").html("请输入验证码,60秒后重新发送！");
 			   				myVar=setInterval(function(){
 			   					num-=1;
 			   					if(num==0){
 			   						clearInterval(myVar); 
 			   						$("#code_tip").html('<a href="javascript:getCode()">重新发送</a>');
 			   					}else
 			   						$("#code_tip").html("请输入验证码,"+num+"秒后重新发送！");
 			   				},1000); 
			   			}else{
			   				msgAlert('发送失败！请稍后重试');
			   			}
		            }
		       });
      },
      onCancel: function(e) {
      }
    });
}
function getCode(){
	var phone=$("#phone").val();
	$.ajax({
        type:"post",
        url: "sms!sendsms_reg.action",
        data: {mobiles:phone,content:'validate'},
        dataType: "json",
        success: function(res){
       		if(res.sendCount>0){
       			num=60;
       			$("#code_tip").html("请输入验证码,60秒后重新发送！");
				myVar=setInterval(function(){
					num-=1;
					if(num==0){
						clearInterval(myVar); 
						$("#code_tip").html('<a href="javascript:getCode()">重新发送</a>');
					}else
						$("#code_tip").html("请输入验证码,"+num+"秒后重新发送！");
				},1000); 
			}else{
				$("#code_tip").html('发送失败！<a href="javascript:getCode()">重新发送</a>');
			}
         }
    });
}
var login_error='${param.error}';
var login_overonlinenontermianl='${param.overonlinenontermianl}';

if(login_error=='true'){

	msgAlert(i18n.userNameOrPassword);
	
	setCookie('cookieUserName','',0);
	setCookie('cookieUserPassword','',0);
}
if(login_overonlinenontermianl=='true'){
	msgAlert(i18n.more_than_linen_count);
}

if(login_error=='account_used'){
	msgAlert(i18n.msg_account_used);
	setCookie('cookieUserName','',0);
	setCookie('cookieUserPassword','',0);
}

$(function(){
	var _loginName = '${param.loginName}';
	if(_loginName!=null && _loginName!=''){
		$('#j_username').val(_loginName);
	}
})
</script>
</body>
</html>
