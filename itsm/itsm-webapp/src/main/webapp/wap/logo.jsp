<%-- <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
 --%>
<%@page contentType="image/jpeg" %>
<%@page import="com.wstuo.common.util.StringUtils"%>
<%@page import="java.io.File"%>
<%@page import="javax.imageio.ImageIO"%>
<%@page import="java.awt.image.BufferedImage"%>
<%@page import="com.wstuo.common.security.utils.AppConfigUtils"%>

<%-- 根据图片名称读取图片 --%>
<%
    // response.getOutputStream()跟response.getWriter()相冲突
    // java.lang.IllegalStateException: getOutputStream() has already been called for this response
    out.clearBuffer(); 
    out = pageContext.pushBody();

    response.setHeader("Pragma","No-cache"); 
    response.setHeader("Cache-Control","no-cache"); 
    response.setDateHeader("Expires", 0); 
    
    // 获取图片的名称
    String picName = request.getParameter("picName");
    if (StringUtils.hasText(picName)) {
        try {
            String IMAGEPATH = AppConfigUtils.getInstance().getImagePath() + '/' + picName;
            IMAGEPATH = IMAGEPATH.replace("WEB-INF/classes/", "");
            File file = new File( IMAGEPATH );
            BufferedImage image = ImageIO.read( file );
            ImageIO.write(image, "png", response.getOutputStream());
        } catch (Exception e) {
            // 图片路径不存在, 不输出内容
        }
    }
%>