 /**  
 * @author WSTUO
 * @description 流程公共主函数.
 * @param select
 */
processCommon=function(){
	return{
		/**
		 * 空值格式化
		 */
		nullFormatInput:function(value){
			if(value!=null){
				return value;
			}else{
				return '';
			}
		},
		/**
		 * 状态格式化
		 * @param value 类型
		 */
		statusFormatInput:function(value){
			if(value !=null && value=='completed'){
				return i18n.lable_complete;
			}else if (value == 'obsolete'){
				return i18n.lable_obsolete;
			}else{
				return value;
			}
		},
		/**
		 * @description 查看流程图
		 * @param _piId 流程pid
		 * @param _title 流程名称
		 */
		showFlowChart:function(_piId,_title){
			var _url = 'jbpm!getActivityCoordinates.action?pid='+_piId;
			$.post(_url,function(res){
				$("#processTracelnstance").html(res);
				$("#flow_img").width("100%");
				$('#processTracelnstanceWin').modal();
			});
		},
	
		showFlowChartByNoStart:function(processKey,_title){
			var _url = '../pages/wstuo/jbpmMge/traceInstance.jsp?processDefinitionId='+processKey;
			$.post(_url,function(res){
				$("#processTracelnstance").html(res);
				$("#flow_img").width("100%");
				$('#processTracelnstanceWin').modal();
			});
		},
		/**
		 * 显示流程历史任务
		 * @param showId 显示历史记录的Table Id
		 * @param pid 流程实例ID
		 * @param flowIsEnd 流程是否已结束
		 */
		getPorcessHistoryTask:function(showId,pid,flowIsEnd){
			$('#'+showId+' tbody').html('');
			$.post('jbpm!findHistoryTask.action','pid='+pid,function(res){
				
				if(res!=null){
					var data = res.data;
					$('#'+showId+' tbody tr').remove();
					var html='';
					for(var i=0;i<data.length;i++){
						var _state = data[i].state;
						if(flowIsEnd && (_state==null || _state=='')){
							_state = i18n.lable_title_jbppm_processEnd;
						}
						html="<tr>"+
						"<td style='width:10%;text-align:center'>[id]</td>"+
						"<td style='width:10%;text-align:center;'>[assign]</td>"+
						"<td style='width:15%;text-align:center'>[createTime]</td>"+
						"<td style='width:10%;text-align:center'>[duration]</td>"+
						"<td style='width:10%;text-align:center'>[activityName]</td>"+
						"<td style='width:15%;text-align:center' id='request_HistoryTaskEndTime"+pid.substring(0,pid.indexOf('.'))+"'>[endTime]</td>"+
						"<td style='width:20%;text-align:center;' id='request_HistoryTaskOutcome"+pid.substring(0,pid.indexOf('.'))+"'>[outcome]</td>"+
						"<td style='width:30%;text-align:center;'>[state]</td>"+
						"</tr>";
						
						html = html.replace('[id]',data[i].id)
						.replace('[assign]',processCommon.nullFormatInput(data[i].assignee))
						.replace('[createTime]',timeFormatter(data[i].createTime))
						.replace('[duration]',parseInt(data[i].duration/1000/60))
						.replace('[activityName]',processCommon.nullFormatInput(data[i].activityName))
						.replace('[endTime]',timeFormatter(data[i].endTime))
						.replace('[outcome]',processCommon.nullFormatInput(data[i].outcome))
						.replace('[state]',processCommon.statusFormatInput(_state));
						$('#'+showId+' tbody').append(html);
						
					}
				}
			});
		},
		//根据当前下一步节点属性设置流程任务框属性
		setFlowHandleByNextNode:function(data,module){
			if(data!=null){
				if(data.activityType!=="" && data.activityType!="join"){
					$('#flow_handle_group_tr,#flow_handle_assignee_tr').show();
				}else{
					$('#flow_handle_group_tr,#flow_handle_assignee_tr').hide();
				}
				$('#flow_handle_activityType').val(data.activityType);
				$('#flow_handle_assignType').val($vl(data.assignType));
				$('#flow_handle_variablesAssigneeType').val(data.variablesAssigneeType);
				$('#flow_handle_leaderNum').val(data.leaderNum);
				$('#flow_handle_variablesAssigneeGroupNo').val(data.variablesAssigneeGroupNo);
				$('#flow_handle_variablesAssigneeGroupName').val(data.variablesAssigneeGroupName);
				
				//验证方法
				$('#flow_handle_validMethod').val($vl(data.validMethod));
				//允许动态指派覆盖已有指派
				$('#flow_handle_allowUseDynamicAssignee').val(data.allowUseDynamicAssignee);
				$('#flow_handle_allowUseVariablesAssignee').val(data.allowUseVariablesAssignee);
				//允许修改默认指派技术员
				/*if(data.allowUdateDefaultAssignee==true || data.allowUdateDefaultAssignee=='true'){
					$('#flow_handle_assigneeName').attr('disabled',false);
					$('#assigneeNameCleanButton').show();
				}else{
					$('#flow_handle_assigneeName').attr('disabled',true);
					$('#assigneeNameCleanButton').hide();
				}
				//允许修改默认指派技术组
				if(data.allowUdateDefaultAssigneeGroup==true || data.allowUdateDefaultAssigneeGroup=='true'){
					$('#flow_handle_assigneeGroupName').attr('disabled',false);
					//$('#assigneeNameCleanButton').css('display','inline');
					$('#groupNameCleanButton').show();
				}else{
					$('#flow_handle_assigneeGroupName').attr('disabled',true);
					//$('#assigneeNameCleanButton').css('display','none');
					$('#groupNameCleanButton').hide();
				}*/
				if(data.activityType=='foreach'){
					$('#flow_handle_approversMemberSet_tr').show();
					$('#flow_handle_approversMemberSet_show').attr('required','true');
				}else{
					$('#flow_handle_approversMemberSet_show').removeAttr('required');
					$('#flow_handle_approversMemberSet_tr').hide();
				}
				//如果下一节点为空，也就是流程结束了，显示邮件填写输入框 (data.activityName==null || data.activityName=='') &&   
				//lqs  20150827  修改了显示条件  如果是请求和变更，就显示
				if(module == "request"||module == "change"){
					$('#flow_handle_closeNoticeEmailAddress_tr').show();
				}else{
					$('#flow_handle_closeNoticeEmailAddress_tr').hide();
				}
			}
		},
		//根据当前节点属性设置流程任务框属性
		setFlowHandleByCurrentNode:function(data){
			if(data!=null){
				//处理备注是否要验证
				if(data.remarkRequired==true || data.remarkRequired=='true'){
					$('#flow_handle_remark').attr('required','true');
				}else{
					$('#flow_handle_remark').removeAttr('required');
				}
				$('#flow_handle_approverTask').val(data.approverTask);
				if(data.approverTask==true || data.approverTask=='true')
					$('#flow_handle_approverTask_tr').show();
				else
					$('#flow_handle_approverTask_tr').hide();
			}
			$('#flow_handle_mailHandlingProcess').val(data.mailHandlingProcess);
		},
		/**
		 * 打开流程处理窗口
		 * @param module:模块(request,change,problem,release)
		 * @param pid:流程ID
		 * @param activityName:当前流程活动名称
		 * @param outcome:流程出口
		 * @param taskId:任务ID
		 * @param taskName:任务名称
		 * @param flowActivityId:流程活动ID
		 */
		openFlowHandleWin:function(module,eno,pid,outcome,taskId,taskName,flowActivityId){
			//清空处理备注
			$('#flow_handle_remark,#closeNoticeEmailAddress').val('');
			//任务ID
			$('#flow_handle_taskId').val(taskId);
			//流程出口
			$('#flow_handle_nextStep').val(outcome);
			//流程活动ID
			$('#flow_handle_activity_id').val(flowActivityId);
			//流程PID
			$('#flow_handle_pid').val(pid);
			$('#flow_handle_eno').val(eno);
			$('#flow_handle_module').val(module);
			$('#task_assignee_activityName').val(taskName);
			
			var frm ={"processHandleDTO.pid":pid,"processHandleDTO.taskName":taskName,
					"processHandleDTO.taskId":taskId,"processHandleDTO.outcome":outcome,
					"processHandleDTO.eno":eno,"processHandleDTO.module":module};
			$.post(module+'!findProcessAssignDTO.action',frm,function(res){
				$('#flow_handle_assigneeGroupNo').val($vl(res.groupId));
				$('#flow_handle_assigneeGroupName').val($vl(res.groupName));
				$('#flow_handle_assigneeNo').val($vl(res.assigneeId));
				$('#flow_handle_assigneeName').val($vl(res.assigneeName));
				processCommon.setFlowHandleByCurrentNode(res.flowActivityDTO);
				processCommon.setFlowHandleByNextNode(res.nextFlowActivityDTO,module);
				$("#flow_handle_win #title").text(i18n['label_request_action']+'-'+outcome);
				$("#flow_handle_win").modal();
				$('#processHandleButton').show();
				$('#openProcessButton').hide();
				$('#processHandleButton').unbind().click(function(){processCommon.processOpt(module)});
			});
			
		},
		/**
		 * 流程处理表单验证
		 */
		flowHandleFromValid:function(){
			if($('#flow_handle_win form').form('validate')){
				//判断动态指派组和技术员
				if($('#flow_handle_assignType').val()=='dynamicAssignee' && $('#flow_handle_assigneeGroupName').val()=='' && $('#flow_handle_assigneeName').val()==''){
					msgAlert(i18n['msg_request_assignCanNotNull']);
					return false;
				}else{
					return true;
				}
			}
		},
		
		/**
		 * 流程处理
		 * @param module 对应模块
		 */
		processOpt:function(module){

			//if(checkEmail($('#closeNoticeEmailAddress').val(),true)){
				var validMethod = $('#flow_handle_validMethod').val();
				
				if(typeof eval(validMethod)== "function"){//判断是否是函数
					if(eval(validMethod+'()')){
						processCommon.processHandleMethod(module);
					}
				}else{
					processCommon.processHandleMethod(module);
				}
			/*}else{
				msgAlert(i18n.validate_email);
				return ;  
			}*/
		},
		processHandleMethod:function(module){
			var frm = $('#flow_handle_from').serialize();
			//显示进程
			startProcess();
			$.post(module+'!processHandle.action',frm,function(){
				//隐藏进程
				endProcess();
				//刷新右菜单中的数值
				//showLeftMenu('../pages/itsm/'+module+'/leftMenu.jsp?random='+Math.random()*10+1,'leftMenu');	
				$('#flow_handle_win').modal('close');
				
				msgAlert(i18n['msg_request_submitSucceccful']);
				location.reload();
			});
		},
		/**
		 * 打开任务处理窗口
		 * @param module:模块(request,change,problem,release)
		 * @param handleType 处理类型
		 * @param eno 编号eno
		 * @param pid:流程ID
		 * @param taskId:任务ID
		 * @param id:流程活动ID
		 */
		openTaskWindow:function(module,handleType,eno,pid,taskId,id){
			$.post('flowProperty!findFlowActivityById.action','flowActivityDTO.id='+id,function(data){
				$('#task_assignee_allowUseDynamicAssignee').val(data.allowUseDynamicAssignee);
				$('#task_assignee_allowUseVariablesAssignee').val(data.allowUseVariablesAssignee);
				$('#task_assignee_assignType').val(data.assignType);
				$('#task_assignee_taskId').val(taskId);
				$('#task_assignee_pid').val(pid);
				$('#task_assignee_eno').val(eno);
				$('#task_assignee_module').val(module);
				$('#task_assignee_handleType').val(handleType);
				if('TaskTake'==handleType){
					$('#task_assignee_tr').hide();
					$("#task_assignee_window #title").text(i18n.label_taskTake);
				}else if('TaskReAssigne'==handleType){
					$('#task_assignee_tr').show();
					$("#task_assignee_window #title").text(i18n.assingeeTask);
				}else{
					$('#task_assignee_tr').show();
					$("#task_assignee_window #title").text(i18n.title_bpm_task_assign);
				}
				$("#task_assignee_window").modal();
				$('#task_assignee_submit_button').unbind().click(function(){processCommon.taskOpt(module)});
			});
			
		},
		/**
		 * 提交任务提取操作
		 * @param module 对应模块
		 */
		taskOpt:function(module){
			var frm = $('#task_assignee_window form').serialize();
			//显示进程
			startProcess();
			$.post(module+'!assignTask.action',frm,function(){
				//隐藏进程
				endProcess();
				//刷新右菜单中的数值
				//showLeftMenu('../pages/itsm/'+module+'/leftMenu.jsp?random='+Math.random()*10+1,'leftMenu');	
				$('#task_assignee_window').modal('close');
				msgAlert(i18n['msg_request_submitSucceccful']);
				location.reload();
			});	
			
		},
		openProcessMethod:function(module){
			var frm = $('#flow_handle_from').serialize();
			//显示进程
			startProcess();
			$.post(module+'!openProcess.action',frm,function(){
				//隐藏进程
				endProcess();
				$('#flow_handle_win').modal('close');
				msgAlert(i18n.openProcess);
				location.reload();
			});
		},
		/**
		 * 开启流程动作
		 */
		openProcess_submit:function(module,eno){
			//if(checkEmail($('#closeNoticeEmailAddress').val(),true)){
				var validMethod = $('#flow_handle_validMethod').val();
				if(typeof eval(validMethod)== "function"){//判断是否是函数
					if(eval(validMethod+'()')){
						processCommon.openProcessMethod(module);
					}
				}else{
					processCommon.openProcessMethod(module);
				}
			/*}else{
				msgAlert(i18n.validate_email);
				return ;
			}*/
			
		},
		openProcess:function(module,enoInput){

			var eno = $("#"+enoInput).val();
			//清空处理备注
			$('#flow_handle_remark,#closeNoticeEmailAddress').val('');
			//任务ID
			$('#flow_handle_taskId').val("");
			//流程出口
			$('#flow_handle_nextStep').val("");
			//流程活动ID
			$('#flow_handle_activity_id').val("");
			//流程PID
			$('#flow_handle_pid').val("");
			$('#task_assignee_activityName').val("");
			$('#flow_handle_eno').val(eno);
			$('#flow_handle_module').val(module);
			var frm = {"processHandleDTO.eno":eno,"processHandleDTO.isOpenProcess":true,
					"processHandleDTO.module":module};
			$.post(module+'!findProcessAssignDTO.action',frm,function(res){
				$('#flow_handle_assigneeGroupNo').val($vl(res.groupId));
				$('#flow_handle_assigneeGroupName').val($vl(res.groupName));
				$('#flow_handle_assigneeNo').val($vl(res.assigneeId));
				$('#flow_handle_assigneeName').val($vl(res.assigneeName));
				processCommon.setFlowHandleByCurrentNode(res.flowActivityDTO);
				processCommon.setFlowHandleByNextNode(res.flowActivityDTO,module);				
				$("#flow_handle_win #title").text(i18n.flow_openProcess_title);
				$("#flow_handle_win").modal();
				$('#processHandleButton').hide();
				$('#openProcessButton').show();
				$('#openProcessButton').unbind().click(function(){processCommon.openProcess_submit(module,eno)});
			});
		},
		
		/**
		 * 任务指派
		 */
		taskAssignee_assigneeName:function(){
			var module = $('#task_assignee_module').val();
			if(module =='request'){
				wstuo.user.userUtil.againAssignSelectUser_group('#task_assignee_assigneeName','#task_assignee_assigneeNo','#requestDetails_assigneeLoginName','')
			}
		},
		/**
		 * 流程指派
		 */
		processHandle_assigneeName:function(){
			
			var module = $('#flow_handle_module').val();
			if(module =='request'){
				if($('#flow_handle_assigneeGroupName').val()!=""){
					wstuo.user.userUtil.againAssignSelectUser_group('#flow_handle_assigneeName','#flow_handle_assigneeNo','','#flow_handle_assigneeGroupNo');
				}else{
					if($('#requestDetails_assigneeNo').val()===""&&$("#requestDetails_assigneeGroupNo").val()===""){
						wstuo.user.userUtil.selectUserHolidayTip('#flow_handle_assigneeName','#flow_handle_assigneeNo','','fullName',topCompanyNo);
					}else {
						if($('#requestDetails_assigneeNo').val()!=""){
							wstuo.user.userUtil.againAssignSelectUser_group('#flow_handle_assigneeName','#flow_handle_assigneeNo','#requestDetails_assigneeLoginName','#requestDetails_assigneeGroupNo');
						}else{
							wstuo.user.userUtil.againAssignSelectUser_group('#flow_handle_assigneeName','#flow_handle_assigneeNo','','#requestDetails_assigneeGroupNo');
						}
					}
				}
			}
		}
	}
}();