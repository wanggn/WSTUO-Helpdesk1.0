<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html>
<head lang="en">
  <meta charset="UTF-8">
  <title>注册试用-WSTUO</title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="format-detection" content="telephone=no">
  <meta name="format-detlogin.htmlection" content="telephone=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
  <meta name="description" content="wstuo,企业一体化解决方案">
<meta name="author" content="WSTUO">
<link rel="icon" type="image/png" href="../images/logo2.png">
<link rel="stylesheet" href="assets/css/amazeui.min.css"/>
<link rel="stylesheet" href="assets/css/app.css">

<%
String version="1.0";
request.setAttribute("version",version);
%>
  <style>
    .header {
      text-align: center;
    }
    .header h1 {
      font-size: 200%;
      color: #333;
      margin-top: 30px;
    }
    .header p {
      font-size: 14px;
    }
  </style>

</head>

<body>
<header data-am-widget="header" class="am-header am-header-default">
    <h1 class="am-header-title">注册试用 </h1>  
</header>
<nav data-am-widget="menu" class="am-menu am-menu-offcanvas1" data-am-menu-offcanvas>
		<a href="javascript:history.go(-1);" class="am-menu-left" >
	    <i style="text-align: right;padding: 0px;" class="am-menu-toggle-icon am-icon-chevron-left"></i>
	  </a>
</nav>
<div class="am-g">
  <div class="am-u-lg-6 am-u-md-8 am-u-sm-centered">
    <br>
     <form id="userReg" class="am-form" name="userLogin" method="POST" action="user!saveuserByPhone.action" onsubmit="return checkCode()">
      
    
      <label for="text">手机号码: &nbsp; &nbsp; <span style="color:red" id="username_tip"></span></label>
      <input type="text" name="userDto.phone" id="j_username" value="" required="true" onblur="vaildateUserByLoginName(this.value)">
     <br/>
      <label for="password">验证码:&nbsp; &nbsp; <span id="code_tip"><a href="javascript:getCode()">获取验证码</a></span></label>
      <input type="text" name="validate" id="validate" value="" required="true">
    
      <br/>
      <label for="password">密码:</label>
      <input type="password" name="userDto.password" id="j_password" value="" required="true">
      <br/>
       <div style="text-align: center;">
       	<div>
       	 <input type="hidden" name="userDto.orgNo" id="orgNo" value="1">
       	<input type="submit" style="width: 100%;padding: 11px" value="注 册" class="am-btn am-btn-primary am-btn-sm">
        </div>
       </div><br/> 
       <div style="text-align: center;">
       	<div>
       	已有账号？<a href="login.jsp" style="width: 40%;padding: 11px">立即登录</a>
        </div>
       </div>
        <br/> 
         <div style="text-align: center;">Helpdesk<%=version%> &copy; <a href="http://wstuo.com" target="_blank" >维思拓</a> 2016  &nbsp; WSTUO<sup>TM</sup></div>
    </form>
    <hr>
  </div>
</div>

<%@ include file="include_tip.jsp"%>                   	
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/amazeui.min.js"></script>
<script src="js/common.js"></script>
<script type="text/javascript">
var validate=true;
function checkCode(){
	if(validate){
		$.post('validateCode.jsp',function(code){
			if(code.indexOf($("#validate").val())){
				$.ajax({
			        type:"post",
			        url: "user!regByPhone.action",
			        data: $('#userReg').serialize(),
			        dataType: "json",
			        success: function(res){
			        	msgAlert('注册成功！');
			        	location.href="login.jsp";
			        }
			    });
			}else
				msgAlert('验证码错误！');
		 });
	}
	return false;
}
var num=60;
var myVar;
function getCode(){
	var phone=$("#j_username").val();
	if(!checkPhone(phone)){
		$("#username_tip").text("手机格式错误！");
		return;
	}
	if(!validate)return;
	$.ajax({
        type:"post",
        url: "sms!sendsms_reg.action",
        data: {mobiles:phone,content:'validate'},
        dataType: "json",
        success: function(res){
       		if(res.sendCount>0){
       			num=60;
				$("#code_tip").html("发送成功！60秒后重新发送！");
				myVar=setInterval(function(){
					num-=1;
					if(num==0){
						clearInterval(myVar); 
						$("#code_tip").html('<a href="javascript:getCode()">重新发送</a>');
					}else
						$("#code_tip").html("发送成功！"+num+"秒后重新发送！");
				},1000); 
			}else{
				$("#code_tip").html('发送失败！<a href="javascript:getCode()">重新发送</a>');
			}
         }
    });
}
function vaildateUserByLoginName(value){
	if(!checkPhone(value)){
		$("#username_tip").text("手机格式错误！");
		return false;
	}
	$.ajax({
         type: "post",
         url: "user!findUserByLoginName.action",
         data: {userName:value},
         dataType: "json",
         success: function(res){
        	if(res || res=="true"){
 				$("#username_tip").text("该手机已注册！");
 				validate=false;
 			}else{
 				$("#username_tip").text("");
 				validate=true;
 			}
          }
     });
}
</script>
</body>
</html>
