<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>  
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
String url="login.jsp";

 if(session.getAttribute("loginUserName")==null){
	response.sendRedirect(url);
	return;
} 
 %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html class="no-js">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<title>请求详情-WSTUO</title>

<link rel="icon" type="image/png" href="assets/img/logo2.png">
<link rel="stylesheet" href="assets/css/amazeui.min.css">
<link rel="stylesheet" href="../js/jquery/jquery-ui-1.11.4.custom/jquery-ui.min.css" />
<link href="../css/upload/fileinput.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="editor/css/wangEditor-mobile.min.css">
<link rel="stylesheet" href="assets/css/app.css">	
<style type="text/css">
.container {
	width:100%; 
	height:220px; 
	border:1px solid #ccc;
	background-color: #fff;
	text-align: left;
	box-shadow: 0 0 10px #ccc;
	text-shadow: none;
}
a{color: #333;}
.am-list .am-list-item-dated a{padding-right:0px }
</style>
<%-- 详细页面权限加载 --%>
<%@ include file="../pages/request/requestDetailsPermission.jsp" %>

<script>
var slaLevelName="${requestDetailDTO.slaLevelName}";
var request_cost_tool_show_hide=false;
var request_task_tool_show_hide=false;
var requestDetail_offmodeNo="${requestDetailDTO.offmodeNo}";
var operationTaskRes = false;
var isAllowAccessRes = ${isAllowAccessRes};
//标识是否需要初始化解决方案编辑器
var request_detail_solutions_res = false;
var normal_action = false;
var companyNo='${sessionScope.companyNo}';//当前用户所在公司No
</script>
</head>
<body>
<!-- Header -->
<header data-am-widget="header" class="am-header am-header-default">
    <h1 class="am-header-title">
     ${requestDetailDTO.etitle }
    </h1>
  </header>
<!-- Menu -->
<nav data-am-widget="menu" class="am-menu am-menu-offcanvas1" data-am-menu-offcanvas>
   <a href="javascript:history.go(-1);" class="am-menu-left">
    <i style="text-align: right;padding: 16px;" class="am-menu-toggle-icon am-icon-chevron-left"></i>
  </a>
  <a href="javascript: location.reload();" class="am-menu-left" style="left: 30px;">
    <i style="text-align: right;padding: 16px;" class="am-menu-toggle-icon am-icon-refresh"></i>
  </a>
  <a href="javascript: void(0)" class="am-menu-toggle">
    <i style="text-align: right;padding: 16px;" class="am-menu-toggle-icon am-icon-bars"></i>
  </a>
  <div class="am-offcanvas">
    <div class="am-offcanvas-bar">

      <ul class="am-menu-nav sm-block-grid-1">      
        <li>
          <a href="user!findUserDetail_wap.action?userName=${sessionScope.loginUserName}">个人信息</a>        
        </li>
        <li >
          <a href="pwdUpdate.jsp">密码修改</a>       
        </li>  
        <li>
          <a href="index.jsp">返回首页</a>       
        </li>   
        <li>
          <a href="logout.jsp">注&nbsp;&nbsp;销</a>       
        </li> 
      </ul>
    </div>
  </div>
</nav>
<!-- List -->
<div  class="am-list-news am-list-news-default">
	 <form class="am-form" id="requestForm">
	 	<input type="hidden" id="requestDetailStatusNo" value="${requestDetailDTO.statusNo }" />
		<input type="hidden" id="requestDetails_statusCode" value="${requestDetailDTO.statusCode }" />
		<input type="hidden" id="requestDetails_requestNo" value="${requestDetailDTO.eno}"/>
		<input type="hidden" id="rquestDetailCreatedByEmail" value="${requestDetailDTO.createdByEmail}"/>
		<input type="hidden" id="rquestDetailCreatedByLoginName" value="${requestDetailDTO.createdByLoginName}"/>
		<input type="hidden" id="requestDetails_requestTitle" value="${requestDetailDTO.etitle}"/>
		<input type="hidden" id="requestDetails_companyNo" value="${requestDetailDTO.companyNo}"/>
		<input type="hidden" id="requestDetails_assigneeGroupNo" value="${requestDetailDTO.assigneeGroupNo}"/>
		<input type="hidden" id="requestDetails_assigneeGroupName" value="${requestDetailDTO.assigneeGroupName}"/>
		<input type="hidden" id="requestDetails_assigneeNo" value="${requestDetailDTO.assigneeNo}"/>
		<input type="hidden" id="requestDetails_assigneeName" value="${requestDetailDTO.assigneeName}"/>
		<input type="hidden" id="requestDetails_assigneeLoginName" value="${requestDetailDTO.assigneeLoginName}"/>
		<input type="hidden" id="requestDetails_maxResponsesTime" value="${requestDetailDTO.maxResponsesTime}"/>
		<input type="hidden" id="requestDetails_maxCompletesTime" value="${requestDetailDTO.maxCompletesTime}"/>
		<input type="hidden" id="requestDetails_createdByName" value="${requestDetailDTO.createdByName}"/>
		<input type="hidden" id="requestDetails_pid" value="${requestDetailDTO.pid}"/>
		<input type="hidden" id="requestDetail_visitRecord" value="${requestDetailDTO.visitRecord}"> 
		<input type="hidden" id="requestDetails_fullName" value="${requestDetailDTO.fullName}"/>
		<input type="hidden" id="requestDetails_categoryEavId" value="${requestDetailDTO.categoryEavId}"/>
		<input type="hidden" id="requestDetails_closeTime" value="${requestDetailDTO.closeTime}"/>
		<input type="hidden" id="requestDetails_requestCategoryNo" value="${requestDetailDTO.requestCategoryNo}"/>
		<input type="hidden" id="requestDetails_processKey" value="${requestDetailDTO.processKey}"/>
		<input type="hidden" id="requestDetails_formId" value="${requestDetailDTO.formId}"/>
		<input type="hidden" id="requestDetails_attendance" value="${requestDetailDTO.attendance}"/>
     </form>
     <div data-am-widget="list_news" class="am-list-news am-list-news-default am-no-layout">
    <!--列表标题-->
      <div class="am-list-news-hd am-cf">
         <!--带更多链接-->
            <h2>${requestDetailDTO.etitle }</h2>
              <span class="am-list-news-more am-fr">${requestDetailDTO.requestCode}</span>
            </div>
  
		    <div class="am-list-news-bd">
		    <ul class="am-list">
		  		<li class="am-g am-list-item-dated">
		           <a class="am-list-item-hd "> <b>所属公司：</b> ${requestDetailDTO.companyName }</a>
		        </li>
		        <li class="am-g am-list-item-dated">
		           <a class="am-list-item-hd "> <b>请求分类：</b> ${requestDetailDTO.requestCategoryName }</a>
		        </li>
		        <li class="am-g am-list-item-dated">
		           <a class="am-list-item-hd "> <b>描述：</b> ${requestDetailDTO.edesc }</a>
		        </li>
		        <li class="am-g am-list-item-dated">
		           <a class="am-list-item-hd "> <b>工作组：</b> ${requestDetailDTO.assigneeGroupName }</a>
		        </li>
		        <li class="am-g am-list-item-dated">
		           <a class="am-list-item-hd "> <b>技术员：</b> ${requestDetailDTO.assigneeName }</a>
		        </li>
		        <li class="am-g am-list-item-dated">
		           <a class="am-list-item-hd "> <b>状态：</b> ${requestDetailDTO.statusName }</a>
		        </li>
		        <li class="am-g am-list-item-dated" id="detail_createdByName">
		           <a class="am-list-item-hd "> <b>请求用户：</b> ${requestDetailDTO.createdByName }</a>
		        </li>
		        <li class="am-g am-list-item-dated">
		           <a class="am-list-item-hd "><b>创建时间 ：</b><fmt:formatDate type="date" value="${requestDetailDTO.createdOn}"  pattern="yyyy-MM-dd HH:mm" /></a>
		        </li>
		        <li class="am-g am-list-item-dated">
		           <a class="am-list-item-hd "> <b>SLA响应时间：</b> <fmt:formatDate type="date" value="${requestDetailDTO.maxResponsesTime}"  pattern="yyyy-MM-dd HH:mm:ss" /></a>
		        </li>
		        <li class="am-g am-list-item-dated">
		           <a class="am-list-item-hd "> <b>SLA完成时间：</b> <fmt:formatDate type="date" value="${requestDetailDTO.maxCompletesTime}"  pattern="yyyy-MM-dd HH:mm:ss" /></a>
		        </li>
		    </ul>
		    <ul class="am-list"  id="detail_formField">
		    </ul>
		    </div>
      </div>
      <div class="am-panel-group" id="accordion">
  <div class="am-panel am-panel-default">
    <div class="am-panel-hd">
      <h4 class="am-panel-title" data-am-collapse="{parent: '#accordion', target: '#do-not-say-1'}">
        历史记录
      </h4>
    </div>
    <div id="do-not-say-1" class="am-panel-collapse am-collapse am-in">
      <div class="am-panel-bd" id="requestHistory" >
        <div class="row">
  				<div class="box col-md-12">
			<table id="requestHistoryRecord" class="table table-bordered" style="width:100%;" cellspacing="1">
				<thead>
					<tr >
					<th style="width:10%;text-align: center;">步骤</th>
					<th style="width:20%;text-align: center;">动作</th>
					<th style="width:40%;text-align: center;">详情</th>
					<th style="width:10%;text-align: center;">操作者</th>
					<th style="width:20%;text-align: center;">时间</th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div></div>
      </div>
    </div>
  </div>
  <div class="am-panel am-panel-default">
    <div class="am-panel-hd">
      <h4 class="am-panel-title" data-am-collapse="{parent: '#accordion', target: '#do-not-say-2'}">
       解决方案
      </h4>
    </div>
    <div id="do-not-say-2" class="am-panel-collapse am-collapse">
      <div class="am-panel-bd">
        <div class="row">
   				<div class="box col-md-12">
					<div id="requestSolutionsDiv" align="left" >
					<form>
					<input type="hidden" name="requestDTO.attachmentStr" id="request_solutions_effect_attachments_str" />
					<input type="hidden" name="requestDTO.knowledgeNo" id="knowledgeNo" value="0"/>
					<div class="container"><textarea style="width:100%; height:150px;" name="requestDTO.solutions" id="request_detail_solutions" >${requestDetailDTO.solutions}</textarea></div>
					<input type="hidden" name="requestDTO.eno" value="${param.eno}" />
					</form>
					</div>
				</div>
				 <c:if test="${requestDetailDTO.statusCode ne 'request_close' and (!(empty requestDetailDTO.assigneeName) || flowTask.assignee eq loginUserName)}">
				<c:if test="${resAuth['ADD_REQUEST_SOLUTIONS_RES'] }"> 
					<div style="padding: 10px; text-align: center; margin-bottom: 18px; margin-top: 8px">
						<a class="btn btn-primary btn-sm" id="saveSolutionsBtn"  style="margin-right: 15px">保存</a> 
						<a class="btn btn-primary btn-sm" id="saveToKnowledgeBtn" >保存并提交知识库</a>
					</div>
				</c:if>
				</c:if> 
			</div>
      </div>
    </div>
  </div>
  <div class="am-panel am-panel-default">
    <div class="am-panel-hd">
      <h4 class="am-panel-title" data-am-collapse="{parent: '#accordion', target: '#do-not-say-3'}">附件</h4>
    </div>
    <div id="do-not-say-3" class="am-panel-collapse am-collapse">
      <div class="am-panel-bd">
       <div class="row">
 		<div class="box col-md-12">
			<input type="hidden" name="problemDTO.effectAttachmentStr" id="request_effect_attachmentStr"/>
			<table width="100%" cellspacing="1" class="lineTable">
				<tr>
					<td>
					<div id="request_effect_success_attachment" style="line-height:25px;color:#555;display: none"></div>
					<div class="hisdiv" id="show_request_effectAttachment">
						<table style="width:100%" class="histable" cellspacing="1">
							<thead>
								<tr>
									<th>ID</th>
									<th>名称</th>
									<th>操作</th>
								</tr>
							</thead>
							<tbody></tbody>
	             			</table>
	               	</div>
					</td>
				</tr>
				</table>
		</div>
			</div>
      </div>
    </div>
  </div>
</div>
</div>
<!-- 公共值 -->
<div id="requestActionComm">
	<form>
		<input type="hidden" name="requestDTO.eno" value="${requestDetailDTO.eno }" />
		<input type="hidden" name="historyRecordDto.operator" id="attachmentCreator" value="${loginUserName}" />
		<input type="hidden" name="requestDTO.currentUser" value="${loginUserName}" />
		<input type="hidden" name="requestDTO.pid" value="${requestDetailDTO.pid}" />
		<input type="hidden" id="requestCode" name="requestDTO.requestCode" value="${requestDetailDTO.requestCode}" />
		<input type="hidden" name="requestDTO.requestCode" id="requestDetail_requestCode" value="${requestDetailDTO.requestCode}" />
		<input type="hidden" id="processNextStep" name="requestDTO.processNextStep" />
	</form>
</div>

<%@ include file="includes_flowWin.jsp" %> 
<%@ include file="requestActionHtml.jsp" %> 
<%@ include file="include_tip.jsp"%>
<!-- List -->
<div class="am-list-news am-list-news-default">

<!-- Navbar -->
<div  data-am-widget="navbar"  class="am-navbar am-cf am-navbar-default" id="">
 <ul class="am-navbar-nav am-cf am-avg-sm-4">
	<c:set var="loginUserName" value="${loginUserName}"/>
	<c:set var="request_assignMyGroup" value="false"/>
	<c:set var="request_startFlow" value="false"/>
	<%-- 常规动作start --%>
  	<c:if test="${(empty requestDetailDTO.closeTime and resAuth['/pages/request!updateRequest.action']) or !empty requestDetailDTO.pid or resAuth['FNULLTEXTSEARCH_RES']}">
  		<script>normal_action=true;</script>
  	</c:if>
  	<c:if test="${empty requestDetailDTO.closeTime}">
  	<sec:authorize url="/pages/email!toEmail.action">
		<script>normal_action=true;</script>
	</sec:authorize>
	</c:if>
	<sec:authorize url="request_manage_order">
		<script>normal_action=true;</script>
	</sec:authorize>
  	<!-- 如果请求未关闭 ,则可以进行编辑-->
	<c:if test="${empty requestDetailDTO.closeTime}">
		<script>
			request_cost_tool_show_hide = true;
			request_task_tool_show_hide = true;
		</script>
		<c:if test="${resAuth['/pages/request!updateRequest.action'] }">
			<li>
			      <a href="addRequest.jsp?eno=${requestDetailDTO.eno }" id="edit_btn">
			        <span ><i class="am-icon-edit"></i></span>
			        <span class="am-navbar-label">编辑</span>
			      </a>
		    </li>
		</c:if>
    </c:if>
    <sec:authorize url="request_detail_flowTrack"></sec:authorize><!--不需求控制，能看到请求肯定能跟踪流程 -->
	<li><c:choose>
		<c:when test="${requestDetailDTO.flowStart eq true}">
		<a href="javascript: void(0);" onclick="processCommon.showFlowChart('${requestDetailDTO.pid}')"  >
		</c:when>
		<c:otherwise>
		<a href="javascript: void(0);" onclick="processCommon.showFlowChartByNoStart('${requestDetailDTO.processKey}')"  >
		</c:otherwise>
	</c:choose><span ><i class="am-icon-edit"></i></span>
		<span class="am-navbar-label">流程跟踪</span></a></li>
	<li class="am-navbar-labels am-navbar-more"><a href="javascript: void(0);">
		<span class="am-icon-angle-up"></span><span class="am-navbar-label">常规动作</span></a></li>
	<li class="am-navbar-labels am-navbar-more" id="flow_action"><a href="javascript: void(0);">
		<span class="am-icon-angle-up"></span><span class="am-navbar-label">流程动作</span></a></li>
	</ul>
	<ul class="am-navbar-actions">
	<c:if test="${requestDetailDTO.hang eq false and empty requestDetailDTO.responsesTime}">
       	<c:if test="${resAuth['RequestNotComprehensiveNotSubmitted_Res'] }">
         	<c:if test="${requestDetailDTO.statusCode ne 'request_notComprehensiveNotSubmitted' }">
         		<li><a onclick="requestAction.openRequestActionWin('request_notComprehensiveNotSubmitted_win')">
         		<span ><i class="am-icon-edit"></i></span>
				<span class="am-navbar-label">描述不全退回</span></a>
				</li>
       		</c:if>
       	 </c:if>
         
         <%-- 描述不全修改重新提交 --%>
         <c:if test="${requestDetailDTO.statusCode eq 'request_notComprehensiveNotSubmitted'}">
         	<c:if test="${requestDetailDTO.createdByName eq fullName}">
         		<li><a onclick="requestAction.openRequestActionWin('request_resubmit_win')">
	         		<span ><i class="am-icon-edit"></i></span>
					<span class="am-navbar-label">重新提交</span></a>
				</li>
         	</c:if>
         </c:if>
      </c:if>   
       
 		<!-- 请求备注 -->
    <c:if test="${resAuth['RequestDealRemark_Res'] }">
    	<li><a onclick="requestAction.openRequestActionWin('requestDealRemark_win')">
	         		<span ><i class="am-icon-edit"></i></span>
					<span class="am-navbar-label">处理备注</span></a>
				</li>
    </c:if>
     	
<c:if test="${requestDetailDTO.hang eq false and empty requestDetailDTO.closeTime}">
  		<!-- 自助解决 -->
  	    <c:if test="${resAuth['SELFHELPRESOLVE_RES'] }">
      		<c:if test="${empty requestDetailDTO.closeTime and requestDetailDTO.statusCode ne 'request_notComprehensiveNotSubmitted' and requestDetailDTO.createdByName eq fullName}">
      			<c:if test="${requestDetailDTO.approvalState ne '待审批' or requestDetailDTO.approvalState ne 'WaitApproval' or empty requestDetailDTO.approvalState}">
     	   			<li><a onclick="requestAction.openRequestActionWin('self_help_resolve_win')">
		         		<span ><i class="am-icon-edit"></i></span>
						<span class="am-navbar-label">自助解决</span></a>
					</li>
     	   			</c:if>
     	   		</c:if>
     	    </c:if>
     	    <!-- 挂起 -->
	<c:if test="${resAuth['REQUESTHANG_RES'] }">
		<c:if test="${requestDetailDTO.statusCode ne 'request_complete'}">
			<c:if test="${requestDetailDTO.statusCode ne 'request_notComprehensiveNotSubmitted'}">
				<c:if test="${requestDetailDTO.approvalState ne '待审批' or requestDetailDTO.approvalState ne 'WaitApproval' or empty requestDetailDTO.approvalState}">
					<li><a onclick="requestAction.openRequestActionWin('requestHang_win')">
		         		<span ><i class="am-icon-edit"></i></span>
						<span class="am-navbar-label">挂起</span></a>
					</li>
				</c:if>
			</c:if>
		</c:if>
	</c:if>
	
	<!-- 请求指派 -->
 	<c:if test="${resAuth['RequestAssign_Res'] and requestDetailDTO.statusCode ne 'request_notComprehensiveNotSubmitted' }">
  		<li><a onclick="requestAction.openRequestActionWin('requestAssign_win')">
	       		<span ><i class="am-icon-edit"></i></span>
			<span class="am-navbar-label">请求指派</span></a>
		</li>
  	</c:if>
  	
 	<!-- 再指派-->
   	<c:if test="${requestDetailDTO.statusCode ne 'request_notComprehensiveNotSubmitted' and resAuth['RequestAgainAssign_Res'] }">
    	<c:if test="${not empty requestDetailDTO.assigneeGroupName or not empty requestDetailDTO.assigneeName or not empty requestDetailDTO.assigneeGroupName }">
    		<c:if test="${empty requestDetailDTO.requestResolvedTime}">
    			<c:if test="${requestDetailDTO.assigneeLoginName eq loginUserName or requestDetailDTO.agentActionShow}">
    				<li><a onclick="requestAction.openRequestActionWin('requestAgainAssign_win')">
				       		<span ><i class="am-icon-edit"></i></span>
						<span class="am-navbar-label">再指派</span></a>
					</li>
    			</c:if>
    		</c:if>	
    	</c:if>
   	</c:if>
            	
    <!-- 提取 -->
	<c:if test="${resAuth['RequestGet_Res'] and requestDetailDTO.statusCode ne 'request_notComprehensiveNotSubmitted' }">
		<c:if test="${empty requestDetailDTO.assigneeName and requestDetailDTO.statusCode ne 'request_complete'}">
	  		<c:if test="${requestDetailDTO.approvalState ne '待审批' or requestDetailDTO.approvalState ne 'WaitApproval' or empty requestDetailDTO.approvalState}">
	  			<c:if test="${requestDetailDTO.statusCode ne 'request_approval'}">
					<li><a onclick="requestAction.openRequestActionWin('requestGet_win')">
				       		<span ><i class="am-icon-edit"></i></span>
						<span class="am-navbar-label">请求提取</span></a>
					</li>
				</c:if>
			</c:if>
		</c:if>
	</c:if>
    	    </c:if>
    	    <c:if test="${requestDetailDTO.hang eq true }">
	<!-- 解除挂起 -->
	<c:if test="${resAuth['REQUESTHANGREMOVE_RES'] }">
		<li><a onclick="requestAction.openRequestActionWin('requestHangRemove_win')">
	       		<span ><i class="am-icon-edit"></i></span>
			<span class="am-navbar-label">解除挂起</span></a>
		</li>
	</c:if>
	
	
</c:if>
 		
         <!-- 回访(默认在请求关闭后回访) -->
<%-- <c:if test="${resAuth['RequestVisit_Res'] and not empty requestDetailDTO.closeTime }">
     			<s:if test="requestDetailDTO.visitRecord==null"><!-- 邮件回访 -->
     				<li><a onclick="requestAction.requestVisit_win('requestVisit')">
				       		<span ><i class="am-icon-edit"></i></span>
						<span class="am-navbar-label">邮件回访</span></a>
					</li>
         		</s:if>
         		<s:else><!-- 重新回访 -->
         		<sec:authorize url="request_action_anewRevisit">
         		<li  id="requestDetails_revisit"><a onclick="requestAction.requestVisit_win('requestVisit_re')">
			       		<span ><i class="am-icon-edit"></i></span>
					<span class="am-navbar-label">重新回访</span></a>
				</li>
         		</sec:authorize>
         		</s:else>
    		</c:if> --%>
 			<c:if test="${requestDetailDTO.assigneeNo eq sessionScope.userId}">
				<c:set var="request_startFlow" value="true"/>
			</c:if>
			<c:forEach items="${sessionScope.technologyGroupNo}" var="aobj">  
			               <c:if test="${requestDetailDTO.assigneeGroupNo eq aobj}">
				<c:set var="request_startFlow" value="true"/>
			   </c:if> 
            </c:forEach>  
            <c:if test="${requestDetailDTO.assigneeGroupNo eq sessionScope.orgNo}">
				<c:set var="request_startFlow" value="true"/>
			</c:if>
  </ul>
  <ul class="am-navbar-actions" id="flow_action_ul">
  <%--开启流程 --%>
    		<c:if test="${resAuth['REQUEST_OPEN_PROCESS']  and requestDetailDTO.statusCode ne 'request_notComprehensiveNotSubmitted' and  requestDetailDTO.statusCode ne 'request_close'}">
    			<c:if test="${requestDetailDTO.flowStart eq false}">
		     		<c:if test="${request_startFlow}">
		     			<li><a onclick="processCommon.openProcess('request','requestDetails_requestNo')">
					       		<span ><i class="am-icon-edit"></i></span>
							<span class="am-navbar-label">开启流程</span></a>
						</li>
			  		</c:if>
		  		</c:if>
	 		</c:if>
		<%-- 流程处理动作start --%>
		<c:if test="${requestDetailDTO.hang eq false and empty requestDetailDTO.closeTime}">
		<s:iterator value="processDetailDTO" id="pd">
			<s:iterator value="#pd.flowTaskDTO" id="flowTask">
				<s:iterator value="#flowTask.participationDTO" id="participations">
					<c:if test="${participations.groupId eq sessionScope.groupId}">
						<c:set var="request_assignMyGroup" value="true"/>
					</c:if>
					<c:if test="${participations.userId eq sessionScope.userId}">
						<c:set var="request_assignMyGroup" value="true"/>
					</c:if>
					<c:forEach items="${sessionScope.technologyGroup}" var="aobj">  
	                    <c:if test="${participations.groupId eq aobj}">
							<c:set var="request_assignMyGroup" value="true"/>
						</c:if> 
	               </c:forEach>
			
				</s:iterator>
				<%-- 任务提取(指派给我组并且任务指派为空的)--%>
				<c:if test="${empty flowTask.assignee and request_assignMyGroup}">
					<li><a onclick="processCommon.openTaskWindow('request','TaskTake','${requestDetailDTO.eno}','${requestDetailDTO.pid}','${flowTask.taskId}','${flowTask.flowActivityDTO.id}')">
					       		<span ><i class="am-icon-edit"></i></span>
							<span class="am-navbar-label">任务提取</span></a>
						</li>
				</c:if>
		
			<%-- 如果任务已指派,则只能由指派的技术员可以查看 --%>
			<c:if test="${not empty flowTask.assignee}">
				<!-- 任务重新指派 start -->
				<sec:authorize url="REQUEST_AGAINASSIGNTASK">
					<li><a onclick="processCommon.openTaskWindow('request','TaskReAssigne','${requestDetailDTO.eno}','${requestDetailDTO.pid}','${flowTask.taskId}','${flowTask.flowActivityDTO.id}')">
					       		<span ><i class="am-icon-edit"></i></span>
							<span class="am-navbar-label">任务重新指派</span></a>
						</li>
				</sec:authorize>
				<!-- 任务重新指派 end -->
				<c:if test="${fn:toUpperCase(loginUserName) eq fn:toUpperCase(flowTask.assignee) or requestDetailDTO.agentActionShow}">
				
				
				<%-- 任务流程start --%>
				<s:iterator value="#flowTask.outcomes" id="oc">
					<li><a onclick="processCommon.openFlowHandleWin('request','${requestDetailDTO.eno}','${requestDetailDTO.pid}','${oc}','${flowTask.taskId}','${flowTask.taskName}','${flowTask.flowActivityDTO.id}')">
					       		<span ><i class="am-icon-edit"></i></span>
							<span class="am-navbar-label">${oc}</span></a>
						</li>
				</s:iterator>
				<%-- 任务流程end --%>
				
			</c:if>
		</c:if>
		
		
	</s:iterator>
	
</s:iterator>
</c:if>
<%-- 流程处理动作end --%>

 	<%-- 请求流程重新开启--%>
    	   <c:if test="${requestDetailDTO.statusCode eq 'request_close' }">
    	   <c:if test="${resAuth['REQUESTPROCESSREOPEN_RES'] }">
    	   	<li><a onclick="requestAction.openRequestActionWin('request_reopen_win')">
					       		<span ><i class="am-icon-edit"></i></span>
							<span class="am-navbar-label">流程重新开启</span></a>
						</li>
		  </c:if>
		  </c:if>
	</ul>
</div>
</div>

<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/amazeui.min.js"></script>
<script src="../js/i18n/i18n_zh_CN.js"></script>
<script src="../js/jquery/jquery-ui-1.11.4.custom/jquery-ui.min.js"></script>
<script type="text/javascript" src="../js/basics/package.js"></script>
<script type="text/javascript" src="../js/basics/import.js"></script>
<script src="../js/jquery/jquery.validate.min.js"></script>
<script src="../js/jquery/messages_zh.js"></script>
<script src="../js/basics/autocomplete.js"></script>
<script type="text/javascript" src="editor/js/lib/zepto.js"></script>
<script type="text/javascript" src="editor/js/lib/zepto.touch.js"></script>
<script type="text/javascript" src="editor/js/wangEditor-mobile.min.js"></script>
<script type="text/javascript">

// 生成编辑器
var editor = new ___E('request_detail_solutions');
// 初始化
editor.init();
</script>
<script src="js/common.js"></script>
<script src="../js/wstuo/tools/historyRecord.js"></script>
<script src="js/requestDetail.js"></script>
<script src="js/requestAction.js"></script>
<script src="js/processCommon.js"></script>
</body>
</html>