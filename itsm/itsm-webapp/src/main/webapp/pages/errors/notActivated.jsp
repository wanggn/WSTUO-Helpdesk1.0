<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../language.jsp" %>
<%
response.setHeader("Cache-Control","no-store");
response.setHeader("Pragrma","no-cache");
response.setDateHeader("Expires",0);
String version=com.wstuo.common.config.register.service.RegisterService.version;
Integer licenseNumber = com.wstuo.common.config.register.service.RegisterService.licenseNumber;

request.setAttribute("version",version);
request.setAttribute("licenseNumber",licenseNumber);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title><fmt:message key="i18n.mainTitle"/></title>
<style>
.lineTableBgDiv{
	background:#eee;
	margin:3px;
}
</style>
</head>

<body>
<div class="lineTableBgDiv" >
	<table style="width:100%" class="lineTable" cellspacing="1">
		<tr>
			<td style="line-height:28px;font-size:12px;color:#FF0000">
				<fmt:message key="tip.notActivation.info" /><br/>
				<fmt:message key="tip.noLicenseKey" /><br/>
                Version：<%=version %><br />
                <fmt:message key="label.license.thLicenseNum"/>：<%=licenseNumber %><br />
				<fmt:message key="label.request.phone" />：4008318185 <br/>
				<fmt:message key="label.user.qqOrMsn" />：<a href="http://wpa.qq.com/msgrd?v=1&Uin=1477274212&Exe=QQ&Site=56135&menu=no" target="_bank">1477274212</a><br/>
				<fmt:message key="label.user.email" />：<a href="mailto:support@WSTUO.com" target="_bank">support@WSTUO.com</a><br/>
				<%-- <fmt:message key="Trial.Website" />：<a href="http://www.WSTUO.com" target="_bank">http://www.WSTUO.com</a><br/>
				<fmt:message key="label.company.website" />：<a href="http://www.bangzhutai.com" target="_bank">http://www.bangzhutai.com</a> --%>
			</td>
		</tr>
	</table>
</div>
<div class="lineTableBgDiv">
		<table style="width:100%" class="lineTable" cellspacing="1">
	<tr>
		
		<td style="line-height:28px;font-size:12px;color:#FF0000">
			<a href="${pageContext.request.contextPath }/pages/itsm/app/activation/activation.jsp"><fmt:message key="tip.notActivation"/></a>
	</tr>

</table>
</div>
</body>

</html>