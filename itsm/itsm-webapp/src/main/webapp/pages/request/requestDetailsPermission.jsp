<%@page import="java.util.Map"%>
<%@page import="com.wstuo.common.security.service.IUserInfoService"%>
<%@page import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@page import="org.springframework.web.context.WebApplicationContext"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	if(session.getAttribute("loginUserName")==null){
		out.print("<script>window.location='login.jsp';</script>");
	}else{
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(session.getServletContext());
		IUserInfoService userInfoService = (IUserInfoService)ctx.getBean("userInfoService");
		if(session.getAttribute("loginUserName")!=null){
		 	Map<String,Object> resAuth=userInfoService.findResAuthByUserName(session.getAttribute("loginUserName").toString(),new String[]{
				  "/pages/request!updateRequest.action",
				  "/pages/email!toEmail.action",
				  "/pages/problem!saveProblem.action",
				  "/pages/change!saveChange.action",
				  "FNULLTEXTSEARCH_RES",
				  "RequestReFitSLA",
				  "RequestApproval_Res",
				  "RequestDeal_Res",
				  "RequestNotComprehensiveNotSubmitted_Res",
				  "RequestGet_Res",
				  "RequestAssign_Res",
				  "RequestReOpen_Res",
				  "RequestAssignTwo_Res",
				  "RequestAssignThree_Res",
				  "RequestAssignFour_Res",
				  "RequestAgainAssign_Res",
				  "RequestBackGroup_Res",
				  "RequestBack_Res",
				  "RequestDealComplete_Res",
				  "RequestClose_Res",
				  "REQUESTCLOSEAUDIT_RES",
				  "RequestUpgradeApply_Res",
				  "RequestUpgrade_Res",
				  "RequestVisit_Res",
				  "RequestDealRemark_Res",
				  "SELFHELPRESOLVE_RES",
				  "REQUESTHANG_RES",
				  "REQUESTHANGREMOVE_RES",
				  "EDITREQUESTSLATIME_RES",
				  "REQUESTPROCESSREOPEN_RES",
				  "VIEW_REQUEST_EXTENDEDINFO_RES",
				  "VIEW_REQUEST_ATTACHMENT_RES",
				  "VIEW_REQUEST_PROCESSHISTORYTASK_RES",
				  "VIEW_REQUEST_HISTORYRECORD_RES",
				  "VIEW_REQUEST_TASK_RES",
				  "VIEW_REQUEST_COST_RES",
				  "VIEW_REQUEST_SOLUTIONS_RES",
				  "ADD_REQUEST_SOLUTIONS_RES",
				  "KNOWLEDGEINFO_ADDKNOWLEDGE",
				  "VIEW_HISTORYEMAIL_RES",
				  "VIEW_VISITRECORD_RES",
				  "VIEW_RELATED_OTHER_REQUEST_RES",
				  "VIEW_RELATED_PROBLEM_RES",
				  "VIEW_RELATED_CHANGE_RES",
				  "VIEW_RELATED_CI_RES",
				  "VIEW_RELATED_CHANGE_RES",
				  "ADD_REQUEST_TASK_RES",
				  "EDIT_REQUEST_TASK_RES",
				  "DELETE_REQUEST_TASK_RES",
				  "ADD_REQUEST_COST_RES",
				  "EDIT_REQUEST_COST_RES",
				  "DELETE_REQUEST_COST_RES",
				  "request_menu_myUntreadRequest",
                  "SELECT_REQUEST_SOLUTIONS_RES",
                  "REQUEST_OPEN_PROCESS"
				 });
		 	request.setAttribute("resAuth",resAuth);
		}
		
	}
%>