<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date" %>
<%@ page import="org.apache.struts2.ServletActionContext" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page import="com.opensymphony.xwork2.ActionContext" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="../language.jsp" %>
<%@ page import="java.io.File,java.util.List,java.util.ArrayList,java.util.Date,com.wstuo.common.security.utils.AppConfigUtils"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<link href='../../css/bootstrap-cerulean.min.css' rel='stylesheet'>
<head>
<title><fmt:message key="request.print.workOrder" /></title>
<style type="text/css">

th,td,tr,body,div,span{font-size:12px;}
#requestPrint_west td, #requestPrint_west th
{
	padding:5px;
	/* border:1px solid black; */
}
a{
	color:#474747;
	text-decoration:none;
	cursor:pointer;
}
/* .spans
{
	border:1px solid #ccc;
	border-radius:3px;
	padding:4px;
	position:relative;
	margin:4px 3px;
	width:60px;
}
.wspan
{
	 margin:4px 3px;
	 float:left;
	 padding:5px;
} */
/*
	width:794px;height:1090px;
*/
</style>
<script type="text/javascript">
var eno='${param.eno}';
</script>

<script src="${pageContext.request.contextPath}/js/jquery/jquery-2.1.4.min.js"></script>
<script src="${pageContext.request.contextPath}/js/basics/jqgrid_common.js"></script>
<script src="${pageContext.request.contextPath}/js/itsm/request/requestPrint.js"></script>
<script src="${pageContext.request.contextPath}/js/i18n/i18n_${lang}.js"></script>
</head>
<body>
<center>
<%
String path=application.getRealPath("jasper");
File file=new File(path);
String images="logo.jpg";
File [] files=file.listFiles();
if(files!=null && files.length>0){
	for(File f:files){
		if(f.isFile()){
			images=f.getName();
		}
	}
}
%>
<c:set var="images" value="<%=images%>"/>
<div style="width:794px;"><center>
<!--startprint-->
<div>
	<table  width="100%">
		<thead>
			<tr style="font-size:12px;" align=left>
				<th width="40%" align="left">
					<c:if test="${empty pagesSet.paName}">
                        <img style="height:60px" src="../../images/logo.png" />
                    </c:if>
				<th align="left"><br/><font size="6px" ><fmt:message key="request.workOrder" /></font></th>
			</tr>
		</thead>
		<tbody>
			<tr height=30px>
				<td><fmt:message key="request.workOrderNo" />:
				     <span  id="request_printCode"></span>
				</td>
				<td align=right>
					<span id="request_printCreatedOn"></span>&nbsp;
				</td>
			</tr>
		</tbody>
	</table>
</div>
<div id="requestPrint_west">
	<table  class="table table-striped table-bordered responsive" width="100%">
		<tbody>
		<tr style="font-size:12px;background:#ccc" align="left">
			<th style="padding:5px; text-align: left" colspan="4"><fmt:message key="request.BasicInfoOfRequestor" /></th>
		</tr>
		<tr height=30>
		<td width="108px" ><fmt:message key="label.request.requestUser" /></td>
		<td width="208px"><span id="request_printCreatedByName"></span></td>
		<td width="109px"><fmt:message key="label.request.phone"/></td>
		<td><span id="request_printCreatedByPhone"></span></td>
		</tr>
		<tr style="font-size:12px;" align="left">
			<th style="text-align: left" colspan="4"><fmt:message key="request.info"/></th>
		</tr>
		</tbody>
		<tr>
		<td><fmt:message key="common.title" /></td>
		<td colspan="3"><span style="text-align:left;font-size:12px;" id="request_printTitle"></span></td>
		</tr>
		
		
		<tr>
		<td><fmt:message key="common.content" /></td>
		<td height="200px" colspan="3">
			<div id="request_printEdesc" style="word-break:break-all;word-wrap:break-word;"></div>
		</td>
		</tr>
		
		<tr>
		<td><fmt:message key="label.request.effect" /></td>
		<td><span style="text-align:left;font-size:12px;" id="request_printRangeName"></span></td>
		<td><fmt:message key="common.state" /></td>
		<td><span style="text-align:left;font-size:12px;" id="request_printStatusName"></span></td>
		</tr>
		
		
		<tr>
		<td><fmt:message key="common.category" /></td>
		<td><span style="text-align:left;font-size:12px;" id="request_printCategoryName"></span></td>
		<td><fmt:message key="label.request.priority" /></td>
		<td><span style="text-align:left;font-size:12px;" id="request_printPriorityName"></span></td>
		</tr>
		

		<tr>
		<td><fmt:message key="label.request.slaReuqestTime" /></td>
		<td><span style="text-align:left;font-size:12px;" id="request_printMaxResponsesTime"></span></td>
		<td><fmt:message key="label.request.slaCompleteTime" /></td>
		<td><span style="text-align:left;font-size:12px;" id="request_printMaxCompletesTime"></span></td>
		</tr>
		
		<tr>
		<td><fmt:message key="label.request.slaExpired" /></td>
		<td><span style="text-align:left;font-size:12px;" id="request_slaExpired"></span></td>
		<td><fmt:message key="common.cerateTime" /></td>
		<td><span style="text-align:left;font-size:12px;" id="request_cerateTime"></span></td>
		</tr>
		
		<tr >
		<td><fmt:message key="label.request.requestTime" /></td>
		<td><span style="text-align:left;font-size:12px;" id="request_printResponsesTime"></span></td>
		<td><fmt:message key="label.request.completeTime" /></td>
		<td><span style="text-align:left;font-size:12px;" id="request_printCloseTime"></span></td>
		</tr>
		<tr >
		<td><fmt:message key="title.request.requestHistory" /></td>
		<td height="130px" colspan="3"><span style="text-align:left;font-size:12px;" id="request_printHistory"></span></td>
		</tr>
		
		<tr >
		<td><fmt:message key="title.request.solutions" /></td>
		<td height="269px" colspan="3"><span style="text-align:left;font-size:12px;" id="request_printSolutions"></span></td>
		</tr>
		
		<tr >
		<td colspan="4" align=right><br/><span style="margin-right:180px"><fmt:message key="lable.workOrderSignature" />：</span><br/><br/>
			<span style="margin-right:180px"><fmt:message key="common.date" />：</span><br/><br/></td>
		</tr>
		</tbody>
	</table>
</div>
<!--endprint-->
</center>
</div>
</center>

</body>