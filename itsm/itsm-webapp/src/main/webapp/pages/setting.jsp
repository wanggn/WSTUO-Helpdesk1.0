﻿<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="com.wstuo.common.config.basicConfig.service.ICurrencyService"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@page import="org.springframework.context.ApplicationContext"%>
<%@page import="com.wstuo.common.tools.service.IPagesSetSerice"%>
<%@page import="com.wstuo.common.config.systemGuide.service.ISystemGuideService"%>
<%@page import="com.wstuo.moduleManage.service.IModuleManageService"%>
<%@page import="com.wstuo.moduleManage.dto.ModuleManageToolDTO"%>
<%@page import="com.wstuo.common.tools.entity.PagesSet"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<fmt:setTimeZone value="${clientTimeZone}" scope="session"/>

<%
String url="login.jsp";
//坐席接受信息
String callNumber="";
if(request.getParameter("callNumber")!=null){
	callNumber=new String(request.getParameter("callNumber").getBytes("iso8859-1"),"UTF-8");
	url=url+"?callNumber="+callNumber+"&";
}
String seatNo="";
if(request.getParameter("seatNo")!=null){
	seatNo=new String(request.getParameter("seatNo").getBytes("iso8859-1"),"UTF-8");
	url=url+"seatNo="+seatNo+"&";
}
String keyLoggers="";
if(request.getParameter("keyLoggers")!=null){
	keyLoggers=new String(request.getParameter("keyLoggers").getBytes("iso8859-1"),"UTF-8");
	url=url+"keyLoggers="+keyLoggers+"&";
}
String other1="";
if(request.getParameter("other1")!=null){
	other1=new String(request.getParameter("other1").getBytes("iso8859-1"),"UTF-8");
	url=url+"other1="+other1+"&";
}
String other2="";
if(request.getParameter("other2")!=null){
	other2=new String(request.getParameter("other2").getBytes("iso8859-1"),"UTF-8");
	url=url+"other2="+other2+"&";
}
if(session.getAttribute("loginUserName")==null){
	response.sendRedirect(url);
	return;
}
response.setHeader("Cache-Control","no-store");
response.setHeader("Pragrma","no-cache");
response.setDateHeader("Expires",0);
//获取当前浏览器语言做为默认的语言版本start
String browserLanguage = (String)session.getAttribute("browserLanguage");
if(browserLanguage==null){
	request.setAttribute("defaultLanguage","zh_CN");
}else{
	request.setAttribute("defaultLanguage",browserLanguage);
}
//获取当前浏览器语言做为默认的语言版本end

ApplicationContext ac = WebApplicationContextUtils.getWebApplicationContext(getServletContext());
IPagesSetSerice systemPage = (IPagesSetSerice)ac.getBean("pagesSetSerice");
PagesSet pagesSet=systemPage.showPagesSet();
request.setAttribute("pagesSet",pagesSet);

ISystemGuideService systemGuide = (ISystemGuideService)ac.getBean("systemGuideService");
//Boolean boos=systemGuide.findGuideBoo();
//System.out.println("====="+systemGuide.findGuideBoo());

//默认货币单位
ICurrencyService currencyService = (ICurrencyService)ac.getBean("currencyService");
request.setAttribute("currency_sign",currencyService.findDefaultCurrency().getSign());
String version=com.wstuo.common.config.register.service.RegisterService.version;
request.setAttribute("version",version);

%>
<c:set var="currency_sign" scope="session" value="${currency_sign}" />

<fmt:setLocale value="${empty cookie['language'].value?defaultLanguage:cookie['language'].value}"/>
<fmt:setBundle basename="i18n.itsmbest"/>
<c:set var="lang" value="${empty cookie['language'].value?defaultLanguage:cookie['language'].value}" />
<!--<c:set var="versionType" value="${empty cookie['versionType'].value?'':cookie['versionType'].value}" />-->
<c:set var="versionType" scope="session" value="ITSOP"/>
<c:set var="currentTime" value="<%=new java.util.Date().getTime()%>" />
<c:if test="${param.callNumber ne ''}">
<c:set var="callNumber" value="${empty cookie['callNumber'].value?'':cookie['callNumber'].value}" />
<c:set var="seatNo" value="${empty cookie['seatNo'].value?'':cookie['seatNo'].value}" />
<c:set var="keyLoggers" value="${empty cookie['keyLoggers'].value?'':cookie['keyLoggers'].value}" />
<c:set var="other1" value="${empty cookie['other1'].value?'':cookie['other1'].value}" />
<c:set var="other2" value="${empty cookie['other2'].value?'':cookie['other2'].value}" />
<%-- systemVersion：OEM版本控制,值非WSTUO为OEM版本(提供给我们合作伙伴的); --%>
<c:set var="systemVersion" scope="session" value="WSTUO" />
<%-- passwordType：密码安全级别；值为Simple,密码安全级别为简单,其他为高(大小写，8位以上...) --%>
<c:set var="passwordType" scope="session" value="Simple" />
</c:if>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<title><fmt:message key="title.mainTab.systemSet"/></title>
<META http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" ></META>
<link rel="stylesheet" href="../styles/comm.css">
<link rel="stylesheet" href="../skin/default/icon.css">
<link rel="stylesheet" href="../scripts/jquery/easyui/themes/default/easyui.css">
<link rel="stylesheet" href="../scripts/jquery/jquery-ui-1.8.2.custom/css/cupertino/jquery-ui-1.8.2.custom.css" />
<link rel="stylesheet" href="../scripts/jquery/jqgrid/css/jqgrid.css" />
<link rel="stylesheet" href="../styles/style.css">
<link rel="stylesheet" href="../scripts/jquery/fullcalendar-1.5/fullcalendar/fullcalendar.css" />
<link rel="stylesheet" href="../scripts/jquery/uploadify/uploadify.css" /> 
<link rel="stylesheet" href="../scripts/jquery/jm-calendar/css/jm-calendar.css" />
<!-- 新增、编辑、详情 -->
<link id="addRequestFormCustom_setting" href="../styles/common/addRequestFormCustom-no.css"  rel="stylesheet" type="text/css"/> 
<link id="addCimFormCustom_setting" href="../styles/common/addCimFormCustom-no.css"  rel="stylesheet" type="text/css"/> 
<link href="../styles/common/formCustomDesign.css"  rel="stylesheet" type="text/css"/>

<script type="text/javascript">
var isckeditor="";
var browser=navigator.appName
var b_version=navigator.appVersion
var version=b_version.split(";");
var trim_Version=""; 
if(version[1]!=null)
{
 trim_Version=version[1].replace(/[ ]/g,"")
}
if((browser == "Microsoft Internet Explorer" && trim_Version=="MSIE8.0") || (browser == "Microsoft Internet Explorer" && trim_Version=="MSIE9.0"))
{
	isckeditor="true";
}
</script>
<script type="text/javascript">
	if(isckeditor != 'true'){
		document.write("<link rel=\"stylesheet\" href=\"../scripts/jquery/ckeditor4.4.7/skins/moono/editor.css?t=D03G5XL\" />");
		document.write("<link rel=\"stylesheet\" href=\"../scripts/jquery/ckeditor4.4.7/contents.css\" />");
	}else{
		document.write("<link rel=\"stylesheet\" href=\"../scripts/jquery/ckeditor3.6/skins/kama/editor.css?t=D03G5XL\" />");
		document.write("<link rel=\"stylesheet\" href=\"../scripts/jquery/ckeditor3.6/skins/kama/dialog.css?t=D03G5XL\" />");
	    document.write("<link rel=\"stylesheet\" href=\"../scripts/jquery/ckeditor3.6/contents.css\" />");
    }
</script>

<link rel="stylesheet" href="../skin/default/leftMenu/styles/leftMenu.css">
<link rel="stylesheet" href="../skin/default/icon-ui.css">
<link rel="stylesheet" href="../skin/default/styles/linkbutton.css">
<link id="formCustomDesign_css" href="../styles/common/formCustomDesign-preview-no.css"  rel="stylesheet" type="text/css"/>
<script type="text/javascript">
var page = 'setting';
var loginID="loginID";

var isProblemHave='${applicationScope.problemHave}';//问题模块是否存在
var isChangeHave='${applicationScope.changeHave}';//变更模块是否存在
var isReleaseHave='${applicationScope.relaseHave}';//发布模块是否存在
var isRequestHave='${applicationScope.requestHave}';//请求模块是否存在
var isCIHave='${applicationScope.cimHave}';//配置项 模块是否存在

var category_num=50;//分类条数
var langBrowser = '${lang}';
</script>
<script src="../scripts/jquery/jquery.min.js"></script>
<script src="../scripts/i18n/i18n_${lang}.js"></script>
<script src="../scripts/jquery/jqgrid/js/jqGrid.js"></script>
<script src="../scripts/jquery/jquery.bgiframe.min.js"></script>
<script src="../scripts/jquery/jquery.xml2json.js"></script>
<script src="../scripts/jquery/easyui/plugins/jquery.form.js"></script>
<script src="../scripts/jquery/easyui/plugins/jquery.layout.js"></script>
<script src="../scripts/jquery/easyui/plugins/jquery.linkbutton.js"></script>
<script src="../scripts/jquery/easyui/plugins/jquery.panel.js"></script>
<script src="../scripts/jquery/easyui/plugins/jquery.tabs.js"></script>
<%-- autocomplete--%>
<script src="../scripts/jquery/jquery-ui-1.8.2.custom/development-bundle/ui/jquery.ui.core.js"></script>
<script src="../scripts/jquery/jquery-ui-1.8.2.custom/development-bundle/ui/jquery.ui.widget.js"></script>
<script src="../scripts/jquery/jquery-ui-1.8.2.custom/development-bundle/ui/jquery.ui.mouse.js"></script>
<script src="../scripts/jquery/jquery-ui-1.8.2.custom/development-bundle/ui/jquery.ui.position.js"></script>
<script src="../scripts/jquery/jquery-ui-1.8.2.custom/development-bundle/ui/jquery.ui.autocomplete.js"></script>
<script src="../scripts/jquery/jquery-ui-1.8.2.custom/development-bundle/ui/jquery.ui.datepicker.js"></script>
<script src="../scripts/jquery/jquery-ui-1.8.2.custom/development-bundle/ui/jquery.ui.resizable.js"></script>
<script src="../scripts/jquery/jquery-ui-1.8.2.custom/development-bundle/ui/jquery.ui.dialog.js"></script>
<script src="../scripts/jquery/jquery-ui-1.8.2.custom/development-bundle/ui/jquery.ui.draggable.js"></script>
<script src="../scripts/jquery/jquery-ui-1.8.2.custom/development-bundle/ui/jquery.ui.button.js"></script>
<script src="../scripts/jquery/jquery-ui-1.8.2.custom/development-bundle/ui/jquery.ui.accordion.js"></script>
<script src="../scripts/jquery/jquery-ui-1.8.2.custom/development-bundle/ui/jquery.ui.progressbar.js"></script>
<script src="../scripts/jquery/jquery-ui-1.8.2.custom/development-bundle/ui/jquery.ui.sortable.js"></script>
<script src="../scripts/jquery/jquery-ui-1.8.2.custom/development-bundle/ui/jquery.ui.droppable.js"></script>
<script src="../scripts/jquery/easyui/plugins/jquery.resizable.js"></script>
<script src="../scripts/jquery/easyui/plugins/jquery.validatebox.js"></script>
<script src="../scripts/jquery/easyui/locale/easyui-lang-${lang}.js"></script>
<script src="../scripts/jquery/easyui/plugins/jquery.parser.js"></script>
<script src="../scripts/jquery/easyui/plugins/jquery.numberbox.js"></script>
<script src="../scripts/jquery/My97DatePicker/WdatePicker.js"></script>


<script src="../scripts/jquery/jstree/js/lib/jquery.cookie.js"></script>
<script src="../scripts/jquery/jstree/js/lib/jquery.hotkeys.js"></script>
<script src="../scripts/jquery/jstree/js/jquery.jstree.js"></script>
<script src="../scripts/jquery/jstree/js/rewritejstree.js"></script>

<script src="../scripts/jquery/jquery.floatingmessage.js"></script>
<script src="../scripts/jquery/contextmenu/jquery.contextmenu.js"></script>
<script src='../scripts/jquery/fullcalendar-1.5/fullcalendar/fullcalendar.js'></script>
<script src="../scripts/jquery/jm-calendar/js/jm-calendar.js?random=${currentTime}"></script>
<script src="../scripts/jquery/uploadify/swfobject.js"></script>
<script src="../scripts/jquery/uploadify/jquery.uploadify.v2.1.4.min.js"></script>
<script src="../scripts/jquery/jquery.query-2.1.7.js"></script>
<script src="../scripts/jquery/jquery-json.js"></script>
<script src="../scripts/jquery/ajaxfileupload/ajaxfileupload.js"></script>
<script type="text/javascript">
	if(isckeditor == 'true'){
		document.write("<script src=\"../scripts/jquery/ckeditor3.6/ckeditor.js\">"+"<\/script>");
	}else{
		document.write("<script src=\"../scripts/jquery/ckeditor4.4.7/ckeditor.js\">"+"<\/script>");
	}
</script>

<script src="../scripts/basics/htmlformat.js"></script>
<script src="../scripts/basics/jsformat.js"></script>
<script src="../scripts/core/import.js"></script>
<script src="../scripts/core/package.js"></script>
<script src="../scripts/basics/jqgrid_common.js"></script>
<script src="../scripts/basics/jquery_common.js"></script>
<script src="../scripts/basics/easyui_common.js"></script>

<script src="../scripts/basics/initUploader.js"></script>
<script src='../scripts/basics/browserProp.js'></script>
<script src='../scripts/basics/windowResize.js'></script>
<%-- flot chart --%>
<script src="../scripts/chart/excanvas.min.js"></script>
<script src="../scripts/basics/bottomMain.js"></script>
<script src='../scripts/leftMenu.js'></script>
<script src="../scripts/basics/tab/tabUtils.js"></script>
<script src="../scripts/itsm/app/autocomplete/autocomplete.js"></script>
<script src="../scripts/basics/ie6.js"></script>
<script src="../scripts/basics/columnChooser.js"></script>
<script src="../scripts/basics/timeZone.js"></script>
<script src="../scripts/setting.js?random=${currentTime}"></script>
<script src="../scripts/basics/resizeWidth.js"></script>
<script src="../scripts/common/config/customFilter/filterGrid_Operation.js"></script>
<script src="../scripts/common/security/defaultCompany.js"></script>
<script src="../scripts/itsm/portal/voiceCard.js"></script>
<script src="../scripts/itsm/request/requestStats.js"></script>
<script src="../scripts/common/config/customFilter/filterCM.js"></script>

<script src='../dwr/engine.js'></script>  
<script src='../dwr/util.js'></script>
<script src='../dwr/interface/getuserpagedwr.js'></script>
<script src='../scripts/itsm/dwr/userAssgin.js'></script>

<script>var lang="${lang}";var isAllRequest_Res=false;var allRequest_Res;</script>
<!-- <script src="../scripts/common/tools/file/fileUpload.js"></script> -->


<sec:authorize url="AllRequest_Res">
<script>isAllRequest_Res=true</script>
</sec:authorize>
<script>
leftMenu.menuProp.secondMenuId = 'systemSetting';
var firstMenu = '${param.firstMenu}';
var secondMenu = '${param.secondMenu}';

var callNumber="";//号码 
if('${param.callNumber}'!=''&&'${param.callNumber}'!=null){
	callNumber='${param.callNumber}';
}else{
	callNumber="${callNumber}";
	setCookie('callNumber',"",300);
}
var seatNo="";//坐席号 
if('${param.seatNo}'!=''&&'${param.seatNo}'!=null){
	seatNo='${param.seatNo}';
}else{
	seatNo="${seatNo}";
	setCookie('seatNo',"",300);
}
var keyLoggers="";//按键记录 
if('${param.keyLoggers}'!=null&&'${param.keyLoggers}'!=''){
	keyLoggers='${param.keyLoggers}';
}else{
	keyLoggers="${keyLoggers}".replace("%2C",",");
	setCookie('keyLoggers',"",300);
}
var other1="";//其他1 
if('${param.other1}'!=null&&'${param.other1}'!=''){
	other1='${param.other1}';
}else{
	other1="${other1}";
	setCookie('other1',"",300);
}
var other2="";//其他2 
if('${param.other2}'!=null&&'${param.other2}'!=''){
	other2='${param.other2}';
}else{
	other2="${other2}";
	setCookie('other2',"",300);
}
$.ajaxSetup({
	contentType: "application/x-www-form-urlencoded; charset=utf-8"
});
window.onerror=function(){return false};
//刷新按钮加载标示
var load_shuaxin_flag=false;
var messageSize=0;//控制标题信息条数
var eventType = "${sessionScope.eventType}";
var pageType = "${sessionScope.pageType}";
var eventID = "${sessionScope.eventId}";
var language='${lang}';//当前语言版本
var fullName="${sessionScope.fullName}";
var userId = "${sessionScope.userId}";
var userName="${sessionScope.loginUserName}";//当前登录用户
var index_userId="${sessionScope.userId}";//当前登录用户id
var companyNo='${sessionScope.companyNo}';//当前用户所在公司No
var companyName='${sessionScope.companyName}';//当前用户所在公司名称
var topCompanyNo='${sessionScope.topCompanyNo}';//最上层公司
var versionType='${sessionScope.versionType}';//系统版本类型
var userRoleCode='${sessionScope.roleCode }';//系统版本类型
var systemVersion = '${sessionScope.systemVersion }';//系统版本
var currency_sign = '${sessionScope.currency_sign}';
var tableModule = "";
var hideCompany=true;//是否要隐藏所属公司字段
var knownErrors;
var onhelp=false;
var knowledge_fullSearchFlag;
var isITSOPUser=false;
if(versionType=='ITSOP')
	hideCompany=false;
if(versionType=='ITSOP' && companyNo==topCompanyNo)
	isITSOPUser=true;

var guideShare=""; 
var guideShareId=""; 
var reqRenovate=true;
var msg="";
//公司信息国际化
var junp_company='<fmt:message key="title.security.companyInfo"/>';
//默认页面
var default_page_resources='<fmt:message key="label.tools.workloadStatusStatistics"/>';
var default_page_tools='<fmt:message key="common.myTask"/>';
var default_page_report='<fmt:message key="title.report.strtusCount" />';
var default_page_basicset='<fmt:message key="title.security.companyInfo"/>';
var default_page_basicset='<fmt:message key="title.security.companyInfo"/>';
var default_page_release='<fmt:message key="title.mainTab.release.manage"/>';
var default_page_availability='<fmt:message key="title.mainTab.availabilityBase"/>';

var label_visit_rate='<fmt:message key="visit_rate_month_x_year" />';

var portalGridID = new Array();

/**浏览器窗口大小改变事件*/
$(window).resize(function(){
	initHeaderHeight();
	resizeCommon();
/* 	var title = $("ul[class=tabs] li[class=tabs-selected] span[class=tabs-title tabs-closable]").text();
    //Tab自适应
    basics.bottomMain.resizeTabByTitle(title);
    resizePortalGrid();//首页Grid列表自动宽度 */
});

//弹屏函数,函数名不能改
function popCustWin(req){
	itsm.portal.voiceCard.openCallWindow(req);
}
//实时显示连接小灵呼后台的状态,函数名不能改
function connectStatus(status){
var messageTd_msg="";
var messageTd = document.getElementById("messageTd");
//以下内容可以修改掉,以你自己的方式去显示
if(status.indexOf('sendComplete7')!=-1)
	messageTd_msg=i18n.label_conn_server_success 
else if(status.indexOf('reconnect')!=-1)
	messageTd_msg=i18n.label_re_conn_server;
else if(status=='6')
	messageTd_msg=i18n.label_conn_server;
else if(status=='9')
	messageTd_msg=i18n.label_conn_server_failure;
else 
	messageTd_msg=i18n.label_conn_server_status+"="+status+"";

 
}
//帮助菜单 Start//
function mousePos(e){
	var x,y;
	var e = e||window.event;
	return {
	x:e.clientX+document.body.scrollLeft+document.documentElement.scrollLeft,
	y:e.clientY+document.body.scrollTop+document.documentElement.scrollTop
	};
};
function update_left(e){
	$('#mm2').css('left',mousePos(e).x-160);  
};
//帮助菜单 End//

var loadCategoryFlag="0";
//点击要自动刷新的Tab
var clickRefreshTab=['<fmt:message key="label.dc.requestStatus" />',
					 '<fmt:message key="label.dc.imode" />',
					 '<fmt:message key="label.dc.priority" />',
					 '<fmt:message key="label.dc.level" />',
					 '<fmt:message key="label.dc.seriousness" />',
					 '<fmt:message key="label.dc.effectRange" />',
					 '<fmt:message key="label.dc.supplier" />',
					 '<fmt:message key="label.dc.useStatus" />',
					 '<fmt:message key="label.dc.location" />',
					 '<fmt:message key="label.dc.brand" />',
					 '<fmt:message key="label.dc.changeStatus" />',
					 '<fmt:message key="label.dc.problemStatus" />',
					 '<fmt:message key="label.dc.workloadStatus" />',
					 '<fmt:message key="label.dc.ciRelationType" />',
					 '<fmt:message key="label.dc.itsopType" />',
             		 '<fmt:message key="setting.requestCategory" />',
                     '<fmt:message key="setting.problemCategory" />',
                     '<fmt:message key="change.changeCategory" />',
                     '<fmt:message key="setting.serviceDirectory" />',
                     '<fmt:message key="knowledge.knowledgeCategory" />',
                     '<fmt:message key="label_software_category" />',
                     '<fmt:message key="setting.releaseCategory" />',
                     '<fmt:message key="label.dc.systemPlatform" />',
                     '<fmt:message key="label.dc.offmode" />',
                     '<fmt:message key="label.dc.releaseStatus" />',
                     '<fmt:message key="label.dc.SLAStatus" />',
                     '<fmt:message key="label_software_type" />',
                     '<fmt:message key="title.request.CICategory" />'
	             	];
var tobeRefreshGrid=[];
//判断数组是否包含元素.
tobeRefreshGrid.contains=function(element) {
	for (var i = 0; i < this.length; i++) {
		if (this[i] == element) {
			return true;
		}
	}
	return false; 
}

/////////DWR JS Start//////
//页面初始化  
function init() { 
   	//dwr.engine.setActiveReverseAjax(true); // 激活反转 重要   
   	getuserpagedwr.setScriptSessionFlag(userId);
    register();
}   
function onPageLoad(){
	getuserpagedwr.onPageLoad(userId);
}
//DWR 服务器异常处理
dwr.engine._errorHandler = function(message, ex) {
	//dwr.engine._debug("Error: " + ex.name + ", " + ex.message, true);
};
/////////DWR JS End//////
//加载标识
function shortcuts(){
    basics.tab.tabUtils.openTabById('helpdeskPortalTab',i18n['label_quick_launch']);
}
//Layoutshou，重绘UI

$(function(){
	$('body').layout('panel','west').panel({
		onCollapse:function(){ //该面板在点击折叠按钮后，执行折叠动作时要做的处理（方法）
			basics.tab.tabUtils.refreshWindow();
		},
		onExpand:function(){  //该面板在点击折叠按钮后，执行展开动作时要做的处理（方法）
			basics.tab.tabUtils.refreshWindow();
		}
	});
	$('#reg_west').panel({
		onResize:function(width, height){
			initHeaderHeight();
		},
		onCollapse:function(){ //该面板在点击折叠按钮后，执行折叠动作时要做的处理（方法）
			initHeaderHeight();
		},
		onExpand:function(){  //该面板在点击折叠按钮后，执行展开动作时要做的处理（方法）
			initHeaderHeight();
		}
	});
	
	
	initHeaderHeight();
	$('#processHeightDiv,#index_fullSearch_height').hide();
	//要显示的DIV ID
	$('#t_code_div,#processTable,#index_fullSearch_table,#t_code_cmd,#copyrightInfo,#guide_company_window,#guide_email_window,#guide_org_window,#guide_user_window').show();
	//获取终端用户时区并保存到会话
	$.post('timeZone!loadClientTimeZone.action','clientTimeZone='+clientTimeZone('GMT').replace('+','^'),function(){});
	
	$('#mm2').mouseover(function(){
		onhelp=true;
        $('#mm2').css('display','block');
    }).mouseout(function(){
    	onhelp=false;
        $('#mm2').css('display','none'); 
  	});
	$('#mb2').mouseover(function(){
        $('#mm2').css('display','block');
    }).mouseout(function(){
    	if(!onhelp){
    		$('#mm2').css('display','none');
        }
  	});
	basics.tab.tabUtils.recordOpenTab();
	
});
</script>
<c:set var="groupId" scope="session" value="Group_${orgNo}"/>
<link rel="stylesheet" href="../styles/hack/hackfix.css" />
</head>
<body class="easyui-layout" id="index_body" onload="dwr.engine.setActiveReverseAjax(true);dwr.engine.setNotifyServerOnPageUnload(true);onPageLoad();">
						
	<!-- 顶部 -->
 	<div region="north" id="index_header_north" border="false" split="false" style="height:45px;">
		<div id="index_header_north_header">
            <!-- 顶部LOGO -->
            <div class="index_header_north_logo"><a id="index_logo_href" target="_blank"></a></div>
            <!-- 顶部公司名 -->
            <div class="index_header_north_company" style="display: none;" id="index_companyName">${sessionScope.companyName}</div>
            <!-- 顶部右边工具栏 -->
            <div class="index_header_north_left">
            	<div style="width:auto;margin-right: 60px;"><fmt:message key="label.index.welcome"/>, ${fullName}</div>
                <div class="index_header_north_opt">
                &nbsp;<a onclick="basics.tab.tabUtils.refreshItsmMainTab()" title="<fmt:message key="common.refresh"/>" refresh="refresh" class="index_refresh"  id="index_refresh" ></a>
							<a onclick="logOut()" title="<fmt:message key="common.loginOut"/>"  class="index_loginout"></a>
							<a  class="index_help"<c:if test="${systemVersion!='WSTUO'}"><c:out value="style=display:none"></c:out></c:if> id="mb2" class="easyui-menubutton l-btn l-btn-plain" onmousemove="update_left(event)" iconcls="icon-help" menu="#mm2"><span class="l-btn-left"><span class="l-btn-text">
    							<span class="m-btn-downarrow"></span></span></span></a> 
							<span id="windowWidth"></span> 
                </div>
                <div id="common_Help_to_id" title='<fmt:message key="system_to_help_detail"/>' class="WSTUO-dialog"  style="width:650px;height:270px;">
					<div style="padding:3px;">
						<table style="width:100%;height:auto;">
							<tr>
								<td align="center" style="font-size:24px;"><fmt:message key="system_to_help_detail" /></td>
							</tr>
							<tr>
							<td style="padding-left:50px;padding-right:50px;font-size:12px;">Ver<%=version%></td>
							</tr>
							<tr>
							<td style="padding-left:50px;padding-right:50px;font-size:12px;"><fmt:message key="system_to_help_Copyright" />©<fmt:message key="system_to_help_guoyu" /></td>
							</tr>
							<tr>
							<td style="padding-left:50px;padding-right:50px;font-size:12px;"><fmt:message key="system_to_help_WSTUO" /></td>
							</tr>
							<tr>
							<td style="padding-left:50px;padding-right:50px;font-size:12px;"><fmt:message key="system_to_help_Systems" /></td>
							</tr>
							<tr>
							<td style="padding-left:50px;padding-right:50px;font-size:12px;"><fmt:message key="system_to_help_Agreement" /></td>
							</tr>
							<tr>
							<td style="padding-left:50px;padding-right:50px;font-size:12px;"><fmt:message key="system_to_help_http" /></td>
							</tr>
							<tr>
							<td style="padding-left:50px;padding-right:50px;font-size:12px;"><fmt:message key="system_to_help_adopted" /></td>
							</tr>
						</table>
						<table>
							<tbody id="tbody"></tbody>
						</table>
					</div>
				</div>
				
            </div>
        </div>
       <%--  <div style="clear: both;"></div>
        <div id="index_header_north_userInfo">
            <div class="index_header_north_userInfo">
            <span class="index_header_north_userInfo_span"></span>
            <span style="position:absolute;+margin-top:1px;" ><fmt:message key="label.index.welcome"/>, ${fullName}&nbsp;&nbsp;-&nbsp;&nbsp;<fmt:message key="title.mainTab.systemSet"/></span>
       		 <!-- T_Code -->
				
            </div>
        </div> --%>
        <div style="clear: both;"></div>
	</div>
	<%-- 主页面左边菜单 --%>
	<div region="west" id="reg_west" split="false" border="false" style="width:155px;overflow: hidden;top:45px">
        <div id="leftMenu_second" style="display: block;">
            <%@ include file="common/security/leftMenu.jsp" %>
        </div>
	</div>
	
	<%-- 主页面主窗口 --%>
	<div region="center" id="regCenter" border="false">
           
        
        
       <div id="itsmMainTab" class="easyui-tabs" fit="false" border="true" style="margin-left: 5px;float: left;height:100%">
       </div>
	</div>

<%--加载提示 --%>
<%@ include file="includes/includes_loading.jsp"%>
<%--显示操作进程 --%>
<%@ include file="includes/includes_process.jsp"%>

<div id="mm2" class="menu-top menu" style="width:175px;display:none;left:935px;top:32px;z-index:110015;">
<div class="menu-item" style="height: 20px;"><div class="menu-text">
<a title="<fmt:message key="common.Help"/>" href="http://www.bangzhutai.com/pages/showresourcesSearch.jsp?ids=381&_page=1" target="_blank"><fmt:message key="common.Help"/></a></div>
</div><div class="menu-item" style="height: 25px;">
<div class="menu-text"><a title="<fmt:message key="common.Help"/>" onclick="opentohelp()">
<fmt:message key="system_to_help_detail"/>
</a></div></div></div>

<div class="selectTypeList_div">
	<ul id="selectTypeList">
	<c:if test="${requestHave eq true}">
		<sec:authorize url="REQUEST_MAIN">
			<li typename="request"><A href="javascript:;"><fmt:message key="title.mainTab.request" /></A> </li>
		</sec:authorize>
		</c:if>
		<c:if test="${problemHave eq true}">
		<sec:authorize url="PROBLEM_MAIN">
		 <li typename="problem"><A href="javascript:;"><fmt:message key="title.mainTab.problem" /></A> </li>
		</sec:authorize>
		</c:if>
		<c:if test="${changeHave eq true}">
		<sec:authorize url="CHANGE_MAIN">
		 <li typename="change"><A href="javascript:;"><fmt:message key="title.mainTab.change" /></A> </li>
		</sec:authorize>
		</c:if>
		<li typename="knowledge"><A href="javascript:;"><fmt:message key="knowledge.knowledge" /></A> </li>
	  	<li class=last></li>
	</ul>
</div>

<%--左键菜单 --%>
<%@ include file="includes/includes_tabMenu.jsp"%>
<%-- 全文搜索 --%>
<%@ include file="includes/includes_fullSearch.jsp"%>
<%--首次加载配置向导 --%>
<sec:authorize url="/pages/organization!mergeCompany.action">
<sec:authorize url="/pages/mailServer!saveOrUpdateEmailServer.action">
<sec:authorize url="/pages/user!save.action">
<sec:authorize url="/pages/organization!save.action">
<%-- 向导 --%>
<%@ include file="common/security/includes/includes_guide.jsp"%>
<script src="../scripts/common/security/guide.js"></script> 
</sec:authorize>
</sec:authorize>
</sec:authorize>
</sec:authorize>
<script type="text/javascript">
var _keyDown = 0;
var _tarName = "BODY";
$(document).keydown(function(event){
	_keyDown = event.which;
	_tarName = event.target.tagName.toUpperCase();
	if(event.keyCode == 8 && _tarName != "INPUT" && _tarName != "TEXTAREA"){
		return false;
	}
});
function opentohelp(){
	windows('common_Help_to_id',{width:650,height:260});
}
</script>

<div style="height: 0px;overflow: hidden;width: 0px;">
<%--系统错误提示框--%>
<div id="systemErrorWin" title="<fmt:message key="msg.tips" />" style="width:300px;height:155px;">
	<div style="padding:3px;" id="systemErrorInfo" ></div>
</div>
<%--信息提示框--%>
<div id="alertMSG" title="<fmt:message key="msg.tips" />"></div>
<%-- 临时 --%>
<div id="temp_html"></div>
<%-- 知识库 --%>
<div id="knowledge_html"></div>
<%-- 面板数据详细信息 --%>
<div id="dashboardDataInfo_html"></div>
<%-- 请求动作页面 --%>
<div id="requestAction_html"></div>
<%-- 添加编辑任务 --%>
<div id="eventTask_html"></div>
<%-- 人工成本 --%>
<div id="eventTimeCost_html"></div>
<%-- 外包客户选择 --%>
<div id="selectCustomer_html"></div>
<%-- 选择关联配置项窗口（多选）--%>
<div id="selectCI_html"></div>
<%-- 选择用户窗口（单选）--%>
<div id="selectUser_html"></div>
<%-- 导入数据窗口--%>
<div id="importCsv_html"></div>
<%-- TCode窗口--%>
<div id="tCode_html"></div>
<%-- 常用分类 窗口--%>
<div id="category_html"></div>
<%-- 报表 窗口--%>
<div id="reportView_html"></div>
<%-- 自定义查询过滤器 窗口--%>
<div id="customFilter_html"></div>
<%-- 选择关联请求窗口 --%>
<div id="relatedRequest_html"></div>
<%-- 选择关联问题窗口 --%>
<div id="relatedProblem_html"></div>
<%-- 选择附件窗口 --%>
<div id="chooseAttachment_html"></div>
<%-- 邮件模板编辑窗口 --%>
<div id="editnotice_html"></div>
<%-- 快捷页面窗口 --%>
<div id="welcome_html"></div>
<%-- 弹屏页面窗口 --%>
<div id="voiceCard_html"></div>
<%-- 导出格式窗口 --%>
<div id="exportInfo_html"></div>
<%-- 扩展属性分钟窗口 --%>
<div id="attributeGroup_html"></div>
<%-- 选择关联变更窗口 --%>
<div id="relatedChange_html"></div>
<%-- 选择技术组窗口 --%>
<div id="selectTechnicalGroup_html"></div>
<%-- 流程公用窗口 --%>
<div id="flowCommonWin_html"></div>
<%-- 邮件新增窗口 --%>
<div id="addnotice_html"></div>
<%-- ACL --%>
<div id="aclWin_html"></div>

</div>
</body>
</html>