<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ include file="../../language.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>配置向导includes文件</title>
</head>
<body>
<script>
<!--
var guide_org_title='<fmt:message key="title.security.orgSet"/>';
var guide_user_title='<fmt:message key="title.security.userManager"/>';
//-->
</script>


<div style="overflow: hidden;width: 0px;height: 0px;position:relative">
<div id="guide_company_window"  title="<fmt:message key="label.basicSettings.title" />" style="width:500px;height:auto">
	<form>
	<div class="lineTableBgDiv">
       		<table style="width:100%" class="lineTable" cellspacing="1">
                
                <tr>
                <td style="width:25%"><fmt:message key="label.basicSettings.companyName"/></td>
                <td style="width:75%"><input id="guide_init_orgName" name="organizationDto.orgName" class="easyui-validatebox" style="width:80%" required="true" /></td>
                </tr>
                
                <tr>
                <td><fmt:message key="label.basicSettings.companyTel"/></td>
                <td><input id="guide_init_officePhone" name="organizationDto.officePhone" class="easyui-validatebox" style="width:80%" validType="phone" /></td>
                </tr>
                
                <tr>
                <td><fmt:message key="label.basicSettings.companyFax"/></td>
                <td><input id="guide_init_officeFax" name="organizationDto.officeFax" class="easyui-validatebox" style="width:80%" validType="phone"  /></td>
                </tr>
                
              
                
                <tr>
                <td><fmt:message key="label.basicSettings.companyEmail"/></td>
                <td><input id="guide_init_email" name="organizationDto.email" class="easyui-validatebox" style="width:80%" validType="email" /></td>
                </tr>
               
                <tr>
                <td><fmt:message key="label.basicSettings.companyAddress"/></td>
                <td><input id="guide_init_address" name="organizationDto.address" style="width:80%" /></td>
                </tr>
                
                
                
                <tr id="homepage">
                <td><fmt:message key="label.basicSettings.companyWebsite"/></td>
                <td><input id="guide_init_homePage" name="organizationDto.homePage" class="easyui-validatebox" style="width:80%" validType="url" /></td>
                </tr>
               
               
               
                <sec:authorize url="/pages/organization!mergeCompany.action">
               
                <tr id="logo">
                <td style="height:32px"><fmt:message key="label.basicSettings.companyLOGO"/></td>
                <td>
                
                
                <input type="hidden" id="guide_init_logo" name="organizationDto.logo" />
                
                <div id="guide_show_company_logo" class="showicon"></div>
					<input id="guide_uploadCompanyLogo" type="file" size="16" name="myFile" onchange="return commonUploadFile('guide_uploadCompanyLogo','guide_init_logo','guide_show_company_logo')"/>
                </td>
                </tr>
                
               
               <tr>
                <td colspan="2" style="height:35px;text-align:right">
                	<input type="hidden" id="guide_init_OrgNo" name="organizationDto.orgNo"/>
                	<a onclick="common.security.guide.saveCompanyInfoAndNext()" class="easyui-linkbutton" icon="icon-redo" ><fmt:message key="label.next"/></a>
                </td>
                </tr>
                </sec:authorize>
                
                </table>
                </div>
                
         </form>


</div>

<div id="guide_email_window"  title='<fmt:message key="label.basicSettings.secondTitle"/>' style="width:500px;height:auto">
	<form>
	<!-- 设置服务邮箱开始 -->
		<input type="hidden" name="emailServerDTO.emailType" value="normal" />
		<div class="lineTableBgDiv">
			<table style="width:100%" class="lineTable" cellspacing="1">
				<tr>
							<td width="120"><b><fmt:message key="label.your.name" />:</b></td>
							<td>
								<input name="emailServerDTO.personal"  style="width:42%" class="easyui-validatebox" />
								(<fmt:message key="label.your.name" /> [email@mail.com])
							</td>
						</tr>
				<tr>
							<td width="120"><fmt:message key="title.basicSettings.sendServiceEmail" /></td>
							<td>
								<input name="emailServerDTO.personalEmailAddress" id="personalEmailAddress"  style="width:60%" class="easyui-validatebox" validType="email"  />
							</td>
						</tr>
						<tr>
							<td width="120"><fmt:message key="title.basicSettings.emailAccount" /></td>
							<td>
								<input name="emailServerDTO.userName" id="userName"  style="width:60%" class="easyui-validatebox" />
							</td>
						</tr>
						
						
						<tr>
							<td width="120"><fmt:message key="i18n.password" /></td>
							<td>
								<input type="password" name="emailServerDTO.password" id="company_password" style="width:60%" class="easyui-validatebox input"  />
							</td>
						</tr>
						
						<tr>
							<td colspan="2"><b>[POP3 <fmt:message key="title.snmp.service"/>]</b></td>
				
						</tr>
			
						<tr>
							<td style="width:30%"><fmt:message key="label.basicSettings.serverAddress"/></td>
							<td style="width:70%">
								<input name="emailServerDTO.pop3ServerAddress" id="pop3_serverAddress" style="width:60%" class="easyui-validatebox input"/>
							</td>
						</tr>
						<tr>
							<td><fmt:message key="label.basicSettings.serverPort"/></td>
							<td>
								<input name="emailServerDTO.pop3ServerPort" id="pop3_serverPort" style="width:30px" class="easyui-numberbox" />
								(Port:110)
							</td>
						</tr>
						
						<tr>
							<td colspan="2" ><b>[SMTP <fmt:message key="title.snmp.service"/>]</b></td>
						</tr>
						
						<tr>
							<td style="width:30%"><fmt:message key="label.basicSettings.serverAddress"/></td>
							<td style="width:70%">
								<input name="emailServerDTO.smtpServerAddress" id="smtp_serverAddress" style="width:60%" class="easyui-validatebox" />
							</td>
						</tr>
						<tr>
							<td><fmt:message key="label.basicSettings.serverPort"/></td>
							<td>
								<input name="emailServerDTO.smtpServerPort" id="smtp_serverPort" style="width:30px" class="easyui-numberbox" />
								(Port:25)
							</td>
						</tr>
			
				
				<tr>
					<td colspan="2" style="padding-top:15px">
							<input type="hidden" name="emailServerDTO.emailServerId" id="emailServerId" value="1" /> 
							<input type="hidden" name="emailServerDTO.attestation" id="attestation"/>
							
							
							
							<table style="width:100%">
							<tr>
							<td style="width:30%;text-align:center">
								<a onclick="common.security.guide.previousToCompany()" class="easyui-linkbutton" icon="icon-undo" ><fmt:message key="label.above"/></a>
							</td>
							<td style="width:30%;text-align:center">
								<a onclick="common.security.guide.jumpMailServerAndNext()" class="easyui-linkbutton" ><fmt:message key="label.skip"/></a>
							</td>
							<td style="width:40%;text-align:center">
								<a onclick="common.security.guide.saveMailServerAndNext()" class="easyui-linkbutton" icon="icon-redo" ><fmt:message key="label.next"/></a>
							</td>
							</tr>
							
							</table>
	
					</td>
				</tr>
				
			</table>
		</div>
   </form>
</div>
<div id="guide_org_window"  title="<fmt:message key="label.basicSettings.thirdTitle" />" style="width:500px;height:auto">

	<form>
	<!-- 设置服务邮箱开始 -->
		<div class="lineTableBgDiv">
			<table style="width:100%" class="lineTable" cellspacing="1">
				<tr>
					<td colspan="2" id="guide_org_movie">
					</td>
				</tr>	
				<tr>
					<td colspan="2" style="padding-top:15px">
							<div style="width:150px;float:left">
								<a onclick="common.security.guide.previousToEmail()" class="easyui-linkbutton" icon="icon-undo" ><fmt:message key="label.above"/></a>
							</div>
							<div style="width:150px;float:right;text-align:right">
								<a onclick="common.security.guide.nextToUser()" class="easyui-linkbutton" icon="icon-redo" ><fmt:message key="label.next"/></a>
							</div>
					</td>
				</tr>
			</table>
		</div>
   </form>
</div>
<div id="guide_user_window"  title="<fmt:message key="label.basicSettings.fourthTitle" />" style="width:500px;height:auto">

	<form>
	<!-- 设置服务邮箱开始 -->
		<div class="lineTableBgDiv">
			<table style="width:100%" class="lineTable" cellspacing="1">
				<tr>
					<td colspan="2" id="guide_user_movie">
					</td>
				</tr>	
				<tr>
					<td colspan="2" style="padding-top:15px">
							<div style="width:150px;float:left">
								<a onclick="common.security.guide.previousToOrg()" class="easyui-linkbutton" icon="icon-undo" ><fmt:message key="label.above"/></a>
							</div>
							<div style="width:150px;float:right;text-align:right">
								<a onclick="common.security.guide.complete()" class="easyui-linkbutton" icon="icon-ok" ><fmt:message key="titie.complete"/></a>
							</div>
					</td>
				</tr>
			</table>
		</div>
   </form>
</div>
</div>
</body>
</html>