<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ include file="../../language.jsp" %>


<!-- 选择用户窗口-->
<div style="width: 0px;height: 0px;overflow: hidden;position:relative">
<div id="selectUser_window" title="<fmt:message key="tool.im.selectUser" />" style="width:800px;height:auto;padding:5px;">
    <div style="width:775px;height:auto;">
		
		<div region="west" split="true" style="width:200px;">
			<div id="selectUser_organizationTree"></div>
		</div>
		
		<div region="center" border="false">
			<div id="selectUser_window_tabs" style="width:auto; height:auto;" >
				<div id ="selectUser_userSearch" style="margin-top: 8px;margin-bottom: 3px;">
					<form id="userRoleFm" onsubmit="return false" >
						<table style="width:100%;text-align: center;" cellspacing="1" >
							<tr> 
								<td>
									<span style="padding-left: 10px;float: left;"><fmt:message key="label.user.selectUserTitle" />：</span>
									<span style="float: left;padding-left: 5px;">
										<select id="selectUser_userGrid_Roles" name="userQueryDto.roleNo" style="width:170px;" class="form-control"></select>
									</span>
									
								 	<span style="float: left;padding-left: 15px;">
										<a class="btn btn-primary btn-sm" plain="true" id="index_related_user_grid_select"><fmt:message key="common.select"/></a>
									</span>
									<span style="padding-left: 15px;float: left;"><fmt:message key="label.loginName" />：</span>
									<span style="float: left;padding-left: 5px;">
										<input type="text" id="selectUser_userGrid_loginName" name="userQueryDto.loginName" class="form-control" style="170px;"/>
									</span>
									<span style="float: left;padding-left: 15px;">
										<button id="selectUser_SearchSelect" class="btn btn-primary btn-sm" plain="true" ><fmt:message key="common.search" /></button>
									</span>								
								</td>
							</tr>
						</table>
						
					</form>
				</div>
				<div title="<fmt:message key="label.userList" />" style="padding:10px 3px 3px 3px">
					<table id="selectUser_userGrid" style="width: 100%;"></table>
					<div id="selectUser_userGridPager"></div>
				
				</div>
			</div>
		</div>
		<div region="south" style="padding:8px;height:45px;">
			<div id="selectUser_south_multi">
				<button class="btn btn-primary btn-sm" id="selectUser_ConfirmSelect"><fmt:message key="label.sla.confirmChoose" /></button>
			</div>
			<div id="selectUser_south_single">
				<fmt:message key="label.selectUser.confirm" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;				
			</div>
		</div>
	</div>
	
	<div id="selectUserByGroup_attendance" class="WSTUO-dialog" align="right" style="margin-right: 17px;border: #99bbe8 1px solid;margin-left: 2px;">
		<input type="radio" id="selectUserSouth_attendance" name='selectUserByGroupSouth' value='' checked="checked" class="selectUserSouthAttendance">
		<label for="selectUserSouth_attendance"><fmt:message key="lable.user.duty" />&nbsp;&nbsp;&nbsp;</label>
		<input type="radio" id="selectUserSouth_all" name='selectUserByGroupSouth' value='' class="selectUserSouthAttendance">
		<label for="selectUserSouth_all" class="selectUserSouthAttendance"><fmt:message key="lable.user.all" />&nbsp;&nbsp;</label>
	</div>
	
</div>
</div>



<div id="searchUserOnSelect" class="WSTUO-dialog" title="<fmt:message key="tool.im.searchUser" />" style="width:350px;height:auto">
<div class="lineTableBgDiv" >
<form id="searchUserOnSelectForm">
			<input name="userQueryDto.orgType" id="searchSelectUser_orgType" type="hidden">
       		<table style="width:100%" class="lineTable" cellspacing="1">
				<tr>
					<td style="width:30%" ><fmt:message key="label.loginName" /></td>
					<td style="width:70%">
						<input name="userQueryDto.loginName" id="searchSelectUser_loginName" class="form-control">
					</td>
				</tr>
				<tr>
					<td style="width:30%" ><fmt:message key="label.userName" /></td>
					<td style="width:70%">
						<input name="userQueryDto.fullName" id="searchSelectUser_loginName" class="form-control">
					</td>
				</tr>
				<tr>
					<td style="width:30%" ><fmt:message key="label.user.mobile" /></td>
					<td style="width:70%">
						<input name="userQueryDto.mobilePhone" id="searchSelectUser_mobile" class="form-control" >
					</td>
				</tr>
				<tr>
					<td style="width:30%" ><fmt:message key="label.user.phone" /></td>
					<td style="width:70%">
						<input name="userQueryDto.officePhone" id="searchSelectUser_phone" class="form-control" >
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<input type="hidden" name="userQueryDto.orgNo" id="searchSelectUser_orgNo" >
						<button class="btn btn-primary btn-sm" id="selectUser_common_dosearch" style="margin-top: 8px;margin-left: 15px;"><fmt:message key="common.search" /></button>
						
					</td>
				</tr>
	</table>
</form>
</div> 
</div>


<!--  显示指定角色的用户列表  -->    
<div id="selectUserByRole_window" class="WSTUO-dialog" title='<fmt:message key="tool.im.selectUser" />' style="width:530px;height:350px;padding:5px">
	<table id="selectUserByRole_grid"></table>
	<div id="selectUserByRole_pager"></div>
</div>    

<!--  显示技术员所在组的用户列表  -->    
<div id="selectUserByGroup_window" class="WSTUO-dialog" title='<fmt:message key="tool.im.selectUser" />' style="width:530px;height:350px;padding:5px">
	<div id="selectUserByGroup_opteron" class="WSTUO-dialog" align="left" style="margin-right: 17px;margin-left: 2px;">
		
		<label for="selectUser_opteron_all" class="radio-inline">
			<input type="radio" id='selectUser_opteron_all' name='selectUserByGroupR' value='' checked="checked">
			<fmt:message key="lable.user.all" />&nbsp;&nbsp;
		</label>
		
		
		<label for="selectUser_opteron_attendance" class="radio-inline">
		<input type="radio" id='selectUser_opteron_attendance' name='selectUserByGroupR' value=''>
		<fmt:message key="lable.user.duty" />&nbsp;&nbsp;&nbsp;
		
		</label>
		
	</div>
	<table id="selectUserByGroup_grid"></table>
	<div id="selectUserByGroup_pager"></div>
	<!--  -->
	
</div>

<!-- 选择服务机构 -->
<div id="index_selectServiceOrg_window" title='<fmt:message key="title.sla.chooseServiceOrg"/>' class="WSTUO-dialog" style="max-height:400px;overflow:auto;width:280px;height:auto;padding:3px;">
	<div id="index_selectServiceOrg_window_tree"></div>
</div>
<%--角色选择窗口--%>
<div id="selectRole_win" class="WSTUO-dialog" title='<fmt:message key="title.orgSettings.role" />' style="width:530px;height:350px;padding:5px">
	<table id="selectRole_grid"></table>
	<div id="selectRole_pager"></div>
</div>
<!-- 用户列表选择窗口  -->    
<div id="selectUserGrid_window" class="WSTUO-dialog" title='<fmt:message key="tool.im.selectUser" />' style="width:530px;height:350px;padding:5px">
	<table id="selectUser_grid"></table>
	<div id="selectUser_pager"></div>
</div>
