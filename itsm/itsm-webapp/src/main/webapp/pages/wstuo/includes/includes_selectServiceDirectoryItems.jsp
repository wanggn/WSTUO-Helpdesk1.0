<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ include file="../../language.jsp" %>     

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>服务目录includes文件</title>
</head>
<body>
<div style="width: 0px;height: 0px;overflow: hidden;position:relative">
<%-- 服务目录 --%>
<div id="index_select_service_dir_window" title='<fmt:message key="setting.subService" />' style="width:470px;height:auto;padding:3px">
	    <div class="easyui-layout" style="width:450px;height:400px;">
			<div region="west" split="true" style="width:200px;height:400px">
			    <div class="hisdiv" >
					<table style="width:100%" class="histable" cellspacing="1" id="index_select_service_dir_table">
					<thead>
					<tr>
					<th><fmt:message key="common.id" /></th>
					<th><fmt:message key="setting.serviceDirectory" /></th>
					</tr>
					
					</thead>
					<tbody style="text-align:center"></tbody>
					
					</table>
				</div>
			</div>
			<div region="center" style="width:200px;">
			    <div class="hisdiv" >
					<table style="width:100%" class="histable" cellspacing="1" id="index_select_service_dir_items_table">
					<thead>
					<tr>
					<th><fmt:message key="common.id" /></th>
					<th><fmt:message key="setting.subService" /></th>
					<th><fmt:message key="common.select" /></th>
					</tr>
					</thead>
					<tbody style="text-align:center"></tbody>
					</table>
				</div>
			</div>
		</div>
</div>
</div>
</body>
</html>