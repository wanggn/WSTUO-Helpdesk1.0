<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ include file="../../language.jsp" %>
<script type="text/javascript">
$(function(){
	var html="";
	var inporgress="";
	$.post("report!workloadStatusStatistics.action",function(data){
		$.each(data,function(i,o){
			if(o[2]>7){
				inporgress="繁忙";
			}else if(o[2]>3){
				inporgress="正常";
			}else{
				inporgress="空闲";
			}
			if(o[6]==null){
				o[6]=0;
			}
			html=html+"<tr><td>"+o[0]+"</td><td>"+o[1]+"</td><td>"+o[2]+"/"+o[3]+"/"+o[4]+"</td><td>"+o[5]+"</td><td>"+inporgress+"</td><td>"+o[6]+"</td></tr>";
		});
		$("#workloadStatusStatisticsinfo tbody").append(html);
	});
	
});
</script>
<div class="row">
		<div class="box col-md-12">
			<div class="box-inner">
				<div class="box-header well" data-original-title="">
					<h2>
						<i class="glyphicon glyphicon-list-alt"></i>&nbsp;技术员忙碌程度统计
					</h2>
					<div class="box-icon">
					</div>
				</div>
				<div class="box-content buttons" >
				 <table id="workloadStatusStatisticsinfo" class="table table-striped table-bordered responsive">
                        <thead>
                        <tr>
                            <th>技术员工号</th>
                            <th>技术员</th>
                            <th>处理数量(处理中/挂起/已完成)</th>
                            <th>已分配请求数量</th>
                            <th>忙碌程度</th>
                            <th>服务总分值</th>
                        </tr>
                        </thead>
                        <tbody>
                           
                        </tbody>
                    </table>
				</div>
			</div>
		</div>
 </div>