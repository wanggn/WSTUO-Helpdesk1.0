<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ include file="../../language.jsp" %>
<script src="../bower_components/flot/excanvas.min.js"></script>
<script src="../bower_components/flot/jquery.flot.js"></script>
<script src="../bower_components/flot/jquery.flot.pie.js"></script>
<script src="../bower_components/flot/jquery.flot.stack.js"></script>
<script src="../bower_components/flot/jquery.flot.resize.js"></script>
<script src="../js/basics/init-chart.js" type="text/javascript"></script>
<script type="text/javascript">
$(function(){
	$.post("report!requestSLACompleteReport.action",function(data){
		var html="";
		var array=["请求总数","按时完成","超时完成","未完成"]
		var attr=[];
		$.each(data,function(i,o){
			if(i==0){
				html=html+"<tr><td>"+array[i]+"</td><td>"+o+"</td></tr>";
			}else{
				html=html+"<tr><td>"+array[i]+"</td><td>"+o+"</td></tr>";
				attr.push({label:array[i], data:o});
			}
		});
		$("#requestSLACompleteinfo tbody").append(html);
		piechart("requestSLACompletepiechart",attr);
	});
});

</script>
<div class="row">
		<div class="box col-md-12">
			<div class="box-inner">
				<div class="box-header well" data-original-title="">
					<h2>
						<i class="glyphicon glyphicon-list-alt"></i>&nbsp;SLA完成率统计
					</h2>
					<div class="box-icon">
					</div>
				</div>
				<div class="box-content buttons" >
				 <table id="requestSLACompleteinfo" class="table table-striped table-bordered responsive">
                        <thead>
                        <tr>
                            <th width="50%">SLA完成率名称</th>
                            <th>个数</th>
                        </tr>
                        </thead>
                        <tbody>
                       
                        </tbody>
                    </table>
                     <div id="hover"></div>
				    <div id="requestSLACompletepiechart" style="height:500px"></div>
				   
				</div>
			</div>
		</div>
 </div>