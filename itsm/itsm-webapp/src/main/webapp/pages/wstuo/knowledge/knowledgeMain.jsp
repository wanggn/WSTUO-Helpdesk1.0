<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ include file="../../language.jsp" %>
<%

String keyWord="";
if(request.getParameter("keyWord")!=null){
	keyWord=new String(request.getParameter("keyWord").getBytes("iso8859-1"),"utf-8"); 
}
String rowValue="";
String colValue="";
if(request.getParameter("rowValue")!=null){
	rowValue=request.getParameter("rowValue");
	rowValue=java.net.URLDecoder.decode(rowValue, "utf-8");
}
if(request.getParameter("colValue")!=null){
	colValue=request.getParameter("colValue");
	colValue=java.net.URLDecoder.decode(colValue, "utf-8");
}
request.setAttribute("rowValue",rowValue);
request.setAttribute("colValue",colValue);
%>
<script>
var rowValue="${rowValue}";
var colValue="${colValue}";
var rowKey='${param.rowKey}';
var colKey='${param.colKey}';
var customFilterNo='${param.customFilterNo}';
var knowledge_queryType='${param.countQueryType}';
var filterId='${param.filterId}';
var sec_saveKnowledgeInfo="0";
var sec_removeKnowledgeItems="0";
var sec_updateKnowledgeItems="0";
var knowledge_fullSearchFlag="${param.fullsearch}";
var kw_opt='${param.opt}';
var type='${param.type}';
var creatorName=userName;
$(document).ready(function(){
	//showKw_btn(kw_opt);
	//绑定日期控件
	DatePicker97(['#know_search_startTime','#know_search_endTime']);
	
	
});
 setTimeout(function(){  
	wstuo.knowledge.leftMenu.countKnowledge();
	endLoading();
},500); 
</script>
<sec:authorize url="KNOWLEDGEINFO_ADDKNOWLEDGE">
<script>sec_saveKnowledgeInfo="1";</script>
</sec:authorize>
<sec:authorize url="/pages/knowledgeInfo!removeKnowledgeItems.action">
<script>sec_removeKnowledgeItems="1";</script>
</sec:authorize>
<sec:authorize url="/pages/knowledgeInfo!updateKnowledgeInfo.action">
<script>sec_updateKnowledgeItems="1";</script>
</sec:authorize>

<script src="../js/wstuo/category/serviceDirectoryUtils.js?random=<%=new java.util.Date().getTime()%>"></script>
<script src="${pageContext.request.contextPath}/js/wstuo/knowledge/knowledgeGrid.js?random=<%=new java.util.Date().getTime()%>"></script>

<script>var keyWord="<%=keyWord%>";</script>
<div id="knowledgeMain_layout" class="easyui-layout" fit="true" border="false" style="height:100%">
  <div class="row">
	<div class="box col-md-10">
		<div class="box-inner">
			<div class="box-header well" data-original-title="">
				<h2>
					<i class="glyphicon glyphicon-list-alt"></i> 知识列表
				</h2>
				<div class="box-icon">
				</div>
			</div>

			<div class="box-content ">
			
				<div  id="knowledgeMain"  style="padding:3px;height:100%">
					<table id="knowledgeGrid"></table>
					<div id="knowledgeGridPager"></div>
					<div id="knowledgeGridToolbar" style="display:none;overflow: hidden;">
					  <!-- 操作项 -->
					  <div id="knowledgeGridACT" style="display: none;"><a  id="kw_app_link" href="javascript:wstuo.knowledge.knowledgeGrid.showAppResult_aff('{kid}')" title="<fmt:message key="label_knowledge_appResult" />"><i class=" glyphicon glyphicon-check"></i></a>&nbsp;&nbsp;<a href="javascript:wstuo.knowledge.knowledgeGrid.showKnowledge_aff('{kid}')" title="<fmt:message key="common.detailInfo" />" ><i class="glyphicon glyphicon-list-alt"></i></a>&nbsp;&nbsp;&nbsp;<sec:authorize url="/pages/knowledgeInfo!updateKnowledgeInfo.action"><a href="javascript:wstuo.knowledge.knowledgeGrid.showEditKnowledge_aff('{kid}')" title="<fmt:message key="common.edit" />"><i class="glyphicon glyphicon-edit"></i></a></sec:authorize>&nbsp;&nbsp;&nbsp;<sec:authorize url="/pages/knowledgeInfo!removeKnowledgeItems.action"><a href="javascript:wstuo.knowledge.knowledgeGrid.deleteKnowledge_aff('{kid}')" title="<fmt:message key="common.delete" />" ><i class="glyphicon glyphicon-trash"></i></a></sec:authorize></div>
					
						<div id="sustainableMainDiv" style="float:left">
							<button class="btn btn-default btn-xs" onClick="wstuo.knowledge.knowledgeGrid.showAddKnowledge()" ><fmt:message key="label.contingencyPlan" /></button>
							<button class="btn btn-default btn-xs" onClick="wstuo.knowledge.knowledgeGrid.showAddKnowledge()" ><fmt:message key="label.disasterRecoveryPlan" /></button>
						</div>
						<sec:authorize url="KNOWLEDGEINFO_ADDKNOWLEDGE">
						<button class="btn btn-default btn-xs" onClick="wstuo.knowledge.knowledgeGrid.showAddKnowledge()"><i class="glyphicon glyphicon-plus"></i>&nbsp;<fmt:message key="common.add" /></button>
						</sec:authorize>
						<sec:authorize url="/pages/knowledgeInfo!updateKnowledgeInfo.action">
						<button class="btn btn-default btn-xs" onClick="wstuo.knowledge.knowledgeGrid.showEditKnowledge()" ><i class="glyphicon glyphicon-edit"></i>&nbsp;<fmt:message key="common.edit" /></button>
						</sec:authorize>
						<!--终端我未审核通过的知识的编辑start-->
						<button class="btn btn-default btn-xs" style="display:none;" id="knowledgeGridEdit" onClick="wstuo.knowledge.knowledgeGrid.showEndUserEditKnowledge()" ><i class="glyphicon glyphicon-edit"></i>&nbsp;<fmt:message key="common.edit" /></button>
						<!--终端我未审核通过的知识的编辑end-->
						<sec:authorize url="/pages/knowledgeInfo!removeKnowledgeItems.action">
						<button class="btn btn-default btn-xs" onClick="wstuo.knowledge.knowledgeGrid.deleteKnowledge()" ><i class="glyphicon glyphicon-trash"></i>&nbsp;<fmt:message key="common.delete" /></button>
						</sec:authorize>
						
						<sec:authorize url="/pages/knowledgeInfo!passKW.action"> 
							<button class="btn btn-default btn-xs" id="kw_app_btn" style="display:none" onClick="wstuo.knowledge.knowledgeGrid.appKw_openwindow('knowledgeMain')" ><i class="glyphicon glyphicon-check"></i>&nbsp;<fmt:message key="label_knowledge_approval" /></button>
						</sec:authorize>
						<sec:authorize url="knowledge_manage_reReferCheck">
						<button class="btn btn-default btn-xs" id="kw_reapp_btn"   onClick="wstuo.knowledge.knowledgeGrid.appKw_reopenwindow('reopenAppinfo')" ><i class="glyphicon glyphicon-check"></i>&nbsp;<fmt:message key="label.knowledgeMain.reopenApp" /></button>
						</sec:authorize>
						<button class="btn btn-default btn-xs" onClick="wstuo.knowledge.knowledgeGrid.search_konwledge_openwindow()" ><i class="glyphicon glyphicon-search"></i>&nbsp;<fmt:message key="common.search" /></button>
					
						<sec:authorize url="pages/knowledgeInfo!importKnowledgeFromExcel.action">
						  <button class="btn btn-default btn-xs" onclick="wstuo.knowledge.knowledgeGrid.importKnowledgeData_openWindow()" ><i class="glyphicon glyphicon-import"></i>&nbsp;<fmt:message key="label.dc.import"/></button>
						</sec:authorize>
						<sec:authorize url="pages/knowledgeInfo!exportKnowledgeFromExcel.action">
						   <button class="btn btn-default btn-xs" onclick="wstuo.knowledge.knowledgeGrid.exportKnowledgeData()"  ><i class="glyphicon glyphicon-share"></i>&nbsp;<fmt:message key="label.dc.export"/></button>
						</sec:authorize>
						<sec:authorize url="itsm_knowledge_knowledgeGrid_getDataByFilterSearch">
						  <button class="btn btn-default btn-xs" style="margin-right:15px"  id="knowledge_customFilter" ><i class="glyphicon glyphicon-filter"></i>&nbsp;<fmt:message key="lable.customFilter.self_defined" /></button>
						<!-- -->
						<select id="knowledge_userToSearch"  onchange="wstuo.knowledge.knowledgeGrid.getDataByFilterSearch(this.value)"></select>
						</sec:authorize>
						<form action="knowledgeInfo!exportKnowledgeFromExcel.action" method="post" id="export_knowledge_form">
						<div id="export_knowledge_values">
						
						</div>
						</form>
						<%-- 
						&nbsp;|&nbsp;<a href="javascript:showHelpFilm('knowledge.flv')" plain="true" class="easyui-linkbutton" icon="icon-help"></a>
						--%>
					
					</div>
				</div>		
			
			</div>
			
		</div>
	</div>
	
	<div class="box col-md-2">
		<div class="box-inner">
			<div class="box-header well" data-original-title="">
				<h2>
					<i class="glyphicon glyphicon-list-alt"></i> 知识类型
				</h2>
				<div class="box-icon">
					<!-- <a href="#" class="btn btn-minimize btn-round btn-default"><i
						class="glyphicon glyphicon-chevron-up"></i></a> -->
				</div>
			</div>

			<div class="box-content ">
			 <!-- 分类树start-->
				<div id="knowledgeCategoryDiv" region="east" border="true" title="<fmt:message key="knowledge.label.knowledgeSort" />" style="width:200px;padding:5px;padding-bottom: 15px;height:auto;">
					<div id="knowledgeCategoryTree" style="overflow: auto;height: 100%;"></div>
				</div>
			 <!-- 分类树 end-->
			</div>
			
		</div>
	</div>
	
</div>

<!-- 搜索知识库 start -->
<div id="knowledgeSearchDiv" class="WSTUO-dialog" style="width: 400px; height: auto" >

			<div class="panel panel-default">
				<div class="panel-body">
					<form  class="form-horizontal">
						<div class="form-group">
							<label class="col-sm-4 control-label" for="firstname"><fmt:message key="knowledge.label.knowledgeSort" /></label>
							<div class="col-sm-5">
								<input id="search_eventId" name="knowledgeQueryDto.eventId" type="hidden"/>
		                        <input name="knowledgeQueryDto.category" id="search_knCategory" class="form-control choose" readonly="readonly" />
		                         <a onclick="cleanIdValue('search_knCategory','search_eventId')" title="<fmt:message key="label.request.clear" />" ><span class="glyphicon glyphicon-remove-circle form-control-feedback choose" ></span></a>
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-4 control-label" for="firstname"><fmt:message key="knowledge.label.knowledgeTitle" /></label>
							<div class="col-sm-5">
							<input id="search_knTitle" name="knowledgeQueryDto.title" class="form-control" />
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-4 control-label" for="firstname"><fmt:message key="tool.affiche.creator" /></label>
							<div class="col-sm-5">
							 <input id="search_creator" name="knowledgeQueryDto.creatorFullName" class="form-control"/>
		                     <sec:authorize url="/pages/user!find.action">
		                       <%--  <a  class="easyui-linkbutton" plain="true" icon="icon-user" title="<fmt:message key="common.select" />"></a> --%>
		                        <a id="search_creator_select" ><span class="glyphicon glyphicon-user form-control-feedback choose" ></span></a>
		                     </sec:authorize>
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-4 control-label" for="firstname"><fmt:message key="label.knowledge.relatedService" /></label>
							<div class="col-sm-5">
							    <div style="padding-top:3px;">
	                              <button class="btn btn-primary btn-sm"  style="float: left;" type="button" onclick="wstuo.category.serviceDirectoryUtils.searchKnowledgeService()" ><fmt:message key="lable.knowled.serviceTitle"/></button>
	                           </div>
	                           <div style="color:#ff0000;" id="search_service_name">
                               </div> 
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-4 control-label" for="firstname"><fmt:message key="common.attachment" /></label>
							<div class="col-sm-5">
							<input id="search_attachmentContent" name="knowledgeQueryDto.attachmentContent" class="form-control" />
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-4 control-label" for="firstname"><fmt:message key="common.createTime" /></label>
							<div class="col-sm-5">
							   <input id="know_search_startTime" name="knowledgeQueryDto.startTime" class="form-control" readonly/>
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-4 control-label" for="firstname"><fmt:message key="setting.label.to" /></label>
							<div class="col-sm-5">
							   <input id="know_search_endTime" name="knowledgeQueryDto.endTime"class="form-control" validType="DateComparison['know_search_startTime']" readonly/>
			                   <a class="easyui-linkbutton" plain="true" icon="icon-clean" onclick="cleanIdValue('know_search_startTime','know_search_endTime')" title="<fmt:message key="label.request.clear" />"></a>
							</div>
						</div>
						
						<input type="hidden" id="knowledgeQueryDto_keyWord" name="knowledgeQueryDto.keyWord" /> 
						<div class="form-group">
								<div class="col-sm-9 col-sm-offset-4">
		                            <button type="button" onclick="$('#knowledgeSearchDiv input').val('');$('#search_service_name').html('')"  class="btn btn-default btn-sm">
				                    <fmt:message key="i18n.reset"/></button>
				                    <button type="button" id="knowledge_search_ok" class="btn btn-primary btn-sm"><fmt:message key="common.search" /></button>
								</div>
						</div>
						
					</form>
				</div>
			</div>
</div>
<!-- 搜索知识库 end -->

<!-- 审批知识 start -->
<div id="knowledge_app_window" class="WSTUO-dialog" title='<fmt:message key="label_knowledge_appKw" />' style="width: 350px; height: auto">
<form>
<div class="lineTableBgDiv">
<table style="width: 100%" class="lineTable" cellspacing="1">
	<tr id="showAppInfo">
		<td><fmt:message key="label_knowledge_appOpinion" /></td>
		<td>
			<input type="radio" name="knowledgeDto.knowledgeStatus" id="knowledge_app_status" value="1" checked="checked"/><fmt:message key="label_knowledge_approvalPass" />
			<input type="radio" name="knowledgeDto.knowledgeStatus" value="-1" /><fmt:message key="label_knowledge_approvalRefuse" />
		</td>
	</tr>
	<tr>
		<td><fmt:message key="common.remark" /></td>
		<td><textarea class="form-control" style="height:100px" name="knowledgeDto.statusDesc"></textarea> </td>
	</tr>
	
	<tr>
		<td colspan="2">
		<input type="hidden" value="app" name="knowledgeDto.knowledgeAppDescRole"/>
		<button id="knowledge_ap_submit" type="button" class="btn btn-primary btn-sm"><fmt:message key="common.submit" /></button>
		</td>
	</tr>
</table>
</div>
</form>
</div>
<!-- 审批知识 end -->

<!-- 重新提交审批知识 start -->
<div id="knowledge_reopenApp_window" class="WSTUO-dialog" title='<fmt:message key="label.knowledgeMain.reopenApp" />' style="width: 350px; height: auto">
<form>
<div class="lineTableBgDiv">
<table style="width: 100%" class="lineTable" cellspacing="1">
	<tr>
		<td><fmt:message key="common.remark" /></td>
		<td><textarea class="form-control" style="height:100px" name="knowledgeDto.statusDesc"></textarea> </td>
	</tr>
	
	<tr>
		<td colspan="2">
		<input type="hidden" value="creator" name="knowledgeDto.knowledgeAppDescRole"/>
		<input type="hidden" value="0" name="knowledgeDto.knowledgeStatus"/>
		<button id="knowledge_reopenApp_submit" type="button" class="btn btn-primary btn-sm"><fmt:message key="common.submit" /></button>
		</td>
	</tr>
</table>
</div>
</form>
</div>
<!-- 重新提交审批知识 end -->
<!-- 审批结果start -->
<div id="knowledge_show_app_window" class="WSTUO-dialog" title='<fmt:message key="label_knowledge_appResult" />' style="width: 350px;">
<div class="lineTableBgDiv">
<table style="width: 100%" class="lineTable" cellspacing="1">
	<tr>
		<td>
			<div style="line-height:25px;display:block;word-break: break-all;word-wrap: break-word;" id="kw_app_detail" ></div>
		</td>
	</tr>
	<tr>
		<td colspan="2">
		<button type="button" class="btn btn-primary btn-sm" onclick="$('#knowledge_show_app_window').dialog('close');"><fmt:message key="label.determine" /></button>
		</td>
	</tr>
</table>
</div>
</div>
<!-- 审批结果end -->
</div>
