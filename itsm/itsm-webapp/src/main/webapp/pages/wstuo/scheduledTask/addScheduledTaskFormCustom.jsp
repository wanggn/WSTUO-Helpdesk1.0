<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.opensymphony.xwork2.ActionContext" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="../../language.jsp" %>

<script src="${pageContext.request.contextPath}/js/wstuo/scheduledTask/addScheduledTaskFormCustom.js?random=<%=new java.util.Date().getTime()%>"></script>
<link id="addRequestFormCustom_is_scheduled" href="../styles/common/addScheduledTaskFormCustom.css"  rel="stylesheet" type="text/css"/> 
<link id="addRequestFormCustom_no_scheduled" href="../styles/common/addScheduledTaskFormCustom-no.css"  rel="stylesheet" type="text/css"/> 

<div fit="true" id="addScheduledTask_layout" style="display: none">
 <div class="row" >
	<div class="box-inner">
		<div class="box-header well" data-original-title="">
			<h2><i class="glyphicon glyphicon-list"></i>&nbsp;添加定期任务</h2>
			<div class="box-icon">
				<a href="#" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-chevron-up"></i></a>
			</div>
		</div>
        <div class="box-content ">
			<ul class="nav nav-tabs" id="orgMgeTab">
				<li class="active"><a href="#list1"><fmt:message key="common.basicInfo" /></a></li>
				<%-- <li><a href="#list2"><fmt:message key="ci.relatedCI" /></a></li> --%>
			    <li><a href="#list3"><fmt:message key="common.attachment" /></a></li>
			    <li><a href="#list4"><fmt:message key="title.scheduled.time.settings" /></a></li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane active" id="list1">
					<form id="addRequestScheduledTaskForm">
						<input type="hidden" name="scheduledTaskDTO.beanId" value="createRequestJob"  />
						<input type="hidden" name="scheduledTaskDTO.scheduledTaskType" value="request"  />
						<input type="hidden" id="addRequestScheduledTask_serviceDirName" />
						<input type="hidden" name="serviceDirNos" id="addRequestScheduledTask_serviceDirIds" >
						<input id="addRequestScheduledTask_formId" type="hidden" name ="requestDTO.formId" value="0" />
	                	<div id="addScheduledTaskRequest_formField" style="width: 90%;margin:0 auto;height: 500px;">
							
						</div>
					</form>
				</div>
				
				<%-- 关联配置项 Start--%>
				<%-- <div class="tab-pane" id="list2">
                	<c:if test="${cimHave eq true}">
						<div title="<fmt:message key="ci.relatedCI" />" style="padding: 2px;">
							<form>
								<div  id="scheduledTaskRelatedCIShow"  class="hisdiv" >
									<table class="histable" style="width:100%" cellspacing="1">
										<thead>
											<tr>
												<td colspan="5" style="text-align:left" class="panelBar">
													<a id="add_scheduledTask__ref_ci_btn" class="btn btn-default btn-xs" plain="true" ><i class="glyphicon glyphicon-plus"></i>&nbsp;<fmt:message key="common.add" /></a>
												</td>
											</tr>
											<tr height="20px">
												<th align="center"><fmt:message key="lable.ci.assetNo" /></th>
												<th align="center"><fmt:message key="label.name"/></th>
												<th align="center"><fmt:message key="common.category" /></th>
												<th align="center"><fmt:message key="common.state" /></th>
												<th align="center"><fmt:message key="ci.operateItems" /></th>
											</tr>
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</form>
						</div>				
					</c:if>
				</div> --%>
				<%-- 关联配置项 End--%>
				<%-- 附件Start--%>
				<div class="tab-pane" id="list3">
					<form>
						<div id="add_scheduledTask_attachment_div" >
							<input type="hidden" name="scheduledTaskDTO.requestDTO.attachmentStr" id="add_scheduledTask_attachmentStr"/>
							<table style="width:100%;margin-top: 5px;" cellspacing="1" >
								<tr>
									<td> 
									<div style="color:#ff0000;line-height:28px"><fmt:message key="msg.attachment.maxsize" /></div>
										<input type="file"  name="filedata" id="add_scheduledTask_file" >
										<div style="height:5px"></div>
										<a class="btn btn-primary btn-sm" href="javascript:if($('#add_scheduledTask_fileQueue').html()!='')$('#add_scheduledTask_file').uploadifyUpload();else msgShow(i18n['msg_add_attachments'],'show');" ><fmt:message key="label.attachment.upload" /></a>
									</td>
								</tr>
								<tr>
									<td>
										<div style="padding-top:8px">
							              <div id="add_scheduledTask_success_attachment" style="line-height:25px;color:#555"></div>
							            </div>
									</td>
								</tr>
							</table>
						</div>
					</form>
                </div>
                <%-- 附件 End--%>
                <%-- 时间变设定 Start--%>
                <div class="tab-pane" id="list4">
					<div id="addScheduledTimeSettings" style="width: 600px;">
						<form>													
							<table style="width:100%" >
								<tr style="height: 30px;">
									<td>
										<input type="radio" name="scheduledTaskDTO.timeType" value="day" onclick="wstuo.scheduledTask.scheduledTask.everyWhatChange(this.value,'add_')"  checked="checked">
										<fmt:message key="title.scheduled.day.plan" />
									</td>	
									<td>
										<input type="radio" name="scheduledTaskDTO.timeType" value="weekly" onclick="wstuo.scheduledTask.scheduledTask.everyWhatChange(this.value,'add_')">
										<fmt:message key="title.scheduled.week.plan" />&nbsp;
									</td>	
									<td>
										<input type="radio" name="scheduledTaskDTO.timeType" value="month" onclick="wstuo.scheduledTask.scheduledTask.everyWhatChange(this.value,'add_')">
										<fmt:message key="title.scheduled.month.plan" />&nbsp;
									</td>												
								</tr>
								<tr style="height: 30px;">
									<td>
										<%-- 周期性计划(天) --%>
										<input type="radio" name="scheduledTaskDTO.timeType" value="cycle" onclick="wstuo.scheduledTask.scheduledTask.everyWhatChange(this.value,'add_')">
										<fmt:message key="title.scheduled.cycle.plan" />(<fmt:message key="label.sla.day" />)											
									</td>
									<td>												
										<%-- 周期性计划(分钟) --%>
										<input type="radio" name="scheduledTaskDTO.timeType" value="cycleMinute" onclick="wstuo.scheduledTask.scheduledTask.everyWhatChange(this.value,'add_')">
										<fmt:message key="title.scheduled.cycle.plan" />(<fmt:message key="label.sla.minute" />)&nbsp;
										</td>	
									<td>
										<input type="radio" name="scheduledTaskDTO.timeType" value="on_off" onclick="wstuo.scheduledTask.scheduledTask.everyWhatChange(this.value,'add_')">
										<fmt:message key="title.scheduled.on-off.plan" />												
									</td>
								</tr>
								<tr>
									<td colspan="3">
										<b>[<span id="add_everyWhat_show" style="margin-top: 5px;"><fmt:message key="label.scheduledTask.day" /></span>]</b><br>
										
										<div id="add_everyWhat_weekly" style="display: none;" class="lineTableBgDiv">
											<!--每周定时维护： -->
											<table style="width:100%" class="lineTableBgDiv" class="lineTable" cellspacing="1">
												<tr><td colspan="4"><input type="checkbox" id="add_scheduledTask_weekWeeks_all" value="1" onclick="wstuo.scheduledTask.scheduledTask.checkAll('add_scheduledTask_weekWeeks_all','add_everyWhat_weekly','scheduledTaskDTO.weekWeeks')"/>
												<fmt:message key="label.date.weekly" />：</td></tr>
												<tr>
													<td><input type="checkbox" value="SUN" name="scheduledTaskDTO.weekWeeks" /><fmt:message key="label.orgSettings.sunday" /></td>
													<td><input type="checkbox" value="MON" name="scheduledTaskDTO.weekWeeks" /><fmt:message key="label.orgSettings.monday" /></td>
													<td><input type="checkbox" value="TUE" name="scheduledTaskDTO.weekWeeks" /><fmt:message key="label.orgSettings.tuesday" /></td>
													<td><input type="checkbox" value="WED" name="scheduledTaskDTO.weekWeeks" /><fmt:message key="label.orgSettings.wednesday" /></td>
												</tr>
												<tr>
													<td><input type="checkbox" value="THU" name="scheduledTaskDTO.weekWeeks" /><fmt:message key="label.orgSettings.thursday" /></td>
													<td><input type="checkbox" value="FRI" name="scheduledTaskDTO.weekWeeks" /><fmt:message key="label.orgSettings.friday" /></td>
													<td><input type="checkbox" value="SAT" name="scheduledTaskDTO.weekWeeks" /><fmt:message key="label.orgSettings.saturday" /></td>
													<td></td>
												</tr>
											</table>
										</div>
										
										<div id="add_everyWhat_monthly" style="display: none;"  class="lineTableBgDiv">
											<!-- 每月定时维护：  -->
											
											<table style="width:100%" class="lineTableBgDiv" class="lineTable" cellspacing="1">
												<tr><td colspan="4"><input type="checkbox" id="add_scheduledTask_monthMonths_all" value="1" onclick="wstuo.scheduledTask.scheduledTask.checkAll('add_scheduledTask_monthMonths_all','add_everyWhat_monthly','scheduledTaskDTO.monthMonths')"/><fmt:message key="label.date.month" />：</td></tr>
												<tr>
													<td><input type="checkbox" value="JAN" name="scheduledTaskDTO.monthMonths" /><fmt:message key="label.date.Jan" /></td>
													<td><input type="checkbox" value="FEB" name="scheduledTaskDTO.monthMonths" /><fmt:message key="label.date.Feb" /></td>
													<td><input type="checkbox" value="MAR" name="scheduledTaskDTO.monthMonths" /><fmt:message key="label.date.Mar" /></td>
													<td><input type="checkbox" value="APR" name="scheduledTaskDTO.monthMonths" /><fmt:message key="label.date.Apr" /></td>
												</tr>
												<tr>
													<td><input type="checkbox" value="MAY" name="scheduledTaskDTO.monthMonths" /><fmt:message key="label.date.May" /></td>
													<td><input type="checkbox" value="JUN" name="scheduledTaskDTO.monthMonths" /><fmt:message key="label.date.Jun" /></td>
													<td><input type="checkbox" value="JUL" name="scheduledTaskDTO.monthMonths" /><fmt:message key="label.date.Jul" /></td>
													<td><input type="checkbox" value="AUG" name="scheduledTaskDTO.monthMonths" /><fmt:message key="label.date.Aug" /></td>
												</tr>
												<tr>
													<td><input type="checkbox" value="SEP" name="scheduledTaskDTO.monthMonths" /><fmt:message key="label.date.Sep" /></td>
													<td><input type="checkbox" value="OCT" name="scheduledTaskDTO.monthMonths" /><fmt:message key="label.date.Oct" /></td>
													<td><input type="checkbox" value="NOV" name="scheduledTaskDTO.monthMonths" /><fmt:message key="label.date.Nov" /></td>
													<td><input type="checkbox" value="DEC" name="scheduledTaskDTO.monthMonths" /><fmt:message key="label.date.Dec" /></td>
												</tr>
												<tr>
													<td colspan="4">
														<h5 style="float: left;color: black;"><fmt:message key="common.date"></fmt:message>：</h5>
														<span  style="float: left;margin-left: 15px;">
														<select id="monthDay_select" name="scheduledTaskDTO.monthDay" class="form-control" style="width: 170px;">
														</select>
														</span>
													</td>
												</tr>
												
											</table>

										</div>
										
										<div id="add_everyWhat_cycle" style="display: none;">
											<!-- 周期性地维护： ：  -->
											<hr>
											<h5 style="float: left;color: black;"><fmt:message key="label.scheduledTask.every"/></h5>													
											<span  style="float: left;margin-left: 15px;">
											<input type="text" class="form-control" value="1" value="1" style="width:50px;" name="scheduledTaskDTO.cyclicalDay">
											</span>
											<h5 style="float: left; margin-left: 5px;color: black;"><fmt:message key="label.scheduledTask.day.one"/></h5>														
										</div>
										
										<%-- 周期性执行(分钟) --%>
										<div id="add_everyWhat_cycleMinute" style="display: none;">												
											<hr>
											<h5 style="float: left;color: black;"><fmt:message key="label.scheduledTask.every"/></h5>
											<span  style="float: left;margin-left: 15px;">
											<input type="text" style="width:50px;" class="form-control" value="1" id="add_scheduledTask_cyclicalMinute" name="scheduledTaskDTO.cyclicalMinute"/>
											</span>
											<h5 style="float: left; margin-left: 5px;color: black;"><fmt:message key="label.sla.minute" /></h5>													
										</div>
										
									</td>
								</tr>
								<tr>
									<td colspan="3">								
										<div id="add_scheduledTask_taskDate" style="width: 100%;margin-top: 10px;">												
										<table cellspacing="1" class="table table-bordered">
											<tr>
												<td><span id="add_scheduledTask_startTime" ><fmt:message key="label.sla.slaStartTime" /></span></td>
												<td><input type="text" id="add_scheduledTask_startTime_input" name="scheduledTaskDTO.taskDate" style="width:170px;" class="form-control" readonly="readonly"></td>
											</tr>	
											<tr >
												<td><span id="add_scheduledTask_endTime"><fmt:message key="label.sla.slaEndTime" /></span></td>
												<td><input type="text" name="scheduledTaskDTO.taskEndDate" id="add_scheduledTask_endTime_input" style="width:170px;" class="form-control" validType="DateComparison['add_scheduledTask_startTime_input']" readonly="readonly"/></td>										
											</tr>
											<tr>
												<td><fmt:message key="label.scheduledTask.specific.time" /></td>
												<td>
													<span style="float: left;">
													<select name="scheduledTaskDTO.taskHour" id="timeType_hours" class="form-control" style="width: 100px;">
														<c:forEach var="i" begin="0" end="23" step="1"> 
															<option value="${i}">${i}</option>
														</c:forEach>
													</select>
													</span>
													<h5 style="float: left; margin-left: 5px;">
													<fmt:message key="label.orgSettings.hour" />
													</h5>
													<!-- 分 -->
													<span style="float: left;margin-left: 5px;">
													<select name="scheduledTaskDTO.taskMinute" class="form-control" style="width: 100px;">
														<c:forEach var="i" begin="0" end="59" step="1"> 
															<option value="${i}">${i}</option>
														</c:forEach>
													</select>
													</span>
													<h5 style="float: left; margin-left: 5px;">
														<fmt:message key="label.orgSettings.minute" />
													</h5>	
												 </td>										
											</tr>
										</table>												
										</div>
									</td>
								</tr>
							</table>								
					</form>
					</div>
                </div>
                <%-- 时间变设定 End--%>
			</div>
		</div>
		<hr>
		<div style="height: 30px;text-align: center;" >
			<a id="saveScheduledTaskBtn" class="btn btn-primary btn-sm" plain="true" ><fmt:message key="common.save" /></a>
			<a id="add_scheduledTask_backList" class="btn btn-primary btn-sm" plain="true"  style="margin-left:15px" ><fmt:message key="common.returnToList" /></a>
			
			<%-- <a id="addRequestScheduledTask_ServicesCatalogNavigation" >
			<fmt:message key="request.servicesCatalogNavigation" /> </a> <span id="addRequestScheduledTask_servicesCatalogNavigationName"></span>
		 --%>
		</div>
		<br>
	</div>
 </div>
 </div>
