<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ include file="../../language.jsp" %>
<script src="../js/basics/timeZone.js"></script>
<script src="../js/wstuo/sysMge/timeZoneMain.js?random=<%=new java.util.Date().getTime()%>"></script>

<!-- <div class="loading" id="timeZoneMain_loading"><img src="../images/icons/loading.gif" /></div> -->

<!-- 主面板开始-->
<div class="content" id="timeZoneMain_content">
	<div align="center" >
	<div class="box-header well" data-original-title="">
          <h2><i class="glyphicon glyphicon-list"></i>&nbsp;<fmt:message key="title.timeZone.setting" /></h2>
          <div class="box-icon">
            
          </div>
     </div>
		<form id="timeZone_form">
		<input type="hidden" name="timeZoneDTO.tid" id="timeZoneId" />
		<table style="width:100%"  cellspacing="1"  class="table table-bordered">
			<tr style="height: 30px;">
				<td style="text-align: right;width: 40%;"  class="center"><h5 style="color: black;"><fmt:message key="title.center.TimeZone" />：</h5></td>
				<td align="left"  >
					<select id="centerTimeZone" name="timeZoneDTO.centerTimeZone"class="form-control" style="width: 50%" >
					</select>
				</td>
			</tr>
			<tr style="height: 30px;">
				<td style="text-align: right;width: 40%;"  class="center"><h5 style="color: black;"><fmt:message key="titie.enduser.TimeZone" />：</h5></td>
				<td align="left"  >
					<select id="enduserTimeZone" name="timeZoneDTO.enduserTimeZone" class="form-control" style="width: 50%">
					</select>
				</td>
			</tr>			
			<tr style="height: 30px;">
				<td align="center" colspan="2"  >
					
					<input type="button" id="timeZone_linkBut" class="btn btn-primary btn-sm" value="<fmt:message key="common.save"/>"/>	
				</td>
			</tr>
		</table>
		</form>
	</div>
</div>