<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ include file="../../language.jsp" %>
<script src="../js/wstuo/onlinelog/userOptLog.js?random=<%=new java.util.Date().getTime()%>"></script>
<sec:authorize url="/pages/useroptlog!findPager.action">

<div id="UserOptLog_content" class="content">


<!--row start  -->
		<div class="row">
			<div class="box col-md-12">
				<div class="box-inner">
					<div class="box-header well" data-original-title="">
						<h2>
							<i class="glyphicon glyphicon-list-alt"></i> 用户操作日志列表
						</h2>
						<div class="box-icon">
							<!-- <a href="#" class="btn btn-minimize btn-round btn-default"><i
								class="glyphicon glyphicon-chevron-up"></i></a> -->
						</div>
					</div>
					
					<!-- box-content -->
					<div class="box-content ">
						<table id="userOptGrid" class="scroll"></table>
						<div id="userOptGridPager" class="scroll" style="text-align: center;"></div>
						<div id="userOptGridToolbar" style="display:none">
						    <button type="button" class="btn btn-default btn-xs" id="userOptGrid_download" >
						       <i class="glyphicon glyphicon-download-alt"></i><fmt:message key="label.problem.attachmentDownload"/>
						    </button>
						</div> 
						
					</div>
					<!-- box-content -->
				</div>
			</div>
		</div>

<!--row end  -->
 <!-- 批量下载压缩包-->
		<div id="zipUserOptLog" title="<fmt:message key="batch_download"/>"  class="WSTUO-dialog" style="width:380px;height:auto">
			<form>
			<div class="lineTableBgDiv">
				<table style="width:100%" class="lineTable" cellspacing="1">
						<tr>
					    	<td>
					    	  <fmt:message key="label.customReport.date"/>
					    	</td>
					    	<td>
					    	  <input id="optZipStartTime" name="startTime" class="form-control" readonly>
					    	</td>
					    	<td width="20px">
					    	   <fmt:message key="setting.label.to"/>
					    	</td>
					    	<td>
									<input id="optZipEndTime" name="zipEndTime" class="form-control" validType="DateComparison['optZipStartTime']"  readonly>
							    	
					    	</td>
				    	</tr>
				    	
				    	<tr>
				    		<td colspan="4">
				        		<button type="button" class="btn btn-primary btn-sm" id="userOptGrid_downloadzip" ><fmt:message key="label.problem.attachmentDownload"/></button>
				        		<button type="button" class="btn btn-default btn-sm"  onclick="cleanIdValue('optZipStartTime','optZipEndTime')" ><fmt:message key="label.request.clear"/></button> 
				        	</td>
				    	</tr>
				</table>
			</div>
			</form>
		</div>
      <!-- 批量下载压缩 包-->
	</div>
</sec:authorize>