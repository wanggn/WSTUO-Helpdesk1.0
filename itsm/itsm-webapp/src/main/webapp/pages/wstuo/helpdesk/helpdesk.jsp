<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="../../language.jsp" %>
<div>
    <ul class="breadcrumb">
        <li>
            <a href="#">主页</a>
        </li>
        <li>
            我的面板
        </li>
    </ul>
</div>
<!-- <div class=" row">
    <div class="col-md-3 col-sm-3 col-xs-6">
        <a data-toggle="tooltip" title="6 new members." class="well top-block" href="#">
            <i class="glyphicon glyphicon-user blue"></i>
            <div>Total Members</div>
            <div>507</div>
            <span class="notification">6</span>
        </a>
    </div>

    <div class="col-md-3 col-sm-3 col-xs-6">
        <a data-toggle="tooltip" title="4 new pro members." class="well top-block" href="#">
            <i class="glyphicon glyphicon-star green"></i>

            <div>Pro Members</div>
            <div>228</div>
            <span class="notification green">4</span>
        </a>
    </div>

    <div class="col-md-3 col-sm-3 col-xs-6">
        <a data-toggle="tooltip" title="$34 new sales." class="well top-block" href="#">
            <i class="glyphicon glyphicon-shopping-cart yellow"></i>

            <div>Sales</div>
            <div>$13320</div>
            <span class="notification yellow">$34</span>
        </a>
    </div>

    <div class="col-md-3 col-sm-3 col-xs-6">
        <a data-toggle="tooltip" title="12 new messages." class="well top-block" href="#">
            <i class="glyphicon glyphicon-envelope red"></i>

            <div>Messages</div>
            <div>25</div>
            <span class="notification red">12</span>
        </a>
    </div>
</div> -->

<div class="row">
	<c:forEach items="${userCustomDTO.viewdtos}" var="data" varStatus="vs">
		<input type="hidden" id="divid_${data.viewId}" value="${data.viewName}">
		<div class="box col-md-4" id="box_${data.viewId}">
	        <div class="box-inner">
	            <div class="box-header well" data-original-title="">
	                <h2><i class="glyphicon glyphicon-list"></i> ${data.viewName}</h2>
					<div class="box-icon">
	                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
	                            class="glyphicon glyphicon-chevron-up"></i></a>
	                    <a href="#" class="btn btn-close btn-round btn-default"><i
	                            class="glyphicon glyphicon-remove"></i></a>
	                </div>
	            </div>
	            <div class="box-content" id="protlet_content_${data.viewId}">
	            	<table id="TAB_ID_${data.viewId}" class="" style="width:100%" cellspacing="1"></table>
	            	<div id="${data.viewId}_0" ></div>
	            </div>
	        </div>
	    </div>
	    <c:if test="${vs.count %3==0 }"></div><div class="row"></c:if>
	</c:forEach>
</div><!--/row-->

<!--查看公告消息 -->
	<div id="afficheDiv" class="WSTUO-dialog" title="<fmt:message key="common.detailInfo"/>" style="max-height:400px;width: 420px; height:auto; padding: 3px; line-height: 22px; ">
		<form id="afficheForm">
		<div class="lineTableBgDiv">
		<table style="width: 100%" class="lineTable" cellspacing="1">
			<tr>
				<td style="width:120px"><fmt:message key="label.title"/></td>
				<td >
					<span id="portal_affTitle" style="display:block;width:99%;" class=".wordbreak"></span>
				</td>
			</tr>

			<tr>
				<td><fmt:message key="label.orgSettings.startTime"/></td>
				<td>
					<span id="portal_affStart"></span>
				</td>
			</tr>
			
			<tr>
				<td><fmt:message key="label.orgSettings.stopTime"/></td>
				<td>
					<span id="portal_affEnd"></span>
				</td>
			</tr>
			
			<tr>
				<td><fmt:message key="common.cerateTime"/></td>
				<td>
					<span id="portal_cerateTime"></span>
				</td>
			</tr>
			
			<tr>
				<td class="minw100"><fmt:message key="tool.affiche.content"/></td>
				<td>
					<span id="portal_affContents" style="display:block;width:99%;"  class=".wordbreak"></span>
				</td>
			</tr>
		</table>
		</div>
		</form>
	</div>
	<!--查看IM消息 -->
	<div id="lookMessageDiv" class="WSTUO-dialog" title="<fmt:message key="common.detailInfo"/>" style="max-height:400px;width: 400px; height:auto; padding: 3px; line-height: 22px; ">
		<form id="lookMessageForm">
	 	<div  class="lineTableBgDiv">
			<table style="width:100%" class="lineTable" cellspacing="1" >
				<tr>
					<td style="width:30%"><fmt:message key="tool.im.sendUser"/></td>
					<td style="width:70%"><span id="im_sendUser"></span></td>
				</tr>  
				<tr>
					<td><fmt:message key="tool.im.sendTime"/></td>
					<td><span id="im_sendTime"></span></td>
				</tr>
				<tr>
					<td><fmt:message key="tool.im.imTitle"/></td>
					<td><span id="im_title"></span></td>
				</tr>  
				 
				<tr>
					<td><fmt:message key="common.state"/></td>
					<td>
						<span id="im_stats"></span>
					</td>
				</tr>  
				
				<tr>
					<td><fmt:message key="tool.im.imContent"/></td>
					<td><span id="im_content"></span></td>
				</tr>
				
			 
			</table>
		</div>
		</form>
	</div>
	<script src="../bower_components/flot/excanvas.min.js"></script>
	<script src="../bower_components/flot/jquery.flot.js"></script>
	<script src="../bower_components/flot/jquery.flot.pie.js"></script>
	<script src="../bower_components/flot/jquery.flot.stack.js"></script>
	<script src="../bower_components/flot/jquery.flot.resize.js"></script>
	<script src="../js/basics/init-chart.js" type="text/javascript"></script>
	<script type="text/javascript" src="../js/wstuo/helpdesk/dashboardDataLoad.js"/></script>
	<script type="text/javascript" src="../js/wstuo/helpdesk/helpdeskPortal.js"/></script>
	
<script>
wstuo.helpdesk.dashboardDataLoad.loadDashboardData();
</script>