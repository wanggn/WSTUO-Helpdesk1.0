<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
response.setHeader("Cache-Control","no-store");
response.setHeader("Pragrma","no-cache");
response.setDateHeader("Expires",0);
//获取当前浏览器语言做为默认的语言版本start
String lang_browserLanguage = (String)session.getAttribute("browserLanguage");
if(lang_browserLanguage==null){
	request.setAttribute("lang_defaultLanguage","zh_CN");
}else{
	request.setAttribute("lang_defaultLanguage",lang_browserLanguage);
}
//获取当前浏览器语言做为默认的语言版本end
%> 
<fmt:setLocale value="${empty cookie['language'].value?lang_defaultLanguage:cookie['language'].value}"/>
<fmt:setBundle basename="i18n.itsmbest"/>
<input type="hidden" id="reportLang" value="${empty cookie['language'].value?lang_defaultLanguage:cookie['language'].value}">
<c:set var="lang" scope="session" value="${empty cookie['language'].value?lang_defaultLanguage:cookie['language'].value}" />