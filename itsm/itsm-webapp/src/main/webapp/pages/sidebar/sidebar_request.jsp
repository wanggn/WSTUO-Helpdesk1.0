<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../language.jsp" %>
<li class="nav-header">请求管理导航</li>
<sec:authorize url="AllRequest_Res">
<li><a class="link" onClick="javascript:itsm.request.requestStats.requestCountSearch('all',this);" href="#"><span> <fmt:message key="title.request.allRequest" /><span style="color: #F00" id="allRequest"></span></span></a></li>
</sec:authorize>
<sec:authorize url="MYREQUEST_RES">
<li><a class="link" onClick="javascript:itsm.request.requestStats.requestCountSearch('myProposedRequest',this)" href="#"><span> <fmt:message key="title.request.myRequest" /><span style="color: #F00" id="myProposedRequest"></span></span></a></li>
</sec:authorize>
<sec:authorize url="request_menu_myGroupReferRequest">
<li><a class="link" onClick="javascript:itsm.request.requestStats.requestCountSearch('myGroupProposedRequest',this)" href="#"><span> <fmt:message key="title.request.myGroupProposedRequest" /><span style="color: #F00" id="myGroupProposedRequest"></span></span></a></li>
</sec:authorize>
<sec:authorize url="REQUEST_MENU_MYUNTREADREQUEST">
<li><a class="link" onClick="javascript:itsm.request.requestStats.requestCountSearch('myNotComprehensiveNotSubmitted',this)" href="#"><span> <fmt:message key="label.MyNotComprehensiveNotSubmitted" /><span style="color: #F00" id="myNotComprehensiveNotSubmitted"></span></span></a></li>
</sec:authorize>
<sec:authorize url="MyOwRquest_Res">
<li><a class="link" onClick="javascript:itsm.request.requestStats.requestCountSearch('myOwnerRequest',this)" href="#"><span> <fmt:message key="title.request.myChargeRequest" /><span style="color: #F00" id="myOwnerRequest"></span></span></a></li>
</sec:authorize>
<sec:authorize url="MyPeRquest_Res">
<li><a class="link" onClick="javascript:itsm.request.requestStats.requestCountSearch('assigneeToMyRequest',this)" href="#"><span> <fmt:message key="title.request.assignToMeRequest" /><span style="color: #F00" id="assigneeToMyRequest"></span></span></a></li>
</sec:authorize>
<sec:authorize url="MyGrRquest_Res">
<li><a class="link" onClick="javascript:itsm.request.requestStats.requestCountSearch('assigneeGroupRequest',this)" href="#"><span> <fmt:message key="title.request.myGroupRequest" /><span style="color: #F00" id="assigneeGroupRequest"></span></span></a></li>
</sec:authorize>
<%-- <sec:authorize url="Request_Depute2Me_Res">
<li><a class="link" onClick="javascript:itsm.request.requestStats.requestCountSearch('actingToMyRequest',this)" href="#"><span> <fmt:message key="lable.proxy.Request" /><span style="color: #F00" id="actingToMyRequest"></span></span></a></li>
</sec:authorize> --%>