<%@page import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@page import="org.springframework.context.ApplicationContext"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>${ciDetailDTO.ciname }详细信息:</title>
<style type="text/css">
body,td,span,div,input,textarea,li,select,option,p,h1,h2,h3,h4,h5,h6,ul,ol{
	font-family: arial, helvetica, sans-serif;
    font-size:12px;
}
.title{
	background-color:#e4edfe;
	font-weight:bold;
	height: 20px;
	padding: 3px;
}
</style>

	
</head>
<body>
 <div class="title"><a name="基本信息">基本信息</a>
 &nbsp;&nbsp;&nbsp;&nbsp;<a href="#其他信息">[其他信息]</a>
 &nbsp;&nbsp;<a href="#硬件信息">[硬件信息]</a>
 &nbsp;&nbsp;<a href="#附件信息">[附件信息]</a></div>
 <input type="hidden" id="ciInfo_CiNos" value="${ciDetailDTO.ciId}">
 
 <table>
 	<tr>
 		<td>所属客户:</td>
 		<td>${ciDetailDTO.companyName }</td>
 		
 	</tr>
	<tr>
		<td>配置项分类:</td>
 		<td>${ciDetailDTO.categoryName }</td>
	</tr>
 	<tr>
 		<td>固定资产编号:</td>
 		<td>${ciDetailDTO.cino}</td>
 	</tr>
	<tr>
 		<td>资产名称:</td>
 		<td>${ciDetailDTO.ciname}</td>
	</tr>
 	<tr>
 		<td>所属项目:</td>
 		<td>${ciDetailDTO.systemPlatform}</td>
 	</tr>
<tr>
 		<td>产品型号:</td>
 		<td>${ciDetailDTO.model}</td>
 	</tr>

 	<tr>
 		<td>序列号:</td>
 		<td>${ciDetailDTO.serialNumber}</td>
 	</tr>
	<tr>
 		<td>条形码:</td>
 		<td>${ciDetailDTO.barcode}</td>
 	</tr>
 	<tr>
 		<td>状态:</td>
 		<td>${ciDetailDTO.status}</td>
 	</tr>
 	<tr>
  		<td>品牌:</td>
 		<td>${ciDetailDTO.brandName}</td>
 	</tr>
 	<tr>
 		<td>产品提供商:</td>
 		<td>${ciDetailDTO.providerName}</td>
 	</tr>
	<tr>
 		<td>位置:</td>
 		<td>${ciDetailDTO.loc}</td>
 	</tr>
 	<tr>
 		<td>采购日期:</td>
 		<td><fmt:formatDate type="date" value="${ciDetailDTO.buyDate}"  pattern="yyyy-MM-dd" /></td>
 	</tr>
	<tr>
 		<td>到货日期:</td>
 		<td><fmt:formatDate type="date" value="${ciDetailDTO.arrivalDate}"  pattern="yyyy-MM-dd" /></td>
 	</tr>
 	<tr>
 		<td>预警日期:</td>
 		<td><fmt:formatDate type="date" value="${ciDetailDTO.warningDate}"  pattern="yyyy-MM-dd" /></td>
 	</tr>
 	<tr>
 		<td>采购单号:</td>
 		<td>${ciDetailDTO.poNo}</td>
 	</tr> 	
 	<tr>
 		<td>资产原值:</td>
 		<td>${ciDetailDTO.assetsOriginalValue}</td>
 				
 	</tr>
 	<tr>
 		<td>与财务对应:</td>
 		<td>
 			<c:if test="${ciDetailDTO.financeCorrespond==true}">
				是
			</c:if>
			<c:if test="${ciDetailDTO.financeCorrespond==false}">
			           否
			</c:if>
		</td>
 	</tr>
 	<tr>
 		<td>折旧率为0:</td>
 		<td>${ciDetailDTO.depreciationIsZeroYears}年</td>
 	</tr>
 	<tr>
 		<td>部门:</td>
 		<td>${ciDetailDTO.department}</td>
 	</tr>
 	
 	<tr>
 		<td>所属业务实体:</td>
 		<td>${ciDetailDTO.workNumber}</td>
 	</tr>
 	<tr>
 		<td>项目:</td>
 		<td>${ciDetailDTO.project}</td>
 	</tr>
 	<tr>
 		<td>来源单位:</td>
 		<td>${ciDetailDTO.sourceUnits}</td>
 	</tr>
 	<tr>
 		<td>生命周期（月）:</td>
 		<td>${ciDetailDTO.lifeCycle}</td>
 	</tr>
 	<tr>
 		<td>保修期（月）:</td>
 		<td>${ciDetailDTO.warranty}</td>
 	</tr>
 	<tr>
 		<td>报废时间:</td>
 		<td><fmt:formatDate type="date" value="${ciDetailDTO.wasteTime}"  pattern="yyyy-MM-dd" /></td>
 	</tr>
 	<tr>
 		<td>借出时间:</td>
 		<td><fmt:formatDate type="date" value="${ciDetailDTO.borrowedTime}"  pattern="yyyy-MM-dd" /></td>
 	</tr>
 	<tr>
 		<td>预计回收时间:</td>
 		<td><fmt:formatDate type="date" value="${ciDetailDTO.expectedRecoverTime}"  pattern="yyyy-MM-dd" /></td>
 	</tr>
 	<tr>
 		<td>回收时间:</td>
 		<td><fmt:formatDate type="date" value="${ciDetailDTO.recoverTime}"  pattern="yyyy-MM-dd" /></td>
 	</tr>
 	<tr>
 		<td>使用权限 :</td>
 		<td>${ciDetailDTO.usePermissions}</td>
 	</tr>
 	<tr>
 		<td>原使用者:</td>
 		<td>${ciDetailDTO.originalUser}</td>
 	</tr>
 	<tr>
 		<td>使用人:</td>
 		<td>${ciDetailDTO.userName}</td>
 	</tr>
 	<tr>
 		<td>拥有者:</td>
 		<td>${ciDetailDTO.owner}</td>
 	</tr>
 	<tr>
 		<td>备注:</td>
 		<td>${ciDetailDTO.CDI}</td>
 	</tr>
 </table>
 
 
 
 <div class="title"><a name="其他信息">其他信息</a>
 &nbsp;&nbsp;&nbsp;&nbsp;<a href="#基本信息">[基本信息]</a>
 &nbsp;&nbsp;<a href="#硬件信息">[硬件信息]</a>
 &nbsp;&nbsp;<a href="#附件信息">[附件信息]</a></div>
  <table>
	<c:set var="oldCiAddGroupName"></c:set>
			<s:if test="attributeList!=null">
			<s:iterator value="attributeList" status="attrs">
			<c:if test="${attrGroupName != oldCiAddGroupName}">
			<thead>
				<tr>
					<th colspan="2" style="text-align:left;color: blue;"><b><s:property value="attrGroupName"/></b></th>
					<%--<th><fmt:message key="common.attributeName" /></th><th><fmt:message key="common.attributeValue" /></th> --%>
					
					<c:set var="oldCiAddGroupName" value="${attrGroupName}"></c:set>
				</tr>
			</thead>
			</c:if>
			
           			<tr>
			    <td style="text-align:left" ><s:property value="attrAsName"/>：</td>
			    <td style="text-align:left;word-break:break-all;">
			    
			    	${ciDetailDTO.attrVals[attrName]}
				</td>
		    </tr>
			</s:iterator>
			</s:if>
			<s:if test="attributeList==null">
				<tr><td style="color:red;font-size:16px">无其他信息</td></tr>
			</s:if>
 	</table>
 


 
  <div class="title"><a name="硬件信息">硬件信息</a>
  &nbsp;&nbsp;&nbsp;&nbsp;<a href="#基本信息">[基本信息]</a>
 &nbsp;&nbsp;<a href="#其他信息">[其他信息]</a>
 &nbsp;&nbsp;<a href="#附件信息">[附件信息]</a></div>
  <table>
	<tr>
	<td colspan="2">
		<fieldset>
			<legend>计算机系统</legend>
			<div style="width: 98%">
				<div style="display:inline-block;width: 48%">计算机名:${hardwareDTOs.computerSystem_name }</div>
				<div style="display:inline-block;width: 48%">计算机型号:${hardwareDTOs.computerSystem_model }</div>
			</div>
			<div style="width: 98%">
				<div style="display:inline-block;width: 48%">所在域:${hardwareDTOs.computerSystem_domain }</div>
				<div style="display:inline-block;width: 48%">用户名:${hardwareDTOs.computerSystem_userName }</div>
			</div>
		</fieldset>
		<fieldset>
			<legend>操作系统</legend>
			<div style="width: 98%">
				<div style="display:inline-block;width: 48%">序列号:${hardwareDTOs.operatingSystem_serialNumber}</div>
				<div style="display:inline-block;width: 48%">系统名称:${hardwareDTOs.operatingSystem_caption }</div>
			</div>
			<div style="width: 98%">
				<div style="display:inline-block;width: 48%">系统版本:${hardwareDTOs.operatingSystem_version }</div>
				<div style="display:inline-block;width: 48%">系统框架:${hardwareDTOs.operatingSystem_osArchitecture }</div>
			</div>
			<div style="width: 98%">
				<div style="display:inline-block;width: 48%">补丁版本:${hardwareDTOs.operatingSystem_csdVersion }</div>
				<div style="display:inline-block;width: 48%">安装时间:${hardwareDTOs.operatingSystem_installDate }</div>
			</div>
			<div style="width: 98%">
				<div style="display:inline-block;width: 48%">最后启动时间:${hardwareDTOs.operatingSystem_lastBootUpTime }</div>
				<div style="display:inline-block;width: 48%"></div>
			</div>
		</fieldset>
		<fieldset>
				<legend>内存</legend>
				<div style="width: 98%">
					<div style="display:inline-block;width: 48%">总的物理内存:${hardwareDTOs.operatingSystem_totalVisibleMemorySize }</div>
					<div style="display:inline-block;width: 48%">总的虚拟内存:${hardwareDTOs.operatingSystem_totalVirtualMemorySize }</div>
				</div>
		</fieldset>
		<fieldset>
				<legend>CPU</legend>
				<div style="width: 98%">
					<div style="display:inline-block;width: 48%">CPU名称:${hardwareDTOs.processor_name }</div>
					<div style="display:inline-block;width: 48%">CPU速度:${hardwareDTOs.processor_maxClockSpeed }</div>
				</div>
				<div style="width: 98%">
					<div style="display:inline-block;width: 48%">二级缓存:${hardwareDTOs.processor_l2CacheSize }</div>
					<div style="display:inline-block;width: 48%">三级缓存:${hardwareDTOs.processor_l3CacheSize }</div>
				</div>
				<div style="width: 98%">
					<div style="display:inline-block;width: 48%">CPU分级:${hardwareDTOs.processor_level }</div>
					<div style="display:inline-block;width: 48%"></div>
				</div>
		</fieldset>
		<fieldset>
				<legend>BIOS</legend>
				<div style="width: 98%">
					<div style="display:inline-block;width: 48%">序列号:${hardwareDTOs.bios_serialNumber }</div>
					<div style="display:inline-block;width: 48%">名称:${hardwareDTOs.bios_name }</div>
				</div>
				<div style="width: 98%">
					<div style="display:inline-block;width: 48%">制造商:${hardwareDTOs.bios_manufacturer }</div>
					<div style="display:inline-block;width: 48%">BIOS版本:${hardwareDTOs.bios_version }</div>
				</div>
				<div style="width: 98%">
					<div style="display:inline-block;width: 48%">BIOS发布时间:${hardwareDTOs.bios_releaseDate }</div>
					<div style="display:inline-block;width: 48%"></div>
				</div>
		</fieldset>
		<fieldset>
				<legend>主板</legend>
				<div style="width: 98%">
					<div style="display:inline-block;width: 48%">名称:${hardwareDTOs.baseBoard_name }</div>
					<div style="display:inline-block;width: 48%">序列号:${hardwareDTOs.baseBoard_serialNumber }</div>
				</div>
				<div style="width: 98%">
					<div style="display:inline-block;width: 48%">主板制造商:${hardwareDTOs.baseBoard_manufacturer }</div>
					<div style="display:inline-block;width: 48%"></div>
				</div>
		</fieldset>
		<fieldset>
				<legend>网卡信息</legend>
				<div style="width: 98%">
					<div style="display:inline-block;width: 48%">IP:${hardwareDTOs.netWork_ip }</div>
					<div style="display:inline-block;width: 48%">MAC:${hardwareDTOs.netWork_mac }</div>
				</div>
				<div style="width: 98%">
					<div style="display:inline-block;width: 48%">名称:${hardwareDTOs.netWork_name }</div>
					<div style="display:inline-block;width: 48%">DHCP:${hardwareDTOs.netWork_dhcp }</div>
				</div>
				<div style="width: 98%">
					<div style="display:inline-block;width: 48%">DHCP服务器:${hardwareDTOs.netWork_dhcpServer }</div>
					<div style="display:inline-block;width: 48%"></div>
				</div>
		</fieldset>
		<fieldset>
				<legend>监视器</legend>
				<div style="width: 98%">
					<div style="display:inline-block;width: 48%">名称:${hardwareDTOs.desktopMonitor_name }</div>
					<div style="display:inline-block;width: 48%">分辨率(高度):${hardwareDTOs.desktopMonitor_screenHeight }</div>
				</div>
				<div style="width: 98%">
					<div style="display:inline-block;width: 48%">分辨率(宽度):${hardwareDTOs.desktopMonitor_screenWidth }</div>
					<div style="display:inline-block;width: 48%">制造商:${hardwareDTOs.desktopMonitor_monitorManufacturer }</div>
				</div>
				<div style="width: 98%">
					<div style="display:inline-block;width: 48%">序列号:${hardwareDTOs.desktopMonitor_serialNumber }</div>
					<div style="display:inline-block;width: 48%"></div>
				</div>
		</fieldset>
		<fieldset>
				<legend>鼠标</legend>
				<div style="width: 98%">
					<div style="display:inline-block;width: 48%">名称:${hardwareDTOs.pointingDevice_Name }</div>
					<div style="display:inline-block;width: 48%">描述:${hardwareDTOs.pointingDevice_description }</div>
				</div>
				<div style="width: 98%">
					<div style="display:inline-block;width: 48%">序列号:${hardwareDTOs.pointingDevice_serialNumber }</div>
					<div style="display:inline-block;width: 48%">制造商:${hardwareDTOs.pointingDevice_manufacturer }</div>
				</div>
		</fieldset>
		<fieldset>
				<legend>键盘</legend>
				<div style="width: 98%">
					<div style="display:inline-block;width: 48%">名称:${hardwareDTOs.keyboard_Name }</div>
					<div style="display:inline-block;width: 48%">描述:${hardwareDTOs.pointingDevice_description }</div>
				</div>
				<div style="width: 98%">
					<div style="display:inline-block;width: 48%">功能键:${hardwareDTOs.keyboard_numberOfFunctionKeys }</div>
					<div style="display:inline-block;width: 48%">序列号:${hardwareDTOs.keyboard_serialNumber }</div>
				</div>
				<div style="width: 98%">
					<div style="display:inline-block;width: 48%">制造商:${hardwareDTOs.keyboard_manufacturer }</div>
					<div style="display:inline-block;width: 48%"></div>
				</div>
		</fieldset>
			<fieldset>
				<legend>物理内存</legend>
				<table>
					<tr><td>名称 </td><td>速度 </td><td>设备位置 </td><td>容量 </td></tr>
					<s:iterator value="listPhy" var="pv">
	     				<tr><td>${pv.pName } </td><td>${pv.pInfo } </td><td>${pv.pSystem } </td><td>${pv.pAvailable } </td></tr>
					 </s:iterator>
				</table>
				 
            			
            </fieldset>	
            <fieldset>
			<legend>逻辑磁盘</legend>
	 			<table>
					<tr><td>名称 </td><td>描述 </td><td>系统文件 </td><td>可用空间 </td><td>总大小</td></tr>
					<s:iterator value="listLogi" var="pv">
	     				<tr><td>${pv.pName } </td><td>${pv.pInfo } </td><td>${pv.pSystem } </td><td>${pv.pAvailable } </td><td>${pv.pSize } </td></tr>
					 </s:iterator>
				</table>
	 		</fieldset>		
	 		
              	<fieldset>
	 				<legend>IDE控制器</legend>
              		 <table>
						<tr><td>名称 </td><td>生产厂家 </td></tr>
						<s:iterator value="listIDE" var="pv">
	     				<tr><td>${pv.pName } </td><td>${pv.pInfo } </td></tr>
					 </s:iterator>
				    </table>
              	</fieldset>			
              	<fieldset>
	 				<legend>USB控制器</legend>
              			 <table>
							<tr><td>名称 </td></tr>
							<s:iterator value="listUSB" var="pv">
		     				<tr><td>${pv.pName } </td></tr>
					 		</s:iterator>
				   		</table>
              	</fieldset>	
              	<fieldset>
	 				<legend>USB Hub</legend>
              			 <table>
							<tr><td>名称 </td></tr>
							<s:iterator value="listUSBHub" var="pv">
		     				<tr><td>${pv.pName } </td></tr>
					 		</s:iterator>
				   		</table>
              	</fieldset>
              	<fieldset>
	 				<legend>端口(COM)</legend>
              			<table>
							<tr><td>名称 </td><td>供应商 </td></tr>
							<s:iterator value="listCOM" var="pv">
		     				<tr><td>${pv.pName } </td><td>${pv.pInfo } </td></tr>
					 		</s:iterator>
				   		</table>
              	</fieldset>	
              	<fieldset>
	 				<legend>端口(LPT)</legend>
              			<table>
							<tr><td>名称 </td><td>描述 </td></tr>
							<s:iterator value="listLPT" var="pv">
		     				<tr><td>${pv.pName } </td><td>${pv.pInfo } </td></tr>
					 		</s:iterator>
				   		</table>
              	</fieldset>	
              	<fieldset>
	 				<legend>光驱</legend>
              			<table>
							<tr><td>名称 </td><td>多媒体类型 </td></tr>
							<s:iterator value="listCD" var="pv">
		     				<tr><td>${pv.pName } </td><td>${pv.pInfo } </td></tr>
					 		</s:iterator>
				   		</table>
              	</fieldset>	
     	</td>
     	</tr>		
 </table>
 
 
 	  <div class="title"><a name="附件信息">附件信息</a>
  &nbsp;&nbsp;&nbsp;&nbsp;<a href="#基本信息">[基本信息]</a>
 &nbsp;&nbsp;<a href="#硬件信息">[硬件信息]</a>
 &nbsp;&nbsp;<a href="#其他信息">[其他信息]</a>
  </div>
	<table>
		<tr><td>编号  </td><td>附件名称  </td><td>附件URL </td><td>操作 </td></tr>
		<s:iterator value="attach" var="pv">
			<tr><td>${pv.aid } </td><td>${pv.attachmentName } </td><td>${pv.url } </td><td><a href=attachment!download.action?downloadAttachmentId=${pv.aid } target=_blank>下载</a> </td></tr>
	</s:iterator>
  	</table>


</body>
<%-- <script type="text/javascript">
$(function(){
	showCiInfo('show_configureItemInfo',$('#ciInfo_CiNos').val(),'itsm.configureItem',true);
	
}); --%>
	


