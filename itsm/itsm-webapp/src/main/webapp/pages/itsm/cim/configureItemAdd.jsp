<%@page import="java.util.Map"%>
<%@page import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@page import="com.wstuo.common.security.service.IUserInfoService"%>
<%@page import="org.springframework.web.context.WebApplicationContext"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ include file="../../language.jsp" %>
<%
    WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(session.getServletContext());
	IUserInfoService userInfoService = (IUserInfoService)ctx.getBean("userInfoService");
	if(session.getAttribute("loginUserName")!=null){
	 	Map<String,Object> resAuth=userInfoService.findResAuthByUserName(session.getAttribute("loginUserName").toString(),new String[]{
			  "CofnigureItem_AssetOriginalValue_Res",
			  "CofnigureItem_Barcode_Res",
			  "CofnigureItem_Supplier_Res",
			  "CofnigureItem_PurchaseDate_Res",
			  "CofnigureItem_ArrivalDate_Res",
			  "CofnigureItem_PurchaseNo_Res",
			  "CofnigureItem_Owner_Res",
			  "CofnigureItem_LifeCycle_Res",
			  "CofnigureItem_WarrantyDate_Res",
			  "CofnigureItem_WarningDate_Res",
			  "CofnigureItem_SerialNumber_Res",
			  "CofnigureItem_Department_Res",
			  "CofnigureItem_Fimapping_Res",
			  "CofnigureItem_ExpiryDate_Res",
			  "CofnigureItem_LendTime_Res",
			  "CofnigureItem_OriginalUser_Res",
			  "CofnigureItem_RecycleTime_Res",
			  "CofnigureItem_PlannedRecycleTime_Res",
			  "CofnigureItem_UsePermission_Res"
			  });
	 	request.setAttribute("resAuth",resAuth);
	}
 	
%>

<script type="text/javascript">
var pageType="${param.pageType}";
var editTemplateId="${param.templateId}";
var ci_add_eavAttributet = '<fmt:message key="config.extendedInfo"/>';
</script>

<link href="${pageContext.request.contextPath}/scripts/jquery/uploadify/uploadify.css"  rel="stylesheet" type="text/css"/>   
<script src="${pageContext.request.contextPath}/scripts/itsm/cim/configureItemAdd.js?random=<%=new java.util.Date().getTime()%>"></script>


<div class="loading" id="configureItemAdd_loading"><img src="${pageContext.request.contextPath}/images/icons/loading.gif" /></div>
	
<div id="configureItemAdd_panel" class="content" fit="true" border="none" style="height: 99%;">

	
	<!-- 新的面板 start-->
	<div class="easyui-layout" style="width: 100%;" id="configureItemAdd_layout" fit="true">
		<!-- 新的面板 start-->
		<div region="north" border="false" class="northBar">
			<div align="left">
				<form>
				<a id="itemAddSave" class="easyui-linkbutton" icon="icon-save" plain="true" title="<fmt:message key="common.save"/>"></a>
				<a id="add_configureItem_backList" class="easyui-linkbutton" plain="true" icon="icon-undo" style="margin-right:15px" title="<fmt:message key="common.returnToList" />"></a>
				<span><fmt:message key="label.template.configureItem" />&nbsp;&nbsp;<select id="configureItemTemplate" onchange="javascript:itsm.cim.configureItemAdd.getTemplateValue(this.value)"></select> </span>
				<input type="hidden" name="templateDTO.templateType" value="configureItem"  />
				<sec:authorize url="TEMPLATE_MAIN">
				<a id="saveconfigureItemTemplateBtn" class="easyui-linkbutton" plain="true" icon="icon-save" style="margin-right:15px"><fmt:message key="label.template.saveTemplate" /></a>			
				<a id="editconfigureItemTemplateBtn" class="easyui-linkbutton" plain="true" icon="icon-save" style="display: none;"><fmt:message key="label.template.editTemplate" /></a>
				</sec:authorize>
				</form>
			</div>
		</div>
		
		<div id="ciBasicInfoDiv" region="west" split="true" title="<fmt:message key="common.basicInfo" />" style="width:450px;height: auto;">
		
			<form>
			
	     	 <div class="lineTableBgDiv">
				<table style="width:100%" class="lineTable" cellspacing="1">
				<!-- 所属客户  -->
				<tr <c:if test="${versionType!='ITSOP'}"><c:out value="style=display:none"></c:out></c:if>>
					<td><fmt:message key="label.belongs.client" />&nbsp;<span style="color:red">*</span></td>
					<td>
						<input type="hidden" name="ciDto.companyNo" id="ci_add_companyNo" />
						<input id="ci_add_companyName" style="width:90%" class="easyui-validatebox input" required="true"/>
						<a id="search_ci_companyName" title="<fmt:message key='common.update' />">
						<img style="vertical-align:middle;" src="../images/icons/userpicker_disabled.gif"></a>
					</td>
				</tr>
	            <tr>
	                <td style="width:25%"><fmt:message key="title.request.CICategory"/>&nbsp;<span style="color:red">*</span></td>
					<td>
						<input type="hidden" name="ciDto.categoryNo" id="ci_add_categoryNo" value="${categoryNo}" />
					  	<input name="ciDto.categoryName" id="ci_add_categoryName"  value="${categoryName}"  class="easyui-validatebox input" required="true" readonly style="cursor:pointer" onclick="itsm.cim.ciCategoryTree.autoUpdateEavAttrCiCategoryTree('configureItemAddCategory','configureItemAddCategoryTree','ci_add_categoryNo','ci_add_categoryName','ci_add_eavAttributet','ci_add_cino')"/>
	                </td>
	            </tr>
	            <tr>    
	                <td><fmt:message key="lable.ci.assetNo"/>&nbsp;<span style="color:red">*</span></td>
	                <td><input name="ciDto.cino" id="ci_add_cino" class="easyui-validatebox input" required="true" validType="nullValueValid" validType="length[1,20]"/></td>
	            </tr>
	            <tr>
	                <td><fmt:message key="title.asset.name"/>&nbsp;<span style="color:red">*</span></td>
	                <td><input name="ciDto.ciname" id="ciname" class="easyui-validatebox input" required="true" validType="nullValueValid" validType="length[1,50]"/>
	                </td>
	            </tr>
	            <tr>       
	                <td><fmt:message key="label.dc.systemPlatform"/></td>
	                <td>
	                	 <select name="ciDto.systemPlatformId" id="ci_systemPlatform" class="easyui-validatebox input">
	                	 	<option value="">-- <fmt:message key="common.pleaseSelect" /> --</option>
			                <c:forEach items="${dictionaryItemsSystemPlatformList}" var="ds">
			                	<option value="${ds.dcode}">${ds.dname}</option>
			                </c:forEach>
		                </select>                                   
	                </td>
	            </tr>
	            <tr>      
	                <td><fmt:message key="ci.productType"/></td>
	                <td><input name="ciDto.model" id="model" class="input" /></td>        
	            </tr>
	            <c:if test="${resAuth['CofnigureItem_SerialNumber_Res'] }">
	            <tr>
	                <td><fmt:message key="ci.serialNumber"/></td>
	                <td><input name="ciDto.serialNumber" id="serialNumber" class="input" /></td>
	            </tr>
	            </c:if>
	            <c:if test="${resAuth['CofnigureItem_Barcode_Res'] }">
	            <tr> 
	                <td><fmt:message key="ci.barcode"/></td>
	                <td><input name="ciDto.barcode" id="barcode" class="input" /></td>
	            </tr>
	            </c:if>
	            <tr>
	                <td ><fmt:message key="common.status"/></td>
	                <td>
		                <select name="ciDto.statusId" id="ci_statusAdd" class="input">
		                	<option value="0">-- <fmt:message key="common.pleaseSelect" /> --</option>
			                <c:forEach items="${dictionaryItemsStatusList}" var="ds">
			                	<option value="${ds.dcode}">${ds.dname}</option>
			                </c:forEach>
		                </select>
	                </td>
	           </tr>
	           <tr>       
	                <td><fmt:message key="ci.brands"/></td>
	                <td>
	                	 <select name="ciDto.brandId" id="ci_brandNameAdd" class="input">
	                	 	<option value="0">-- <fmt:message key="common.pleaseSelect" /> --</option>
			                <c:forEach items="${dictionaryItemsBrandList}" var="ds">
			                	<option value="${ds.dcode}">${ds.dname}</option>
			                </c:forEach>
		                </select>                                   
	                </td>
	            </tr>
	            <c:if test="${resAuth['CofnigureItem_Supplier_Res'] }">
	            <tr>
	                <td><fmt:message key="label.supplier"/></td>
	                <td>
	                	<select name="ciDto.providerId" id="ci_providerAdd" class="input">
	                		<option value="0">-- <fmt:message key="common.pleaseSelect" /> --</option>
			                <c:forEach items="${dictionaryItemsProviderList}" var="ds">
			                	<option value="${ds.dcode}">${ds.dname}</option>
			                </c:forEach>
		                </select>
	                </td>
	           </tr>
	           </c:if>
	            <tr>       
	                <td><fmt:message key="ci.location"/></td>
	                <td>
	                	<input type=hidden name="ciDto.locId" id="ci_locid">
              			<input id="ci_loc"  class="easyui-validatebox choose" readonly/>
	                </td>
	            </tr>
	            <c:if test="${resAuth['CofnigureItem_PurchaseDate_Res'] }">
	            <tr>
	                <td><fmt:message key="ci.purchaseDate"/></td>
	                <td><input name="ciDto.buyDate" id="configureItem_add_buyDate" class="easyui-validatebox choose"  readonly/></td>
	            </tr>
	            </c:if>
	            <c:if test="${resAuth['CofnigureItem_ArrivalDate_Res'] }">
	            <tr>    
	                <td><fmt:message key="ci.arrivalDate"/></td>
	                <td><input name="ciDto.arrivalDate" id="configureItem_add_arrivalDate" class="easyui-validatebox choose" validType="DateComparisonCI['configureItem_add_buyDate']"  readonly /></td>
	            </tr>
	            </c:if>
	            <c:if test="${resAuth['CofnigureItem_WarningDate_Res'] }">
	            <tr>    
	                <td><fmt:message key="ci.warningDate"/></td>
	                <td><input name="ciDto.warningDate" id="warningDate" class="choose"  readonly/></td>
	            </tr>
	            </c:if>
	            <c:if test="${resAuth['CofnigureItem_PurchaseNo_Res'] }">
	            <tr>
	                <td><fmt:message key="ci.purchaseNo"/></td>
	                <td><input name="ciDto.poNo" id="poNo" class="input" /></td>
	            </tr>
	            </c:if>
	            <c:if test="${resAuth['CofnigureItem_AssetOriginalValue_Res'] }">
	            <sec:authorize url="CofnigureItem_AssetOriginalValue_Res">
	            <tr>
	            	<td><fmt:message key="ci.assetOriginalValue"/></td>
	            	<td><input name="ciDto.assetsOriginalValue" id="assetsOriginalValue" class="easyui-numberbox input" validType="length[1,9]" min="0" /></td>
	            </tr>
	            </sec:authorize>
	            </c:if>
	            <c:if test="${resAuth['CofnigureItem_Fimapping_Res'] }">
	            <tr>
	            	<td><fmt:message key="ci.FIMapping"/></td>
	            	<td>
	            	<input name="ciDto.financeCorrespond" type="radio" value="true" /><fmt:message key="tool.affiche.yes" />
	            	<input name="ciDto.financeCorrespond" type="radio" value="false" checked /><fmt:message key="tool.affiche.no" />
	            	</td>
	            </tr>
	            </c:if>
	            
	            <!-- 多少年折旧率为0 -->
	            <tr>
	            	<td><fmt:message key="ci.depreciationIsZeroYears"/></td>
	            	<td>
	            	<input name="ciDto.depreciationIsZeroYears" id="depreciationIsZeroYears" class="easyui-validatebox easyui-numberbox input" min="0" validType="length[1,13]" max="999999999" style="width:80%"/>
	            	<fmt:message key="title.report.year"/>
	            	</td>
	            </tr>
	            <tr><td colspan="2">&nbsp;<br><br><br><br></td></tr>
	          </table>
	          </div>
	          </form>
		
		</div>
		
		<div region="center" id="configureItemAdd_tabs" title="<fmt:message key="common.detailInfo" />" style="padding-bottom: 15px;height: auto;">
			<div class="easyui-tabs" fit="true" border="false" id="addCim_tabs">
				<!-- 其他信息  -->
				<div title="<fmt:message key="ci.ciOtherInfo" />" >
					<div class="lineTableBgDiv">
					<form>
					<table style="width:100%" class="lineTable" cellspacing="1">
						<c:if test="${resAuth['CofnigureItem_Department_Res'] }">
						<tr>
			            	<td><fmt:message key="ci.department" /></td>
			            	<td><input name="ciDto.department" style="cursor:pointer;" class="input" id="configureItem_add_department" readonly/>
			            	</td>
			            </tr>
			            </c:if>
			            <tr>
			            	<td><fmt:message key="label.ci.businessEntity" /></td>
			            	<td><input name="ciDto.workNumber" id="workNumber" class="input"/></td>
			            </tr>
			            <tr>
			            	<td><fmt:message key="label.ci.project" /></td>
			            	<td><input name="ciDto.project" id="project" class="input"/></td>
			            </tr>
			            <tr>
			            	<td><fmt:message key="ci.productProvider" /></td>
			            	<td><input name="ciDto.sourceUnits" id="sourceUnits" class="input"/></td>
			            </tr>
			            <c:if test="${resAuth['CofnigureItem_LifeCycle_Res'] }">
			            <tr>
			                <td><fmt:message key="ci.lifeCycle"/></td>
			                <td><input name="ciDto.lifeCycle" id="lifeCycle" maxlength="9"  class="easyui-validatebox easyui-numberbox input" min="0" /></td>
			            </tr>
			            </c:if>
			            <c:if test="${resAuth['CofnigureItem_WarrantyDate_Res'] }">
			            <tr>   
			                <td><fmt:message key="ci.warrantyDate"/></td>
			                <td><input name="ciDto.warranty" id="warranty" maxlength="9"  class="easyui-validatebox easyui-numberbox input" min="0"/></td>
			            </tr>
			            </c:if>
			            <c:if test="${resAuth['CofnigureItem_ExpiryDate_Res'] }">
			            <tr>
			            	<td><fmt:message key="ci.expiryDate" /></td>
			            	<td><input name="ciDto.wasteTime" id="wasteTime" class=" choose"  readonly/></td>
			            </tr>
			            </c:if>
			            <c:if test="${resAuth['CofnigureItem_LendTime_Res'] }">
			            <tr>
			            	<td><fmt:message key="ci.lendTime" /></td>
			            	<td><input name="ciDto.borrowedTime" id="borrowedTime" class=" choose"  readonly/></td>
			            </tr>
			            </c:if>
			            <c:if test="${resAuth['CofnigureItem_PlannedRecycleTime_Res'] }">
			            <tr>
			            	<td><fmt:message key="ci.plannedRecycleTime" /></td>
			            	<td><input name="ciDto.expectedRecoverTime" id="expectedRecoverTime" class="choose" readonly/></td>
			            </tr>
			            </c:if>
			            <c:if test="${resAuth['CofnigureItem_RecycleTime_Res'] }">
			            <tr>
			            	<td><fmt:message key="ci.recycleTime" /></td>
			            	<td><input name="ciDto.recoverTime" id="recoverTime" class="easyui-validatebox choose" readonly/></td>
			            </tr>
			            </c:if>
			            <c:if test="${resAuth['CofnigureItem_UsePermission_Res'] }">
			            <tr>
			            	<td><fmt:message key="ci.usePermission" /></td>
			            	<td><input name="ciDto.usePermissions" id="usePermissions" class="input"/></td>
			            </tr>
			            </c:if>
			            <c:if test="${resAuth['CofnigureItem_OriginalUser_Res'] }">
			            <tr>
			            	<td><fmt:message key="ci.originalUser"  /></td>
			            	<td><input name="ciDto.originalUser" style="width: 80%;" class="input" id="ci_add_originalUser" readonly/>
			            	&nbsp;&nbsp;
			            	<input type="hidden" name="ciDto.originalUserId" style="width: 80%;" class="input" id="ci_add_originalUserId" />
			                <a id="ci_add_originalUser_select"><img style="vertical-align:middle;" src="../skin/default/images/user.png"></a>
			            	<a class="easyui-linkbutton" plain="true" icon="icon-clean" onclick="cleanIdValue('ci_add_originalUser','ci_add_originalUserId')" title="<fmt:message key="label.request.clear" />"></a>
			            	</td>
			            </tr>
			            </c:if>
			            <tr>
			                <td align="left"><fmt:message key="ci.use"/></td>
			                <td><input name="ciDto.userName" style="width: 80%;" class="input" id="ci_add_useName" readonly/>
			                &nbsp;&nbsp;
			                <input type="hidden" name="ciDto.userNameId" style="width: 80%;" class="input" id="ci_add_useNameId" />
			                <a id="ci_add_useName_select"><img style="vertical-align:middle;" src="../skin/default/images/user.png"></a>
			            	<a class="easyui-linkbutton" plain="true" icon="icon-clean" onclick="cleanIdValue('ci_add_useName','ci_add_useNameId')" title="<fmt:message key="label.request.clear" />"></a>
			           		</td>
			            </tr>
			            <c:if test="${resAuth['CofnigureItem_Owner_Res'] }">
			            <tr>    
			                <td><fmt:message key="ci.owner"/></td>
			                <td><input name="ciDto.owner" style="width: 80%;" class="input" id="ci_add_owner" readonly>
			                &nbsp;&nbsp;
			                <input type="hidden" name="ciDto.ownerId" style="width: 80%;" class="input" id="ci_add_ownerId" >
			                <a id="ci_add_owner_select"><img style="vertical-align:middle;" src="../skin/default/images/user.png"></a>
			                <a class="easyui-linkbutton" plain="true" icon="icon-clean" onclick="cleanIdValue('ci_add_owner','ci_add_ownerId')" title="<fmt:message key="label.request.clear" />"></a>
			                </td>  
			            </tr>
			            </c:if>
			            <tr>
			            	<td><fmt:message key="label.role.roleMark" /></td>
			            	<td><input name="ciDto.CDI" id="CDI" class="input"/></td>
			            </tr>
					</table>	
					</form>
					</div>
					
				</div>
				
				<!-- 扩展信息 start -->
				<div title="<fmt:message key="config.extendedInfo"/>">
					
					<form id="ci_add_eavAttributet_form">
					<div class="hisdiv" id="ci_add_eavAttributet">
					<table style="width:100%" class="histable" cellspacing="1">
						<thead>
							<tr>
								<%--<th><fmt:message key="common.attributeName" /></th><th><fmt:message key="common.attributeValue" /></th> --%>
							</tr>
						</thead>
						<tbody>
						<tr><td colspan="2" style="color: red"><b><fmt:message key="label.notExtendedInfo" /></b></td></tr>
						</tbody>
             		</table>
               		</div>
					
				</form>	
				</div>
				<div title="<fmt:message key="common.attachment"/>">
					<form>
                	<div class="lineTableBgDiv">
					<table style="width:100%" class="lineTable" cellspacing="1">
                		<tr>
			              <td>
			              <div style="color:#ff0000;line-height:28px"><fmt:message key="msg.attachment.maxsize" /></div>
			               <div class="diyLinkbutton">
                               <div style="float: left;cursor: pointer;">
                                <input type="file"  name="filedata" id="addConfigureItem_uploadAttachments">
    			               </div>
                               <div style="float: left;margin-left: 15px;">
    				 		    <a class="easyui-linkbutton" icon="icon-upload" href="javascript:if($('#addConfigureItem_fileQueue').html()!='')$('#addConfigureItem_uploadAttachments').uploadifyUpload();else msgShow(i18n['msg_add_attachments'],'show');" ><fmt:message key="label.attachment.upload" /></a>
    	             		   </div>
    	             		   <div style="float: left;margin-left: 15px;"><a href="javascript:common.config.attachment.chooseAttachment.showAttachmentGrid('addConfigureItem_uploadedAttachments','ciDto.aids')" class="easyui-linkbutton" icon="icon-selectOldFile" id="chooseAtt"><fmt:message key="common.attachmentchoose" /></a></div>
			               </div>
                           <div style="clear: both;"></div>
                           <div id="addConfigureItem_uploadedAttachments" style="line-height:25px;color:#555"></div>
                           <div id="addConfigureItem_fileQueue"></div>
			               <input style=" width: 100%" type="hidden" name="ciDto.attachmentStr" id="addConfigureItem_attachments"/>
			              </td>
			        	</tr>
			        	
                	</table>
                	</div>
                	</form>
                	<%-- <form id="uploadForm_CiAdd" method="post" enctype="multipart/form-data">
					  <table border="0" cellspacing="1" class="fu_list">
			              <tr height="40px">
				        	<td colspan="2">
				        	<table border="0" cellspacing="0"><tr><td >
					       			<a href="javascript:void(0);" class="files" id="idFile_CiAdd"></a>
					       		</td><td>
							        <a class="easyui-linkbutton" style="float:left" plain="true"  icon="icon-ok" id="idBtnupload_CiAdd"><fmt:message key="label.startUpload" /></a>
							         &nbsp;&nbsp;&nbsp;
									<a class="easyui-linkbutton" plain="true" icon="icon-cancel" onclick="javascript:void(0);" id="idBtndel_CiAdd"><fmt:message key="label.allcancel" /></a>
									 &nbsp;&nbsp;&nbsp;
									<a plain="true" href="javascript:common.config.attachment.chooseAttachment.showAttachmentGrid('addConfigureItem_uploadedAttachments','ciDto.aids')" class="easyui-linkbutton" icon="icon-ok" id="chooseAtt"><fmt:message key="common.attachmentchoose" /></a>
								</td></tr></table>
							</td>
				      		</tr>
				          <tbody id="idFileList_CiAdd">
				          </tbody>
						</table>
					</form> --%>
                	
				</div>
				<!-- 软件配置参数 start -->
				<div title="<fmt:message key="label.ci.softSettingParam"/>">
					
					<form>
					<div class="hisdiv" id="ci_add_softSetting">
					<table style="width:100%" class="histable" cellspacing="1">
						<tr>
							<td width="30%"><fmt:message key="label.ci.softSetParam"/></td>
							<td width="60%" align="left"><textarea style="width: 80%" name="ciDto.softSetingParam" id="addci_softSetingParam"></textarea> </td>
						</tr>
						<tr>
							<td width="30%"><fmt:message key="label.ci.softSettingAm"/></td>
							<td width="60%" align="left"><textarea style="width: 80%" name="ciDto.softConfigureAm" id="addci_softConfigureAm"></textarea> </td>
						</tr>
						<tr>
							<td width="30%"><fmt:message key="label.ci.softSettingRemark1"/></td>
							<td width="60%" align="left"><textarea style="width: 80%" name="ciDto.softRemark1" id="addci_softRemark1"></textarea></td>
						</tr>
						<tr>
							<td width="30%"><fmt:message key="label.ci.softSettingRemark2"/></td>
							<td width="60%" align="left"><textarea style="width: 80%" name="ciDto.softRemark2" id="addci_softRemark2"></textarea></td>
						</tr>
						<tr>
							<td width="30%"><fmt:message key="label.ci.softSettingRemark3"/></td>
							<td width="60%" align="left"><textarea style="width: 80%" name="ciDto.softRemark3" id="addci_softRemark3"></textarea></td>
						</tr>
             		</table>
               		</div>
					<div class="lineTableBgDiv">
					<table style="width:100%" class="lineTable" cellspacing="1">
                		<tr>
			              <td>
			              <div style="color:#ff0000;line-height:28px"><fmt:message key="msg.attachment.maxsize" /></div>
			             	<div class="diyLinkbutton">
			             		<div style="float: left;cursor: pointer;">
                              <input type="file"  name="filedata" id="addConfigureItem_uploadSoftAttachments">
    			               </div>
			             		<div style="float: left;margin-left: 15px;">
					 		    	<a class="easyui-linkbutton" icon="icon-upload" href="javascript:if($('#addConfigureItem_SoftfileQueue').html()!='')$('#addConfigureItem_uploadSoftAttachments').uploadifyUpload();else msgShow(i18n['msg_add_attachments'],'show');" ><fmt:message key="label.attachment.upload" /></a>
		             		   </div>
	             		   <div style="float: left;margin-left: 15px;"><a href="javascript:common.config.attachment.chooseAttachment.showAttachmentGrid('add_uploadedSoftAttachments','ciDto.softAids')" class="easyui-linkbutton" icon="icon-selectOldFile" id="chooseAtt"><fmt:message key="common.attachmentchoose" /></a></div>
			               </div>
			               <br>
				 		   <div style="clear: both;"></div>
			               <div id="addConfigureItem_SoftfileQueue"></div>
			               <input style=" width: 100%" type="hidden" name="ciDto.softAttachmentStr" id="addConfigureItem_Softattachments"/>
              				<div id="add_uploadedSoftAttachments" style="line-height:25px;color:#555"></div>
			              </td>
			        	</tr>
			        	
                	</table>
                	
                	</div>
				</form>	
					<%-- <form id="uploadForm_CiAddSoft" method="post" enctype="multipart/form-data">
					  <table border="0" cellspacing="1" class="fu_list">
			              <tr height="40px">
				        	<td colspan="2">
				        	<table border="0" cellspacing="0"><tr><td >
					       			<a href="javascript:void(0);" class="files" id="idFile_CiAddSoft"></a>
					       		</td><td>
							        <a class="easyui-linkbutton" style="float:left" plain="true"  icon="icon-ok" id="idBtnupload_CiAddSoft"><fmt:message key="label.startUpload" /></a>
							         &nbsp;&nbsp;&nbsp;
									<a class="easyui-linkbutton" plain="true" icon="icon-cancel" onclick="javascript:void(0);" id="idBtndel_CiAddSoft"><fmt:message key="label.allcancel" /></a>
									 &nbsp;&nbsp;&nbsp;
									<a plain="true" href="javascript:common.config.attachment.chooseAttachment.showAttachmentGrid('add_uploadedSoftAttachments','ciDto.softAids')" class="easyui-linkbutton" icon="icon-ok" id="chooseAtt"><fmt:message key="common.attachmentchoose" /></a>
								</td></tr></table>
							</td>
				      		</tr>
				          <tbody id="idFileList_CiAddSoft">
				          </tbody>
						</table>
					</form>
					 --%>
				</div>
				<!-- 软件配置参数end -->
				<%--硬件 --%>
                <div id="configureItemEdit_hardware" title="<fmt:message key="label_hardware" />" >
                <form>
                	<input type="hidden" name="hardwareDTOs.hardwareId" value="${hardwareDTOs.hardwareId}" />
                
                	<fieldset> 
		 				<legend><fmt:message key="label_computer_system" /></legend>
		 				<table width="98%">
		 					<tr>
		 						<td><fmt:message key="config.computerName" />:<br/>
		 						<input class="input" name="hardwareDTOs.computerSystem_name" value="${hardwareDTOs.computerSystem_name}" /></td>
		 						<td><fmt:message key="config_computerSystem_model" />:<br/>
		 						<input class="input" name="hardwareDTOs.computerSystem_model" value="${hardwareDTOs.computerSystem_model}" /></td>
		 					</tr>
		 					<tr>
		 						<td><fmt:message key="config_computerSystem_domain" />:<br/>
		 						<input class="input" name="hardwareDTOs.computerSystem_domain" value="${hardwareDTOs.computerSystem_domain}" /></td>
		 						<td><fmt:message key="common.Username" />:<br/>
		 						<input class="input" name="hardwareDTOs.computerSystem_userName" value="${hardwareDTOs.computerSystem_userName}" /></td>
		 					</tr>
		 				</table>
		 			</fieldset>
		 			
		 			<fieldset>
		 				<legend><fmt:message key="config.systemName" /></legend>
		 				<table width="98%">
		 					<tr>
		 						<td><fmt:message key="ci.serialNumber" />:<br/>
		 						<input class="input" name="hardwareDTOs.operatingSystem_serialNumber" value="${hardwareDTOs.operatingSystem_serialNumber}" /></td>
		 						<td><fmt:message key="label.snmp.systemName" />:<br/>
		 						<input class="input" name="hardwareDTOs.operatingSystem_caption" value="${hardwareDTOs.operatingSystem_caption}" /></td>
		 					</tr>
		 					<tr>
		 						<td><fmt:message key="config.systemVersion" />:<br/>
		 						<input class="input" name="hardwareDTOs.operatingSystem_version" value="${hardwareDTOs.operatingSystem_version}"/></td>
		 						<td><fmt:message key="config_operatingSystem_osArchitecture" />:<br/>
		 						<input class="input" name="hardwareDTOs.operatingSystem_osArchitecture"  value="${hardwareDTOs.operatingSystem_osArchitecture}" /></td>
		 					</tr>
		 					<tr>
		 						<td><fmt:message key="config_operatingSystem_csdVersion" />:<br/>
		 						<input class="input" name="hardwareDTOs.operatingSystem_csdVersion"  value="${hardwareDTOs.operatingSystem_csdVersion}" /></td>
		 						<td><fmt:message key="config_operatingSystem_installDate" />:<br/>
		 						<input class="input" id="operatingSystem_installDate_add" readonly name="hardwareDTOs.operatingSystem_installDate"  value="${hardwareDTOs.operatingSystem_installDate}" /></td>
		 					</tr>
		 					<tr>
		 						<td><fmt:message key="config_operatingSystem_lastBootUpTime" />:<br/>
		 						<input class="input" id="operatingSystem_lastBootUpTime_add" readonly name="hardwareDTOs.operatingSystem_lastBootUpTime"  value="${hardwareDTOs.operatingSystem_lastBootUpTime}" /></td>
		 						<td></td>
		 					</tr>
		 				</table>
		 			</fieldset>
		 			
		 			<fieldset>
		 				<legend><fmt:message key="config_memory" /></legend>
		 				<table width="98%">
		 					<tr>
		 						<td><fmt:message key="config_operatingSystem_totalVisibleMemorySize" />:<br/>
		 						<input class="input" name="hardwareDTOs.operatingSystem_totalVisibleMemorySize" value="${hardwareDTOs.operatingSystem_totalVisibleMemorySize}" /></td>
		 						<td><fmt:message key="config_operatingSystem_totalVirtualMemorySize" />:<br/>
		 						<input class="input" name="hardwareDTOs.operatingSystem_totalVirtualMemorySize"  value="${hardwareDTOs.operatingSystem_totalVirtualMemorySize}" /></td>
		 					</tr>
		 				</table>
		 			</fieldset>
		 			<fieldset>
		 				<legend>CPU</legend>
		 				<table width="98%">
		 					<tr>
		 						<td><fmt:message key="config_processor_name" />:<br/>
		 						<input class="input" name="hardwareDTOs.processor_name" value="${hardwareDTOs.processor_name}" /></td>
		 						<td><fmt:message key="config_processor_maxClockSpeed" />:<br/>
		 						<input class="input" name="hardwareDTOs.processor_maxClockSpeed"  value="${hardwareDTOs.processor_maxClockSpeed}" /></td>
		 					</tr>
		 					<tr>
		 						<td><fmt:message key="config_processor_l2CacheSize" />:<br/>
		 						<input class="input" name="hardwareDTOs.processor_l2CacheSize"  value="${hardwareDTOs.processor_l2CacheSize}"  /></td>
		 						<td><fmt:message key="config_processor_l3CacheSize" />:<br/>
		 						<input class="input" name="hardwareDTOs.processor_l3CacheSize" value="${hardwareDTOs.processor_l3CacheSize}"  /></td>
		 					</tr>
		 					<tr>
		 						<td><fmt:message key="config_processor_level" />:<br/>
		 						<input class="input" name="hardwareDTOs.processor_level" value="${hardwareDTOs.processor_level}" /></td>
		 						<td></td>
		 					</tr>
		 				</table>
		 			</fieldset>
		 			<fieldset>
		 				<legend>BIOS</legend>
		 				<table width="98%">
		 					<tr>
		 						<td><fmt:message key="ci.serialNumber" />:<br/>
		 						<input class="input" name="hardwareDTOs.bios_serialNumber" value="${hardwareDTOs.bios_serialNumber}" /></td>
		 						<td><fmt:message key="label.name" />:<br/>
		 						<input class="input" name="hardwareDTOs.bios_name" value="${hardwareDTOs.bios_name}" /></td>
		 					</tr>
		 					<tr>
		 						<td><fmt:message key="config_manufacturer" />:<br/>
		 						<input class="input" name="hardwareDTOs.bios_manufacturer" value="${hardwareDTOs.bios_manufacturer}" /></td>
		 						<td><fmt:message key="config_bios_version" />:<br/>
		 						<input class="input" name="hardwareDTOs.bios_version" value="${hardwareDTOs.bios_version}" /></td>
		 					</tr>
		 					<tr>
		 						<td><fmt:message key="config_bios_releaseDate" />:<br/>
		 						<input class="input" id="bios_releaseDate_add" readonly name="hardwareDTOs.bios_releaseDate" value="${hardwareDTOs.bios_releaseDate}" /></td>
		 						<td></td>
		 					</tr>
		 				</table>
		 			</fieldset>
		 			
		 			<fieldset>
		 				<legend><fmt:message key="config_baseBoard_name" /></legend>
		 				<table width="98%">
		 					<tr>
		 						<td><fmt:message key="label.name" />:<br/>
		 						<input class="input" name="hardwareDTOs.baseBoard_name" value="${hardwareDTOs.baseBoard_name}" /></td>
		 						<td><fmt:message key="ci.serialNumber" />:<br/>
		 						<input class="input" name="hardwareDTOs.baseBoard_serialNumber" value="${hardwareDTOs.baseBoard_serialNumber}" /></td>
		 					</tr>
		 					<tr>
		 						<td><fmt:message key="config.mainboard" />:<br/>
		 						<input class="input" name="hardwareDTOs.baseBoard_manufacturer" value="${hardwareDTOs.baseBoard_manufacturer}" /></td>
		 						<td></td>
		 					</tr>
		 				
		 				</table>
		 			</fieldset>
		 			
		 			<fieldset>
		 				<legend><fmt:message key="config_netWork_info" /></legend>
		 				<table width="98%">
		 					<tr>
		 						<td>IP:<br/>
		 						<input class="input easyui-validatebox" validType="ip" name="hardwareDTOs.netWork_ip" value="${hardwareDTOs.netWork_ip}" /></td>
		 						<td>MAC:<br/>
		 						<input class="input" name="hardwareDTOs.netWork_mac" value="${hardwareDTOs.netWork_mac}" /></td>
		 					</tr>
		 					<tr>
		 						<td><fmt:message key="label.name" />:<br/>
		 						<input class="input" name="hardwareDTOs.netWork_name" value="${hardwareDTOs.netWork_name}" /></td>
		 						<td>DHCP:<br/>
		 						<input class="input" name="hardwareDTOs.netWork_dhcp" value="${hardwareDTOs.netWork_dhcp}" /></td>
		 					</tr>
		 					<tr>
		 						<td><fmt:message key="config_dhcp_server" />:<br/>
		 						<input class="input" name="hardwareDTOs.netWork_dhcpServer" value="${hardwareDTOs.netWork_dhcpServer}" /></td>
		 						<td></td>
		 					</tr>
		 				</table>
		 			</fieldset>
		 			<fieldset>
		 				<legend><fmt:message key="config_desktopMonitor_name" /></legend>
		 				<table width="98%">
		 					<tr>
		 						<td><fmt:message key="label.name" />:<br/>
		 						<input class="input" name="hardwareDTOs.desktopMonitor_name" value="${hardwareDTOs.desktopMonitor_name}" /></td>
		 						<td><fmt:message key="config_desktopMonitor_height" />:<br/>
		 						<input class="input" name="hardwareDTOs.desktopMonitor_screenHeight" value="${hardwareDTOs.desktopMonitor_screenHeight}" /></td>
		 					</tr>
		 					<tr>
		 						<td><fmt:message key="config_desktopMonitor_width" />:<br/>
		 						<input class="input" name="hardwareDTOs.desktopMonitor_screenWidth" value="${hardwareDTOs.desktopMonitor_screenWidth}" /></td>
		 						<td><fmt:message key="config_manufacturer" />:<br/>
		 						<input class="input" name="hardwareDTOs.desktopMonitor_monitorManufacturer" value="${hardwareDTOs.desktopMonitor_monitorManufacturer}" /></td>
		 					</tr>
		 					<tr>
		 						<td><fmt:message key="ci.serialNumber" />:<br/>
		 						<input class="input" name="hardwareDTOs.desktopMonitor_serialNumber" value="${hardwareDTOs.desktopMonitor_serialNumber}" /></td>
		 						<td></td>
		 					</tr>
		 				</table>
		 			</fieldset>
		 			
		 			<fieldset>
		 				<legend><fmt:message key="config_pointingDevice" /></legend>
		 				<table width="98%">
		 					<tr>
		 						<td width="120px"><fmt:message key="label.name" />:<br/>
		 						<input class="input" name="hardwareDTOs.pointingDevice_Name" value="${hardwareDTOs.pointingDevice_Name}" /></td>
		 						<td width="120px"><fmt:message key="label.user.description" />:<br/>
		 						<input class="input" name="hardwareDTOs.pointingDevice_description" value="${hardwareDTOs.pointingDevice_description}" /></td>
		 					</tr>
		 					<tr>
		 						<td><fmt:message key="ci.serialNumber" />:<br/>
		 						<input class="input" name="hardwareDTOs.pointingDevice_serialNumber" value="${hardwareDTOs.pointingDevice_serialNumber}" /></td>
		 						<td><fmt:message key="config_manufacturer" />:<br/>
		 						<input class="input" name="hardwareDTOs.pointingDevice_manufacturer" value="${hardwareDTOs.pointingDevice_manufacturer}" /></td>
		 					</tr>
		 				
		 				</table>
		 			</fieldset>
		 			<fieldset>
		 				<legend><fmt:message key="config_keyboard" /></legend>
		 				<table width="98%">
		 					<tr>
		 						<td><fmt:message key="label.name" />:<br/>
		 						<input class="input" name="hardwareDTOs.keyboard_Name" value="${hardwareDTOs.keyboard_Name}" /></td>
		 						<td><fmt:message key="label.user.description" />:<br/>
		 						<input class="input" name="hardwareDTOs.keyboard_description" value="${hardwareDTOs.keyboard_description}" /></td>
		 					</tr>
		 					<tr>
		 						<td><fmt:message key="config_keyboard_numberOfFunctionKeys" />:<br/>
		 						<input class="input" name="hardwareDTOs.keyboard_numberOfFunctionKeys" value="${hardwareDTOs.keyboard_numberOfFunctionKeys}" /></td>
		 						<td><fmt:message key="ci.serialNumber" />:<br/>
		 						<input class="input" name="hardwareDTOs.keyboard_serialNumber" value="${hardwareDTOs.keyboard_serialNumber}" /></td>
		 					</tr>
		 					<tr>
		 						<td><fmt:message key="config_manufacturer" />:<br/>
		 						<input class="input" name="hardwareDTOs.keyboard_manufacturer" value="${hardwareDTOs.keyboard_manufacturer}" /></td>
		 						<td></td>
		 					</tr>
		 				</table>
		 			</fieldset>
     	  			</form> 
                </div>
                <%---硬件END --%>
				<div title='<fmt:message key="label.ci.ciServiceDir" />'>
				
							<form id="add_ci_relatedService">

								<div class="hisdiv">
									<table class="histable" id="add_ci_serviceDirectory" style="width:100%" cellspacing="1">
									<thead>
									<tr>
										<td colspan="3" style="text-align:left">
											<a id="add_ci_service_add" class="easyui-linkbutton" plain="true" icon="icon-add" title="<fmt:message key="common.add" />"></a>
										</td>
									</tr>
									<tr height="20px">
										<%-- <th align="center"><fmt:message key="common.id"/></th> --%>
										<th align="center"><fmt:message key="lable.ci.ciServiceDirName"/> </th>
										<th align="center"><fmt:message key="label.rule.operation"/> </th>
									</tr>
									</thead>
									<!-- 服务目录显示 -->
									<tbody id="add_ci_serviceDirectory_tbody">
										
									</tbody>
									
									</table>
								</div>
							</form>
				</div>
	    	</div>
			<!-- 扩展信息 end -->
		</div>
	</div>
<!-- 新的面板 end-->

<!-- 选择配置项分类 -->
<div id="configureItemAddCategory" class="WSTUO-dialog" title="<fmt:message key="title.request.CICategory"/>" style="width:400px;height:auto;padding:5px;">
	<div id="configureItemAddCategoryTree"></div>
</div> 

<div id="configureItemTemplateName" class="WSTUO-dialog" title="<fmt:message key="label.template.name"/>" style="width:auto;height:auto">
	<form>
	<div style="margin: 10px;"><fmt:message key="label.template.configureItemName" />&nbsp;&nbsp;<input id="configureItemTemplateNameInput" name="templateDTO.templateName" style="width: 150px;" class="easyui-validatebox input" required="true" validtype="length[1,200]"/>
	<a class="easyui-linkbutton" style="margin-top: 5px;" id="addTemplateOk" icon="icon-save" plain="true"><fmt:message key="common.save"/></a>
	<input type="hidden" id="configureItemTemplateId" name="templateDTO.templateId"/>
	</div>
	</form>
</div>

<div id="CIselectServiceDirDiv" title="<fmt:message key="label.sla.selectServiceDir"/>" class="WSTUO-dialog" style="width:250px;height:400px; padding:10px; line-height:20px;" >
	
	<div id="CISelectServiceDirTreeDiv"></div>
	<br/>
	<div style="border:#99bbe8 1px solid;padding:5px;text-align:center">
	<a class="easyui-linkbutton" icon="icon-ok" plain="true" id="CISelectServiceDir_getSelectedNodes"><fmt:message key="label.sla.confirmChoose"/></a>
	</div>
</div>



</div>




