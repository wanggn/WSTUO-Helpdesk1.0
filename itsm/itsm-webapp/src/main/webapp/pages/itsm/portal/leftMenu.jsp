<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.opensymphony.xwork2.ActionContext" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="leftMenu_second" id="leftMenu_second_new" >
    <ul class="leftMenu_second_items" style="top: 0px;">
      <sec:authorize url="/pages/request!saveRequest.action">
        <li onclick="leftMenu.secondMenuClick();common.tools.tcode.t_code.docmd('CRNR');" class="ico-create-request menu-second " id="secondMenu_new_request">
            <span class="leftMenu_secondMenu_item_title"><fmt:message key="title.mainTab.request"/></span>
        </li>
       </sec:authorize>
       <c:if test="${applicationScope.problemHave eq true}">
      <sec:authorize url="/pages/problem!saveProblem.action">
        <li onclick="leftMenu.secondMenuClick();common.tools.tcode.t_code.docmd('CRNP');" class="ico-create-problem menu-second " id="secondMenu_new_problem">
            <span class="leftMenu_secondMenu_item_title"><fmt:message key="title.mainTab.problem"/></span>
        </li>
      </sec:authorize>
      </c:if>
      <c:if test="${applicationScope.changeHave eq true}">
     <sec:authorize url="/pages/change!saveChange.action">
        <li onclick="leftMenu.secondMenuClick();common.tools.tcode.t_code.docmd('CRNC');" class="ico-create-change menu-second " id="secondMenu_new_change">
            <span class="leftMenu_secondMenu_item_title"><fmt:message key="title.mainTab.change"/></span>
        </li>
      </sec:authorize>
      </c:if>
      <c:if test="${applicationScope.releaseHave eq true}">
    <sec:authorize url="RELEASE_MANAGE_addIssue">
        <li onclick="leftMenu.secondMenuClick();common.tools.tcode.t_code.docmd('CRRL');" class="ico-create-release menu-second " id="secondMenu_new_release">
            <span class="leftMenu_secondMenu_item_title"><fmt:message key="notice.release"/></span>
        </li>
    </sec:authorize>
    </c:if>
    <c:if test="${applicationScope.cimHave eq true}">
    <sec:authorize url="/pages/ci!cItemSave.action">
        <li onclick="leftMenu.secondMenuClick();common.tools.tcode.t_code.docmd('CRNCI');" class="ico-create-cim menu-second " id="secondMenu_new_configuration">
            <span class="leftMenu_secondMenu_item_title"><fmt:message key="title.mainTab.configItem"/></span>
        </li>
    </sec:authorize>
    </c:if>
 <sec:authorize url="KNOWLEDGEINFO_ADDKNOWLEDGE">
        <li onclick="leftMenu.secondMenuClick();common.tools.tcode.t_code.docmd('CRNK');" class="ico-create-knowlege menu-second " id="secondMenu_new_knowledge">
            <span class="leftMenu_secondMenu_item_title"><fmt:message key="title.mainTab.knowledgeBase"/></span>
        </li>
   </sec:authorize>
    </ul>
</div>

<script>
$(document).ready(function(){
	leftMenu.secondMenuAction();
});
</script>

