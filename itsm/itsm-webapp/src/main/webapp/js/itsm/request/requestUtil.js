$package("itsm.request"); 

 /**  
 * @author Van  
 * @constructor WSTO
 * @description 选择关联请求公共方法.
 * @date 2011-05-28
 * @since version 1.0 
 */
itsm.request.requestUtil = function() {
	
	this.index_loadRelatedRequestGridFlag="0";
    this._companyNo=0;
	//载入
	return {

		/**
		 * @description 选择关联请求Grid
		 * @param p_url post submit url
		 * @param p_postData post submit data
		 */
		selectRelatedRequestGrid:function(p_url,p_postData){
			if(p_url===''){
				p_url='request!findRequests.action?requestQueryDTO.companyNo='+_companyNo+'&requestQueryDTO.lastUpdater='+userName;
			}
			//var arr=$.toJSON(p_postData);
			//var finalPostData=$.param({'page':'1','rows':'10','sidx':'eno','sord':'desc','fullTextQueryDTO.alias':arr['fullTextQueryDTO.alias'],'fullTextQueryDTO.queryString':arr['fullTextQueryDTO.queryString']},true);
			//var _fullSearchData = $.param({'fullTextQueryDTO.alias':'RequestInfo','fullTextQueryDTO.queryString':queryString});
			if(p_postData===''){
				p_postData={'page':'1','rows':'15','sidx':'eno','sord':'desc'};
			}
			var params=$.extend({},jqGridParams,{
				url:p_url,
				postData:p_postData,
				colNames:[i18n.common_id,i18n.common_title,i18n.requester,i18n.priority,i18n.common_state,i18n.common_createTime,''],
				colModel:[
						  {name:'requestCode',width:15,align:'center'},
						  {name:'etitle',width:20,align:'center'},
						  {name:'createdByName',width:15,align:'left',sortable:false},
						  {name:'priorityName',width:15,align:'left',sortable:false},
						  {name:'statusName',width:15,align:'left',sortable:false},
						  {name:'createdOn',width:25,align:'left',sortable:false,formatter:timeFormatter},
						  {name:'eno',hidden:true}
						  ],
						 ondblClickRow:function(rowIds){itsm.request.requestUtil.selectRequestAndClose(rowIds);},
				jsonReader: $.extend(jqGridJsonReader, {id:"eno"}),
				sortname:'eno',
				autowidth:false,
				width:640,
				pager:'#index_related_request_grid_pager',
				onPaging:function(pageButton){
					var _url = $("#index_related_request_grid").jqGrid("getGridParam", "url");    
					var _currentPage = $("#index_related_request_grid").jqGrid("getGridParam", "page");
					var _rows = $("#index_related_request_grid").jqGrid("getGridParam", "rowNum");
					if(_url.indexOf("?page=")!=-1){
						_url=_url.substring(0,_url.indexOf("?page="));
					}
					if(_url.indexOf('request!findRequests.action')==-1)//判断是否是全文搜索路径
						$('#index_related_request_grid').jqGrid('setGridParam',{url:_url+'?page='+_currentPage+"&rows="+_rows}).trigger('reloadGrid');
					else{
						var postData = $("#index_related_request_grid").jqGrid("getGridParam", "postData");     
						$.extend(postData, {'page':_currentPage,'rows':_rows});
						$('#index_related_request_grid').jqGrid('setGridParam',{url:_url,postData:postData}).trigger('reloadGrid');
					}
				}
			});
			$("#index_related_request_grid").jqGrid(params);
			$("#index_related_request_grid").navGrid('#index_related_request_grid_pager',navGridParams);
			
			//列表操作项
			$("#t_index_related_request_grid").css(jqGridTopStyles);
			$("#t_index_related_request_grid").html($('#index_related_request_grid_toolbar').html());
			
			//搜索
			//$('#index_related_request_grid_search').click(itsm.request.requestUtil.searchRelatedRequest);//搜索
		},
		/**
		 * @description 双击选择并关闭选择关联请求
		 * @param rowIds 选中行的ID
		 */
		selectRequestAndClose:function(rowIds){
			var selectTal=$("ul[class=tabs] li[class=tabs-selected] span[class=tabs-title tabs-closable]").text();
			if(selectTal == i18n.change_detail){
				itsm.change.relatedRequest.addRelatedRequest(rowIds);
				$('#index_related_request_window').dialog('close');
			}else if(selectTal == i18n.probelm_detail){
				itsm.problem.relatedRequestGrid.add_reletedRequest(rowIds);
				$('#index_related_request_window').dialog('close');
			}
		},
		/**
		 * @description 打开选择关联请求窗口
		 * @param callback 回调函数
		 * @param p_companyNo 公司编号
		 */
		openSelectWindow:function(callback,p_companyNo){
			var queryString = $('#problemDetails_etitle').val();
			$('#index_related_request_grid_etitle').val(queryString || '');
			_companyNo=p_companyNo;
			var _url="request!findRequests.action?requestQueryDTO.companyNo="+_companyNo+'&requestQueryDTO.lastUpdater='+userName;
			
//			$('#index_related_request_grid').jqGrid('setGridParam',{page:1,url:_url}).trigger('reloadGrid');
			
			$('#index_related_request_grid_requestCode').show();
			if(index_loadRelatedRequestGridFlag=="0"){
				itsm.request.requestUtil.selectRelatedRequestGrid('','');//加载列表
				index_loadRelatedRequestGridFlag="1";
			}else{
				//itsm.request.requestUtil.selectRelatedRequestGrid('','');//加载列表
				var postData = {};
				$.extend(postData, {page:1});
				$('#index_related_request_grid').jqGrid('setGridParam',{postData:postData,url:'request!findRequests.action?requestQueryDTO.companyNo='+_companyNo+'&requestQueryDTO.lastUpdater='+userName}).trigger('reloadGrid');
			}
			//选定
			$('#index_related_request_grid_select').unbind().click(function(){
				itsm.request.requestUtil.confirmSelect(callback);
			});
			
			//绑定搜索事件
			$('#index_related_request_grid_search').unbind().click(itsm.request.requestUtil.searchRelatedRequest);//搜索
			windows('index_related_request_window',{width:650});
			//关联请求查询类型切换
			itsm.request.requestUtil.relatedRequestSearchTypeChange($('#related_request_search_type').val());
			
		},
		
		/**
		 * @description 选定选中的请求
		 * @param callback 回调函数
		 */
		confirmSelect:function(callback){
			var rowIds = $("#index_related_request_grid").getGridParam('selarrrow');
			if(rowIds=='' && rowIds.length==0){
				msgAlert(i18n.msg_atLeastChooseOneData,'info');
			}else{
				callback(rowIds);
				$('#index_related_request_window').dialog('close');
			}
		},
		
		/**
		 * @description 搜索请求
		 */
		searchRelatedRequest:function(){
			var _queryString=$('#index_related_request_grid_etitle').val();
			
				var stype = $("#related_request_search_type").val();
				if(stype == '1'){//全文检索
					if(_queryString != ''){
						itsm.request.requestUtil.doFullsearch();
					}else{
						msgShow(i18n['validate_notNull'],'show');
					}
				}else{
					var _url = 'request!findRequests.action?requestQueryDTO.companyNo='+_companyNo+'&requestQueryDTO.lastUpdater='+userName;
					var sdata = $('#index_related_request_grid_form').getForm();
					$('#index_related_request_grid').jqGrid('setGridParam',{page:1,url:_url,postData:sdata}).trigger('reloadGrid');
				}
			
		},
		
		/**
		 * @description 全文检索.
		 * @param queryString 查询条件
		 * @param callback 回调函数
		 */
		fullsearchOpenwindow:function(queryString,callback){
			var _url="request!analogousRequest.action?page=1&rows=15";
			var _fullSearchData = $.param({'fullTextQueryDTO.alias':'RequestInfo','fullTextQueryDTO.queryString':queryString});		
			
			if(index_loadRelatedRequestGridFlag=="0"){
				itsm.request.requestUtil.selectRelatedRequestGrid(_url,_fullSearchData);//加载列表
				index_loadRelatedRequestGridFlag="1";
				$('#index_related_request_grid_select').click(function(){
					itsm.request.requestUtil.confirmSelect(callback);
				});//选定
			}else{		
				//var _postData = $("#index_related_request_grid").jqGrid("getGridParam", "postData");    
				//_fullSearchData=$.extend({},_postData,_fullSearchData);
				$('#index_related_request_grid').jqGrid('setGridParam',{page:1,url:_url,postData:_fullSearchData}).trigger('reloadGrid');
			}
			$('#index_related_request_grid_search').unbind().click(itsm.request.requestUtil.doFullsearch);//搜索
			$('#index_related_request_grid_etitle').val(queryString);
			$('#index_related_request_grid_requestCode').hide();
			windows('index_related_request_window');
		},
		/**
		 * @description 提交全文搜索
		 */
		doFullsearch:function(){
			//var _postData = $("#index_related_request_grid").jqGrid("getGridParam", "postData");
			var _url="request!analogousRequest.action";
			var _queryString=$('#index_related_request_grid_etitle').val();
			var rowListNum = $('#index_related_request_grid').jqGrid('getGridParam', 'rowNum');
			var _fullSearchData = $.param({'fullTextQueryDTO.alias':'RequestInfo','fullTextQueryDTO.queryString':_queryString,'fullTextQueryDTO.companyNo':_companyNo,'rows':rowListNum},true);	
			//_fullSearchData=$.extend({},_postData,_fullSearchData);
			$('#index_related_request_grid').jqGrid('setGridParam',{page:1,url:_url,postData:_fullSearchData}).trigger('reloadGrid');
		},
		
		/**
		 * 关联请求查询类型切换
		 */
		relatedRequestSearchTypeChange:function(value){
			if(value==0 || value=='0'){
				$('#index_related_request_grid_requestCode').show();
			}else{
				$('#index_related_request_grid_requestCode').hide();
			}
		}
		
	};
}();