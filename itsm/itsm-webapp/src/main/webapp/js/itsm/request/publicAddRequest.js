$package("itsm.request");
$import('wstuo.tools.xssUtil');
 /**  
 * @fileOverview 新增请求主函数.
 * @author QXY
 * @version 1.0  
 */  
 /**  
 * @author QXY  
 * @constructor WSTO
 * @description 新增请求主函数.
 * @date 2010-11-17
 * @since version 1.0 
 * @returns
 * @param select
 */
$import('itsm.cim.ciCategoryTree');
itsm.request.publicAddRequest = function(){
	this._cname='';
	var obj;
	var thisFileValue;
	var data=[];
	var page=1;
	return {
		 //请求分类弹出方法
		selectRequestCategory:function(){
			itsm.request.publicAddRequest.showSelectTree('#requestPublicWindow'
					,'#requestCatePublic'
					,'Request'
					,'#addRequestCategoryName'
					,'#addRequestCategoryNos'
					,'#reques_tTitle'
					,'#textContent','request_add_eavAttributets','requestDTO',
					function(){
						$('#requestcateName,#addRequestCategoryNo').val('');
					});
		},
		showSelectTree:function(windowId,treePanel,param,namePut,idPut,otherPut1,otherPut2,showAttrId,dtoName,callback){
			 var maxViewLevel=0;
			 if(namePut=='#addRequestCategoryName' || namePut=='#editRequestCategoryName' 
				 || namePut=='#add_problem_categoryName' || namePut=='#problem_edit_categoryName'
					 || namePut=='#change_add_categoryName' || namePut=='#change_edit_categoryName'
						 || namePut=='#add_release_categoryName' || namePut=='#edit_release_categoryName'
						 
			 ){
				 //maxViewLevel=1;
			 }
			 $(treePanel).jstree({
					json_data: {
						ajax: {
						  type:"post",
						  url : "event!getCategoryTree.action?num=0",
						  data:function(n){
					    	  return {'types': param,'parentEventId':n.attr ? n.attr("id").replace("node_",""):0,'maxViewLevel':maxViewLevel};//types is in action
					      },
		                  dataType: 'json',
		                  cache:false
					 }
					},
					plugins : ["themes", "json_data","cookies", "ui", "crrm"]
			})
			/*.bind('loaded.jstree', function(e,data){data.inst.open_all(-1);})*/     //默认展开所有节点
			.bind("select_node.jstree",function(e,data){
				//if(data.rslt.obj.attr('state')==null || data.rslt.obj.attr('state')=='' || data.rslt.obj.attr('state')==undefined){
					$(namePut).val(data.rslt.obj.attr("cname"));
					$(idPut).val(data.rslt.obj.attr("id"));
					catName=data.rslt.obj.attr("cname");
					/*if(catName==i18n.label_request_requestType||catName==i18n.default_title_problemCategory||catName==i18n.label_request_changeCategory||catName==i18n.label_release_releaseCategory){
						$(namePut).val("");
						$(idPut).val("");
					}*/
					_cname=data.rslt.obj.attr("cname");
					//itsm.cim.ciCategoryTree.showAttributet(data.rslt.obj.attr('eavId'),showAttrId,dtoName);
					layer.closeAll();//关闭窗口
					(callback && typeof(callback) === "function") && callback();
//				}else{
//					layer.msg("请选择子节点!",1,8);
//				}
			});
			   $.layer({
			        type : 1,
			        loading : {type : 2},
			        title : "Esc退出",
			        offset : [($HEIGHT-220)/2, ''],
			        moveType: 1,
			        page: {
			        	dom: treePanel 
			        },
			        shade : [0.1 , '#000' , true],
			        fadeIn: 300,
			        border : [4 , 0.1 , '#000', true],
			        area : ['250px','350px']
			 }); 				
			$(".xubox_main").css("overflow-y","scroll");
			$(".xubox_main").css("overflow-x","hidden");
		 },
		 //保存 请求
		 saveRequest:function(){
			 layer.load();
			 //没有选择了附件
			 if(thisFileValue){
				 itsm.request.publicAddRequest.addrequestupload("request");
			 }else{
				 itsm.request.publicAddRequest.saverequests();
			 }
		 },
		 saverequests:function(){
			 $.each($("#idForm input[type='text']").add("textarea"),function(ind,val){
					$(this).val(wstuo.tools.xssUtil.html_encode($(this).val()));
			 });
			 var frm=$("#idForm").serialize();
			 var url="request!saveTicket.action";
			 $.post(url,frm,function(res){
				if(res!=null){
					layer.closeAll();
					layer.msg("保存成功！1秒后关闭",1,1);
					$("#idForm")[0].reset();
					setTimeout("location.reload()",1000);
//					itsm.request.publicAddRequest.setDisabled();
				} else {
					layer.closeAll();
					layer.msg("保存失败!",1,8);
				}
			 });
		 },
		 //选择请求部门
		 selectOrg:function(){
			 itsm.request.publicAddRequest.showAll_2('#show_org_win','#show_org_tree','#orgNames','#org_orgNo',$('#compId').val());
		 },
		 /**
			 * 显示所有机构，含面板
			 */
			showAll_2:function(windowPanel,treePanel,namePut,idPut,_companyNo){
				/*windows(windowPanel.replace('#',''),{width:380});*/
				$.layer({
			        type : 1,
			        loading : {type : 2},
			        title : false,
			        offset : [($HEIGHT-220)/2, ''],
			        moveType: 1,
			        page: {
			        	dom: treePanel 
			        },
			        shade : [0.1 , '#000' , true],
			        fadeIn: 300,
			        border : [4 , 0.1 , '#000', true],
			        area : ['200px','220px']
			    });
				$(treePanel).jstree({
					json_data:{
						ajax: {
							url : "organization!findAll.action?companyNo="+_companyNo,
							data:function(n){
						    	  return {'parentOrgNo':n.attr ? n.attr("orgNo").replace("node_",""):0};//types is in action
							},
							cache: false}
					},
					plugins : ["themes", "json_data", "ui", "crrm"]

				})
				//.bind('loaded.jstree', function(e,data){data.inst.open_all(-1);})
				.bind('select_node.jstree',function(e,data){
					var orgType=data.rslt.obj.attr("orgType");
					if(orgType!='innerPanel' && orgType!='servicePanel'){
						$(namePut).val(data.rslt.obj.attr("orgName"));
						$(idPut).val(data.rslt.obj.attr("orgNo"));
						/*$(windowPanel).dialog('close');//关闭窗口*/
						layer.closeAll();
						$('#search_user_orgType').val('');
						if(orgType=='innerPanel'){		
							$('#search_user_orgType').val('allInner');
							return;
						}
						if(orgType=='ROOT'){
							$('#user_orgName').val('');
							$('#user_search_orgName').val('');
							$('#itsop_user_orgName').val('');
							$('#search_requestorOrgName').val('');
							$('#search_assigneGroupName').val('');
							return;
						}
						if(orgType=='servicePanel'){		
							$('#search_user_orgType').val('allService');
							return;
						}
					}
				});
			},
			//选择用户
			selectUser:function(){
				var url="user!find.action?userQueryDto.companyNo=1";
				$.post(url,function(res){
					var htmls="<table style='font-family: 微软雅黑;'><tr><th width='100px'>登录名</th><th width='100px'>角色名</th><th width='150px'>邮件地址</th><th width='100px'>所属部门</th></tr>";
					obj = res.data;
					for ( var i = 0; i < res.data.length; i++) {
						htmls = htmls+"<tr class='showUser' onclick='itsm.request.publicAddRequest.setUserInfo("+i+");'>" +
								"<td align='center' width='100px'>"+res.data[i].loginName+"</td>" +
								"<td align='center' width='100px'>"+res.data[i].lastName+"</td>" +
								"<td align='center' width='100px'>"+res.data[i].email+"</td>" +
								"<td align='center' width='100px'>"+res.data[i].orgName+"</td>" +
								"</tr>";
					}
					htmls = htmls + "</table>";
					$("#show_user_div").html(htmls);
					$.layer({
				        type : 1,
				        loading : {type : 2},
				        title : false,
				        offset : [($HEIGHT-220)/2, ''],
				        moveType: 1,
				        page: {
				        	dom: '#show_user_div' 
				        },
				        shade : [0.3 , '#000' , true],
				        fadeIn: 300,
				        border : [4 , 0.1 , '#000', true],
				        area : ['450px','220px']
				    });
				});
			},
			setUserInfo:function(ids){
				$("#requestEmail").val(obj[ids].email);
				$("#requestPhone").val(obj[ids].phone);
				$("#requestUserName").val($.trim(obj[ids].fullName));
				$("#requestUserId").val(obj[ids].userId);
				layer.closeAll();
			},
			addrequestupload:function(types){
		        var FileName = thisFileValue;
			    FileName = FileName.substring(FileName.lastIndexOf('.')+1, FileName.length).toLowerCase(); 
			    itsm.request.publicAddRequest.uploadrequestimage(FileName,types);
			},
			uploadrequestimage:function(FileName,types){
				var imageFileNametoreuqest=new Date().getTime()+"."+FileName;
				$.ajaxFileUpload({
					url:'uploadFile!uploadAttr.action',
		            fileElementId:'file', 
		            secureuri: false,
		            dataType:'string',
					success: function (data, status){
						var data =data.replace("<pre>","").replace("</pre>","");
						var obj = eval('(' + data + ')');;
						$('#file').val("");
						$('#fileput').val(obj.msg);
						$("#requestattchementStr").val(thisFileValue+"=="+obj.msg+"=="+obj.fileSize+"-s-");
						if(types == "request"){
							itsm.request.publicAddRequest.saverequests();
						}else{
							itsm.request.publicAddRequest.saveDraft();
						}
					},
					error: function (data, status){
						layer.msg("上传文件失败!",1,8);
		            }
				});
			},
			//存草稿
			saveRequestDraft:function(){
				if(thisFileValue){
					itsm.request.publicAddRequest.addrequestupload("draft");
				}else{
					itsm.request.publicAddRequest.saveDraft();
				}
			},
			saveDraft:function(){
				 if($.trim($("#reques_tTitle").val()) == ""){
						layer.msg("请输入请求标题！",1,8);
						$("#reques_tTitle").focus();
						return false;
				 }
				itsm.request.publicAddRequest.setDisabledNo();
				 $.each($("#idForm input[type='text']").add("textarea"),function(ind,val){
						$(this).val(wstuo.tools.xssUtil.html_encode($(this).val()));
				 });
	        	var url="draft!saveRequestDraft.action";
	        	$.post(url,$("#idForm").serialize(),function(){
	        		$("#idForm")[0].reset();
	        		$("#editRequestDraftID").val(0);
	        		$('#filename').html('');
	        		$("#pages div").remove();
	        		data=[];
	        		page=1;
	        		itsm.request.publicAddRequest.query();
	        		layer.msg("保存成功！1秒后关闭",1,1);
	        		itsm.request.publicAddRequest.setDisabled();
	        	}); 
			},
			//查询
			query:function(){
		    	var requestDraftUrl="draft!findPageRequestDraft.action";
		    	var loginName=$("#loginName").val();
		    	var parm={'queryDTO.creator':loginName,'queryDTO.loginName':loginName,'rows':'7','page':page,'sidx':'requestDraftID','sord':'desc'};
		    	$.post(requestDraftUrl,parm,function(res){
		    		if(res.data.length==0){
		    			page--;
		    			layer.msg("没有更多了！", 1, 8);
		    		}
		    		for(var i=0;i<res.data.length;i++){
		    			data.push(res.data[i]);
		    			$("#pages").append('<div onclick="itsm.request.publicAddRequest.doShow('+((page-1)*7+i)+');" ondblclick="itsm.request.publicAddRequest.doDel(this,'+res.data[i].requestDraftID+');" class="btn-glow msgs" style="display:none;width: 85%;margin-left: 7px;margin-bottom: 6px;"><span class="word" style="color:blue;font-size:14px;">'+res.data[i].requestDraftName+'</span></br><i class="icon-time"></i> 保存于 '+res.data[i].createTime+'</div>');
		    		}
		    		$(".msgs").fadeIn();
		    		$("#pages").append('<div onclick="itsm.request.publicAddRequest.showMore(this);" class="btn-glow primary" style="width: 85%;margin-left: 7px;margin-bottom: 5px;"><i class="icon-refresh"></i> 查看更多 </div>');
		    		document.getElementById('pages').scrollTop = document.getElementById('pages').scrollHeight;
		    	});
		    },
		    findPage:function(){
		    	var requestDraftUrl="draft!findPageRequestDraft.action";
		    	var loginName=$("#loginName").val();
		    	var parm={'queryDTO.creator':loginName,'queryDTO.loginName':loginName,'rows':'7','page':page,'sidx':'requestDraftID','sord':'desc'};
		    	$.post(requestDraftUrl,parm,function(res){
		    		for(var i=0;i<res.data.length;i++){
		    			data.push(res.data[i]);
		    			$("#pages").append('<div onclick="itsm.request.publicAddRequest.doShow('+((page-1)*7+i)+');" ondblclick="itsm.request.publicAddRequest.doDel(this,'+res.data[i].requestDraftID+');" class="btn-glow msgs" style="display:none;width: 85%;margin-left: 7px;margin-bottom: 6px;"><span class="word" style="color:blue;font-size:14px;">'+res.data[i].requestDraftName+'</span></br><i class="icon-time"></i> 保存于 '+res.data[i].createTime+'</div>');
		    		}
		    		$(".msgs").fadeIn();
		    		$("#pages").append('<div onclick="itsm.request.publicAddRequest.showMore(this);" class="btn-glow primary" style="width: 85%;margin-left: 7px;margin-bottom: 5px;"><i class="icon-refresh"></i> 查看更多 </div>');
		    		document.getElementById('pages').scrollTop = document.getElementById('pages').scrollHeight;
		    	});
		    },
		    findOrg:function(){
		    	var sUrl1="organization!findAll.action?companyNo=1&_=1399187842670&parentOrgNo=0";
		        $.post(sUrl1,function(data){
		        });
		    },
		    findEvent:function(){
		    	 var sUrl2="event!getCategoryTree.action?parentEventId=0&types=Request&num=0";
		         $.post(sUrl2,function(data){
		         	//alert(data.data.length)
		         });
		    },
		    doShow:function(ids){
		    	var json=decodeURI(data[ids].requestJson);
		    	json = json.replace(/&amp;/g,"','");
		    	json = json.replace(/=/g,"':'");
		    	json =  "{'"+json +"'}";
		    	json = json.replace(/\'/g,"\"");
		    	json = json.replace(/templateDTO./g,"");
		    	json = json.replace(/requestDTO./g,"");
		    	var obj = eval('(' + data[ids].requestJson + ')');
		    	$("#addRequestCategoryNos").val(obj.requestCategoryNo);
		    	$("#addRequestCategoryName").val(obj.requestCategoryName.replace("%2F","/"));
		    	$("#addRequestServiceDirIds").val(obj.serviceDirIds);
		    	$("#addRequestServiceDirName").val(obj.requestServiceDirName);
		    	$("#reques_tTitle").val(wstuo.tools.xssUtil.html_code(obj.etitle));
		    	$("#org_orgNo").val(obj.organizationNo);
		    	$("#orgNames").val(obj.orgName);
		    	$("#textContent").val(wstuo.tools.xssUtil.html_code(obj.edesc));
		    	if(obj.email)
		    		$("#requestEmail").val(obj.email.replace("%40","@"));
		    	$("#requestPhone").val(obj.phone);
//		    	$("#requestUserName").val(obj.creator);
		    	$("#requestUserId").val(obj.createdByNo);
		    	$("#officeAddress").val(obj.officeAddress);
		    	$("#requestMoblie").val(obj.moblie);
		    	$("#requestattchementStr").val(obj.attachmentStr);
		    	$("#editRequestDraftID").val(data[ids].requestDraftID);
		    	$("#filename").html(obj.attachmentStr.split('==')[0]);
		    	//	$("#fileput").val(obj.attachmentStr.substring(0,obj.attachmentStr.indexOf("%")));
		    },
		    doDel:function(event,ids){
		    	if(confirm("确认删除草稿?")){
		    		$.post("draft!deleteRequestDraftById.action?ids=" + ids,function(data){
		    			if(data){
		    				$(event).fadeOut(function(){
		    					$(event).remove();
		    				});
		    			}
		    		});
		   		}
		    },
		    checkPhone:function(){
		    	var phone = $("#requestMoblie").val();
		    	if($.trim(phone) == ""){
		    		return false;
		    	}	
		    	var filter = /^((\+?86)|(\(\+86\)))?1\d{10}$/;
				if (filter.test(phone)) {
				    return true;
				}
				return false;
		    },
		    checkMail:function (){
				var email = $("#requestEmail").val();
				if($.trim(email) == ""){
					return false;
				}
			    var filter  = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
			    if (filter.test(email)) {
			        return true;
			    }
			    return false;
			},
			checkTel:function(){
				var tel = $("#requestPhone").val();
				
				if(tel == ""){
					return true;
				}
				var filter = /^((\+?86)|(\(\+86\)))?\d{3,4}-\d{7,8}(-\d{3,4})?$/; 
				if (filter.test(tel)) {
				    return true;
				}
				return false;
			},
			showMore:function(event){
		    	page++;
		    	$(event).remove();
		    	itsm.request.publicAddRequest.query();
		    },
		    //解锁表单
		    setDisabledNo:function(){
//		    	$('#fileput').attr('disabled',false);
//				$('#addRequestCategoryName').attr('disabled',false);
//				$('#orgNames').attr('disabled',false);
		    },
		    setDisabled:function(){
//		    	$('#fileput').attr('disabled','disabled');
//				$('#addRequestCategoryName').attr('disabled','disabled');
//				$('#orgNames').attr('disabled','disabled');
//				$('#addRequestServiceDirName').attr('disabled','disabled');  
//				$('#requestUserName').attr('disabled','disabled');
		    },
		    selectServiceDir:function(){
		    	itsm.request.publicAddRequest.showKnowledgeServiceTree('#show_service_dir_win','#show_service_dir_tree',function(e,data){
					//if(data.rslt.obj.attr('state')==null || data.rslt.obj.attr('state')=='' || data.rslt.obj.attr('state')==undefined){
						$('#addRequestServiceDirName').val(data.rslt.obj.attr('cname'));
						$('#addRequestServiceDirIds').val(data.rslt.obj.attr('id'));
						layer.closeAll();
					/*}else{
						layer.msg("请选择子节点!",1,8);
					}*/
					
				});
		    },
		    /**
		     * 加载用户信息
		     */
		    loadUserInfo:function(){
		    	if(userId){
			    	$.post("user!findUserById.action","userDto.userId="+userId,function(data){
			    		if(!$("#requestEmail").val()){
			    			$("#requestEmail").val(data.email);
			    		}
			    		if(!$("#officeAddress").val()){
			    			$("#officeAddress").val(data.officeAddress);
			    		}
			    		if(!$("#requestPhone").val()){
			    			$("#requestPhone").val(data.phone);
			    		}
			    		if(!$("#requestMoblie").val()){
			    			$("#requestMoblie").val(data.moblie);
			    		}
			    	});
		    	}
		    },
		    /**
			 * 显示知识库服务目录
			 */
			showKnowledgeServiceTree:function(showDiv,treeDIV,selectNode){
				 $(showDiv).jstree({
						json_data: {
							ajax: {
							  type:"post",
							  url : "event!getCategoryTree.action?num=0",
						      data:function(n){
						    	  return {'types': 'Service','parentEventId':n.attr ? n.attr("id").replace("node_",""):0};//types is in action
						      },
			                  dataType: 'json',
			                  cache:false
						 	}
						},
					"plugins" : ["themes", "json_data","cookies", "ui", "crrm"]
				})
				//.bind('loaded.jstree', function(e,data){data.inst.open_all(-1);})
				.bind("select_node.jstree", function(e,data){
					selectNode(e,data);
				 });
				 
				 $.layer({
				        type : 1,
				        loading : {type : 2},
				        title : 'Esc退出',
				        offset : [($HEIGHT-220)/2, ''],
				        moveType: 1,
				        page: {
				        	dom: showDiv 
				        },
				        shade : [0.1 , '#000' , true],
				        fadeIn: 300,
				        border : [4 , 0.1 , '#000', true],
				        area : ['250px','350px']
				 });
				 $(".xubox_main").css("overflow-y","scroll");
				 $(".xubox_main").css("overflow-x","hidden");
				 
			},
			init:function(){
			/*	$("#org_orgNo").val(userOrgNo);
				$("#orgNames").val(userOrgName);*/
				//提交方法
				$("#submit").click(function(){
					if($("#addRequestServiceDirName").val() == ""){
						layer.msg("请选择服务目录！",1,8);
						return false;
					}
					if($("#addRequestCategoryName").val() == ""){
						layer.msg("请选择请求分类！",1,8);
						return false;
					}
					if($.trim($("#reques_tTitle").val()) == ""){
						layer.msg("请输入请求标题！",1,8);
						$("#reques_tTitle").focus();
						return false;
					}
					if($.trim($("#textContent").val()) == ""){
						layer.msg("请输入请求描述！",1,8);
						$("#textContent").focus();
						return false;
					}
					/*if($.trim($("#requestEmail").val()) == ""){
						layer.msg("请输入E-mail！",1,8);
						$("#requestEmail").focus();
						return false;
					}

					if($.trim($("#officeAddress").val()) == ""){
						layer.msg("请输入办公地址！",1,8);
						$("#officeAddress").focus();
						return false;
					}
					if($.trim($("#requestMoblie").val()) == ""){
						layer.msg("请输入手机号码！",1,8);
						$("#requestMoblie").focus();
						return false;
					}
					if($("#requestUserName").val() == ""){
						layer.msg("请选择联系人！",1,8);
						return false;
					}
					if(!itsm.request.publicAddRequest.checkMail()){
						layer.msg("E-mail格式不正确！", 1, 8);
						$("#requestEmail").focus();
						return false;
					}
					if(!itsm.request.publicAddRequest.checkTel()){
						layer.msg("电话格式不正确！", 1, 8);
						$("#requestPhone").focus();
						return false;
					}
					if(!itsm.request.publicAddRequest.checkPhone()){
						layer.msg("手机号码格式不正确", 1, 8);
						$("#requestMoblie").focus();
						return false;
					}
					*/
//					itsm.request.publicAddRequest.setDisabledNo();
					itsm.request.publicAddRequest.saveRequest();
				});
				//请求分类
				$("#sel1").click(function(){itsm.request.publicAddRequest.selectRequestCategory();});
				$("#addRequestCategoryName").click(function(){itsm.request.publicAddRequest.selectRequestCategory();});
				//机构
				/*$("#sel2").click(function(){itsm.request.publicAddRequest.selectOrg();});
				//用户
				$("#sel3").click(function(){itsm.request.publicAddRequest.selectUser();});*/
				//附件
				//$("#sel4").click(function(){$("#file").click();});
				//附件
				$("#file").change(function(){
					var fileExt='*.doc;*.docx;*.xls;*.rar;*.zip;*.txt;*.pdf;*.jpg;*.png;*.gif;*.bmp;*.swf;*.xlsx;*.gif;*.jpg;*.png;*.bmp;*.ppt;*.vsd;*.pptx';
					if(fileExt.indexOf(this.value.substring(this.value.lastIndexOf('.')).toLowerCase())>0){
						thisFileValue = this.value;
						$('#filename').html('');
					}else{
						layer.msg("该文件不能上传！！",1,8);
						this.value='';
					}
						
					
		        });
				//草稿
				$("#saveDraft").click(function(){
					itsm.request.publicAddRequest.saveRequestDraft();
		        });
				//重置表单
//				$("#resForm").click(function(){$("#idForm")[0].reset();});
				$("#resForm").click(function(){
					$('#addRequestServiceDirIds').val('');
					$('#addRequestServiceDirName').val('');
					$('#compId').val('');
					$('#addRequestCategoryNos').val('');
					$('#addRequestCategoryName').val('');
					$('#reques_tTitle,#file,#requestattchementStr').val('');
					$('#textContent,#requestEmail,#officeAddress,#requestPhone,#requestMoblie').val('');
					$("#file").after('<input style="width: 420px;" name="filedata" id="file" type="file">');
					$("#file:first").remove();
				});
				//查询
				page=1;
				itsm.request.publicAddRequest.findPage();
				itsm.request.publicAddRequest.loadUserInfo();
				//机构
				/*itsm.request.publicAddRequest.findOrg();
				itsm.request.publicAddRequest.findEvent();*/
				$('.slider-button').click(function() {
					if ($(this).hasClass("on")) {
		                 $(this).removeClass('on').html($(this).data("off-text"));  
		                 $("#edit").show();
		                 $("#edit").animate({width:'250px'}, 200);
		             } else {
		                 $(this).addClass('on').html($(this).data("on-text"));
		                 $("#edit").animate({width:'0px'},200, function(){
		                 	$("#edit").hide();
		                 });
		             }
				});
				itsm.request.publicAddRequest.setDisabled();
				//选择服务目录
				$("#selectServiceDir_linkBut").click(function(){itsm.request.publicAddRequest.selectServiceDir();});
				$("#addRequestServiceDirName").click(function(){itsm.request.publicAddRequest.selectServiceDir();});
			}
	}
}();
//载入
$(document).ready(itsm.request.publicAddRequest.init);