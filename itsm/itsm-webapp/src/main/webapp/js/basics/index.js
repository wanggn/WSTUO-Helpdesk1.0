﻿$package('basics.index');
$import("wstuo.includes");
basics.index=function(){
	var msie = navigator.userAgent.match(/msie/i);
	var cur_url='';//当前URl
	var old_url='';//上一个URl
	return {
		showIndexLogo:function(){
			var url = 'organization!findCompany.action?companyNo='+companyNo;
			$.post(url,function(res){
			    var src = '../images/logo2.png';
				if($(res.logo)!=''){
				    src = "logo.jsp?picName=" + res.logo;
				}
				$('#index_logo').attr('src',src);
				var name=res.orgName;
				if(name.length>5)name=name.substr(0,5);
				$("#index_Name").text(name);
			});	
		},
		fullsearch:function(){
			var type=$.cookie('fullsearchType');
			if(type=='request'){
				basics.index.initContent("request/requestMain.jsp?fullsearch=yes");
			}else if(type=='knowledge'){
				basics.index.initContent("wstuo/knowledge/knowledgeMain.jsp?fullsearch=yes");
			}
			return false;
		},
		getCur_url:function(){
			return cur_url;
		},
		loadDefaultCompany:function(companyNoId,companyNameId){
			$(companyNoId).val(companyNo);
			$(companyNameId).val(companyName);
		},
		/**
		 * 初始化表单验证
		 */
		initValidate:function(){
			$("form[event]").each(function(){
				var rules='{';
				$(this).find('[validType]').each(function(){
					var name=$(this).attr('name');
					var valid=$(this).attr('validType');
					if(valid.indexOf("[")>0){
						rules+="'"+name+"':{"+valid.substring(0,valid.indexOf("["))+":"+valid.substring(valid.indexOf("["))+"},"
					}
					else
						rules+="'"+name+"':{"+valid+":true},"
				});
				if(rules.length>1){
					rules=rules.substring(0, rules.length-1)+"}";
				}else
					rules+="}";
				$(this).submit(function(){return false});
				$(this).validate({
				    rules:eval('('+rules+')'),
					errorPlacement: function ( error, element ) {
						$(element).tooltip({
							placement:'bottom',
							title:error.text()
						});
					},
					success: function ( label, element ) {
						$(element).tooltip('destroy');
					},
					submitHandler: function (e) {
						eval($(e).attr("event")+"()");
					}
				});
			});
		},
		/**
		 * 加载左部菜单
		 */
		initMenu:function(url){
			$.ajax({
				mimeType: 'text/html; charset=utf-8', 
				url: url,
				type: 'post',
				success: function(data) {
					$('#main-menu').html(data);
					initAccordion();//渲染菜单
					basics.index.initMenuEvent();
					basics.index.initActive();
				},
				dataType: "html",
				async: false
			});
		},
		refreshContent:function(){
			if(cur_url)
				basics.index.initContent(cur_url);
		},
		back:function(){
			if(old_url)
				basics.index.initContent(old_url);
		},
		/**
		 * 加载中部内容
		 */
		initContent:function(url){
			//window.location.hash = url;
			if(url!=cur_url)
				old_url=cur_url;
			cur_url=url;
			var data;
			if(url.indexOf("?")>0){
				var ele=url.split("?");
				url=ele[0];
				data=ele[1];
			}
			$.ajax({
				mimeType: 'text/html; charset=utf-8',
				url: url,
				type: 'post',
				data:data,
				success: function(data) {
					$('.ui-dialog').remove();
					$('#ajax-content').html(data);
					docReady();
					basics.index.initValidate();
				},
				error: function (jqXHR, textStatus, errorThrown) {
					basics.index.initContent("errors/404.jsp");
				},
				dataType: "html",
				async: false
			});
		},
		initActive:function(){
			$("#main-menu").find(".link").click(function(e){
				if (msie) e.which = 1;
		        if (e.which != 1 ) return;
				$("#main-menu li").removeClass('active');
				$(this).parent().addClass('active');
			});	
		},
		initMenuEvent:function(){
			$("#main-menu").find(".ajax-link").click(function(e){
				if (msie) e.which = 1;
		        if (e.which != 1 ) return;
				e.preventDefault();
				$("#main-menu li").removeClass('active');
				$(this).parent().addClass('active');
				var url=$(this).attr('href');
				common_url+='@'+url+'='+$(this).text();
				$.cookie('common_url', common_url, {expires: 365});
				basics.index.initContent($(this).attr('href'));
			});
		},
		init:function(){
			basics.index.showIndexLogo();
			basics.index.initContent("view!findAllViewAndUserView.action?queryDTO.customType=1&queryDTO.userName="+userName);
			wstuo.includes.loadSelectUserIncludesFile();
			wstuo.includes.loadImportCsvIncludesFile();
			basics.index.initMenuEvent();
			$(".top-menu").find(".ajax-link").click(function(e){
				if (msie) e.which = 1;
		        if (e.which != 1) return;
				if($(this).attr('href')=="#")return false;
				e.preventDefault();
				$(".top-menu").find("li").removeClass('active');
				$(this).parent().addClass('active');
				if($(this).attr('menuUrl'))
					basics.index.initMenu($(this).attr('menuUrl'));
				basics.index.initContent($(this).attr('href'));
			});
			$(document).ajaxError(function(e, xhr, settings, exception) {
				//隐藏进程
				endProcess();
				endLoading();
				if(xhr.status==606){
					msgAlert(i18n['common_systemTimeout'],'info');
					setTimeout(function(){
						location.reload();
					},2000);
				}
				
				if(xhr.status==403){
					msgAlert(i18n['error403']+'<a href="http://wstuo.com"  target="_blank">'+i18n['contactUs']+'</a>','error');
				};
				if(xhr.status==404){
					msgAlert(i18n['error404']+'<a href="http://wstuo.com"  target="_blank">'+i18n['contactUs']+'</a>','error');
					//location.reload();//404也刷新页面
				};
				if(xhr.status==500){
					msgAlert(i18n['error500']+'<a href="http://wstuo.com"  target="_blank">'+i18n['contactUs']+'</a>','error');
					/*
					$('#systemErrorInfo').html('');
					var msg=xhr.responseText;
					if(msg!=null && msg.indexOf("org.hibernate.exception.DataException")!=-1){
						msgAlert(i18n['error_data_dataToLong'],'info');
						return;
					}

					var error500Content='<table width="100%">'+
					'<tr><td rowspan="2" style="font-size:50px"><i class="glyphicon glyphicon-remove-circle red"></i></td><td style="padding:5px"><br/>{errorMsg}</td></tr>'+
					'<tr><td  align="left"><a id="showHideErrorMsg500" href="#" style="">[{showHiddBut}]</a></td></tr>'+
					'<tr><td colspan="2"><div style="text-align:left"><span id="errorMsg500" style="width: autopx;height:autopx;display:none;">{errorInfo}<span></div></td></tr>'+
					'</table>'
					$('#systemErrorInfo').html(error500Content.replace('{errorMsg}',i18n['error500']+'<a href="http://wstuo.com"  target="_blank">'+
							i18n['contactUs']+'</a>').replace('{showHiddBut}',i18n['lable_showHide_errorMsg']));//.replace('{errorInfo}',xhr.responseText))
					$('#showHideErrorMsg500').click(function(){$('#errorMsg500').toggle()});
					windows('systemErrorWin',{width:420,height:200});*/
					return;
					
				};
			});
		}
	}
}();
$(function(){basics.index.init()});
