$package('wstuo.affiche');
$import('wstuo.tools.xssUtil');

 /**  
 * @author coney  
 * @constructor affiche
 * @description tools/Affiche.jsp
 * @date 2010-11-17
 */

wstuo.affiche.affiche = function() 
{
	this.addAfficheForm_formular;
	this.editAfficheForm_formular;
	return {
		/**
		 * 加载操作项
		 */
		taskActFormatter:function(){
			return $('#afficheActFormatterDiv').html();
		},
		/**
		 * 加载公告列表.
		 */
		showAfficheGrid:function() {
			var params = $.extend({},jqGridParams, {	
				url:'affiche!find.action?afficheQueryDto.companyNo='+companyNo,
				colNames:[i18n['number'],i18n['title'],i18n['title_startTime'],i18n['title_endTime'],i18n['msg_title_promulgator'],i18n['msg_title_publishTime'],i18n['common_action'],'','','',],
				colModel:[
			   		{name:'affId',width:60, align:'center',sortable:true},
			   		{name:'affTitle',width:150},
			   		{name:'affStart',sortable:true,align:'center',width:100,formatter:timeFormatterOnlyData},
			   		{name:'affEnd',sortable:true,align:'center',width:100,formatter:timeFormatterOnlyData},
			   		{name:'affCreator',sortable:true,align:'center',width:100},
			   		{name:'affCreaTime',sortable:true,align:'center',width:100,formatter:timeFormatter},
			   		{name:'act', width:80,sortable:false,align:'center', formatter:function(cell,event,data){
                 	   return $('#afficheActFormatterDiv').html().replace(/{affId}/g,event.rowId);
                    }},
			   		{name:'visibleState',hidden:true},
			   		{name:'noticeByEmal',hidden:true},
			   		{name:'affContents',hidden:true}
			   	],		
				jsonReader:$.extend({},jqGridJsonReader,{id:'affId'}),
				sortname:'affId',
				pager:'#affichePager'
			});
			$("#afficheGrid").jqGrid(params);
			$("#afficheGrid").navGrid('#affichePager',navGridParams);
			//列表操作项
			$("#t_afficheGrid").css(jqGridTopStyles);
			$("#t_afficheGrid").append($('#afficheGridToolbar').html());
			
			//自适应大小
			//setGridWidth("#afficheGrid",20);
		},
		/**
		 * @description 打开添加公告窗口.
		 */
		add_openwindow:function(){
			$('#affContents').val('');
			initCkeditor('affContents','Simple',function(){});
			resetForm('#addAfficheForm');
			$('#affiche_add_companyNo').val(companyNo);
			windows_beforeClose(
					'addAffiche',
					{width:520},
					function(){
						var instance = CKEDITOR.instances['affContents'];
					    if(instance)
					    {
					        CKEDITOR.remove(instance);
					        $('#cke_affContents').remove();
					    }
					}
			);
		},
		/**
		 * @description 执行添加公告.
		 */
		add_affiche:function(){
			var affTitle=$('#affTitle').val();
			$('#affTitle').val(wstuo.tools.xssUtil.html_encode(trim(affTitle)));
				startLoading();
				var edesc="";
				var oEditor = CKEDITOR.instances.affContents;
				edesc=trim(oEditor.getData());
				if(edesc.length>0){
					$('#affContents').val(edesc);
					var _param = $('#addAffiche form').serialize();
					$('#addAffiche').dialog('close');
					resetForm('#addAfficheForm');
					var _url='affiche!saveAffice.action';
					$.post(_url,_param,function(){
						$('#afficheGrid').trigger('reloadGrid');
						endLoading();
						msgShow(i18n['msg_add_successful'],'show');
					})
				}else{
					endLoading();
					msgAlert(i18n['warn_affContents_required'],'info');
				}
		},
		/**
		 * @description 工具栏删除公告.
		 */
		toolbar_delete_aff:function(){
			checkBeforeDeleteGrid('#afficheGrid', wstuo.affiche.affiche.deleteAffiche);
		},
		
		/**
		 * @description 工具栏删除公告.
		 */
		toolbar_delete:function(affId){
			msgConfirm(i18n['msg_msg'],'<br/>'+i18n['msg_confirmDelete'],function(){
				wstuo.affiche.affiche.deleteAffiche(affId);
			});
		},
		
		/**
		 * @description 执行删除公告.
		 */
		deleteAffiche:function(rowIds) {
			var _param = $.param({'ids':rowIds},true);
			$.post("affiche!delete.action", _param, function(){
				$('#afficheGrid').trigger('reloadGrid');
				msgShow(i18n['msg_deleteSuccessful'],'show');
			});
		},

		/**
		 * @description 打开搜索公告窗口.
		 */
		search_openwindow:function(){
			$('#searchVisibleState').click(function(){
	    		if($('#searchVisibleState').attr("checked")){
	    			$('#searchVisibleState').val(1)
	    		}else
	    			$('#searchVisibleState').val(0)
	    	});
	    	$('#affiche_search_companyNo').val(companyNo);

			windows('searchAffiche',{width:410});
		},


		/**
		 * @description 提交查询公告.
		 */
		search_do:function(){		
				var sdata = $('#searchAffiche form').getForm();
				var postData = $("#afficheGrid").jqGrid("getGridParam", "postData");
				$.extend(postData, sdata);  //将postData中的查询参数加上查询表单的参数		
				$('#afficheGrid').trigger('reloadGrid',[{"page":"1"}]);

			
		},

		
		/**
		 * @description 打开编辑公告窗口.
		 * @param rowData:行数据
		 */
		edit_openwindow:function(rowData) {
			var postUrl="affiche!findAfficheDTOById.action";
			var param="afficheDto.affId="+rowData.affId;
			$.post(postUrl,param,function(afficheDto){
				initCkeditor('editContents','Simple',function(){
					var oEditor = CKEDITOR.instances.editContents;
					oEditor.setData(afficheDto.affContents);
				});
				$("#editId").val(afficheDto.affId);
				$("#editTitle").val(wstuo.tools.xssUtil.html_code(afficheDto.affTitle));
				$("#edit_editStart").val(afficheDto.affStart);
				$("#edit_editEnd").val(afficheDto.affEnd);
				//$("#editContents").val(rowData.affContents);
				$("#edit_affCreator").val(afficheDto.affCreator);
				if(afficheDto.visibleState==1){
					$('#editVisibleState').attr("checked",'checked')
				}else{
					$('#editVisibleState').removeAttr("checked");
				}
				
				if(afficheDto.noticeByEmal=='false'){
					$('#editNoticeByEmal1').attr("checked",'false')
				}else{
					$('#editNoticeByEmal').attr("checked",'ture');
				}
				$('#affiche_edit_companyNo').val(companyNo);
				
				
				windows_beforeClose('editAffiche',{width:520},function(){
					var instance = CKEDITOR.instances['editContents'];
				    if(instance){
				        CKEDITOR.remove(instance);
				        $('#cke_editContents').remove();
				    }
				});
			});
			
		},

		
		/**
		 * @description 调用方法编辑公告.
		 */
		toolbar_edit : function(rowId){
			var rowData= $("#afficheGrid").jqGrid('getRowData',rowId);  // 行数据  
			wstuo.affiche.affiche.edit_openwindow(rowData);
		},
		
		/**
		 * @description 调用方法编辑公告.
		 */
		toolbar_edit_aff : function(){
			
			checkBeforeEditGrid('#afficheGrid', wstuo.affiche.affiche.edit_openwindow);
		},
		
		/**
		 * @description  提交编辑公告.
		 */
		edit_affiche:function(){
			var editTitle=$('#editTitle').val();
			$('#editTitle').val(wstuo.tools.xssUtil.html_encode(trim(editTitle)));
				var oEditor = CKEDITOR.instances.editContents;
				var edesc=trim(oEditor.getData());
				if(edesc.length>0){
					$('#editContents').val(edesc);
					var params=$("#editAffiche form").serialize();
				    var url = "affiche!update.action";
				    $.post(url, params, function(){ 
				    	$('#editAffiche').dialog('close');
				    	//$('#editAffiche').dialog('close')
				        $("#afficheGrid").trigger("reloadGrid");
				        msgShow(i18n['msg_edit_successful'],'show');
				    });
				}else{
					msgAlert(i18n['warn_affContents_required'],'info');
				}
		},
		/**
		 * @description 添加验证表单
		 */
		addFormValidator:function(){
			addAfficheForm_formular = $("#addAfficheForm").formValidator({
				forms: {			
					affTitle: {
						rules: {
							required: true
						},
						msg: {	
							required: i18n['validate_notNull']
						}
					},
					affContents: {
						rules: {
							required: true
						},
						msg:{
							required: i18n['validate_notNull']
						}
					},
					add_affStart:{
						rules: {
							required: true
						},
						msg:{
							required: i18n['validate_notNull']
						}
					},
					add_affEnd:{
						rules: {
							required: true,
							dateComparison:'add_affStart'
						},
						msg:{
							required: i18n['validate_notNull'],
							dateComparison:i18n['error_dateComparison']
						}
					}
				}
			});
			editAfficheForm_formular=$("#editAfficheForm").formValidator({
				
				
				forms: {			
					editTitle: {
						rules: {
							required: true
						},
						msg: {	
							required: i18n['validate_notNull']
						}
					},
					editContents:{
						rules: {
							required: true
						},
						msg:{
							required: i18n['validate_notNull']
						}
					},
					edit_editStart:{
						rules: {
							required: true
						},
						msg:{
							required: i18n['validate_notNull']
						}
					},
					edit_editEnd:{
						rules: {
							required: true,
							dateComparison:'edit_editStart'
						},
						msg:{
							required: i18n['validate_notNull'],
							dateComparison:i18n['error_dateComparison']
						}
					}
				}
			});
		},
		/**
		 * @description 初始化
		 */
		init:function(){	
			//绑定日期控件
			DatePicker97(['#add_affStart','#add_affEnd','#edit_editStart','#edit_editEnd','#searchStart1','#searchEnd1']);
			$("#affiche_loading").hide();
			$("#affiche_content").show();
			wstuo.affiche.affiche.showAfficheGrid();
		}

	};	
}();
$(document).ready(wstuo.affiche.affiche.init);