$package("wstuo.scheduledTask");
$import("wstuo.scheduledTask.scheduledTask")
$import('itsm.itsop.selectCompany');
if(isCIHave){
	$import("itsm.cim.configureItemUtil");
	$import('itsm.cim.ciCategoryTree');
}
$import("wstuo.dictionary.dataDictionaryUtil");
$import("wstuo.category.eventCategoryTree");
$import("wstuo.user.userUtil");
$import("wstuo.schedule.setMonthly");
$import('wstuo.knowledge.knowledgeTree');
$import('wstuo.category.serviceCatalog');

/**  
 * @author QXY  
 * @constructor users
 * @description 定期任务添加
 * @date 2011-10-11
 * @since version 1.0 
 */
wstuo.scheduledTask.addScheduledTask=function(){
	
	return {
		
		/**
		 * 保存定期任务
		 */
		saveScheduledTask:function(){
			if($('#scheduledTask_add_eavAttributet_form').form('validate')){
				if($("#addScheduledTimeSettings form").form('validate')){
					if($('#addScheduledTaskForm').form('validate')){
						if($("#addScheduledTimeSettings input[name='scheduledTaskDTO.timeType']:checked").val() == "month" && $("#monthDay_select").val()==null){
							msgAlert(i18n.scheduledTask_monthPlan_monthDayIsNotNull,'info');
						}else{
							var myDate=new Date() 
				    		var month=myDate.getMonth()+1;
							if(!DateComparison($('#add_scheduledTask_endTime_input').val(),myDate.getFullYear()+'-'+month+'-'+(myDate.getDate()-1))){
								var _edesc="";
								if(CKEDITOR.instances['add_scheduledTask_edesc']){
									var oEditor = CKEDITOR.instances.add_scheduledTask_edesc;
									_edesc = oEditor.getData();
								}
								var bool=trim(_edesc)==''?false:true;
								if(!bool){
									msgAlert(i18n['titleAndContentCannotBeNull'],'info');
								}else{
									$('#add_scheduledTask_edesc').val(_edesc);
									var frm = $('#addScheduledTask_layout form').serialize();
									var url = 'scheduledTask!saveScheduledTask.action';
									//调用
									startProcess();
									$.post(url,frm, function(res){
											endProcess();
										
											basics.index.initContent("../pages/wstuo/sysMge/sysMge_scheduledTask.jsp");
											
											$('#scheduledTasksGrid').trigger('reloadGrid');
											msgShow(i18n['msg_add_successful'],'show');
										
									});
								}
							}else{
								msgAlert(i18n['tip_endTime_cannot_be_before_startTime'],'info');
								$('#add_scheduledTask').tabs('select', i18n.scheduledTask_time_setting);
							}
						}
					}
				}else{
					$('#add_scheduledTask').tabs('select', i18n.scheduledTask_time_setting);
					//为了防止验证信息不在左上角显示
					$("#addScheduledTimeSettings form").form('validate');
				}
			}else{
				msgAlert(i18n.eavAttributet_notNull,'info');
				$('#add_scheduledTask').tabs('select', i18n.config_extendedInfo);
			}
		},
		/**
		 * 选择请求分类
		 * @param showAttrId 显示div的id
		 * @param dtoName 
		 */	
		selectRequestCategory:function(showAttrId,dtoName){
			wstuo.category.eventCategoryTree.showSelectTree('#request_category_select_window'
					,'#request_category_select_tree'
					,'Request'
					,'#add_scheduledTask_categoryName'
					,'#add_scheduledTask_categoryNo'
					,''
					,'',showAttrId,dtoName);
		},
		/**
		 * 初始化
		 */	
		init:function(){
			//绑定日期控件
			DatePicker97(['#add_scheduledTask_startTime_input','#add_scheduledTask_endTime_input']);
			$("#addScheduledTask_loading").hide();
			$("#addScheduledTask_panel").show();
			//设定月份总天数
			wstuo.schedule.setMonthly.setmonthDay('add_everyWhat_monthly','monthDay_select');
			wstuo.dictionary.dataDictionaryUtil.loadOptionsByCode('effectRange','#add_scheduledTask_effectRange');
			wstuo.dictionary.dataDictionaryUtil.loadOptionsByCode('seriousness','#add_scheduledTask_seriousness');
			wstuo.dictionary.dataDictionaryUtil.loadOptionsByCode('imode','#add_scheduledTask_imode');
			wstuo.dictionary.dataDictionaryUtil.loadOptionsByCode('level','#add_scheduledTask_level');
			wstuo.dictionary.dataDictionaryUtil.loadOptionsByCode('priority','#add_scheduledTask_priority');
			
			setTimeout(function(){
				getUploader('#add_scheduledTask_file','#add_scheduledTask_attachmentStr','#add_scheduledTask_success_attachment','');
				initCkeditor('add_scheduledTask_edesc','Simple',function(){});
			},0)
			
//			lazyInitEditor('#add_scheduledTask_edesc','streamline');
			
			$('#add_scheduledTask_companyName').click(function(){//选择公司
				itsm.itsop.selectCompany.openSelectCompanyWin('#add_scheduledTask_companyNo','#add_scheduledTask_companyName','#add_scheduledTask_createdName,#add_scheduledTask_createdNo');
			});
			//$('#add_scheduledTask_categoryName').click(wstuo.scheduledTask.addScheduledTask.selectRequestCategory);
			
			$('#add_scheduledTask_createdName').click(function(){
				wstuo.user.userUtil.selectUser('#add_scheduledTask_createdName','#add_scheduledTask_createdNo','','loginName',$('#add_scheduledTask_companyNo').val());
			})
			
			//绑定选择配置项
			$('#add_scheduledTask__ref_ci_btn').click(function(){
				itsm.cim.configureItemUtil.requestSelectCI('#scheduledTaskRelatedCIShow',$('#add_scheduledTask_companyNo').val(),'');
			});
			
			
			$('#saveScheduledTaskBtn').click(wstuo.scheduledTask.addScheduledTask.saveScheduledTask);//保存定期任务
			
			$('#add_scheduledTask_backList').click(function(){
	
				basics.index.initContent("../pages/wstuo/sysMge/sysMge_scheduledTask.jsp");
			
			});//返回列表
			
			//加载默认公司
			basics.index.loadDefaultCompany('#add_scheduledTask_companyNo','#add_scheduledTask_companyName');
			
/*			$("#add_scheduledTask_ref_requestServiceDirName").click(function(){
				wstuo.knowledge.knowledgeTree.selectKnowledgeServiceFilter('#knowledge_services_select_window','#knowledge_services_select_tree','#add_scheduledTask_ref_requestServiceDirName','#add_scheduledTask_ref_requestServiceDirNo');			
			});*/
			//服务目录
			$('#add_scheduledTask_service_add').click(function(){
				wstuo.category.serviceCatalog.selectServiceDir('#add_scheduledTask_serviceDirectory_tbody');
			});
			
		}
	}
}();
$(document).ready(wstuo.scheduledTask.addScheduledTask.init);
