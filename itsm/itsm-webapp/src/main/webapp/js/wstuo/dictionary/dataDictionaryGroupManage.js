 /**  
 * @author QXY  
 * @constructor dictionaryGroup
 * @description 数据字典分组管理
 * @date 2010-11-17
 * @since version 1.0 
 */ 
$import('wstuo.category.eventCategoryTree');
$import('wstuo.category.eventCategory');
var dictionaryGroup = function(){
	this.operator;
	/**
	 * @description 数据字典分组列表
	 * 
	 */
	this.show_grid=function(){		
		var params = $.extend({},jqGridParams, {
			url:'dataDictionaryGroup!find.action',
			colNames:['ID',i18n['name'],i18n['code'],i18n['type'],'',''],
			colModel:[
					{name:'groupNo',width:30,sortable:true,align:'center'},					
					{name:'groupName',width:100,sortable:true},			
					{name:'groupCode',width:100,sortable:true},
					{name:'groupType',width:100,sortable:true},
					{name:'dataFlag',hidden:true},
					{name:'treeNo',hidden:true}	
			    ],
		    viewrecords:true,
		    multiselect: true,
			jsonReader: $.extend(jqGridJsonReader, {id: "groupNo"}),
			sortname:'groupNo',
			onSelectRow:reloadItem,
			pager:'#dataDictionaryGroupsPager'
		});
		$("#dataDictionaryGroupsGrid").jqGrid(params);
		$("#dataDictionaryGroupsGrid").navGrid('#dataDictionaryGroupsPager',navGridParams);
		//列表操作项
		$("#t_dataDictionaryGroupsGrid").css(jqGridTopStyles);
		$("#t_dataDictionaryGroupsGrid").append($('#dataDictionaryGroupsToolbar').html());
		//自适应宽度
		setGridWidth("#dataDictionaryGroupsGrid",10);
	}
	this.reloadItem=function(id){
		var data=$("#dataDictionaryGroupsGrid").getRowData(id);
		if(data.groupType=='tree'){
			wstuo.category.eventCategoryTree.showCategoryTree('#dictionary_div_tree',data.groupCode,dictionaryGroup.saveCategory);
			$("#dictionary_tree").show();
			$("#dictionary_item").hide();
			$("#eventCategoryCode").val(data.groupCode);
		}else{
			var _url = 'dataDictionaryItems!find.action?dataDictionaryQueryDto.groupCode='+data.groupCode;	
			var _param= $.param({'dataDictionaryQueryDto.dname':'','dataDictionaryQueryDto.description':'',rows:20});								
			$('#dictionaryItemGrid').jqGrid('setGridParam',{url:_url,postData:_param,page:1,rows:20}).trigger('reloadGrid');
			$("#groupCode,#searchItem_groupCode").val(data.groupCode);
			$("#dictionary_tree").hide();
			$("#dictionary_item").show();
		}

	}
	/**
	 * 添加数据字典分组
	 */
	this.addDictionaryItem=function(){
		windows('addDataDictionaryGroupDiv',{title:i18n['newAdd'],width:400});
		operator = 'saveGroup';
	}
	/**
	 * 添加数据字典分组
	 */
	this.searchDictionaryItem=function(){
		windows('searchDataDictionaryGroupDiv',{width:400,modal: false});
	}
	 /**
	  * @description 新增数据字典组
	  * */
	this.saveDictionaryGroups = function(){

		var bb = $('#addDataDictionaryGroupDiv form').serialize();
		var url = "dataDictionaryGroup!"+operator+".action";
		var _groupNo=$('#add_groupNo').val();
		$.post(url, bb, function(){
			$('#addDataDictionaryGroupDiv').dialog('close');
			$('#dataDictionaryGroupsGrid').trigger('reloadGrid');
			//清空框内内容
			$('#addDataDictionaryGroupDiv [name="ddgDto.groupCode"]').val("").removeAttr("disabled");
			$('#addDataDictionaryGroupDiv [name="ddgDto.groupType"]').removeAttr("disabled");
			$('#addDataDictionaryGroupDiv [name="ddgDto.groupName"]').val("");
			$('#addDataDictionaryGroupDiv [name="ddgDto.groupNo"]').val("");
			msgShow(i18n['common_operation_success'],'info');
		});	
		
	}
   /**	

    * @description 删除数据字典组
    * */
	this.delDictionaryGroups = function(){		
		var rowIds = $('#dataDictionaryGroupsGrid').jqGrid('getGridParam','selarrrow');
		if(rowIds==""){
			msgAlert(i18n['selectDeleteDataTip'],'info');
		}else{
			var _param = $.param({'groupNo':rowIds},true);
			//条件的判断是否删除
			   if(_param != null && _param != '') {
				   for ( var _int = 0; _int < rowIds.length; _int++) {
					   var data = $('#dataDictionaryGroupsGrid').getRowData(rowIds[_int]);
					   if(data.dataFlag==1){
						   msgAlert(i18n['ERROR_SYSTEM_DATA_CAN_NOT_DELETE'],'info');
						   return;
					   }
				   }
			    	msgConfirm(i18n['tip'], '<br/>'+i18n['deleteComfirm'], function(){
			    		$.post("dataDictionaryGroup!removeGroup.action", _param, function(){
							$('#dataDictionaryGroupsGrid').trigger('reloadGrid');
							msgShow(i18n['deleteSuccess'],'info');
						}, "json");
			    	});
			   }
		}
	}
	 /**
	  * @description 编辑数据字典组
	  * */
	this.editDictionaryGroups = function() {		
		checkBeforeEditGrid('#dataDictionaryGroupsGrid',function(data){
			if(data.dataFlag==1){
				msgAlert(i18n['ERROR_SYSTEM_DATA_CAN_NOT_EDIT'],'info');
				return;
			}
			operator = 'mergeGroup';	
			$('#addDataDictionaryGroupDiv [name="ddgDto.groupCode"]').val(data.groupCode).attr("disabled",true);
			$('#addDataDictionaryGroupDiv [name="ddgDto.treeNo"]').val(data.treeNo);
			$('#addDataDictionaryGroupDiv [name="ddgDto.groupName"]').val(data.groupName);
			$('#addDataDictionaryGroupDiv [name="ddgDto.groupNo"]').val(data.groupNo);
			$('#addDataDictionaryGroupDiv [name="ddgDto.groupType"]').val(data.groupType).attr("disabled","disabled");
			windows('addDataDictionaryGroupDiv',{title:i18n['edit'],width:400,close:function(){
				resetForm("#addDataDictionaryGroupDiv form");
			}});
	   });
	}
	 /**
	  * @description 搜索数据字典组
	  * */
	this.searchDictionaryGroups = function() {	
		var _url = 'dataDictionaryGroup!find.action';
		var sdata = $('#searchDataDictionaryGroupDiv form').getForm();
		var postData = $("#dataDictionaryGroupsGrid").jqGrid("getGridParam", "postData");     
		$.extend(postData, sdata);
		$('#dataDictionaryGroupsGrid').jqGrid('setGridParam',{page:1,url:_url}).trigger('reloadGrid');
		return false;
	}
	/**
	 * 导出数据字典组
	 */
	this.exportDataDictionaryGroup=function(){
		$("#searchDataDictionaryGroupDiv form").submit();
	}
	return {
		/**
		 * 添加数据字典分组
		 */
		saveCategory:function(opt){
			if($("#eventCategoryCode").val()=="Request"){
				$('#'+opt+'_category_form').show();
			}else
				$('#'+opt+'_category_form').hide();
			windows(opt+'EventCategoryTree',{title:i18n[opt],width:400});
		},
		/**
		 * 初始化
		 * @private
		 */
		init: function(){
			//加载数据列表
			show_grid();
			//创建事件
			//$('#link_dataDictionary_add_ok').click(saveDictionaryGroups);
			$('#link_dataDictionary_search_ok').click(searchDictionaryGroups);
			$('#dataDictionaryGroupManage_export').click(exportDataDictionaryGroup);
			$.post("formCustom!findSimilarFormCustom.action?formCustomQueryDTO.type=request",function(data){
				$("#add_form,#edit_form").html('<option value="">--'+i18n.common_pleaseChoose+'--</option>');
				$.each(data,function(i,obj){
					$("#add_form,#edit_form").append('<option value="'+obj.formCustomId+'">'+obj.formCustomName+'</option>');
				});
			});
			wstuo.category.eventCategory.init();
		}
	}
}();
$(document).ready(dictionaryGroup.init);