$package('wstuo.slaMge');
/**  
 * @author WSTUO  
 * @constructor WSTO
 * @description SLA详细信息主函数.

 */
wstuo.slaMge.slaDetail=function(){
	
	
	this.slaDetailGrids=[];
	return{
		/**
		 * @description 列表伸缩
		 */
		slaDetailPanelExpandOrCollapse:function(){
			 $(slaDetailGrids).each(function(i, g) {
			      setGridWidth(g, 'slaDetailMainTab', 10);
			 });
		},
		/**
		 * @description 获取事件
		 */
		fitSLADetailsGrids:function(){
			$('#slaDetailWest').panel({
				onCollapse:wstuo.slaMge.slaDetail.slaDetailPanelExpandOrCollapse,
				onExpand:wstuo.slaMge.slaDetail.slaDetailPanelExpandOrCollapse,
				onResize:function(width, height){
				setTimeout(function(){
					wstuo.slaMge.slaDetail.slaDetailPanelExpandOrCollapse();
				},0);
			}
			});
		},
		
		/**
		 * 返回列表集
		 */
		sla_returnToList:function(){
			
			basics.index.initContent("../pages/wstuo/slaMge/slaMain.jsp");
	
		},
		
		/**
		 * @description 初始化
		 */
		init:function(){
/*			$("#SLAServiceManage_loading").hide();
			$("#SLAServiceManage_content").show();
*/
			setTimeout(function(){
				//wstuo.slaMge.slaDetail.fitSLADetailsGrids();
				setTimeout(function(){
					//实例化SLA规则列表
					wstuo.slaMge.slaDetail_ruleGrid.init();
					//实例化自动升级列表
					wstuo.slaMge.slaDetail_updateGrid.init();
				},100);
			},0);
			
			$('#sla_returnToList').click(wstuo.slaMge.slaDetail.sla_returnToList);
			
		}
	};
 }();
$(document).ready(wstuo.slaMge.slaDetail.init);
