$package('wstuo.customer');
$import('wstuo.orgMge.organizationTreeUtil');
 /**  
 * @author wstuo 
 * @constructor wstuo
 * @description itsop/ITSOPUserMain.jsp
 * @since version 1.0 
 */
wstuo.customer.ITSOPCustomer_userGrid = function() {
	this.itsopUserOperation;
	return {
		/**
		 * 加载用户列表.
		 */
		showGrid:function(){
			var _url="user!find.action";
			var _postData={'userQueryDto.companyNo':_itsop_user_companyNo};
			if($("#ITSOPCustomer_userGrid").html()!=''){
				$("#ITSOPCustomer_userGrid").jqGrid('setGridParam',{url:_url,postData:_postData}).trigger('reloadGrid');
			}else{
				var params = $.extend({},jqGridParams, {	
					url:_url,
					postData:_postData,
					colNames:['ID',i18n['title_user_loginName'],i18n['title_user_lastName'],i18n['title_user_firstName'],i18n['title_user_org'],
					          i18n['title_user_phone'],i18n['title_user_mobile'],'Email',i18n['title_org_role'],i18n['common_state'],
					          '','','','','','','','','','',''],
				 	colModel:[
				 	          {name:'userId',align:'center',width:30},
				 	          {name:'loginName',align:'center',width:80},
				 	          {name:'firstName',align:'center',width:30},
				 	          {name:'lastName',align:'center',width:50},
				 	          {name:'orgName',align:'center',sortable:false,width:80},
				 	          {name:'phone',align:'center',width:80},
				 	          {name:'moblie',align:'center',width:80},
				 	          {name:'email',align:'center',width:80},
				 	          {name:'roles',align:'left',width:80},
				 	          {name:'userState',align:'center',width:40,formatter:wstuo.user.userUtil.userStateFormat},
				 	          {name:'userState',hidden:true},
				 	          {name:'job',hidden:true},
				 	          {name:'password',hidden:true},
				 	          {name:'officePhone',hidden:true},
				 	          {name:'orgNo',hidden:true},
				 	          {name:'description',hidden:true},
				 	          {name:'fax',hidden:true},
				 	          {name:'msn',hidden:true},
				 	          {name:'userCost',hidden:true},
				 	          {name:'officeAddress',hidden:true},
				 	          {name:'remark',hidden:true}
			 	],
				jsonReader: $.extend(jqGridJsonReader, {id: "userId"}),
				sortname:'userId',
				pager:'#ITSOPCustomer_userGridPager'
				});
				$("#ITSOPCustomer_userGrid").jqGrid(params);
				$("#ITSOPCustomer_userGrid").navGrid('#ITSOPCustomer_userGridPager',navGridParams);
				//列表操作项
				$("#t_ITSOPCustomer_userGrid").css(jqGridTopStyles);
				$("#t_ITSOPCustomer_userGrid").append($('#ITSOPCustomer_userGridToolbar').html());
				
				setGridWidth("#ITSOPCustomer_userGrid",15);
				wstuo.customer.ITSOPCustomer_userGrid.setEvent();
			}
		},
		
		/**
		 * 根据客户加载用户列表.
		 * @param id 公司id
		 */
		showUserGridByCustomer:function(id){
			
			$('#itsop_user_search_companyNo,#itsop_user_companyNo').val(id);
			if(_itsop_user_companyNo==0){
				_itsop_user_companyNo=id;
				wstuo.customer.ITSOPCustomer_userGrid.showGrid();

			}else{
				_itsop_user_companyNo=id;
				var postData = $("#ITSOPCustomer_userGrid").jqGrid("getGridParam", "postData");
				$.extend(postData,{'companyNo':id,'userQueryDto.companyNo':id,'userQueryDto.email':'','userQueryDto.firstName':'','userQueryDto.lastName':'','userQueryDto.loginName':'','userQueryDto.orgNo':'','userQueryDto.limit':''});  
				$('#ITSOPCustomer_userGrid').jqGrid('setGridParam',{postData:postData}).trigger('reloadGrid',[{"page":"1"}]);
			}
			
		},
		
		/**
		 * 打开新增用户窗口.
		 */
		addUser_openWindow:function(){
			//隐显密码区域
			$('#itsop_reset_user_password').hide();			
			$('#itsop_user_password,#itsop_user_confirm_password').show();
			
			//移除只读属性
			$('#itsop_user_loginName').removeAttr("readonly");
			var _url = 'organization!getOrgByOrgNo.action';
			$.post(_url, 'orgNo='+_itsop_user_companyNo, function(data){
				$('#itsop_user_orgName').val(data.orgName);
				$('#itsop_user_orgNo').val(data.orgNo);
			});
			
			//重置表单
			resetForm('#add_edit_itsop_user_window form');
			$('#itsop_user_companyNo').val(_itsop_user_companyNo);
			wstuo.user.userUtil.add_loadRole(-1,"itsop_user_roles",true);
			
			itsopUserOperation='save';

		    windows('add_edit_itsop_user_window',{width:500});
			
			//$('#add_edit_itsop_user_window').dialog({autoOpen: true,width:710});

			//$('#ITSOP_User_Detail').tabs();//展开
			
			//$.parser.parse($('#add_edit_itsop_user_window'));
			
		},
		
		/**
		 * 提交新增用户
		 */
		saveUser:function(){
	
				var loginName= $('#itsop_user_loginName').val();
				$('#itsop_user_loginName').val(loginName.replace(/[ ]/g,""));
				
				var lastName= $('#itsop_user_lastName').val();
				$('#itsop_user_lastName').val(lastName.replace(/[ ]/g,""));
				
				var firstName= $('#itsop_user_firstName').val();
				$('#itsop_user_firstName').val(firstName.replace(/[ ]/g,""));
				//if($('#add_edit_itsop_user_window form').form('validate')){
					if(itsopUserOperation=='save'){
						/*if($('#itsopUserPassword').val().length<6){
							msgAlert(i18n['add_msg_trial_wrong_password'],'info');
						}*/
						var pass=$('#itsopUserPassword').val();
						var parent=/^[A-Za-z]+$/;
						/*if(pass.length<6){
							msgAlert(i18n.add_msg_trial_wrong_password,'info');
						}else if(!isNaN(pass)){
							msgAlert(i18n.lable_password_num,'info');
						}
						else if(parent.test(pass)){
							msgAlert(i18n.lable_password_chart,'info');
						}
						else{*/
							$.post('user!userExist.action','userDto.userId='+$("#itsop_user_userId").val()+'&userDto.loginName='+$("#itsop_user_loginName").val(),function(data){
								if(data){
									
									wstuo.customer.ITSOPCustomer_userGrid.saveOrEditUser();
										
								}else{
									msgShow(i18n['error_userExist'],'show');
								}
							});
						//}
					}else{
						
						wstuo.customer.ITSOPCustomer_userGrid.saveOrEditUser();
					}
				//}	
		},
		
		/**
		 * 添加或编辑用户.
		 */
		saveOrEditUser:function(){
			
			var _form = $('#itsop_user_form').serialize();
			var _url = 'user!'+itsopUserOperation+'.action';
			if(_form.indexOf('userDto.roleIds')>-1){
				
				startProcess();
				$.post(_url, _form, function(){
					endProcess();
					
					msgShow(i18n['common_operation_success'],'show');
					$('#add_edit_itsop_user_window').dialog('close');
					$('#ITSOPCustomer_userGrid').trigger('reloadGrid');
				});
			}else{
				msgShow(i18n['msg_plase_set_role'],'show');
			}
		},
		
		/**
		 * 打开编辑窗口.
		 */
		editUser_openWindow:function(){
			checkBeforeEditGrid('#ITSOPCustomer_userGrid',function(rowData){
				itsopUserOperation = 'merge';
				$.post('user!findUserDetail.action','userDto.userId='+rowData.userId,function(data){
					$('#itsop_user_companyNo').val(_itsop_user_companyNo);
					$('#itsop_user_userId').val($vl(data.userId));
					$('#itsop_user_loginName').val($vl(data.loginName));
					$('#itsop_user_firstName').val($vl(data.firstName));
					$('#itsop_user_lastName').val($vl(data.lastName));
					$('#itsop_user_email').val($vl(data.email));
					$('#itsopUserPassword,#itsopUserPasswordConfirm').val($vl(data.password));
					$('#itsop_reset_user_password').show();
					$('#itsop_user_password,#itsop_user_confirm_password').hide();
					
					$('#itsop_user_icCard').val($vl(data.icCard));
					$('#itsop_user_pinyin').val($vl(data.pinyin));
					$('#itsop_user_birthday').val(timeFormatter2($vl(data.birthday)));
					$('#itsop_user_job').val($vl(data.job));
					$('#itsop_user_orgName').val($vl(data.orgName));
					$('#itsop_user_orgNo').val($vl(data.orgNo));
					//$('#ITSOPUser_password').val($vl(data.password));
					
					$('#itsop_user_position').val($vl(data.position));
					if(data.sex==false){
						$('#itsop_user_sex_woman').attr("checked",'false');
					}else
					{
						$('#itsop_user_sex_man').attr("checked",'ture');
					}
					
	
					$('#itsop_user_moblie').val($vl(data.moblie));
					$('#itsop_user_phone').val($vl(data.phone));
					$('#itsop_user_fax').val($vl(data.fax));
					$('#itsop_user_msn').val($vl(data.msn));
					$('#itsop_user_officeAddress').val($vl(data.officeAddress));
					$('#itsop_user_description').val($vl(data.description));
	
			
					//单选按钮选择项
					if(data.userState==false || data.userState=='false'){
						$('#itsop_user_userState1').attr("checked",'false');
					}else{
						$('#itsop_user_userState').attr("checked",'ture');
					}
					
					$('#itsop_user_loginName').attr("readonly",'readonly');
					wstuo.user.userUtil.add_loadRole(data.userId,"itsop_user_roles",true);
					
					
					$('#add_edit_itsop_user_window').dialog({autoOpen: true,width:500});
					
					//$('#ITSOP_User_Detail').tabs();//展开
					//$.parser.parse($('#add_edit_itsop_user_window'));
				})
				
			});
		},
	
		
		/**
		 * 删除用户.
		 */
		deleteUser:function(){
			var rowId = $("#ITSOPCustomer_userGrid").getGridParam('selrow');
			if(rowId!=null && rowId!=''){
				checkBeforeDeleteGridInLine('#ITSOPCustomer_userGrid',function(rowId){
					var _param = $.param({'ids':rowId},true);
					$.post("user!delUser.action", _param, function(data){
							$('#ITSOPCustomer_userGrid').trigger('reloadGrid');
							msgShow(i18n['msg_user_deleteSuccessful'],'show');
					});				
				});
			}else{
				msgShow(i18n['msg_atLeastChooseOneData'],'show');
			}
			
			
		},
		/**
		 * 设置用户角色
		 */
		setUserRole_openWindow:function(){
			checkBeforeMethod('#ITSOPCustomer_userGrid',function(ids){
				if(ids.length>1){
					wstuo.user.userUtil.add_loadRole(null,"set_itsop_user_role_window",true);
					
				}else{
					wstuo.user.userUtil.add_loadRole(ids[0],"set_itsop_user_role_window",true);
				}
				windows('set_itsop_user_role_window',{width:400});
			});

		},
		
		/**
		 * 修改角色
		 */
		updateUserRole:function(){
			checkBeforeMethod('#ITSOPCustomer_userGrid',function(ids){
				var _param = $.param({'ids':ids},true);
				var _form = $('#set_itsop_user_role_window form').serialize();
				
				if(_form.indexOf('userDto.roleIds')>-1){
					$.post("user!updateRoleByUserId.action",_form+'&'+_param, function(){
						
						$('#set_itsop_user_role_window').dialog('close');
						$('#ITSOPCustomer_userGrid').trigger('reloadGrid');
						
						msgShow(i18n['msg_user_roleSetSuccessful'],'show');

					});
				}else{
					msgShow(i18n['msg_plase_set_role'],'show');
				}
			});
		},
		
		/**
		 * 搜索用户
		 */
		searchUser:function(){
			//判断是否根目录
			var orgNo=$('#itsop_user_search_orgNo').val();
			
			if(orgNo=='1'){
				$('#itsop_user_search_orgNo').val($('#ITSOPUserGrid').getGridParam('selrow'));
			}
			var _sdata=$('#search_itsop_user_window form').getForm();
			var _postData = $("#ITSOPCustomer_userGrid").jqGrid("getGridParam", "postData");
			$.extend(_postData,_sdata);  
			$('#ITSOPCustomer_userGrid').jqGrid('setGridParam',{postData:_postData}).trigger('reloadGrid',[{"page":"1"}]);
		},
		
		/**
		 * 设置点击事件
		 */
		setEvent:function(){
			//添加
			$('#ITSOPCustomer_userGrid_add').click(wstuo.customer.ITSOPCustomer_userGrid.addUser_openWindow);
			//选择用户所属机构
			$('#itsop_user_orgName').click(function(){
				wstuo.orgMge.organizationTreeUtil.showAll_2('#index_selectORG_window','#index_selectORG_tree','#itsop_user_orgName','#itsop_user_orgNo',_itsop_user_companyNo);
			});
			//执行保存
			$('#itsop_user_save_btn').click(wstuo.customer.ITSOPCustomer_userGrid.saveUser);
			
			//编辑
			$('#ITSOPCustomer_userGrid_edit').click(wstuo.customer.ITSOPCustomer_userGrid.editUser_openWindow);
			
			//删除
			$('#ITSOPCustomer_userGrid_delete').click(wstuo.customer.ITSOPCustomer_userGrid.deleteUser);
			
			//搜索
			$('#ITSOPCustomer_userGrid_search').click(function(){
				$("#search_itsop_user_window input").val('');
				windows('search_itsop_user_window',{width:400});
			});
			
			//执行搜索
			$('#search_itsop_user_search').click(wstuo.customer.ITSOPCustomer_userGrid.searchUser);
			
			
			//设置角色
			$('#ITSOPCustomer_userGrid_role').click(wstuo.customer.ITSOPCustomer_userGrid.setUserRole_openWindow);
			$('#set_itsop_user_role_save').click(wstuo.customer.ITSOPCustomer_userGrid.updateUserRole);
			
		},
		
		init:function(){
			//加载列表
			if(_itsop_user_companyNo!=0){
				wstuo.customer.ITSOPCustomer_userGrid.showGrid();
			}
		}
	}
}();






