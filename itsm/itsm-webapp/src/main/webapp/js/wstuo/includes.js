﻿$package('wstuo.includes');

wstuo.includes=function(){
	return {
		/**
		 * @description 加载流程窗口includes文件
		 */
		loadFlowCommonWinIncludesFile:function(){
			if($('#flowCommonWin_html').is(":empty")){
				$('#flowCommonWin_html').load('includes/includes_flowWin.jsp',function(){
				});
			}
		},
		loadSelectCIIncludesFile:function(){
			if($('#selectCI_html').is(":empty")){
				$('#selectCI_html').load('itsm/cim/selectCI.jsp',function(){
				
				});
			}
		},
		//加载导入数据窗口includes文件
		loadImportCsvIncludesFile:function(){
			if($('#importCsv_html').is(":empty")){
				$('#importCsv_html').load('includes/includes_importExcel.jsp',function(){
					//$.parser.parse($('#importCsv_html'));
				});
			}
		},
		
		/**
		 * 加载服务目录includes文件
		 */
		loadServiceDirectoryItemsIncludesFile:function(){
			if($('#knowledge_html').is(":empty")){
				$('#knowledge_html').load('wstuo/includes/includes_selectServiceDirectoryItems.jsp',function(){
					//$.parser.parse($('#knowledge_html'));	
				});
			}
		},
		/**
		 * 加载选择外包客户窗口includes文件
		 */
		loadSelectCustomerIncludesFile:function(){
			if($('#selectCustomer_html').is(":empty")){
				$('#selectCustomer_html').load('wstuo/includes/includes_selectCustomer.jsp',function(){
					//$.parser.parse($('#selectCustomer_html'));
				});
			}
		},
		/**
		 * 加载面板数据详细信息includes文件
		 */
		loadDashboardDataInfoIncludesFile:function(){
			if($("#dashboardDataInfo_html").is(":empty")){
				$('#dashboardDataInfo_html').load('wstuo/includes/includes_dashboardDataInfo.jsp',function(){
					//$.parser.parse($('#dashboardDataInfo_html'));
				});
			}
		},
		/**
		 * 
		 * 加载选择用户窗口（单选）includes文件
		 */
		loadSelectUserIncludesFile:function(){
			if($('#selectUser_html').is(":empty")){
				$('#selectUser_html').load('wstuo/includes/includes_selectUser.jsp',function(){
					//$.parser.parse($('#selectUser_html'));
				});
			}
		},
		/**
		 * ACL权限
		 */
		loadAclWinIncludesFile:function(){
			if($('#aclWin_html').is(":empty")){
				$('#aclWin_html').load('wstuo/includes/includes_acl.jsp',function(){
					//$.parser.parse($('#aclWin_html'));
				});
			}
		},
		/**
		 * 加载分类选择includes文件
		 */
		loadCategoryIncludesFile:function(){
			if($('#category_html').is(":empty")){
				$('#category_html').load('wstuo/includes/includes_categorys.jsp',function(){
					//$.parser.parse($('#category_html'));
				});
			}
		},
		/**
		 * 加载自定义查询过滤器includes文件
		 */
		loadCustomFilterIncludesFile:function(){
			if($('#customFilter_html').is(":empty")){
				$('#customFilter_html').load('wstuo/includes/includes_customFilter.jsp',function(){
					//$.parser.parse($('#customFilter_html'));
				});
			}
		},
		/**
		 * 加载选择附件窗口includes文件
		 */
		loadRelatedAttachmentIncludesFile:function(){
			if($('#chooseAttachment_html').is(":empty")){
				$('#chooseAttachment_html').load('wstuo/includes/includes_chooseAttachment.jsp',function(){
					//$.parser.parse($('#chooseAttachment_html'));
				});
			}
		},
		/**
		 * 配置向导窗口文件
		 */
		loadGuideIncludesFile:function(){
			if($('#guide_html').is(":empty")){
				$('#guide_html').load('wstuo/includes/includes_guide.jsp',function(){
					//$.parser.parse($('#guide_html'));
				});
			}
		},
		/**
		 * 导出详细文件
		 */
		loadExportInfoIncludesFile:function(){
			if($('#exportInfo_html').is(":empty")){
				$('#exportInfo_html').load('wstuo/includes/includes_exportManage.jsp',function(){
					//$.parser.parse($('#exportInfo_html'));
				});
			}
		},
		loadRequestActionIncludesFile:function(){
			if($('#requestAction_html').is(":empty")){
				$('#requestAction_html').load('request/includes/requestActionHtml.jsp',function(){
				});
			}
		},
		/**
		 * 加载选择关联请求窗口includes文件
		 */
		loadRelatedRequestIncludesFile:function(){
			if($('#relatedRequest_html').is(":empty")){
				$('#relatedRequest_html').load('request/includes/includes_relatedRequest.jsp',function(){
				});
			}
		},
		
		//加载TCode帮助窗口includes文件
		loadTCodeIncludesFile:function(){
			if(!_loadTCodeIncludesFileFlag){
				$('#tCode_html').load('includes/includes_tcodeHelp.jsp',function(){
					$.parser.parse($('#tCode_html'));
				});
			}
			_loadTCodeIncludesFileFlag=true;
		},
		
		//快捷页面窗口文件
		loadWelcomeIncludesFile:function(){
			if(!_loadWelcomeFileFlag){
				$('#welcome_html').load('includes/includes_welcome.jsp',function(){
					$.parser.parse($('#welcome_html'));
				});
			}
			_loadWelcomeFileFlag=true;
		},
		//语音卡窗口文件
		loadVoiceCardIncludesFile:function(){
			if(!_loadVoiceCardFileFlag){
				$('#voiceCard_html').load('includes/includes_voiceCard.jsp',function(){
					$.parser.parse($('#voiceCard_html'));
				});
			}
			_loadVoiceCardFileFlag=true;
		},
		
		init:function(){
			
		}
	}
}();
