
 /**  
 * @author WSTUO 
 * @constructor flowDesigner
 * @description for 流程设计器

 **/
var flowDesigner=function(){
	this.msgAlert = function(msg){
		alert(msg);
	}
	/**
	 * 查找Task节点
	 */
	this.findNodeByNodeName_task=function(){
		var rawXml = $('#rawXmlArea').val().replace(/&/g,'$');
		var _tr="";
		$.post('jbpm!findNodeByNodeName.action','rawXml='+rawXml+'&nodeDTO.nodeTag=task',function(data){
			if(data!=null && data!=''){
				$('#allTaskNode tbody').html('');
				for(i=0;i<data.length;i++){
					_tr='<tr>'+
						'<td align="center">[Tag]</td>'+
						'<td align="center">[Index]</td>'+
						'<td align="center">[Name]</td>'+
						'<td align="center"><input id="[GroupsId]" value="[Candidate-Groups]" /></td>'+
						'<td align="center"><input id="[AssigneeId]" value="[Assignee]" /></td>'+
						'<td align="center">[<a href="javascript:updateNode(\'[p_tag]\',[p_index])">Update</a>]</td>'+
						'</tr>'
						
					_tr=_tr.replace('[Tag]',data[i].nodeTag)
					.replace('[Index]',data[i].index)
					.replace('[Name]',data[i].nodeAttribute['name'])
					.replace('[GroupsId]','GroupsId_'+data[i].index)
					.replace('[AssigneeId]','AssigneeId_'+data[i].index)
					.replace('[p_tag]',data[i].nodeTag)
					.replace('[p_index]',data[i].index)
					
					if(data[i].nodeAttribute['candidate-groups']!=undefined)
						_tr=_tr.replace('[Candidate-Groups]',data[i].nodeAttribute['candidate-groups'])
					else
						_tr=_tr.replace('[Candidate-Groups]','')
						
						
					if(data[i].nodeAttribute.assignee!=undefined)	
						_tr=_tr.replace('[Assignee]',data[i].nodeAttribute.assignee)
					else
						_tr=_tr.replace('[Assignee]','')
						
					$('#allTaskNode tbody').append(_tr);
				}
				
			}
		});
	};
	/**
	 * 查找foreach节点
	 */
	this.findNodeByNodeName_foreach=function(){
		var rawXml = $('#rawXmlArea').val().replace(/&/g,'$');
		var _tr="";
		$.post('jbpm!findNodeByNodeName.action','rawXml='+rawXml+'&nodeDTO.nodeTag=task',function(data){
			if(data!=null && data!=''){
				$('#allTaskNode tbody').html('');
				for(i=0;i<data.length;i++){
					_tr='<tr>'+
						'<td align="center">[Tag]</td>'+
						'<td align="center">[Index]</td>'+
						'<td align="center">[Name]</td>'+
						'<td align="center"><input id="[GroupsId]" value="[Candidate-Groups]" /></td>'+
						'<td align="center"><input id="[AssigneeId]" value="[Assignee]" /></td>'+
						'<td align="center">[<a href="javascript:updateNode(\'[p_tag]\',[p_index])">Update</a>]</td>'+
						'</tr>'
						
					_tr=_tr.replace('[Tag]',data[i].nodeTag)
					.replace('[Index]',data[i].index)
					.replace('[Name]',data[i].nodeAttribute['name'])
					.replace('[GroupsId]','GroupsId_'+data[i].index)
					.replace('[AssigneeId]','AssigneeId_'+data[i].index)
					.replace('[p_tag]',data[i].nodeTag)
					.replace('[p_index]',data[i].index)
					
					if(data[i].nodeAttribute['candidate-groups']!=undefined)
						_tr=_tr.replace('[Candidate-Groups]',data[i].nodeAttribute['candidate-groups'])
					else
						_tr=_tr.replace('[Candidate-Groups]','')
						
						
					if(data[i].nodeAttribute.assignee!=undefined)	
						_tr=_tr.replace('[Assignee]',data[i].nodeAttribute.assignee)
					else
						_tr=_tr.replace('[Assignee]','')
						
					$('#allTaskNode tbody').append(_tr);
				}
				
			}
		});
	}
	/**
	 * 更新节点
	 * @param tag 节点标识
	 * @Param index  节点位置
 	 */
	this.updateNode=function(tag,index){
		$.post('jbpm!updateNode.action','nodeDTO.nodeTag='+tag+'&nodeDTO.index='+index+'&nodeDTO.nodeAttribute["candidate-groups"]='+$('#GroupsId_'+index).val()+'&nodeDTO.nodeAttribute["assignee"]='+$('#AssigneeId_'+index).val(),function(){
		    msgAlert('Update Success');
		})
	}
	/**
	 * 生成流程文件
	 * @param _deployFlag 根据标识判断是导出还是部署，saveAndDeploy为部署
	 */
	this.updateFLowProperties=function(_deployFlag){
		var _param="nodeDTO.nodeTag=process&nodeDTO.index=0";
		if($('#flowName').val()!=''){
			_param=_param+"&nodeDTO.nodeAttribute['name']="+$('#flowModule').val()+$('#flowName').val()
		}
//		if($('#flowVersion').val()!=''){
//			_param=_param+'&nodeDTO.nodeAttribute["version"]='+$('#flowVersion').val()
//		}
//		if($('#flowKey').val()!=''){
//			_param=_param+'&nodeDTO.nodeAttribute["key"]='+$('#flowKey').val()
//		}
//		if($('#flowDescription').val()!=''){
//			_param=_param+'&nodeDTO.nodeAttribute["description"]='+$('#flowDescription').val()
//		}
		$.post('jbpm!updateNode.action',_param,function(){
			if(_deployFlag=="saveAndDeploy"){
				$.post('jbpm!deployJpdlFileByDrawingPage.action',function(data){
				    msgAlert(i18n['tip_save_and_deploy_success']);
				});
			}else{
				window.location.href="jbpm!exportJpdlFile.action";
			}
		})
	}
	/**
	 * 生成流程文件
	 * @param _deployFlag 根据标识判断是导出还是部署，saveAndDeploy为部署
	 */
	this.generateFlowFile=function(_deployFlag){
		if($('#flowName').val()==''){
			msgAlert(i18n['err_nameNotNull']);
		}else{
			//生成流程原始文件
			$("#rawXmlArea").val(processApplet.getData());
			var rawXml = $('#rawXmlArea').val().replace(/&/g,'$');
			//通过XSLT转JPDL
			$.post('jbpm!rawToJPDL.action','rawXml='+rawXml,function(){
				updateFLowProperties(_deployFlag);
			})
		}
	}
	return {
		init:function(){
			$(document).ajaxError(function(e, xhr, settings, exception) {
				if(xhr.status==606){
				    msgAlert(i18n['common_systemTimeout']);
				}
				if(xhr.status==403){
				    msgAlert(i18n['error403']);
				};
				if(xhr.status==404){
				    msgAlert(i18n['error404']);
					//location.reload();//404也刷新页面
				};
				if(xhr.status==500){
					var msg=xhr.responseText;
					msgAlert(i18n['msg_process_delpoyFailure']+'\n');
				};
			});
		}
	}
}();
$(document).ready(flowDesigner.init);