$package('wstuo.jbpmMge'); 
/**  
 * @fileOverview 我已处理的任务
 * @author Tan
 * @version 1.0  
 */  
 /**  
 * @author Tan  
 * @constructor myProcessedTasks
 * @description 我已处理的任务
 * @date 2012-12-19
 * @since version 1.0 
 */

wstuo.jbpmMge.myProcessedTasks = function(){

	return {
		/**
		 * 操作项格式化
		 */
		viewDetailedForma:function(cellvalue, options, rowObject){
			var executionId=rowObject.executionId;
			var variables = cellvalue;
			var _href='<a href="javascript:{href}">['+i18n['common_view']+']</a>';
			
			var dto = variables.dto;
			if(dto!=undefined && dto!=null){
				if(executionId.indexOf('Changes')>=0 || executionId.indexOf('Change')>=0 || executionId.indexOf('change')>=0){
					return _href.replace('{href}','basics.tab.tabUtils.reOpenTab(\'change!changeInfo.action?queryDTO.eno='+dto.eno+'\',\''+i18n["change_detail"]+'\')');
				}else if(executionId.indexOf('problem')>=0 || executionId.indexOf('Problem')>=0){
					return _href.replace('{href}','basics.tab.tabUtils.reOpenTab(\'problem!findProblemById.action?eno='+dto.eno+'\',\''+i18n["probelm_detail"]+'\')');
				}else if(executionId.indexOf('release')>=0 || executionId.indexOf('Release')>=0){
					return _href.replace('{href}','basics.tab.tabUtils.reOpenTab(\'release!releaseDetails.action?releaseDTO.eno='+dto.eno+'\',\''+i18n["release_details"]+'\')');
				}else if(executionId.indexOf('request')>=0 || executionId.indexOf('Request')>=0){
					return _href.replace('{href}','basics.tab.tabUtils.reOpenTab(\'request!requestDetails.action?eno='+dto.eno+'\',\''+i18n["request_detail"]+'\')');
				}
			}else if(variables.eno!=null){
				if(executionId.indexOf('Changes')>=0 || executionId.indexOf('Change')>=0 || executionId.indexOf('change')>=0){
					return _href.replace('{href}','basics.tab.tabUtils.reOpenTab(\'change!changeInfo.action?queryDTO.eno='+variables.eno+'\',\''+i18n["change_detail"]+'\')');
				}else if(executionId.indexOf('problem')>=0 || executionId.indexOf('Problem')>=0){
					return _href.replace('{href}','basics.tab.tabUtils.reOpenTab(\'problem!findProblemById.action?eno='+variables.eno+'\',\''+i18n["probelm_detail"]+'\')');
				}else if(executionId.indexOf('release')>=0 || executionId.indexOf('Release')>=0){
					return _href.replace('{href}','basics.tab.tabUtils.reOpenTab(\'release!releaseDetails.action?releaseDTO.eno='+variables.eno+'\',\''+i18n["release_details"]+'\')');
				}else if(executionId.indexOf('request')>=0 || executionId.indexOf('Request')>=0){
					return _href.replace('{href}','basics.tab.tabUtils.reOpenTab(\'request!requestDetails.action?eno='+variables.eno+'\',\''+i18n["request_detail"]+'\')');
				}
			}else{
				return '';
			}
			
		},
		
		/**
		 * @description 类型格式化
		 * @param cellvalue 当前列值
		 * @param options 事件
		 * @param rowObject 行数据
		 * @private
		 */
		taskTypeForma:function(cellvalue, options, rowObject){
			var executionId=rowObject.executionId;
			if(executionId.indexOf('Changes')>=0 || executionId.indexOf('Change')>=0 || executionId.indexOf('change')>=0){
				return i18n['change']
			}else if(executionId.indexOf('problem')>=0 || executionId.indexOf('Problem')>=0){
				return i18n['problem']
			}else if(executionId.indexOf('release')>=0 || executionId.indexOf('Release')>=0){
				return i18n['release']
			}else if(executionId.indexOf('request')>=0 || executionId.indexOf('Request')>=0){
				return i18n['request']
			}
		},
		/**
		 * 显示我已处理的任务
		 */
		showTaskGrid:function(){
			var params=$.extend({},jqGridParams,{
				url:'jbpm!findHistoryTask.action',
				postData:{'assignee':userName},
				colNames:
						[
						i18n.label_bpm_task_id
						,i18n.title_request_assignTo
						,i18n.common_createTime
						,i18n.title_jbpm_duration+"("+i18n.label_ms+")"
						,i18n.label_bpm_task_name
						,i18n.title_endTime
						,i18n.label_bpm_outcome
						,i18n.status
						],
				colModel:[
				          {name:'id',sortable:false,align:'center',align:'center'},
						  {name:'assignee',width:150,align:'center',sortable:false},
						  {name:'createTime',hidden:false,sortable:false,align:'center',formatter:timeFormatter},
						  {name:'duration',width:100,sortable:false,align:'center',formatter:wstuo.jbpmMge.myProcessedTasks.durationFormatter},
						  {name:'activityName',sortable:false,align:'center'},
						  {name:'endTime',sortable:false,align:'center',formatter:timeFormatter},
						  {name:'outcome',sortable:false,align:'center'},
						  {name:'state',sortable:false,align:'center',formatter:wstuo.jbpmMge.myProcessedTasks.statusFormatter}
					  ],
				toolbar:false,
				multiselect:false,
				jsonReader: $.extend(jqGridJsonReader,{id:"id"}),
				sortname:'id',
				pager:'#myProcessedTasksPager'
			});
			$("#myProcessedTasksGrid").jqGrid(params);
			$("#myProcessedTasksGrid").navGrid('#myProcessedTasksPager',navGridParams);
			//自适应大小
			setGridWidth("#myProcessedTasksGrid","regCenter",20);
		},
		/**
		 * 持续时间格式化
		 */
		durationFormatter:function(cell,event,data){
			return data.duration;
		},
		/**
		 * 状态格式化
		 */
		statusFormatter:function(cell,event,data){
			var status= data.state;
			return status.replace(/completed/g,i18n['lable_complete']);
		},
		/**
		 * 初始化
		 * @param
		 */
		init:function(){
			wstuo.jbpmMge.myProcessedTasks.showTaskGrid();
		}
	}
}();

$(document).ready(wstuo.jbpmMge.myProcessedTasks.init);

