$package('wstuo.tools.exportManage');
/**  
 * @author Tan  
 * @constructor exportManage
 * @description exportManage
 * @date 2010-11-17
 */
wstuo.tools.exportManage.exportManage=function(){
	var code='';
	return {
		/**
		 * @description 显示导出列表
		 */
		showAllFile:function(){
			var _url="exportInfo!findExportInfoPager.action?exportInfoDTO.companyNo="+companyNo;
			var params = $.extend({},jqGridParams, {
				url: _url,
				colNames:['ID',i18n['title_backup_fileName'],i18n['type'],i18n['common_createTime'],i18n['title_backup_fileSize'],i18n['status']], 
				colModel:[
							{name:'exportInfo_id',width:60,align:'center',sortable:true},
							{name:'exportFileName',width:100,align:'center',formatter:wstuo.tools.exportManage.exportManage.nameClick},
							{name:'exportType',width:50,align:'center',formatter:wstuo.tools.exportManage.exportManage.exportTypeGetI18n},
							{name:'createTime',width:50,align:'center',formatter:timeFormatter},
							{name:'exportFileSize',width:50,align:'center',formatter:wstuo.tools.exportManage.exportManage.sizeFormat},
							{name:'exportStatus',width:50,sortable:false,align:'center',formatter:wstuo.tools.exportManage.exportManage.exportStatus}
						],
				jsonReader:$.extend({},jqGridJsonReader,{id:'exportInfo_id'}),
				sortname:'exportInfo_id',
				pager:'#exportInfoePager'
			});
			$("#exportInfoGrid").jqGrid(params);
			$("#exportInfoGrid").navGrid('#exportInfoePager',navGridParams);
			$("#t_exportInfoGrid").css(jqGridTopStyles);
			$("#t_exportInfoGrid").append($('#exportInfoToolbar').html());
		},
		/**
		 * @description 获取导出类型的国际化
		 */
		exportTypeGetI18n:function(exportType){
			exportType=exportType.toLowerCase();
			return i18n[exportType];
		},
		/**
		 * @description 导出状态
		 */
		exportStatus:function(cellvalue, options, rowObject){
			if(cellvalue=="exportComplete"){
				return "<a href=javascript:wstuo.tools.exportManage.exportManage.exportDownload('"+rowObject.exportFileName+"')><i class='glyphicon glyphicon-download-alt'></i><a>";
			}else{
				code="loading";
				return "<img src=../images/icons/exportLoading.gif />";
			}
		},
		/**
		 * @description 导出
		 */
		exportDownload:function(exportFileName){
/*			$('#export_exportFileName').val(exportFileName);
			$('#export_common_form').submit();*/
			$.post("ci!exportData.action", "categoryName="+exportFileName, function(data){
				if(data=="ERROE_FILENOTFIND")
					msgShow(i18n[data],'show');
				else{
					location.href="ci!exportData.action?categoryName="+exportFileName;
				}
			});
		},
		nameClick:function(cellvalue, options, rowObject){
			if(rowObject.exportStatus=="exportComplete"){
				return "<a href=ci!exportData.action?categoryName="+cellvalue+">"+cellvalue+"<a>";
			}else{
				return cellvalue;
			}
		},
		/**
		 * @description 格式化导出文件大小
		 */
		sizeFormat:function(cellvalue, options, rowObject){
			if((cellvalue/1024).toFixed(2)>1024){
				var num=wstuo.tools.exportManage.exportManage.usaFormat(((cellvalue/1024)/1024).toFixed(2));
				return  num+" MB";
			}else{
				var num=wstuo.tools.exportManage.exportManage.usaFormat((cellvalue/1024).toFixed(2));
				return  num+" KB";
			}
		},
		usaFormat:function(num){
			num = num.toString().replace(/\$|\,/g,'');
		    if(isNaN(num))
		    num = "0";
		    sign = (num == (num = Math.abs(num)));
		    num = Math.floor(num*100+0.50000000001);
		    cents = num%100;
		    num = Math.floor(num/100).toString();
		    if(cents<10)
		    cents = "0" + cents;
		    for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
		    num = num.substring(0,num.length-(4*i+3))+','+
		    num.substring(num.length-(4*i+3));
		    return (((sign)?'':'-') + num + '.' + cents);
		},
		deleteClick:function(){
			checkBeforeDeleteGrid('#exportInfoGrid', wstuo.tools.exportManage.exportManage.deleteExportFile);
		},
		/**
		 * @description  删除
		 */
		deleteExportFile:function(rowsId){
			var url="exportInfo!deleteExportInfos.action";
			var param = $.param({'ids':rowsId},true);
			$.post(url, param, function()
			{
				$('#exportInfoGrid').trigger('reloadGrid');
				msgShow(i18n['msg_deleteSuccessful'],'show');
			}, "json");
		},
		/**
		 * @description 初始化
		 */
		init:function(){
			wstuo.tools.exportManage.exportManage.showAllFile();
			$("#export_delete_but").click(wstuo.tools.exportManage.exportManage.deleteClick);

			var intervalId = window.setInterval(function(){
				if(code==="loading"){
					tabValue=$('#itsmMainTab').tabs('getSelected').panel('options').title;
					if(tabValue == i18n['exportDown']){
						code="";
						$('#exportInfoGrid').trigger('reloadGrid');
					}
				}else{
					intervalId=window.clearInterval(intervalId);
				}
			},8000); 
		}
	}
}();
$(document).ready(wstuo.tools.exportManage.exportManage.init);