 $import("wstuo.noticeRule.noticeGrid");
/**  
 * @author QXY  
 * @constructor WSTO
 * @description "noticeRule"
 * @date 2010-11-17
 * @since version 1.0 
 */ 

 var noticeRule = function(){
	
	//更新保存
	this.noticeRule_opt = function(id){
		var _params = $("#"+id+"_noticeRule form").serialize();
		$.post('noticeRule!noticeRuleUpdate.action', _params, function(){
			msgShow(i18n['editSuccess'],'show');
		});
	}
	//给值
	this.setValues=function(){
		$.post('noticeRule!findPager.action',function(data){
			var notice=data.data;
			for(var i=0;i<notice.length;i++){
				if(notice[i].noticeRuleNo.indexOf("request")>-1){
					$('#noticeRule_url').val(notice[i].url);
				}
				var noticeType = wstuo.noticeRule.noticeGrid.noticesTypeFormatter(null,null,notice[i]);
				if(noticeType==""){
					noticeType = i18n.leftMenuName_Unknown;
				}
				var noticeMethod = wstuo.noticeRule.noticeGrid.noticeMethodFormatter(null,null,notice[i]);
				if(noticeMethod==""){
					noticeMethod=i18n.leftMenuName_Unknown;
				}
				$("#"+notice[i].module+"_noticeModule").append("<div id=\""+notice[i].noticeRuleNo+"\" class=\"lineTableBgDiv\">"+
            			"<form>"+
            			"<input type=\"hidden\" name=\"noticeRuleNos['"+notice[i].noticeRuleNo+"']\" value=\""+notice[i].noticeRuleNo+"\" />"+
            			"<table style=\"width:100%\" class=\"lineTable\" cellspacing=\"1\">"+
            			"	<tr>"+
            			"	<td><input type=\"checkbox\" id=\""+notice[i].noticeRuleNo+"_check_useStatues\" name=\"useStatuses['"+notice[i].noticeRuleNo+"']\" value=\"true\" />"+
            			notice[i].noticeRuleName+
            			"	<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color='#666666'>"+i18n.noticeMethod+":"+
            			noticeMethod+
            			"	&nbsp;&nbsp;&nbsp;&nbsp;"+i18n.notice_noticeObject+":"+
            			noticeType+
            			"	</font></td>"+
            			"	</tr>"+
						"	</table>"+
						"	</form>"+
						"</div>");
				if(notice[i].useStatus){
					$('#'+notice[i].noticeRuleNo+'_check_useStatues').attr("checked",'ture')
				}
			}
		});
	}
	
	this.selectAll=function(id){
		$("#"+id+"_noticeModule [type='checkbox']").attr("checked",'true');//全选
	}
	this.doSelect=function(id){//反选
		$("#"+id+"_noticeModule [type='checkbox']").each(function(){
		    if($(this).attr("checked")){
		    	$(this).removeAttr("checked");
		    }
		    else{
		    	$(this).attr("checked",'true');
		    }
		 });
	}
	//载入
	return {
		init:function(){	
			$("#noticeRule_loading").hide();
			$("#noticeRule_panel").show();
			
			setTimeout(function(){
				//$('#noticeRuleDiv').tabs('select',defaultSelectNoticeRuleTab);
				setValues();
			},500);
			
		}
	}
}();
$(document).ready(noticeRule.init);
