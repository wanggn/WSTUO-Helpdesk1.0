﻿ $package('common.jbpm.includes');
/**  
 * @fileOverview "Includes"
 * @author QXY
 * @version 1.0  
 */  
 /**  
 * @author QXY  
 * @constructor Includes
 * @description "用于加载jbpm Includes文件JS主函数"
 * @date 2012-7-31
 * @since version 1.0 
 */ 
common.jbpm.includes.includes=function(){
	this._loadFlowCommonWinIncludesFileFlag=false;
	return {
		/**
		 * @description 加载流程窗口includes文件
		 */
		loadFlowCommonWinIncludesFile:function(){
			if(!_loadFlowCommonWinIncludesFileFlag){
				$('#flowCommonWin_html').load('common/jbpm/includes/includes_flowWin.jsp',function(){
					$.parser.parse($('#flowCommonWin_html'));
				});
			}
			_loadFlowCommonWinIncludesFileFlag=true;
		},
		init:function(){
			
		}
	}
}();
$(function(){common.jbpm.includes.includes.init});
