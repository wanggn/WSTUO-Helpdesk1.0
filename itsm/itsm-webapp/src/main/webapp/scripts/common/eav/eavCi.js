$package('common.eav');
$import('common.eav.selectAttributeGroup');
$import('common.eav.includes.includes');
$import('common.config.category.CICategoryUtil');
$import('common.config.systemGuide.systemGuide');
/**  
 * @fileOverview 扩展属性主函数
 * @author QXY
 * @version 1.0  
 */  
 /**  
 * @author QXY  
 * @constructor 扩展属性主函数
 * @description 扩展属性主函数
 * @date 2010-11-17
 * @since version 1.0 
 */  
common.eav.eavCi = function(){
	var eavNo;
	var operator;
	this.deleteAttrItemNo='';
	return { 
		addClone:function(tab){
			$(tab).append("<tr><td><input name='attributesDTO.attrItemName' class='input'></td><td><span><img onclick='$(this).parent().parent().parent().remove()' style='cursor: pointer' src='../skin/default/images/grid_delete.png' /></span></td></tr>");
		},
		/**
		 * @private
		 * @description 扩展属性
		 * */
		eavCi_grid:function(){		
			var params = $.extend({},jqGridParams,{	
				url:'eav!findEavEntity.action',
				colNames:[i18n.name,i18n.description,'',i18n.table_colNames_eav_extends,'parentEavNo',''],
				colModel:[{name:'eavName',width:40,align:'center'},
						{name:'desc',width:60,align:'center',sortable:false},
						{name:'eavCode',width:30,hidden:true},
						{name:'parentEavName',width:30,hidden:false,sortable:false},
						{name:'parentEavNo',width:30,hidden:true,sortable:false},
						{name:'eavNo',hidden:true}
                         ],
				jsonReader: $.extend(jqGridJsonReader, {id: "eavNo"}),
				sortname:'eavNo',
				pager:'#eavCiPager',
				multiselect: true,
				onSelectRow:common.eav.eavCi.reloadEavAttrs
				});
				$("#eavCiList").jqGrid(params);
				$("#eavCiList").navGrid('#eavCiPager',navGridParams);
				//列表操作项
				$("#t_eavCiList").css(jqGridTopStyles);
				$("#t_eavCiList").append($('#eavCiToolbar').html());
				
				//自适应宽度
				setGridWidth("#eavCiList","regCenter",20);
		},
	
      /**
       * @description 添加属性
       * */
		addEav:function(){
			operator="saveEavEntity";
			resetForm('#addEavCiDivForm');
			
			var rowId = $("#eavCiList").jqGrid('getGridParam','selrow');
			var rowData = $("#eavCiList").jqGrid('getRowData',rowId);
			$("#add_parentEavNo").val( rowData['eavNo'] );
			
			windows('addEavCiDiv',{width:400});
		},
		
		
		/**
		 * @description 保存属性
		 * */
		saveEav:function(){
			if( $("#add_eavNo").val() !== "" && $("#add_eavNo").val() === $("#add_parentEavNo").val() ){
				msgShow(i18n.msg_eav_extends_tip1,'show');
				return;
			}
			if($('#addEavCiDivForm').form('validate')){
				var _params = $('#addEavCiDiv form').serialize();
				startProcess();
				$.post('eav!'+operator+'.action', _params, function(){
					msgShow(i18n.saveSuccess,'show');
					$('#eavCiList').trigger('reloadGrid');
					$('#add_eavName').val('');
					$('#add_entityDesc').val('');
					$('#addEavCiDiv').dialog('close');
					/**保存向导状态*/
					common.config.systemGuide.systemGuide.saveSystemGuide("eavCiGuide");
					endProcess();
					common.config.category.CICategoryUtil.loadEavsToSelect("#add_parentEavNo");//加载列表
				});
			}
		},
		/**
		 * @description 编辑属性时获取id
		 */
		editEav:function(){
			checkBeforeEditGrid("#eavCiList",common.eav.eavCi.editEav_opt);
		},
		/**
		 * @description 打开编辑属性面板
		 * @parma rowsData 列表对象
		 */
		editEav_opt:function(rowsData){
			resetForm('#addEavCiDivForm');
			operator="updateEavEntity";
			$('#add_eavNo').val(rowsData.eavNo);
			$('#add_eavCode').val(rowsData.eavCode);
			$('#add_eavName').val(rowsData.eavName);
			$('#add_entityDesc').val(rowsData.desc);
			$("#add_parentEavNo").val( rowsData['parentEavNo'] );
			
			windows('addEavCiDiv',{width:400});
		},
		/**
		 * @description 删除属性时获取id
		 */
		deleteEav:function(){
			checkBeforeDeleteGrid("#eavCiList",common.eav.eavCi.deleteEav_opt);
		},
		/**
		 * @description 执行删除
		 * @parma rowIds 属性id数组
		 */
		deleteEav_opt:function(rowIds){
			var _params = $.param({'eavNos':rowIds},true);
			
			$.post('eav!deleteEavEntity.action', _params, function(res){
				if(res){
					msgShow(i18n.deleteSuccess,'show');
					$.post('ciCategory!deleteEavEntity.action', _params, function(){});
					$('#eavCiList').trigger('reloadGrid');
					eavNo=null;
					$('#eavAttrsList').jqGrid('setGridParam',{url:'attrs!find.action?attrsQueryDTO.eavNo=0'}).trigger('reloadGrid',[{"page":"1"}]);
				}else{
					msgShow(i18n.ERROR_SYSTEM_DATA_CAN_NOT_DELETE,'show');
				}
			});
		},
		/**
		 * @description 国际化属性类型
		 * @parma value 属性字段类型
		 */
		attrTypeForma:function(value){
			if(value=='Lob')
				return i18n.longText;
			if(value=='Integer')
				return i18n.integer;
			if(value=='Double')
				return i18n.decimal;
			if(value=='Date')
				return i18n.date;
			if(value=='Radio')
				return i18n.radio;
			if(value=='Checkbox')
				return i18n.checkbox;
			if(value=='DataDictionaray'){
				return i18n.eav_realdatadictionary;
			}
			else
				return i18n.text;
		},     
		/**
		 * @description 国际化数据字典类型
		 * @parma value 数据字典dname
		 */
		attrDataDicForma:function(value){ 
			if(value!==""){
				var name="label_dc_"+value;      
				return i18n[name];           
			}
			return "";
		},           
		/**
		 * @description 打开扩展属性搜索面板
		 */
		searchEav:function(){
			windows('searchEavDiv',{width:400,modal: false});
			//禁止回车事件
			$("#searchEav_eavName").keydown(function (e) {
	            var curKey = e.which;
	            if (curKey == 13) {
	                return false;
	            }
	        });
		},
		/**
		 * @description 扩展属性搜索查询
		 */
		searchEavOpt:function(){
			var _params = $('#searchEavDivForm').getForm();
			var postData = $("#eavCiList").jqGrid("getGridParam", "postData");       
			$.extend(postData, _params);  //将postData中的查询参数加上查询表单的参数		
			$('#eavCiList').trigger('reloadGrid',[{"page":"1"}]);
			
		},
		/**
		 * @description 打开搜索扩展字段面板
		 */
		searchEavAttrs:function(){
			windows('searchEavAttrsDiv',{width:400});
		},
		/**
		 * @description 搜索扩展字段
		 */
		searchEavAttrsOpt:function(){
			var _params = $('#searchEavAttrsDivForm').getForm();
			var postData = $("#eavAttrsList").jqGrid("getGridParam", "postData");  
			$.extend(postData, _params);  //将postData中的查询参数加上查询表单的参数	
			$('#eavAttrsList').trigger('reloadGrid',[{"page":"1"}]);
			
		},
		
        /**
         * @description 扩展属性字段列表
         **/
		eavAttrsList:function(){
			var params = $.extend({},jqGridParams, {
				url:'attrs!find.action?attrsQueryDTO.eavNo=0',  
				caption:i18n.extendedAttributeItems,
				colNames:[i18n.name,i18n.label_beLongAttrGroup,'',i18n.type,i18n.eav_realdatadictionary,'','',i18n.eav_required,i18n.length,'','',i18n.sort,''],
				colModel:[
                        {name:'attrAsName',width:50,align:'center',sortable:false},
						{name:'attrGroupName',width:50,index:'attrGroup',align:'center',sortable:false},
						{name:'attrGroupNo',width:50,hidden:true},
						{name:'attrType',width:30,align:'center',formatter:common.eav.eavCi.attrTypeForma,sortable:false},
						{name:'attrdataDictionary',width:30,align:'center',formatter:common.eav.eavCi.attrDataDicForma,sortable:false,hidden:true},  
						{name:'attrdataDictionary',width:30,hidden:true}, 
						{name:'attrType',width:30,hidden:true},
						{name:'attrIsNull',width:30,align:'center',formatter:common.eav.eavCi.isNullForma,sortable:false},
						{name:'length',width:20,align:'center',hidden:true},
						{name:'attrNo',hidden:true},
						{name:'attrCode',width:30,hidden:true},
						{name:'sortNo',width:30,hidden:false,align:'center',sortable:false},      
						{name:'attrName',hidden:true}
                        ],
				jsonReader: $.extend(jqGridJsonReader, {id: "attrNo"}),
				sortname:'sortNo',
				pager:'#eavAttrsPager'
				});
			
				$("#eavAttrsList").jqGrid(params);
				$("#eavAttrsList").navGrid('#eavAttrsPager',navGridParams);
				//列表操作项
				$("#t_eavAttrsList").css(jqGridTopStyles);
				$("#t_eavAttrsList").append($('#eavAttrsToolbar').html());
				
				//自适应宽度
				setGridWidth("#eavAttrsList","regCenter",20);
		},	
		/**
		 * @description 国际化true和false
		 **/
		isNullForma:function(value){
			if(value){
				return i18n.no;
			}else{
				return i18n.yes;
			}
		},
		
		/**
		 * @description 根据ID刷新扩展属性 
		 **/
		reloadEavAttrs:function(id){
			eavNo=id;
			var _url = 'attrs!find.action?attrsQueryDTO.eavNo='+id;	
			$('#eavAttrsList').jqGrid('setGridParam',{url:_url})
				.trigger('reloadGrid',[{"page":"1"}]).jqGrid('setGridParam',{url:_url});
		},
		
        /**
         * @description 打开新增扩展属性字典面板
         **/
		openAttrsDiv:function(){
			if(eavNo===null || eavNo==="0" || eavNo===undefined){
				msgAlert(i18n.msg_chooseEav,'info');	
			}else{
				$("#add_attrAsName").val("");
				$("#add_attrGroupName").val("");
				$("#add_attrGroupNo").val("");
				$("#add_attrType_select").val("String");
				$("#dataDictionarayTradd,#CheckboxTradd,#RadioTradd").attr("class","display_none"); 
				$("#add_dataDictionary_Classify").val("");
				$("#add_sortNo").val("0");
				$("#table_CheckboxTradd #table_RadioTradd").empty();
				windows('addAttrDiv',{width:400,height:300});
			}
		},
		
        /**
         * @description 新增扩展属性字典
         **/
		saveAttrs:function(){	
			if($('#addAttrForm').form('validate')){
				if($("#add_dataDictionary_Classify").val()===""&&$('#add_attrType_select').val()=="DataDictionaray"){
					msgAlert(i18n.eav_datadictionaryIsNotNull,'info');	
				}else{
					var form = $('#addAttrDiv form').serialize();
					//$('#addAttrDiv').dialog('close');
					$('#addAttrDiv form')[0].reset();
					form=form+"&attributesDTO.eavNo="+eavNo;
					startProcess();
					$.post('attrs!save.action', form, function(){
						$('#addAttrDiv').dialog('close');
						$('#eavAttrsList').trigger('reloadGrid');
						msgShow(i18n.saveSuccess,'show');
						$('#add_attrName').val('');
						$('#add_attrAsName').val('');
						$('#add_length').val('0');
						$("#add_attrIsNull").attr("checked","");
						$("#addAttrForm .display_black").attr("class","display_none");
						/**保存向导状态*/
						common.config.systemGuide.systemGuide.saveSystemGuide("eavCiGuide");
						endProcess();
					});
				}
				
			}
		},
		/**
		 * @description 编辑扩展属性字段时获取id
		 **/
		openEditAttrsDiv:function(){
			checkBeforeEditGrid("#eavAttrsList",common.eav.eavCi.editAttrsMethod);
		},
		/**
		 * @description 编辑扩展属性Method
		 **/            
		editAttrsMethod:function(rowData){
			$('#edit_attrNo').val(rowData.attrNo);
			$('#edit_attrCode').val(rowData.attrCode);    
			$('#edit_attrName').val(rowData.attrName);
			$('#edit_attrAsName').val(rowData.attrAsName);
			$('#edit_attrType').val(rowData.attrType);
			$('#edit_attrType_select').val(rowData.attrType);
/*			if(rowData.attrType=="DataDictionaray"){ 
				$("#dataDictionarayTredit").removeAttr("style");  
			}else{     
				$("#dataDictionarayTredit").attr("style","display:none;"); 
			}*/
			var trName="#editAttrForm #"+rowData.attrType+"Tredit";
			$("#editAttrForm .display_black").attr("class","display_none");
			$(trName).attr("class","display_black");
			
			$('#edit_dataDictionary').val(rowData.attrdataDictionary);                      
			$('#edit_dataDictionary_Classify').val(rowData.attrdataDictionary);
			$('#edit_length').val(rowData.length);
			$('#edit_sortNo').val(rowData.sortNo);
			$('#edit_attrGroupName').val(rowData.attrGroupName);
			$('#edit_attrGroupNo').val(rowData.attrGroupNo);
			$('#edit_attrIsNull').val(rowData.attrIsNull);
			if(rowData.attrIsNull=='是'){
				$("#edit_checked_attrIsNull").attr("checked","checked");
			}else{
				$("#edit_checked_attrIsNull").attr("checked","");
			}
			$("#table_CheckboxTredit,#table_RadioTredit").empty();
			if(rowData.attrType==='Radio' || rowData.attrType==='Checkbox'){
				$.post('attrs!findAttributeItems.action', "attributesDTO.attrNo="+rowData.attrNo, function(data){
					var _int=0;
					for(var key in data)  {
						var html="<tr><td><input name='attributesDTO.attrItemNo' type='hidden' value='"+key+"'><input name='attributesDTO.attrItemName' class='input' value='"+data[key]+"'></td><td><span>{img}</span></td></tr>";
						var img="<img src='../skin/default/images/add.png' onclick=\"common.eav.eavCi.addClone('#table_"+rowData.attrType+"Tredit')\" style='cursor: pointer'/>";
						if(_int>0){
							img="<img onclick='common.eav.eavCi.deleteAttrItem(this,"+key+");' style='cursor: pointer' src='../skin/default/images/grid_delete.png' />";
						}
						$("#table_"+rowData.attrType+"Tredit").append(html.replace(/{img}/g,img));
						_int++;
					}
				});
			}
			windows('editAttrDiv',{width:400,height:300});
		},
		deleteAttrItem:function(event,key){
			$(event).parent().parent().parent().remove();
			deleteAttrItemNo=deleteAttrItemNo+','+key;
		},
		/**
		 * @description 编辑扩展属性
		 **/
		editSaveAttrs:function(){		
			if($('#editAttrForm').form('validate')){
				if($("#edit_dataDictionary_Classify").val()===""&&$('#edit_attrType_select').val()=="DataDictionaray"){
					msgAlert(i18n.eav_datadictionaryIsNotNull,'info');	
				}else{
					var _params = $('#editAttrForm').serialize();
					if(document.getElementById("edit_checked_attrIsNull").checked){
						_params=_params+"&attributesDTO.eavNo="+eavNo+"&attributesDTO.attrIsNull=false";
					}else{
						_params=_params+"&attributesDTO.eavNo="+eavNo+"&attributesDTO.attrIsNull=true";
					}
					$.post('attrs!update.action', _params+"&attributesDTO.deleteAttrItemNo="+deleteAttrItemNo.substr(1).split(','), function(){
						$('#editAttrDiv').dialog('close');
						$('#eavAttrsList').trigger('reloadGrid');
						msgShow(i18n.editSuccess,'show');
						$('#edit_attrName').val('');
						$('#edit_attrAsName').val('');
						$('#edit_length').val('0');
						deleteAttrItemNo='';
						$("#editAttrDiv .display_black table").html('');
						$("#editAttrDiv .display_black").attr("class","display_none");
						$("#table_"+$("#edit_attrType").val()+"Tredit").html("");
						/**保存向导状态*/
						common.config.systemGuide.systemGuide.saveSystemGuide("eavCiGuide");
					});
				}
			}
		},
		
		/**
		 * @description 删除属性字段时获取id 
		 **/
		deleteEavAttrs:function(){
			checkBeforeDeleteGrid("#eavAttrsList",common.eav.eavCi.deleteEavAttrsMethod);
		},
		/**
		 * @description  删除字段
		 * @param rowIds 扩展字段id数组
		 **/
		deleteEavAttrsMethod:function(rowIds){
			var pp = $.param({'attrsIds':rowIds},true);
			$.post("attrs!delete.action", pp, function(data){
				if(data){
					$('#eavAttrsList').trigger('reloadGrid');
					msgShow(i18n.deleteSuccess,'show');
				}else{
					msgShow(i18n['msg_canNotDeleteSystemData'],'warning');
				}
			}, "json");
		},
		/**
		 * @description  查询数据字典
		 * @param select 控件id
		 **/
		loadDataDictionarysToSelect:function(select){
			$(select).html('');
			$('<option value="">--'+i18n.pleaseSelect+'--</option>').appendTo(select);
			var url="dataDictionaryGroup!findDataDictionary.action";
			$.post(url,function(dataDictionary){     
				$.each(dataDictionary,function(key,value){
					$("<option value='"+value.groupCode+"'>"+value.groupName+"</option>").appendTo(select);
				});   
			});            
		},
		/**
		 * @description 隐藏扩展属性
		 * @param value 扩展属性类型
		 * @param name add or edit
		 **/
		showDataDictionaray:function(value,name){  
			var trName="#"+name+"AttrForm #"+value+"Tr"+name;
			$("#"+name+"AttrForm .display_black").attr("class","display_none");
			$(trName).attr("class","display_black");
			$(trName+" table").html('<tr><td><input name="attributesDTO.attrItemName" class="input"></td><td><span><img src="../skin/default/images/add.png" onclick="common.eav.eavCi.addClone(\''+trName+' table\')" style="cursor: pointer"/></span></td></tr>');
		},
		/**
		 * @description 初始化
		 */
		init:function(){
			showAndHidePanel("#eavCi_panel","#eavCi_loading");
			common.eav.includes.includes.loadAttributeGroupIncludesFile();
			//扩展名
			common.eav.eavCi.eavCi_grid();
			$("#add_attrGroupName").click(function(){
				common.eav.selectAttributeGroup.openGrid('add_attrGroupNo','add_attrGroupName');
			});
			$("#edit_attrGroupName").click(function(){
				common.eav.selectAttributeGroup.openGrid('edit_attrGroupNo','edit_attrGroupName');
			});
			$("#search_EavAttrGroupName").click(function(){
				common.eav.selectAttributeGroup.openGrid('search_attrGroupNo','search_EavAttrGroupName');
			});
			
			//加载数据字典分组
			common.eav.eavCi.loadDataDictionarysToSelect("#add_dataDictionary_Classify");
			common.eav.eavCi.loadDataDictionarysToSelect("#edit_dataDictionary_Classify");
			common.eav.eavCi.loadDataDictionarysToSelect("#find_dataDictionary_Classify");
			common.config.category.CICategoryUtil.loadEavsToSelect("#add_parentEavNo");//加载列表
		}
	};
}();
$(document).ready(common.eav.eavCi.init);
