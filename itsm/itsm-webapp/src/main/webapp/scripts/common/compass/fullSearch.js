$package("common.compass"); 
 /**  
 * @fileOverview "全文检索"
 * @author Van  
 * @constructor WSTO
 * @description 全文检索
 * @date 2011-06-13
 * @version 1.0  
 * @since version 1.0 
 */
common.compass.fullSearch = function() {
	this.enterFullSearch_KeywordId='';
	this.enterFullSearch_TypeName='';
	this.fullsearch_foucs=false;
	this.fullsearch_knowledgeId="";
	return {
		/**
		 * 打开窗口
		 * @param p_keyword 关键字
		 * @param p_searchType 类型
		 */
		openFullsearchWindow:function(p_keyword,p_searchType){
			
			$('#fullSearchKeyWord').val(p_keyword);
			$("input[name='fullSearch_type'][value='"+p_searchType+"']").attr("checked",true);//根据Value值设置Radio为选中状态;
			windows('index_fullSearch_window',{width:360});
		},
		/**
		 * 同步搜索框的值
		 * @param keywordId 关键字文本框ID
		 * @param searchTypeName 类型
		 */
		setFullsearchValue:function(keywordId,searchTypeName){//同步值
			var keyWord=$(keywordId).val();
			if(keyWord != '' &&  keyWord !=null){
				if(fullsearch_knowledgeId==""){//请求,问题,变更
					var searchType=$("input[name='"+searchTypeName+"']:checked").val();
					$('#fullSearchKeyWord').val(keyWord);
					$("input[name='fullSearch_type'][value='"+searchType+"']").attr("checked",true);  //根据Value值设置Radio为选中状态;
					common.compass.fullSearch.doFullSearch();//执行搜索
				}else{
					common.compass.fullSearch.searchKnowledge(fullsearch_knowledgeId);
				}
			}else{
				msgShow(i18n.validate_notNull,'show');
			}
		},
		/**
		 * 全文搜索回车事件
		 */
		enterPress:function(e){
			var e = e || window.event;
			if(e!=undefined && e.keyCode == 13){
				common.compass.fullSearch.setFullsearchValueNew('#lf_protal_fullSearchKeyWord','#type');
				return false;
			} 
		},
		/**
		 * 同步搜索框的值 will
		 * @param keywordId 关键字文本框ID
		 * @param searchTypeName 类型id
		 */
		setFullsearchValueNew:function(keywordId,searchTypeId){//同步值
			var keyWord=$(keywordId).val();
			if(keyWord != '' &&  keyWord !=null){
				if(fullsearch_knowledgeId==""){//请求,问题,变更
					var searchType=$(searchTypeId).attr("typename");
					$('#fullSearchKeyWord').val(keyWord);
					//$("input[name='fullSearch_type'][value='"+searchType+"']").attr("checked",true);  //根据Value值设置Radio为选中状态;
					common.compass.fullSearch.doFullSearchNew(keyWord,searchType);//执行搜索
				}else{
					common.compass.fullSearch.searchKnowledge(fullsearch_knowledgeId);
				}
			}else{
				msgShow(i18n.validate_notNull,'show');
			}
		},
		doFullSearchNew:function(keyWord,searchType){
			if(searchType=="request"){
				//打开或选择对应的页面并显示
				basics.tab.tabUtils.openTabCallback(i18n['title_request_requestGrid'],'../pages/itsm/request/requestMain.jsp?fullsearch=yes',function(){
					var _url="request!analogousRequest.action";
					var postData = $("#requestGrid").jqGrid("getGridParam", "postData");
					$.extend(postData,{'fullTextQueryDTO.alias':'RequestInfo','fullTextQueryDTO.queryString':keyWord});
					$('#requestGrid').jqGrid('setGridParam',{page:1,url:_url,postData:postData}).trigger('reloadGrid');
				});
			}else if(searchType=="problem"){
				//打开或选择对应的页面并显示
				basics.tab.tabUtils.openTabCallback(i18n['problem_grid'],'../pages/itsm/problem/problemMain.jsp?fullsearch=yes',function(){					
					var _url="problem!fullSearch.action";
					var postData = $("#problemGrid").jqGrid("getGridParam", "postData");     					
					$.extend(postData, {'fullTextQueryDTO.alias':'Problems','fullTextQueryDTO.queryString':keyWord});
					$('#problemGrid').jqGrid('setGridParam',{page:1,url:_url,postData:postData}).trigger('reloadGrid');
				});	

			}else if(searchType=="change"){
				//打开或选择对应的页面并显示
				basics.tab.tabUtils.openTabCallback(i18n['title_changeList'],"../pages/itsm/change/changeMain.jsp?fullsearch=yes",function(){
					var _url="change!fullSearch.action";
					var postData = $("#changeGrid").jqGrid("getGridParam", "postData");  
					$.extend(postData, {'fullTextQueryDTO.alias':'Changes','fullTextQueryDTO.queryString':keyWord});			
					$('#changeGrid').jqGrid('setGridParam',{page:1,url:_url,postData:postData}).trigger('reloadGrid');
				});
			}else if(searchType=="knowledge"){
				//打开或选择对应的页面并显示
				basics.tab.tabUtils.openTabCallback(i18n['title_request_knowledgeGrid'],"../pages/common/knowledge/knowledgeMain.jsp?fullsearch=yes",function(){
					var _url="knowledgeInfo!fullSearch.action";
					var postData = $("#knowledgeGrid").jqGrid("getGridParam", "postData");  
					$.extend(postData, {'fullTextQueryDto.alias':'KnowledgeInfo','fullTextQueryDto.queryString':keyWord});			
					$('#knowledgeGrid').jqGrid('setGridParam',{page:1,url:_url,postData:postData}).trigger('reloadGrid');
				});
			}
			setTimeout(function(){	
				fullsearch_foucs=true;	
			},200);
		},
		
		/**
		 * 设置搜索标识
		 */
		setSearchFlag:function(){
			fullsearch_foucs=true;
			fullsearch_knowledgeId='';
		},
		/**
		 * 设置全文检索ID
		 * @param p_id 全文检索ID 
		 */
		setFullSearchId:function(p_id){			
			fullsearch_knowledgeId=p_id;
		},

		/**
		 * 执行全文搜索
		 */
		doFullSearch:function(){
			var keyWord=$('#fullSearchKeyWord').val();
			var searchType=$("input[name='fullSearch_type']:checked").val();
			if(searchType=="request"){
				//打开或选择对应的页面并显示
				basics.tab.tabUtils.openTabCallback(i18n['title_request_requestGrid'],'../pages/itsm/request/requestMain.jsp?fullsearch=yes',function(){
					var _url="request!analogousRequest.action";
					var postData = $("#requestGrid").jqGrid("getGridParam", "postData");
					$.extend(postData,{'fullTextQueryDTO.alias':'RequestInfo','fullTextQueryDTO.queryString':keyWord});
					$('#requestGrid').jqGrid('setGridParam',{page:1,url:_url,postData:postData}).trigger('reloadGrid');
				});
			}else if(searchType=="problem"){
				//打开或选择对应的页面并显示
				basics.tab.tabUtils.openTabCallback(i18n['problem_grid'],'../pages/itsm/problem/problemMain.jsp?fullsearch=yes',function(){					
					var _url="problem!fullSearch.action";
					var postData = $("#problemGrid").jqGrid("getGridParam", "postData");     					
					$.extend(postData, {'fullTextQueryDTO.alias':'Problems','fullTextQueryDTO.queryString':keyWord});
					$('#problemGrid').jqGrid('setGridParam',{page:1,url:_url,postData:postData}).trigger('reloadGrid');
				});	

			}else if(searchType=="change"){
				//打开或选择对应的页面并显示
				basics.tab.tabUtils.openTabCallback(i18n['title_changeList'],"../pages/itsm/change/changeMain.jsp?fullsearch=yes",function(){
					var _url="change!fullSearch.action";
					var postData = $("#changeGrid").jqGrid("getGridParam", "postData");  
					$.extend(postData, {'fullTextQueryDTO.alias':'Changes','fullTextQueryDTO.queryString':keyWord});			
					$('#changeGrid').jqGrid('setGridParam',{page:1,url:_url,postData:postData}).trigger('reloadGrid');
				});
			}else if(searchType=="knowledge"){
				//打开或选择对应的页面并显示
				basics.tab.tabUtils.openTabCallback(i18n['title_request_knowledgeGrid'],"../pages/common/knowledge/knowledgeMain.jsp?fullsearch=yes",function(){
					var _url="knowledgeInfo!fullSearch.action";
					var postData = $("#knowledgeGrid").jqGrid("getGridParam", "postData");  
					$.extend(postData, {'fullTextQueryDto.alias':'KnowledgeInfo','fullTextQueryDto.queryString':keyWord});			
					$('#knowledgeGrid').jqGrid('setGridParam',{page:1,url:_url,postData:postData}).trigger('reloadGrid');
				});
			}
			setTimeout(function(){	
				fullsearch_foucs=true;	
			},200);
		},
		/**
		 * 设置查询值
		 * @param p_enterFullSearch_KeywordId 关键字文本框ID
		 * @param p_enterFullSearch_TypeName 类型
		 */
		setEnterFullSearch:function(p_enterFullSearch_KeywordId,p_enterFullSearch_TypeName){
			enterFullSearch_KeywordId=p_enterFullSearch_KeywordId;
			enterFullSearch_TypeName=p_enterFullSearch_TypeName;
		},
		/**
		 * 全文检索回车
		 */
		enterFullSearch:function(){
				setTimeout(function(){
					if(fullsearch_foucs||fullsearch_knowledgeId!=""){
						$('.WSTUO-dialog').dialog('close');
						common.compass.fullSearch.setFullsearchValue(enterFullSearch_KeywordId,enterFullSearch_TypeName);	
						fullsearch_foucs=false;
					}
				},0);
		},
		/**
		 * 全文检索框失去焦点
		 */
		disable:function(){
			fullsearch_foucs=false;
			fullsearch_knowledgeId="";
		},
		/**
		 * 全文检索知识. 
		 * @param  keyWordId  关键字文本框ID
		 */
		searchKnowledge:function(keyWordId){
			var _keyWord=$(keyWordId).val();
			if(_keyWord != ''){
				$('#fullSearchKeyWord').val(_keyWord);
				//打开或选择对应的页面并显示
				basics.tab.tabUtils.openTabCallback(i18n['title_request_knowledgeGrid'],"../pages/common/knowledge/knowledgeMain.jsp?fullsearch=yes",function(){
					var _url="knowledgeInfo!fullSearch.action";
					var postData = $("#knowledgeGrid").jqGrid("getGridParam", "postData");  
					$.extend(postData, {'fullTextQueryDto.alias':'KnowledgeInfo','fullTextQueryDto.queryString':_keyWord});			
					$('#knowledgeGrid').jqGrid('setGridParam',{page:1,url:_url,postData:postData}).trigger('reloadGrid');				
				});
			}
		},
		/**
		 * 全文检索框失去焦点
		 */
		onblurEvent:function(){
			fullsearch_knowledgeId='';
			fullsearch_foucs=false;
		},
		/**
		 * 初始化加载
		 */
		init:function(){
			$('#fullSearchBtn').unbind().click(function(){
				common.compass.fullSearch.setFullsearchValue('#fullSearchKeyWord','fullSearch_type');
			});
		}
	};
}();
//载入
$(document).ready(common.compass.fullSearch.init);