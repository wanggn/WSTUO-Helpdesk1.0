$package('common.security');
$import("common.security.userUtil");
$import('itsm.itsop.selectCompany');
/**  
 * @author Van  
 * @constructor WSTO
 * @description 用户在线日志.
 * @date 2010-11-17
 * @since version 1.0 
 */
common.security.userOnlineManage = function(){
	return {
		/**
		 * @description 创建在线列表
		 */
		 showUserOnlineGrid:function(){
				var params = $.extend({},jqGridParams, {	
					url:'useronlinelog!findPager.action?useronlinelogQueryDTO.isSearchOffLine=true&useronlinelogQueryDTO.companyNo='+companyNo,
					colNames:['ID',i18n['user'],i18n['name1'],i18n['onlineTime'],i18n['offlineTime'],i18n['onlineTotal']],
					colModel:[
							  {name:'id',width:70,align:'center'},
							  {name:'userName',align:'center',width:120,sortable:true},
							  {name:'fullName',align:'center',width:120,sortable:true},
							  {name:'onlineTime',width:70,align:'center',sortable:true,formatter:timeFormatter},
							  {name:'offlineTime',width:120,align:'center',sortable:true,formatter:timeFormatter},
							  {name:'period',hidden:true}
							  
							  ],
					jsonReader: $.extend(jqGridJsonReader, {id: "id"}),
					sortname:'id',
					multiselect:true,
					pager:'#userOnlineManageGridPager',
					gridComplete:function(){//删除admin用户记录
						var array = $("#userOnlineManageGrid").getDataIDs();
						var datas = null;
						for(var i = 0 ; i < array.length; i++){
							datas = $("#userOnlineManageGrid").getRowData(array[i]);
							if(datas.userName=='admin'){
								$("#userOnlineManageGrid").delRowData(datas.id);
							}
						}
					}
				});
				$("#userOnlineManageGrid").jqGrid(params);
				$("#userOnlineManageGrid").navGrid('#userOnlineManageGridPager',navGridParams);
				
				//列表操作项
				$("#t_userOnlineManageGrid").css(jqGridTopStyles);
				$("#t_userOnlineManageGrid").append($('#userOnlineManageGridToolbar').html());
				//自适应宽度
				setGridWidth("#userOnlineManageGrid","regCenter",20);
		},
		/**
		 * @description 让用户下线
		 */
		offline:function(){
			checkBeforeMethod('#userOnlineManageGrid',function(rowIds){
				msgConfirm(i18n.Operation_Tips,i18n.label_security_offline,function(){
					for(var id in rowIds){
						common.security.userOnlineManage.offlineOperation(rowIds[id]);
					}
				});
			});
		},
		/**
		 * 
		 *  
		 */
		offlineOperation:function(id){
			var rowData = $('#userOnlineManageGrid').getRowData(id);
			$.post("user!loginOutByLoginName.action","userDto.loginName="+rowData.userName,function(data){
				if(data){
					msgShow(i18n.label_security_offlined,"show");
					$('#userOnlineManageGrid').trigger("reloadGrid");
				}else{
					msgShow(i18n.label_security_failure,"show");
				}
			});
		},
//		/**
//		 * @description 选择用户
//		 */
//		selectUserOnlineUserOpenWinow:function(){
//			common.security.userUtil.selectUser('#useronlinemanage_user_Name','','','loginName','-1');
//		},
//		/**
//		 * @description 搜索在线用户
//		 */
//		searchUserOnline_do:function(){		
//			
//			
//			if($('#searchUserOnlineManage form').form('validate')){
//				//获得表单对象
//				var sdata = $('#searchUserOnlineManage form').getForm();
//				var postData = $('#userOnlineGrid').jqGrid('getGridParam', 'postData');       
//				$.extend(postData, sdata);		
//				var _url = 'useronlinelog!findPager.action?userOnlineLogQueryDTO.isSearchOffLine=true';	
//				$('#userOnlineGrid').jqGrid('setGridParam',{url:_url,page:1}).trigger('reloadGrid');
//			}
//		},
//		
//		/**
//		 * @description 刷新在线用户用户列表
//		 */
//		refreshUserOnlineManagejqGrid:function(){
//			
//			$('#useronlinemanage_keyWord').val("");
//			$('#onlineTime').val("");
//			$('#onlineEndTime').val("");
//			$('#offlineTime').val("");
//			$('#offlineEndTime').val("");
//	
//			//获得表单对象
//			var sdata = $('#searchUserOnlineManage form').getForm();		
//			var postData = $('#userOnlineGrid').jqGrid('getGridParam', 'postData');       
//			$.extend(postData, sdata); 
//			var _url = 'useronlinelog!findPager.action?userOnlineLogQueryDTO.isSearchOffLine=true';	
//			
//			$('#userOnlineGrid').jqGrid('setGridParam',{url:_url,page:1}).trigger('reloadGrid');
//		},
		/**
		 * 初始化加载
		 */
		init:function(){
			$("#UserOnlineManage_loading").hide();
			$("#UserOnlineManage_content").show();
			common.security.userOnlineManage.showUserOnlineGrid();
			$('#userOnlineManageGrid_offline').click(common.security.userOnlineManage.offline);
//
//			$('#userOnlineGrid_search').click(function(){
//				bindControl('#onlineTime,#onlineEndTime,#offlineTime,#offlineEndTime');
//				windows('searchUserOnlineManage',{width:450,modal: false,colse:function(){
//					$('#searchUserOnlineManage').click();
//				}});
//			});
//			
//			
//			$('#useronlinemanage_user_Name_select').click(common.security.userOnlineManage.selectUserOnlineUserOpenWinow);
//			
//			$('#userOnlineGrid_doSearch').click(common.security.userOnlineManage.searchUserOnline_do);
//			
//			
//			//选择公司
//			$('#useronlinemanage_companyName').click(function(){
//				itsm.itsop.selectCompany.openSelectCompanyWin('#useronlinemanage_companyNo','#useronlinemanage_companyName','','all');
//			});
//			//加载默认公司
//			common.security.defaultCompany.loadDefaultCompany('#useronlinemanage_companyNo','#useronlinemanage_companyName');
		}
	}
 }();
//载入
 $(document).ready(common.security.userOnlineManage.init);

