$package('common.security');
$import('common.security.functionTree');

/**  
 * @author Van  
 * @constructor 功能列表.
 * @description 资源管理主函数.
 * @date 2011-02-25
 * @since version 1.0 
 */

common.security.functionGrid = function(){	
	
	
	return {
		
		/**
		 * @description 图标格式化.
		 * @param cell 当前列值
		 * @param event 事件
		 * @param data 行数据
		 * @private
		 */
		functionIconFormatter:function(cell,opt,data){
			
			if(data.resIcon == null || data.resIcon==""){
				return i18n['label_resource_notUpload'];
			}else{
				return "<img src='organization!getImageUrlforimageStream.action?imgFilePath="+data.resIcon+"' width='20px' height='20px' />";
			}
		},
		
		
		/**
		 * @description 格式化.
		 * @param cell 当前列值
		 * @param event 事件
		 * @param data 行数据
		 * @private
		 */
		functionActionFormatter:function(cell,opt,data){
			
			
			return actionFormat('1','1')
			.replace('[edit]','common.security.functionGrid.openEditFunctionWindowOnLine('+data.resNo+')')
			.replace('[delete]','common.security.functionGrid.deleteFunction('+data.resNo+')');
			
		},
		
		/**
		 * @description 功能列表.
		 */
		showFunctionGrid:function(){

			var params = $.extend({},jqGridParams, {	

								url : 'function!findGrid.action',
								//caption : i18n['caption_functionGrid'],
								colNames : [ 
								        i18n['title_resource_code'],
										i18n['common_name'],
										i18n['title_resource_icon'],
										i18n['title_resource_url'],
										i18n['common_action'], 
										'', 
										'', 
										'', 
										'' 
										],
								colModel : [
								        {name : 'resCode',width : 20},
										{name : 'resName',width : 15,align : 'center'},
										{name : 'resIconPanel',width : 15,align : 'center',formatter : common.security.functionGrid.functionIconFormatter,sortable:false},
										{name : 'resUrl',width : 30,align : 'center'},
										{name : 'act',width : 20,align : 'center',formatter : common.security.functionGrid.functionActionFormatter,sortable:false}, 
										{name : 'parentFunctionName',hidden : true}, 
										{name : 'parentFunctionNo',hidden : true}, 
										{name : 'resIcon',hidden : true}, 
										{name : 'resNo',hidden : true} 
										],
								jsonReader : $.extend(jqGridJsonReader, {
									id : "resNo"
								}),
								sortname : 'resNo',
								pager : '#functionPager'
							});
			$("#functionGrid").jqGrid(params);
			$("#functionGrid").navGrid('#functionPager', navGridParams);
			//列表操作项
			$("#t_functionGrid").css(jqGridTopStyles);
			$("#t_functionGrid").append($('#functionToolbar').html());
			//自适应宽度
			setGridWidth('#functionGrid', 'resourceTabMain', 10);
			resourceGrids.push('#functionGrid');
		},
		
		
	/**
	 * @description 打开新增功能窗口.
	 */
	openAddFunctionWindow:function(){
		$('#addFunction_win form input').val('');
		windows('addFunction_win',{width:500});
	},
	
	/**
	 * 验证父功能节点.
	 */
	validateParentFunction:function(){
		var parentNo=$('#function_parentFunctionNo').val();
		if(parentNo==null || parentNo==''){
			msgAlert(i18n['msg_chooseParentFunction'],'info');
			return false;
		}else{
			return true;
		}
	},
	
	 /**
	  * @description 添加功能.
	  */
	 addFunction:function(){
			if(common.security.functionGrid.validateParentFunction()&&$('#addFunctionForm_win').form('validate')){
				//验证资源名称
				 var resName=$('#addFunction_resName_win').val();
				 var url_resName="function!findExist.action?functionQueryDTO.resName="+resName;
				 $.post(url_resName,function(res){
					if(res==true){
						msgAlert(i18n['msg_resource_functionNameExist'],'info');
						return;
					}else{
						//验证资源编码 start
						var resCode=$('#addFunction_resCode_win').val();
						var url_resCode="function!findExist.action?functionQueryDTO.resCode="+resCode;
						 //判断操作编码
						 $.post(url_resCode,function(res){
								if(res==true){
									msgAlert(i18n['msg_resource_functionCodeExist'],'info');
									return;
								}else{
									//验证资源编码 start
									var url_resCodeOperation="operation!findOperationExist.action?operationQueryDto.resCode="+resCode;
									 //判断操作编码
									 $.post(url_resCodeOperation,function(res){
											if(res==true){
												msgAlert(i18n['msg_resource_operationCodeExist'],'info');
												return;
											}else{
												//判断资源地址 start
												var resUrl=$('#addFunction_resUrl_win').val();
												var url_resUrl="function!findExist.action?functionQueryDTO.resUrl="+resUrl;
												 //判断操作编码
												 $.post(url_resUrl,function(res){
														if(res==true){
															msgAlert(i18n['msg_resource_functionUrlExist'],'info');
															return;
														}else{
															 var frm = $('#addFunction_win form').serialize();
															 var url = 'function!addFunction.action';
															 $.post(url,frm, function(){
																 	common.security.functionGrid.refreshGrid();//刷新数据列表
																 	msgShow(i18n['addSuccess'],'show');
																 	resetForm("#addFunctionForm_win");//清空表单数据
																 	$('#show_addFunction_resIcon_win').html("");//清空图标
															 });
														}
												 });
												//判断资源地址end
											}
									 });
									//验证资源编码 end
								} 
							 });
					}
					 
				 });
			}
	 },
	 
	 
	/**
	 * @description 打开编辑功能窗口.
	 */
	openEditFunctionWindow:function (){
		
		//调用方法验证
		checkBeforeEditGrid('#functionGrid',function(rowData){
		
			common.security.functionGrid.openEditFunctionCommon(rowData);
		});

	},
	
	/**
	 * 行内编辑.
	 * @param rowId 行编号
	 */
	openEditFunctionWindowOnLine:function(rowId){
		
		 var rowData=$('#functionGrid').getRowData(rowId);	 
		
		 common.security.functionGrid.openEditFunctionCommon(rowData);
		
	},
	
	
	/**
	 * 编辑窗口.
	 * @param rowData 行数据
	 */
	openEditFunctionCommon:function(rowData){
		
		 $('#editFunction_resName_win').val(rowData.resName);
		 $('#editFunction_resCode_win').val(rowData.resCode);
		 $('#editFunction_resIcon_win').val(rowData.resIcon);
		 $('#editFunction_resUrl_win').val(rowData.resUrl);
		 $('#editFunction_resNo_win').val(rowData.resNo);
		 $('#editFunction_parentFunction_win').val(rowData.parentFunctionName);
		 $('#editFunction_parentFunctionNo').val(rowData.parentFunctionNo);
		 if(rowData.resIcon!='' && rowData.resIcon!=null){
			 $('#show_editFunction_resIcon_win').html('<img src="organization!getImageUrlforimageStream.action?imgFilePath='+rowData.resIcon+'" width="30px" height="30px" />');
		 }else{
			 $('#show_editFunction_resIcon_win').html('');
		 }
		 windows('editFunction_win',{width:500});
	},
	
	
	
	
	 /**
	  * @description 进行编辑操作.
	  */
	 editFunction:function(){
		 
		 if($('#editFunctionForm_win').form('validate')){
			 var frm = $('#editFunction_win form').serialize();
			 var url = 'function!updateFunction.action';
			 $.post(url,frm, function(){	
				 
				common.security.functionGrid.refreshGrid();
				resetForm("#editFunctionForm_win");//清空表单数据
			 	msgShow(i18n['editSuccess'],'show');
			 	
			 });
		 }
	 },
	
	
	/**
	 * @description 关闭编辑窗口.
	 */
	closeEditFunctionWindow:function(){
		 $('#editFunction_win').dialog('close');
	},
	

	/**
	 * @description 删除功能.
	 */
	deleteFunctions:function(){
		
		checkBeforeDeleteGrid('#functionGrid',function(rowIds){
			
			var param = $.param({'resNos':rowIds},true);
			$.post("function!deleteFunctionByIds.action", param, function(data){
				if(data){
					
					common.security.functionGrid.refreshGrid();
					msgShow(i18n['msg_deleteSuccessful'],'show');
					
				}else{
					
					msgAlert(i18n['msg_canNotDeleteSystemData'],'info');
				}	
				
			}, "json");
			
			
			//清空左边信息
			var idsStr=rowIds.join(",")+",";
			var resNo_base=$('#editCurrentFunction_resNo').val()+",";
			
			if(idsStr.indexOf(resNo_base)!=-1){
				
				$('#edit_resName').val("ROOT");
				$('#edit_resCode').val("");
				$('#edit_icon').val("");
				$('#edit_resUrl').val("");
				$('#edit_resNo').val("");
				$('#add_parentFunctionNo').val("-1");
				$('#search_FunctionNo').val("-1");
			}
			
		});
	},

	
	/**
	 * @description 行内删除功能.
	 * @param rowId  行编号
	 */
	deleteFunction:function(rowId){
		
		
		confirmBeforeDelete(function(){
			
			
			var param = $.param({'functionDto.resNo':rowId},true);
			$.post("function!deleteFunction.action", param, function(data){
								
				if(data){
					
					common.security.functionGrid.refreshGrid();	
					
					msgShow(i18n['msg_deleteSuccessful'],'show');	
				}
				else{
					
					msgAlert(i18n['msg_canNotDeleteSystemData'],'info');
				}					
			
			}, "json");
			
		});
		
		

	},
	
	/**
	 * 刷新
	 */
	refreshGrid:function(){
		
		$('.WSTUO-dialog').dialog('close');
		$('#functionGrid').trigger('reloadGrid');		
		common.security.functionTree.showFunctionTree('#functionTreeDIV');//刷新功能树结构
		
	},
	
	/**
	 * @description 打开搜索功能窗口.
	 */
	openSearchFunctionWindow:function(){
		windows('searchFunction_win',{width:380,modal: false});
	},

	/**
	 * @description 进行搜索操作.
	 */
	searchFunction:function(){
		
		var sdata = $('#searchFunction_win form').getForm();
		var postData = $("#functionGrid").jqGrid("getGridParam", "postData");       
		$.extend(postData, sdata);
		
		var _url = 'function!findGrid.action';		
		$('#functionGrid').jqGrid('setGridParam',{url:_url}).trigger('reloadGrid');
	},
	
	/**
	 * 导出数据
	 */
	exportData:function(){
		
		var searchData = $('#searchFunction_win form').getForm();
		var gridData = $("#functionGrid").jqGrid("getGridParam", "postData");   
		
		for(i in searchData){    	   	
			
			if(searchData[i]!="" && searchData[i]!=null){
				
				if(i=='functionQueryDTO.resName'){$('#exportFunction_resName').val(searchData[i]);}
				if(i=='functionQueryDTO.resCode'){$('#exportFunction_resCode').val(searchData[i]);}
				if(i=='functionQueryDTO.resUrl'){$('#exportFunction_resUrl').val(searchData[i]);}
			}
		}
		$('#exportFunction_sidx').val(gridData.sidx);
		$('#exportFunction_sord').val(gridData.sord);
		$('#exportFunction_page').val(gridData.page);
		$('#exportFunction_rows').val(gridData.rows);
		$('#exportFunctionForm').submit();
		
	},
	
	/**
	 * 导入数据.
	 */
	importData:function(){

		importCSV('function!importFunction.action',function(res){
			
			var msg=i18n['msg_dc_importFailure']
			
			if(res=="success"){
				i18n['msg_dc_dataImportSuccessful']
			}
			
			$('#index_import_excel_window').dialog('close');
			$('#functionGrid').trigger('reloadGrid');
			common.security.functionTree.showFunctionTree('#functionTreeDIV');//刷新功能树结构
        	msgShow(msg,'show');
        	resetForm('#index_import_excel_window form');
		});

	},
	
	/**
	 * 初始化加载
	 */
	init:function(){
		//加载列表
		common.security.functionGrid.showFunctionGrid();

		$("#functionGrid_add").click(common.security.functionGrid.openAddFunctionWindow);
		$("#functionGrid_edit").click(common.security.functionGrid.openEditFunctionWindow);
		$("#functionGrid_delete").click(common.security.functionGrid.deleteFunctions);
		$("#functionGrid_search").click(common.security.functionGrid.openSearchFunctionWindow);
		
		$("#addFunctionBtn_OK").click(common.security.functionGrid.addFunction);
		$("#editFunctionBtn_OK").click(common.security.functionGrid.editFunction);
		$("#searchFunctionBtn_OK").click(common.security.functionGrid.searchFunction);
		
		//添加所属功能
		$('#addFunction_parentFunction_win').click(function(){		
			common.security.functionTree.showSelectFunctionTree('#selectFunctionTree_win','#function_parentFunctionNo','#addFunction_parentFunction_win','#selectFunctionTreeDIV');
		});
		
		
		$('#editFunction_parentFunction_win').click(function(){
			common.security.functionTree.showSelectFunctionTree('#selectFunctionTree_win','#editFunction_parentFunctionNo','#editFunction_parentFunction_win','#selectFunctionTreeDIV');
		});
		
		//导出
		$('#exportFunction').click(common.security.functionGrid.exportData);
		
		//导入
		$('#importFunction').click(function(){
			readyImportCSV(common.security.functionGrid.importData);
		});
	}
	};

}();

