$package("common.security"); 
$import('common.security.functionTree');

/**  
 * @author Van&&Tan  
 * @constructor role
 * @description 角色管理主函数.
 * @date 2011-02-25
 * @since version 1.0 
 */
common.security.role=function(){

	var operator='';
	this.result=null;
	this.roleIds='';
	this.noSelect='';
	this.functionNo='';
	this.editRoleIds='';
	this.optItem='';
	this.str='';
	return{
		/**
		 * @description 状态格式化.
		 * @param cell 当前列值
		 * @param options(下拉选项) 属性
		 */
		roleStateFormat:function(cellvalue, options){
			if(cellvalue)
				return i18n['common_enable'];
			else
				return i18n['common_disable'];
		},
		
		/**
		 * @description 状态格式化.
		 * @param cell 当前列值
		 * @param event 事件
		 * @param data 行数据
		 * @private
		 */
		roleGridFormatter:function(cell,opt,data){
			var actionHTML=$('#roleGridAct').html();
			actionHTML=actionHTML.replace(/\{id}/g,data.roleId);
			return actionHTML;
		},
		
		/**
		 * @description 加载角色列表.
		 */
		showRoleGrid:function(){
			var params = $.extend({},jqGridParams, {	
				url:'role!find.action',
				colNames:['ID',i18n['title_role_roleName'],i18n['title_role_roleCode'],i18n['common_desc'],i18n['common_remark'],i18n['common_state'],i18n['common_action'],''],
			 	colModel:[
			 	          {name:'roleId',align:'left',width:30},
			 	          {name:'roleName',align:'left',width:80},
			 	          {name:'roleCode',align:'left',width:80},
			 	          {name:'description',align:'left',width:120,sortable:false},
			 	          {name:'remark',align:'left',width:80,sortable:false},
			 	          {name:'roleState',align:'center',width:30,formatter:common.security.role.roleStateFormat},
			 	          {name:'act',align:'center',sortable:false,width:80,formatter:common.security.role.roleGridFormatter},
			 	          {name:'roleState',hidden:true}
			 	],
				jsonReader: $.extend(jqGridJsonReader, {id: "roleId"}),
				sortname:'roleId',
				pager:'#rolePager'
			});
			$("#roleGrid").jqGrid(params);
			$("#roleGrid").navGrid('#rolePager',navGridParams);
			//列表操作项
			$("#t_roleGrid").css(jqGridTopStyles);
			$("#t_roleGrid").append($('#roleTopMenu').html());
			
			//自适应宽度
			setGridWidth("#roleGrid","regCenter",20);
			
		},
		
		
		/**
		 * @description 新增窗口.
		 */
		addWin:function(){
			$('#roleId').val();
			$("#addRole form")[0].reset();
			$('#roleCode').removeAttr("disabled");
			windows('addRole',{width:420});
			operator = 'save';
		},
		/** 
		 * @description 新增角色.
		 */
		addRole:function() {
			if($('#addOrEditRoleForm').form('validate')){
				var ROLE=$('#roleCode').val();
				if(ROLE.indexOf("ROLE_")==-1){
					
					ROLE="ROLE_"+ROLE;
					$('#roleCode').val(ROLE);
				}
				$.post('role!existByRoleCode.action','roleDto.roleCode='+ROLE,function(data){
					if(data || $('#roleCode').val()==$('#back_roleCode').val()){
						$.post('role!existByRoleName.action','roleDto.roleName='+$('#roleName').val(),function(result){
							if(result || $('#roleName').val()==$('#back_roleName').val()){
							
									var role_form = $('#addRole form').serialize();
									var url = 'role!'+operator+'.action';
									$.post(url, role_form, function(){
										
										$('#addRole').dialog('close');
										$('#roleGrid').trigger('reloadGrid');
										if("editRole"===operator)
											msgShow(i18n['msg_edit_successful'],'show');
										else
											msgShow(i18n['msg_add_successful'],'show');
										var url = 'systemGuide!guideSaveUpdate.action';
										 $.post(url, {'systemGuide.roleGuide':true},function(res){
											
										 });
									});
							}else{
								msgAlert(i18n['msg_role_roleNameExist'],'info');	
							}
						});
					}else{
						msgAlert(i18n['msg_role_roleCodeExist'],'info');	
					}
				});
			}
			
		},
		
		/** 
		 * @description 删除角色.
		 * @param rowIds 行编号
		 */
		delRole:function(rowIds) {
			if(rowIds==""){	
				msgAlert(i18n['msg_atLeastChooseOneData'],'info');
			}else{
				msgConfirm(i18n['msg_msg'],"<br>"+i18n['msg_confirmDelete'], function(){
					var pp = $.param({'ids':rowIds},true);
					$.post("role!deleteRole.action", pp, function(res){
						if(res==0){
							$('#roleGrid').trigger('reloadGrid');
							msgShow(i18n.msg_deleteSuccessful,'show');
						}else if(res==1){
							msgAlert(i18n.msg_canNotDeleteSystemData,'info');					
						}else if(res==2){
							msgAlert(i18n.ERROR_DATA_CAN_NOT_DELETE,'info');
						}
					}, "json");
				});
			}
		},

	

		/** 
		 * @description 编辑角色.
		 * @param rowIds 行编号
		 */
		editRole:function(rowId) {
			operator = 'editRole';
			if(rowId==null){
				msgAlert(i18n['msg_atLeastChooseOneData'],'info');
			}else{
				var data = $("#roleGrid").getRowData(rowId);
				if(data.roleCode=='ROLE_ENDUSER' || data.roleCode=='ROLE_SYSADMIN' || data.roleCode=='ROLE_APPROVER' || data.roleCode=='ROLE_ITSOP_MANAGEMENT'){
					msgAlert(i18n['msg_system_enduser_role_not_edit'],'info');
				}else{
					var role=' ROLE_CMDBSUPERVISOR ROLE_CHANGECONTROLMANAGER ROLE_PROBLEMMANAGER ROLE_FOURTHLINEENGINEER'
						+'ROLE_SECONDLINEENGINEER ROLE_HELPDESKENGINEER ROLE_HELPDESKTEAMLEADER ROLE_ITSERVICEMANAGER ROLE_THIRDLINEENGINEER' 
						+'ROLE_COMMON_ADMIN ROLE_SUPER_ADMIN_DELEGATE ROLE_SUPER_ADMIN_DELEGATE ROLE_KNOWLEDGEBASEADMIN';
					
					resetForm('#addOrEditRoleForm');
					$('#roleId').val(data.roleId);
					$('#roleName').val(data.roleName);
					
					$('#back_roleCode').val(data.roleCode);
					$('#back_roleName').val(data.roleName);
					$('#remark').val(data.remark);
					$('#role_description').val(data.description);
					if(data.roleState=='false'){
						$('#roleState1').attr("checked",'false')
					}else{
						$('#roleState').attr("checked",'ture');
					}
					if(role.indexOf(data.roleCode) > 0 ){
						$('#roleCode').attr("disabled",'disabled');
						$('#roleCode').val(data.roleCode);
					}else{
						$('#roleCode').removeAttr("disabled");
						$('#roleCode').val(data.roleCode);
					}
					windows('addRole',{width:420});
				}
			}
		},
		/**
		 * @description 搜索角色.
		 */
		search:function(){
			var sdata=$('#searchRole form').getForm();
			var postData = $("#roleGrid").jqGrid("getGridParam", "postData");       
			$.extend(postData, sdata);
			var _url = 'role!find.action';		
			$('#roleGrid').jqGrid('setGridParam',{url:_url})
				.trigger('reloadGrid',[{"page":"1"}]).jqGrid('setGridParam',{url:_url});
		},
		
		//--------------------------角色权限分配 start-------------------------
		/**
		 * 判断是否是终端用户和系统管理角色
		 * @param rowIds 行编号
		 */
		enduser:function(rowIds){
			
			var result="";
			if(rowIds.length==undefined){
				var _data = $("#roleGrid").getRowData(rowIds);
				if(_data.roleCode=='ROLE_ENDUSER' || _data.roleCode=='ROLE_SYSADMIN' 
					|| _data.roleCode=='ROLE_APPROVER' || _data.roleCode=='ROLE_ITSOP_MANAGEMENT'){
					result=true;
				}
			}else{
				for(var i=0;i<=rowIds.length;i++){
					var data = $("#roleGrid").getRowData(rowIds[i]);
					if(data.roleCode=='ROLE_ENDUSER' || data.roleCode=='ROLE_SYSADMIN' 
						|| data.roleCode=='ROLE_APPROVER' || data.roleCode=='ROLE_ITSOP_MANAGEMENT'){
						result=true;
					}
				}
			}
			
			return result;
		},
		/**
		 * 判断是否是系统管理员
		 * @param rowIds 行编号
		 */
		systeAdminRole:function(rowIds){
			
			var result="";
			if(rowIds.length==undefined){
				var _data = $("#roleGrid").getRowData(rowIds);
				if(_data.roleCode=='ROLE_SYSADMIN' || _data.roleCode=='ROLE_APPROVER'){
					result=true;
				}
			}else{
				for(var i=0;i<=rowIds.length;i++){
					var data = $("#roleGrid").getRowData(rowIds[i]);
					if(data.roleCode=='ROLE_SYSADMIN' || data.roleCode=='ROLE_APPROVER'){
						result=true;
					}
				}
			}
			
			return result;
		},
		
		/**
		 * 判断是否是终端用户
		 * @param rowIds 行编号
		 */
		endUserRole:function(rowIds){
			
			var result="";
			if(rowIds.length==undefined){
				var _data = $("#roleGrid").getRowData(rowIds);
				if(_data.roleCode=='ROLE_ENDUSER' || _data.roleCode=='ROLE_ITSOP_MANAGEMENT'){
					result=true;
				}
			}else{
				for(var i=0;i<=rowIds.length;i++){
					var data = $("#roleGrid").getRowData(rowIds[i]);
					if(data.roleCode=='ROLE_ENDUSER' || data.roleCode=='ROLE_ITSOP_MANAGEMENT'){
						result=true;
					}
				}
			}
			
			return result;
		},
		
		/**
		 * @description 打开权限分配窗口
		 * @param rowId    行编号
		 * @param isRoles  是否管理员角色权限
		 */
		open_func:function(rowId,isRoles){
			result=null;
			roleIds=''
			noSelect=''
			optItem='';
			if(rowId==""){
				msgAlert(i18n['msg_atLeastChooseOneData'],'info');
			}else{	
				if(common.security.role.systeAdminRole(rowId)){
					msgAlert(i18n['msg_system_role_not_res_assign'],'info');
				}else{
					if(isRoles){
						$.post('role!getResourceByRoleId.action',"roleId="+rowId,function(data){
							if(data!=null){
								result=data.split(',');
								roleIds=data;
							}else
								result=null;
						})
					}	
					editRoleIds=rowId;
					$('#operationOptions').html(i18n['label_role_chooseFunction']);
					if(common.security.role.endUserRole(rowId))
						common.security.role.functionTree('Knowledge_Category_View');
					else
						common.security.role.functionTree('');
					windows('assignFunc',{width:620,height:450,open:function(){
						$("#assignFunc").nextAll().remove();
						$("#assignFunc").parent().css("width","630px");
					}});
				}
			}
			
		},
		
		/**
		 * @description 保存权限分配
		 */
		save_func:function(){
			if(result!=null && result!=''){
				str="str="+result+"-"+editRoleIds
				startProcess();
				$.post('role!setResourceByRoleIds.action',str,function(){
					endProcess();
					$('#assignFunc').dialog('close');
					msgShow(i18n['msg_role_roleSetSuccess'],'show');
					
				})
				
			}else{
				msgAlert(i18n['msg_please_ermission'],'info');
			}
			
			
		},
		
		/**
		 * @description 加载操作选
		 * @param event   事件
		 * @param data    数据
		 */
		loadOpertion:function(event, data)
		{
			var resName=data.rslt.obj.attr("resName");
			var resNo=data.rslt.obj.attr("resNo");
			if(resName!=null){
				
				functionNo=resNo;
				common.security.role.optionsByFunId();
			}
		},
		/**
		 * @description 功能TREE
		 * @param resCode 角色代码
		 * */
		functionTree:function (resCode){
			
			common.security.functionTree.functionTreeCallback('#functionTree',resCode,common.security.role.loadOpertion);

		},

		/**
		 * @description 全选
		 * */
	    checkAll:function () {
	    	if($('#checkOther_but').attr('checked')){
	    		$('#checkOther_but').attr('checked',false);
	    	}
	    	
	    	if($('#checkAll_but').attr('checked'))
	    		$('input[name="optItem"]').attr("checked",true);
	    	else
	    		$('input[name="optItem"]').attr("checked",false);
	    	common.security.role.getOptSelect();
	    	
	    },
	    /**
		 * @description 反选
		 * */
	    checkOther:function(){
	    	if($('#checkAll_but').attr('checked')){
	    		$('#checkAll_but').attr('checked',false);
	    	}
		    $("input[name='optItem']").each(function() {
		    	if(this.checked == false)
		    		this.checked = true;
		    	else
		    		this.checked = false;
		    })
		    common.security.role.getOptSelect();
	    },
		
	    /**
		 * @description 获取操作项选中的值
		 */
		getOptSelect:function(){
			
			noSelect='';
			$(".optItemId").each(function(){ //由于复选框一般选中的是多个,所以可以循环输出
				if($(this).attr('checked')){
					if(roleIds=="")
						roleIds=$(this).val();
					else
						roleIds=roleIds+","+$(this).val();
				}else{
					roleIds+=''
					if(noSelect=="")
						noSelect=$(this).val();
					else
					    noSelect=noSelect+","+$(this).val();

				}
			});
			
			
			
			
			/*把数组唯一化*/
			Array.prototype.in_array = function($){
		        for (var i=0;i<this.length;i++ )
		                if(this[i]===$)return true;
		        return false;
			};
			
			Array.prototype.array_unique=function(){
                for (var i = 0, l = this.length,$ = []; i < l; i++)
                        if (!$.in_array(this[i])) $.push(this[i]);
                return $;
			
			};
	
			
			/*把数组指定值删除*/
			Array.prototype.indexOf = function(val) {  
			   for (var i = 0; i < this.length; i++) { 
					   if (this[i] ==val) return i; 
			   }	   
			   return -1;  
			};  
			Array.prototype.remove = function(val) {
				for(var j=0;j<val.length;j++){ 
					var index = this.indexOf(val[j]);  
					if (index > -1) {  
						this.splice(index, 1);
					}    
			    }  
			};
			
			/*清除没选中的值*/
			var noSelectToIds=noSelect.split(',');
			var roleIdsToIds=roleIds.split(',');
			roleIdsToIds=roleIdsToIds.array_unique();
			roleIdsToIds.remove(noSelectToIds);
		
			result=roleIdsToIds.array_unique();
			
			setTimeout(function(){
				roleIds=result
			},0)
			
		},
		
		 /**
		 * @description 根据功能ID加载相应操作
		 */
		optionsByFunId:function(){
			$('#operationOptions').html('');
			$.post("operation!find.action","functionNo="+functionNo+"&rows=10000",function(data){
				var opt=data.data
				if(opt!=""){
					optItem='<table width=100%><tr><td colspan=2>'+
					'<input type=radio id=checkAll_but onclick=common.security.role.checkAll() />'+i18n['common_selectAll']+
					'<input type=radio id=checkOther_but onclick=common.security.role.checkOther() />'+i18n['common_deSelect']+'</td></tr>';
				}else{
					optItem='<pre>'+i18n.role_msg+'</pre>';
				}
				if(opt!=null)
				{
					for(var i=0; i<opt.length;i++){
						if(i % 2==0){
							optItem=optItem+"<tr>"
						}
						optItem=optItem+"<td><input type='checkbox' name='optItem' onclick='common.security.role.getOptSelect()' class='optItemId' "+
						"value='"+opt[i].resNo+"'"
						if(result!=null){
							for(var j=0; j<result.length;j++)
							{	
								if(result[j]==opt[i].resNo)
								{
									optItem=optItem+" checked ";
								}
							}
						}
						optItem=optItem+" />"+opt[i].resName+"</td>"
					}
					optItem+='</table>';
					$('#operationOptions').html(optItem);
				}
			},"json");
		},
		/**
		 * 导出
		 */
		exportRoleView:function(){
			$("#searchRole form").submit();
		},
		
		
		/**
		 * 导入数据.
		 */
		importRoleData:function(){
			windows('importRoleDataWindow',{width:400});
		},
		
		/**
		 * @descriptionadd 导入角色数据
		 * */
		importRoleExcel:function()
		{
			var path=$('#importRoleFile').val();
			if(path!=""){
				startProcess();
				$.ajaxFileUpload({
		            url:'role!importRoleData.action',
		            secureuri:false,
		            fileElementId:'importRoleFile', 
		            dataType:'json',
		            success: function(data,status){
		            	
		            	$('#importUserDataWindow').dialog('close');
		            	if(data=="FileNotFound"){
							msgAlert(i18n['msg_dc_fileNotExists'],'error');
							endProcess();
						}else if(data=="IOError"){
							msgAlert(i18n['msg_dc_importFailure'],'error');
							endProcess();
						}else{
							
							msgAlert(i18n['msg_dc_dataImportSuccessful']
							+'<br>['+
							data
							.replace('Total',i18n['opertionTotal'])
							.replace('Insert',i18n['newAdd'])
							.replace('Update',i18n['update'])
							.replace('Failure',i18n['failure'])+']'
							,'show');
							$('#roleGrid').trigger('reloadGrid');
							endProcess();
						}
		            	$('#importRoleDataWindow').dialog('close');
		            }
		        });
			}else{
				msgAlert(i18n['msg_dc_fileNull'],'info');
				endProcess();
			}

		},
		/**
		 * @description 页面事件
		 */
		webEvent:function(){
			$('#link_role_edit').click(function(){common.security.role.editRole($("#roleGrid").getGridParam("selrow"))})
			$('#link_role_delete').click(function(){common.security.role.delRole($("#roleGrid").getGridParam("selarrrow"))})
			$('#link_role_search').click(function(){windows('searchRole',{width:400,modal: false})})
			$('#link_role_assign').click(function(){
				if(common.security.role.enduser($("#roleGrid").getGridParam("selarrrow"))){
					msgAlert(i18n['msg_system_enduser_role_not_res_assign'],'info');
				}else{
					if($("#roleGrid").getGridParam("selarrrow").length == 1){
						common.security.role.open_func($("#roleGrid").getGridParam("selarrrow"),true)
					}else{
						common.security.role.open_func($("#roleGrid").getGridParam("selarrrow"),false)
					}
				}
			});
			$('#exportRoleData').click(common.security.role.exportRoleView);
			$('#importRoleData').click(common.security.role.importRoleData);
		},
		/**
		 * 初始化加载
		 */
		init:function(){
			$("#roleMain_loading").hide();
			 $("#roleMain_content").show();
			 common.security.role.showRoleGrid();
			 common.security.role.webEvent();
			 $('#roleGrid_export').click(common.security.role.exportRoleView);//导出
			 $('#roleGrid_import').click(common.security.role.importRoleData);//导入
		}
	}
}();
/**载入**/
$(document).ready(common.security.role.init);
