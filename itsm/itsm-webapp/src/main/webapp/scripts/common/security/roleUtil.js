$package('common.security');
$import('common.security.role');

/**  
 * @author QXY  
 * @constructor Tan
 * @description 角色公共函数(Window页面在selectUser.jsp)
 * @date 2013-09-17
 * @since version 1.0 
 */
common.security.roleUtil = function(){
	
	return {
		
		roleActFormt:function(){
			return '';
		},
		//显示角色选择列表
		showRoleGrid:function(callBack){
			if(basics.ie6.htmlIsNull("#selectRole_grid")){
				var _url = 'role!find.action';
				$('#selectRole_grid').jqGrid('setGridParam',{url:_url})
					.trigger('reloadGrid',[{"page":"1"}]).jqGrid('setGridParam',{url:_url});
			}else{
				var params = $.extend({},jqGridParams, {
					url:'role!find.action',
					caption:'',
					colNames:['ID',i18n['title_role_roleName'],i18n['title_role_roleCode'],i18n['common_desc'],i18n['common_remark'],i18n['common_state'],i18n['common_action'],''],
				 	colModel:[
				 	          {name:'roleId',align:'left',width:45},
				 	          {name:'roleName',align:'left',width:160},
				 	          {name:'roleCode',align:'left',width:180},
				 	          {name:'description',align:'left',width:120,sortable:false,hidden:true},
				 	          {name:'remark',align:'left',width:80,hidden:true},
				 	          {name:'roleState',align:'center',width:60,formatter:common.security.role.roleStateFormat},
				 	          {name:'act',align:'center',sortable:false,width:80,formatter:common.security.roleUtil.roleActFormt,hidden:true},
				 	          {name:'roleState',hidden:true}
				 	],
					jsonReader: $.extend(jqGridJsonReader, {id: "roleId"}),
					sortname:'roleId',
					pager:'#selectRole_pager'
				});
				$("#selectRole_grid").jqGrid(params);
				$("#selectRole_grid").navGrid('#selectRole_pager',navGridParams);
				//列表操作项
				$("#t_selectRole_grid").css(jqGridTopStyles);
				$("#t_selectRole_grid").append('<form id="selectRoleSearchForm">名称:<input name="roleQueryDto.roleName" id="selectRole_roleName" />'+
						'&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void(0)" id="selectRole_search"><b>['+i18n.search+']</b></a>'+
						'&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void(0)" id="multipleSelectConfirm_link"><b>['+i18n.Confirm+']</b></a>'+
						'</form>');
			}
			//绑定搜索
			$('#selectRole_search').unbind().click(common.security.roleUtil.roleSearch);
			//绑定确认
			$('#multipleSelectConfirm_link').unbind().click(function(){common.security.roleUtil.multipleSelectConfirm(callBack)});
			windows('selectRole_win',{width:500});
		},
		/**
		 * 角色搜索
		 */
		roleSearch:function(){
			var sdata=$('#selectRoleSearchForm').getForm();
			var postData = $("#selectRole_grid").jqGrid("getGridParam", "postData");       
			$.extend(postData, sdata);
			var _url = 'role!find.action';
			$('#selectRole_grid').jqGrid('setGridParam',{url:_url})
				.trigger('reloadGrid',[{"page":"1"}]).jqGrid('setGridParam',{url:_url});
		},
		
		/**
		 * 单选确定选择按钮
		 */
		radioSelectConfirm:function(callBack){
			
		},
		/**
		 * 多选确定选择按钮
		 */
		multipleSelectConfirm:function(callBack){
			var rowIds = $("#selectRole_grid").getGridParam('selarrrow');
			if(rowIds==''){
				msgAlert(i18n.msg_atLeastChooseOneData,'info');
			}else{
				callBack(rowIds);
				$('#selectRole_win').dialog('close');
			}
		},
		init:function(){
			
		}
	};
}();