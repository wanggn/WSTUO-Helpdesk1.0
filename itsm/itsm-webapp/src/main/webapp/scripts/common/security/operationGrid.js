
$package('common.security');

/**  
 * @author Van  
 * @constructor resourceMain
 * @description 资源管理主函数.
 * @date 2011-02-25
 * @since version 1.0 
 */
common.security.operationGrid = function() {
	
	
	return {
		
		
		/**
		 * @description 图标格式化.
		 * @param 字段的值
		 * @param 行的数据
		 * @param 行的索引
		 */
		/**
		 * @description 图标格式化.
		 * @param cell 当前列值
		 * @param event 事件
		 * @param data 行数据
		 */
		iconFormatter:function(cell,opt,data){
		
			if(data.resIcon==null || data.resIcon==""){
				return i18n['label_resource_notUpload'];
			}else{
				
				return "<img src='organization!getImageUrlforimageStream.action?imgFilePath="+data.resIcon+"' width='20px' height='20px' />";
			}
		},

		/**
		 * @description 格式化操作
		 * @param cell 当前列值
		 * @param event 事件
		 * @param data 行数据
		 */
		operationGridFormatter:function(cell,opt,data){
			
			return '<div style="padding:5px">'+
			'<img style="cursor:pointer" title="'+i18n['common_edit']+'" src="../skin/default/images/grid_edit.png" title="" onclick="common.security.operationGrid.editInLine('+opt.rowId+')"/>'+
			'<img style="margin-left:8px;cursor:pointer" title="'+i18n['label_tree_delete']+'" src="../skin/default/images/grid_delete.png" onclick="common.security.operationGrid.deleteOperation('+data.resNo+')"/>'+
			'</div>';
		},

		
		/**
		 * @description  加载操作列表
		 */
		showOperationGrid:function()
		{
			
			var params = $.extend({},jqGridParams, {	
				url:'operation!find.action',
				colNames:[i18n['title_resource_code'],i18n['common_name'],i18n['title_resource_icon'],i18n['title_resource_url'],i18n['common_action'],'',''],
				colModel:[
						  {name:'resCode',width:20,align:'center'},
						  {name:'resName',width:15,align:'center'},
						  {name:'resIconPanel',width:15,align:'center',formatter:common.security.operationGrid.iconFormatter,sortable:false},
						  {name:'resUrl',width:30,align:'center'},
						  {name:'act', width:20,align:'center',formatter:common.security.operationGrid.operationGridFormatter,sortable:false},
						  {name:'resNo',hidden:true},
						  {name:'resIcon',hidden:true}
						  ],
				jsonReader: $.extend(jqGridJsonReader, {id: "resNo"}),
				sortname:'resNo',
				pager:'#operationPager'
				});
				$("#operationGrid").jqGrid(params);
				$("#operationGrid").navGrid('#operationPager',navGridParams);
				//列表操作项
				$("#t_operationGrid").css(jqGridTopStyles);
				$("#t_operationGrid").append($('#operationToolbar').html());
				
				//自适应宽度
				setGridWidth('#operationGrid', 'resourceTabMain', 10);
				
				resourceGrids.push('#operationGrid');
			
		},
	
	
	
		/**
		 * @description 打开新增操作窗口.
		 */
		openAddOperationWindow:function (){
	
			if(common.security.operationGrid.validateParentFunction()){
				$('#opeartion_resCode').val('');
				$("#opeartion_resName").val('');
				$("#opeartion_resUrl").val('');
				windows('addOperation_win',{width:500});
			}
		},
		
		/**
		 * @description Close add operation window.
		 */
		addOperation_closewindow:function(){
			
			$('#addOperation_win').dialog('close');		
		},
		
		
		
		/**
		 * 验证是否选择功能节点.
		 */
		validateParentFunction:function(){
			
			var functionNo=$('#operation_functionNo').val();
			
			if(functionNo==null || functionNo==""){
				msgAlert(i18n['msg_chooseFunction'],'info');
				return false;
			}else{
				return true;
			}
		},
		
		
	
		/**
		 * @description Do add operation.
		 */
		addOperation:function (){
			if(common.security.operationGrid.validateParentFunction() && $('#addOperationForm_win').form('validate')){
				//验证资源名称
				var resName=$('#opeartion_resName').val();
				 var url_resName="operation!findOperationExist.action?operationQueryDto.resName="+resName;
				 $.post(url_resName,function(res){
					if(res==true){
						msgAlert(i18n['msg_resource_operationNameExist'],'info');
						return;
					}else{
						var resCodeFunction=$('#opeartion_resCode').val();
						 var url_resCodeFunction="function!findExist.action?functionQueryDTO.resCode="+resCodeFunction;
						 $.post(url_resCodeFunction,function(res){
								if(res==true){
									msgAlert(i18n['msg_resource_functionCodeExist'],'info');
									return;
								}else{
									//验证资源编码 start
									var resCode=$('#opeartion_resCode').val();
									var url_resCode="operation!findOperationExist.action?operationQueryDto.resCode="+resCode;
									 //判断操作编码
									 $.post(url_resCode,function(res){
											if(res==true){
												msgAlert(i18n['msg_resource_operationCodeExist'],'info');
												return;
											}else{
												//判断资源地址 start
												var resUrl=$('#opeartion_resUrl').val();
												var url_resUrl="operation!findOperationExist.action?operationQueryDto.resUrl="+resUrl;
												 //判断操作编码
												 $.post(url_resUrl,function(res){
														if(res==true){
															msgAlert(i18n['msg_resource_functionUrlExist'],'info');
															return;
														}else{
															//开始新增
															 var frm = $('#addOperation_win form').serialize();
															 var url = 'operation!addOperation.action';
															 $.post(url,frm, function(){
																 	$('#addOperation_win').dialog('close');
																	$('#operationGrid').trigger('reloadGrid');//刷新列表
																	$('#opeartion_resName,#opeartion_resCode,#opeartion_resUrl').val('');
																	$('#show_opicon').html("");
																	msgShow(i18n['addSuccess'],'show');
															 });
														}
												 });
												//判断资源地址end
											}
									 });
									//验证资源编码 end
								} 
							 });
					}
				 });
			}
		},
		
	
	
		
		
	
		/**
		 * @description 编辑操作
		 */
		openEditOperationWindow:function(){
			
			checkBeforeEditGrid("#operationGrid",function(rowData){
				 //填充数据
				common.security.operationGrid.editFillData(rowData);
			});
		},
	
		
		/**
		 * 行内编辑
		 */
		editInLine:function(rowId){
			
			var rowData=$('#operationGrid').getRowData(rowId);
			common.security.operationGrid.editFillData(rowData);
			
		},
		
		/**
		 * 填充数据.
		 */
		editFillData:function(rowData){
			
			 $('#edit_opeartion_resName').val(rowData.resName);
			 $('#edit_opeartion_resCode').val(rowData.resCode);
			 $('#edit_opeartion_resIcon').val(rowData.resIcon);
			 $('#edit_opeartion_resNo').val(rowData.resNo);
			 $('#edit_opeartion_resUrl').val(rowData.resUrl);
			 if(rowData.resIcon!='' && rowData.resIcon!=null){
				 $('#show_editOpeartion_resIcon').html('<img src="organization!getImageUrlforimageStream.action?imgFilePath='+rowData.resIcon+'" width="30px" height="30px" />');
			 }else{
				 $('#show_editOpeartion_resIcon').html('');
			 }
			 windows('editOperation_win',{width:500});
		},
		
		
		/**
		 * @description 关闭编辑窗口
		 */
		closeEditOperationWindow:function(){
			$('#editOperation_win').dialog('close');
		},
		
		/**
		 * @description 编辑操作
		 */
		editOperation:function (){
			
			if($('#editOperationForm_win').form('validate')){
				var url = 'operation!updateOperation.action';
				submitFormCommon("#editOperation_win",url,function(r){
					
					var sdata = {functionNo: $('#operation_functionNo').val()+"" };   
					var postData = $("#operationGrid").jqGrid("getGridParam", "postData");  
					$.extend(postData,sdata); //传入值	
					$('#operationGrid').trigger('reloadGrid');
					common.security.operationGrid.closeEditOperationWindow();
					msgShow(i18n['editSuccess'],'show');
					
				});
			}
		},
		
		/**
		 * @description 删除操作
		 */
		deleteOperations:function(){
			
			checkBeforeDeleteGrid("#operationGrid",function(rowIds){
				
				var param = $.param({'resNos':rowIds},true);
				$.post("operation!deleteOperations.action", param, function(){
						$('#operationGrid').trigger('reloadGrid');
						msgShow(i18n['msg_deleteSuccessful'],'show');
				}, "json");
				
			});
		},
	
		/**
		 * @description 行内删除操作.
		 * @param rowId   行的编号
		 */
		deleteOperation:function(rowId){
			
			confirmBeforeDelete(function(){
				
				var param = $.param({'operationDto.resNo':rowId},true);
				$.post("operation!deleteOperation.action", param, function(data){

					if(data){
						
						$('#operationGrid').trigger('reloadGrid');
						msgShow(i18n['msg_deleteSuccessful'],'show');
					}
					else{
						
						msgAlert(i18n['msg_canNotDeleteSystemData'],'info');
					}	
					
				}, "json");
				
				
			});


		},
	
		/**
		 * @description 打开搜索窗口.
		 */
		openSearchOperationWindow:function(){
			windows('searchOperation_win',{width:380});
		},
	
		/**
		 * @description 搜索操作.
		 */
		searchOperation:function(){
			var sdata = $('#searchOperation_win form').getForm();
			var postData = $("#operationGrid").jqGrid("getGridParam", "postData");       
			$.extend(postData, sdata,{"functionNo":$("#search_FunctionNo").val()});
			
			var _url = 'operation!find.action';		
			$('#operationGrid').jqGrid('setGridParam',{url:_url}).trigger('reloadGrid');
			
		},
	
		/**
		 * @description 根据功能显示操作列表.
		 */
		showGridByFunction:function(){
			$('#search_operationQueryDto_resName').val('');
			$('#search_operationQueryDto_resCode').val('');
			$('#search_operationQueryDto_resUrl').val('');
			var sdata = $('#searchOperation_win form').getForm();
			var postData = $("#operationGrid").jqGrid("getGridParam", "postData");       
			$.extend(postData,sdata,{"functionNo":$("#search_FunctionNo").val()});
			var _url = 'operation!find.action';		
			$('#operationGrid').jqGrid('setGridParam',{url:_url,page: 1}).trigger('reloadGrid');
			
		},
		
		/**
		 * 导出数据
		 */
		exportData:function(){
			
			$('#exportOperation_resName').val('');
			$('#exportOperation_resCode').val('');
			$('#exportOperation_resUrl').val('');
			$('#exportOperation_functionNo').val('');
			//var searchData = $('#searchOperation_win form').getForm();
			var gridData = $("#operationGrid").jqGrid("getGridParam", "postData");   
			
			for(i in gridData){    	   	
				if(gridData[i]!="" && gridData[i]!=null){
					if(i=='operationQueryDto.resName'){$('#exportOperation_resName').val(gridData[i]);}
					if(i=='operationQueryDto.resCode'){$('#exportOperation_resCode').val(gridData[i]);}
					if(i=='operationQueryDto.resUrl'){$('#exportOperation_resUrl').val(gridData[i]);}
					if(i=='functionNo'){$('#exportOperation_functionNo').val(gridData[i]);}
				}
			}
			
			$('#exportOperation_page').val(gridData.page);
			$('#exportOperation_rows').val(gridData.rows);
			$('#exportOperationForm').submit();
			
		},
		
		/**
		 * 导入数据.
		 */
		importData:function(){
			

			importCSV('operation!importOperation.action',function(data){
				$('#index_import_excel_window').dialog('close');
				$('#operationGrid').trigger('reloadGrid');
	        	
				
				if(data=="success"){
	            	msgShow(i18n['msg_dc_dataImportSuccessful'],'show');
				}else{
					msgShow(i18n['msg_dc_importFailure'],'show');
				}
				
	        	resetForm('#index_import_excel_window form');
			});

		},
		
		
		/**
		 * 载入
		 */
		init:function(){
			//加载列表
			common.security.operationGrid.showOperationGrid();
			
			//事件
			$('#operationGrid_add').click(common.security.operationGrid.openAddOperationWindow);
			$('#operationGrid_edit').click(common.security.operationGrid.openEditOperationWindow);
			$('#operationGrid_delete').click(common.security.operationGrid.deleteOperations);
			$('#operationGrid_search').click(common.security.operationGrid.openSearchOperationWindow);
			
			$("#addOperationBtn_OK").click(common.security.operationGrid.addOperation);
			$("#editOperationBtn_OK").click(common.security.operationGrid.editOperation);
			$("#searchOperationBtn_OK").click(common.security.operationGrid.searchOperation);
			
			
			//导出
			$('#exportOperation').click(common.security.operationGrid.exportData);
			
			//导入
			$('#importOperation').click(function(){
				readyImportCSV(common.security.operationGrid.importData);
			});
		}

	};
	
}();
