﻿ /**  
 * @fileOverview "Includes"
 * @author QXY
 * @version 1.0  
 */  
 /**  
 * @author QXY  
 * @constructor Includes
 * @description "用于加载Includes文件JS主函数"
 * @date 2012-7-31
 * @since version 1.0 
 */ 
$package('common.report.includes');
/**  
 * @fileOverview "加载报表弹出框"
 * @author Martin  
 * @constructor Martin
 * @description 加载报表弹出框
 * @date 2012-06-13
 * @version 1.0  
 * @since version 1.0 
 */
common.report.includes.includes=function(){
	//报表显示
	this._loadReportViewIncludesFileFlag=false;
	return {
		//加载报表includes文件
		loadReportViewIncludesFile:function(){
			if(!_loadReportViewIncludesFileFlag){
				$('#reportView_html').load('common/report/includes/includes_preViewReport.jsp',function(){
					$.parser.parse($('#reportView_html'));
				});
			}
			_loadReportViewIncludesFileFlag=true;
		},
		init:function(){
			
		}
	}
}();
$(function(){common.report.includes.includes.init});
