/**  
 * @author Van  
 * @constructor WSTO
 * @description 自定义报表.
 * @date 2010-11-17
 * @since version 1.0 
 */ 
$package('common.report');


/**  
 * @fileOverview "多项报表分组统计项管理"
 * @author Van  
 * @constructor WSTO
 * @description 多项报表分组统计项管理
 * @date 2011-06-13
 * @version 1.0  
 * @since version 1.0 
 */
common.report.multiGroupStisticGrid=function(){
	

	this.reportStisticOperator='save';//动作标识
	this.loadMultiGroupStisticGridFlag=false;//加载标识
	
	return {
		
		
	
		/**
		 * 格式化统计字段.
		 * 
		 */
		stisticFieldFormatter:function(cell,event,data){
			
			var entityClass=$('#add_edit_mutilGroupReport_entityClass').val();
			
			if(entityClass!=null && entityClass!=''){
				
				return i18n[report_stistic_fields[entityClass][data.stisticField]];
				
			}else{
				
				return data.stisticField;
			}
		},
		
		/**
		 * 格式化统计字段.
		 * 
		 */
		stisticTypeFormatter:function(cell,event,data){
		
			
			var arr={
					'count':'label_dynamicReports_count',
					'sum':'label_dynamicReports_sum',
					'avg':'label_dynamicReports_avg',
					'max':'label_dynamicReports_max',
					'min':'label_dynamicReports_min'
						
			};
			
			return i18n[arr[data.stisticType]];
			
		},
		/**
		 * 重新加载列表
		 */
		refreshGrid:function(){
			
			var reportId=$('#add_edit_mutilGroupReport_reportId').val();
			
			if(reportId==null || reportId==''){//新增
				reportId=-1;
			}
			var entityClass=$('#add_edit_mutilGroupReport_entityClass').val();
			if(entityClass==null || entityClass==''){//新增
				entityClass="NO";
			}
			
			var _postData = $("#multiGroupStisticGrid").jqGrid("getGridParam", "postData");       
			$.extend(_postData, {'mutilGroupStisticQueryDTO.reportId':reportId,'mutilGroupStisticQueryDTO.entityClass':entityClass});	
			$('#multiGroupStisticGrid').trigger('reloadGrid',[{"page":"1"}]);
			
		},
		
		
		/**
		 * 显示列表.
		 */
		showGrid:function(){
			
			var reportId=$('#add_edit_mutilGroupReport_reportId').val();
			
			
			
			if(reportId==null || reportId==''){//新增
				reportId=-1;
			}

			common.report.multiGroupStisticGrid.loadGrid(reportId);
			if(loadMultiGroupStisticGridFlag==false){//全新加载
				loadMultiGroupStisticGridFlag=true;
			}else{//刷新列表
				var _postData = $("#multiGroupStisticGrid").jqGrid("getGridParam", "postData");       
				$.extend(_postData, {'mutilGroupStisticQueryDTO.reportId':reportId});	
				$('#multiGroupStisticGrid').trigger('reloadGrid',[{"page":"1"}]);
				
			}
		},

		/**
		 * 加载列表.
		 */
		loadGrid:function(reportId){
			windows('multiGroupStisticGrid_window');
			if($('#multiGroupStisticGridPager').html() == ''){
				var params = $.extend({},jqGridParams, {	
					url:'stistic!findPager.action',
					postData:{'mutilGroupStisticQueryDTO.reportId':reportId},
					colNames:[i18n['label_dynamicReports_stisticName'],i18n['label_dynamicReports_statisticField'],i18n['label_dynamicReports_statisticType'],'','','',''],
					colModel:[
					    {name:'stisticFieldName',align:'center',width:40},
				   		{name:'statField_label',sortable:false,align:'center',width:30,formatter:common.report.multiGroupStisticGrid.stisticFieldFormatter},
				   		{name:'statType_label', sortable:false,align:'center',width:30,formatter:common.report.multiGroupStisticGrid.stisticTypeFormatter},
				   		{name:'sid',hidden:true},
				   		{name:'isRelated',hidden:true},
				   		{name:'stisticField',hidden:true},
				   		{name:'stisticType',hidden:true}
				   	],		
					jsonReader: $.extend(jqGridJsonReader, {id: "sid"}),
					sortname:'sid',
					pager:'#multiGroupStisticGridPager'
				});
				$("#multiGroupStisticGrid").jqGrid(params);
				$("#multiGroupStisticGrid").navGrid('#multiGroupStisticGridPager',navGridParams);
				//列表操作项
				$("#t_multiGroupStisticGrid").css(jqGridTopStyles);
				$("#t_multiGroupStisticGrid").html($('#multiGroupStisticGridToolbar').html());
				

				$('#multiGroupStisticGrid_add,#multiGroupStisticGrid_save').unbind();

				//添加
				$('#multiGroupStisticGrid_add').click(common.report.multiGroupStisticGrid.preAdd);
				//编辑
				$('#multiGroupStisticGrid_edit').click(common.report.multiGroupStisticGrid.preEdit);
				//保存
				$('#multiGroupStisticGrid_save').click(common.report.multiGroupStisticGrid.doSave);
				//删除
				$('#multiGroupStisticGrid_delete').click(common.report.multiGroupStisticGrid.doDelete);
			}
			

		},
		
		/**
		 * 根据类型查找.
		 */
		queryByEntityClass:function(entityClass){
			
			var _postData = $("#multiGroupStisticGrid").jqGrid("getGridParam", "postData");       
			$.extend(_postData, {'mutilGroupStisticQueryDTO.entityClass':entityClass});	
			$('#multiGroupStisticGrid').trigger('reloadGrid',[{"page":"1"}]);
			
		},
		/**
		 * 根据类型加载字段
		 */
		loadStisticField:function(entityClass){
			
			
			//加载统计字段
			$('#add_edit_stistic_stisticField').html('<option value="">-- '+i18n['label_dynamicReports_chooseCountField']+' --</option>');
			
			
			$.each(report_stistic_fields[entityClass],function(k,v){
				$('<option value="'+k+'">'+i18n[v]+'</option>').appendTo('#add_edit_stistic_stisticField');
			});
			
		},
		
		/**
		 * 新增
		 */
		preAdd:function(){
			
			var entityClass=$('#add_edit_mutilGroupReport_entityClass').val();
			
			
			$('#add_edit_stistic_stisticField').change(function(){				
				$('#add_edit_stistic_stisticFieldName').val($(this).find("option:selected").text());
			});
			if(entityClass!=null && entityClass!=''){
				resetForm('#multiGroupStisticGrid_add_edit_window form');
				windows('multiGroupStisticGrid_add_edit_window',{width:400});
				reportStisticOperator='save';
			}else{
				
				msgAlert(i18n['label_dynamicReports_selectReportType'],'info');
			}
			
			
		},
		/**
		 * 保存
		 */
		doSave:function(){
			$('#multiGroupStisticGrid_save').attr("disabled",true);
			startProcess();
			if($('#multiGroupStisticGrid_add_edit_window form').form('validate')){
				$('#add_edit_stistic_reportId').val($('#add_edit_mutilGroupReport_reportId').val());//报表编号
				$('#add_edit_stistic_entityClass').val($('#add_edit_mutilGroupReport_entityClass').val());//类型

				var _param = $('#multiGroupStisticGrid_add_edit_window form').serialize();
				endProcess();

				$.post('stistic!'+reportStisticOperator+'.action',_param,function(data){	
					
					$('#multiGroupStisticGrid').trigger('reloadGrid');
					$('#multiGroupStisticGrid_add_edit_window').dialog('close');
					msgShow(i18n['saveSuccess'],'show');
				});				
			}else{
				endProcess();
			}
			$('#multiGroupStisticGrid_save').attr("disabled",false);//removeAttr
		},
		
		/**
		 * 打开编辑窗口.
		 */
		preEdit:function(){
			
			checkBeforeEditGrid('#multiGroupStisticGrid', function(data){
				
				$('#add_edit_stistic_stisticFieldName').val($vl(data.stisticFieldName));
				$('#add_edit_stistic_sid').val($vl(data.sid));
				$('#add_edit_stistic_isRelated').val($vl(data.isRelated));
				
				
				//打开窗口
				windows('multiGroupStisticGrid_add_edit_window',{width:450});
				reportStisticOperator='merge';

				setTimeout(function(){//延迟加载

					$('#add_edit_stistic_stisticField').val($vl(data.stisticField));
					$('#add_edit_stistic_stisticType').val($vl(data.stisticType));			

				},500);
			});
		},
		/**
		 * 删除
		 */
		doDelete:function(){

				checkBeforeDeleteGrid('#multiGroupStisticGrid', function(ids){
					var reportId=$('#add_edit_mutilGroupReport_reportId').val();
					if(reportId==null || reportId==''){//新增
						reportId=-1;
					}

					
					var _param = $.param({'sids':ids,'reportId':reportId},true);
					$.post("stistic!delete.action", _param, function(){
						
						$('#multiGroupStisticGrid').trigger('reloadGrid');
						msgShow(i18n['msg_deleteSuccessful'],'show');
						
					}, "json");
					
					
				});

			
		}
		
		
		

	};
	
}();
