$package('common.config.server')
/**  
 * @author QXY  
 * @constructor WSTO
 * @description 服务器路径函数
 * @date 2010-11-17
 * @since version 1.0 
 */  
common.config.server.serverUrlSet=function(){
	return {
		/**
		 * 加载服务器路径
		 */
		loadServerUrl:function(){
			/*console.info(window.top.document.URL);
			console.info(window.location.host);
			console.info(window.location.port );
			console.info(window.location.href);
			console.info(location.hostname);
			console.info(window.location.protocol)*/
			var _url = window.location.host;
			var _hostname = location.hostname;
			var _port = window.location.port;
			if(_url.indexOf(":") >= 0){
				_url = "("+window.location.protocol+"//"+ window.location.host+"/itsm-webapp"+")";
			}else{
				if((common.config.server.serverUrlSet.check_IP(_hostname) || _hostname=="localhost") && _port=="80"){
					_url = "("+window.location.protocol+"//"+ window.location.host+"/itsm-webapp"+")";
				}else{
					_url = "("+window.location.protocol+"//"+ window.location.host+")";
				}
			}
			$('#currentUrl').html(_url);
			$.post('serverUrl!findServerUrl.action',function(data){
				if(data!=null){
					$('#serverUrlId').val(data.id);
					$('#serverUrlName').val(data.urlName);
				}
			});
		},
		check_IP:function(ip){  
		   var re=/^(\d+)\.(\d+)\.(\d+)\.(\d+)$/;//正则表达式     
		   if(re.test(ip)) {    
		       if( RegExp.$1<256 && RegExp.$2<256 && RegExp.$3<256 && RegExp.$4<256)   
		    	   return true;     
		   }     
		   return false;      
		},
		/**
		 * 保存服务器路径
		 */
		saveServerUrl:function(){
			if($('#serverUrl_form').form('validate')){
				var _param = $("#serverUrl_form").serialize();
				$.post('serverUrl!saveServerUrl.action',_param,function(data){
					msgShow(i18n['saveSuccess'],'show');
				});
			}
		},
		/**
		 * 初始化
		 * @private 
		 */
		init:function(){
			$('#serverUrlSet_loading').hide();
			$('#serverUrlSet_content').show();
			$('#serverUrl_linkBut').click(common.config.server.serverUrlSet.saveServerUrl);
			common.config.server.serverUrlSet.loadServerUrl();
		}
	}
}();
$(document).ready(common.config.server.serverUrlSet.init);