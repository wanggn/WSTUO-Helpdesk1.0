$package('common.config.authorization');
/**  
 * @fileOverview 配置项授权管理
 * @author Van  
 * @constructor WSTO
 * @description 配置项授权管理
 * @date 2011-06-13
 * @version 1.0  
 * @since version 1.0 
 */
common.config.authorization.ciAuthorization= function() {
	var url='authorization!AuthorizationSave.action';
	return{
		/**
		 * @description 类型格式化.
		 * @param cell 当前列值
		 * @param event 事件
		 * @param data 数据
		 * @private
		 */
		typeNameFormatter:function(cell,event,data){
			if(cell!=null && cell!='' && cell!=undefined){
				cell="lable_"+cell;
				return i18n.lable_ciType;
			}else{
				return '';
			}
		},
		/**
		 * 授权管理列表
		 * @private
		 */
		showAuthorizationGrid:function() {
			var params = $.extend({},jqGridParams, {	
				url:'authorization!findPagerAuthorization.action',
				colNames:['ID',i18n.name,i18n.type,i18n.title_creator,i18n.common_createTime,'','',''],
				colModel:[
				    {name:'authId',align:'center',width:45},
				    {name:'authName',align:'center',width:45},
				    {name:'authType',align:'center',width:45,formatter:common.config.authorization.ciAuthorization.typeNameFormatter},
				    {name:'creator',align:'center',width:45},
				    {name:'createTime',align:'center',width:45},
				    {name:'authPawd',hidden:true},
				    {name:'authId',hidden:true},
				    {name:'authType',hidden:true},
			   		
				],	
				toolbar: [true,"top"],
				jsonReader: $.extend(jqGridJsonReader,{id: "authId"}),
				sortname:'authId',
				pager:'#authorizationPager'
			});
			$("#authorizationGrid").jqGrid(params);
			$("#authorizationGrid").navGrid('#authorizationPager',navGridParams);
			//列表操作项
			$("#t_authorizationGrid").css(jqGridTopStyles);
			$("#t_authorizationGrid").append($('#authorizationItem').html());
			//自适应宽度
			setGridWidth("#authorizationGrid","regCenter",20);
		},
		/**
		 * 打开授权管理窗口
		 * @private
		 */
		openAuthorization:function(){
			url='authorization!AuthorizationSave.action';
			$('#myAuthorizationForm')[0].reset();  
			$('#authdtoType').removeAttr("disabled");
			windows('addOrEditAuthorizationDiv',{width:490,height:180,close:function(){
				$('#addOrEditAuthorizationDiv').click();
			}});
		},
		/**
		 * 添加授权
		 * @private
		 */
		addAuthorization:function(){
			 if($('#myAuthorizationForm').form('validate')){
				 if(url=='authorization!AuthorizationSave.action'){
				 startProcess();
				 $.post('authorization!findByAuthType.action','atype=ciType',function(data){
					 if(data==null){
						 var _param = $('#addOrEditAuthorizationDiv form').serialize();
							$.post(url,_param,function(data){
								$('#addOrEditAuthorizationDiv').dialog('close');
					 			$('#authorizationGrid').trigger('reloadGrid');		 		
					 			resetForm('#myAuthorizationForm');
					 			endLoading();
					 			msgShow(i18n['saveSuccess'],'show');
					 			endProcess();
							});
					 }else{
						 msgShow(i18n.authorization_type,'show');
						 endProcess();
					 }
				 });
				 }else{
					 $('#authdtoType').removeAttr("disabled");
					 var _param = $('#addOrEditAuthorizationDiv form').serialize();
						$.post(url,_param,function(data){
							$('#addOrEditAuthorizationDiv').dialog('close');
				 			$('#authorizationGrid').trigger('reloadGrid');		 		
				 			resetForm('#myAuthorizationForm');
				 			endLoading();
				 			msgShow(i18n['saveSuccess'],'show');
						});
				 }
			 }
		},
		/**
		 * 编辑选择列表数据
		 * @private
		 */
		editAuthorization_aff:function(){
			var rowId = jQuery("#authorizationGrid").jqGrid('getGridParam','selrow');
			if(rowId==null){
				msgAlert(i18n['msg_atLeastChooseOneData'],'info');
			}else{
				var rowData=$("#authorizationGrid").getRowData(rowId);
				common.config.authorization.ciAuthorization.editAuthorization_win(rowData);
			}
		},
		/**
		 * 编辑显示默认值
		 * @param rowData 数据
		 * @private
		 */
		editAuthorization_win:function(rowData){
			url='authorization!AuthorizationUpdate.action';
			resetForm('#myTaskForm');
			$('#authdtoType').attr("disabled","true");
			$('#authdtoName').val(rowData.authName);
			$('#authdtoType').val(rowData.authType);
			$('#authdtoPawd').val(rowData.authPawd);
			$('#authdtoId').val(rowData.authId);
			windows('addOrEditAuthorizationDiv',{width:490,height:180,close:function(){
				$('#addOrEditAuthorizationDiv').click();
			}});
		},
		/**
		 * 选择需要删除的数据
		 * @private
		 */
		deleteAuthorization_aff:function(){
			checkBeforeDeleteGrid('#authorizationGrid', common.config.authorization.ciAuthorization.deleteAuthorization);
		},
		/**
		 * 执行删除.
		 * @param rowIds 编号
		 * @private
		 */
		deleteAuthorization:function(rowIds){
			var _param = $.param({'ids':rowIds},true);
			$.post("authorization!AuthorizationDelete.action", _param, function(){
				$('#authorizationGrid').trigger('reloadGrid');		
				msgShow(i18n['deleteSuccess'],'show');
			}, "json");
		},
		/**
		 * 初始化
		 * @private
		 */
		init:function(){
			$("#myAuthorization_loading").hide();
			$("#myAuthorization_content").show();
			common.config.authorization.ciAuthorization.showAuthorizationGrid();
			$('#link_authorization_add').click(common.config.authorization.ciAuthorization.openAuthorization);
			$('#link_authorization_edit').click(common.config.authorization.ciAuthorization.editAuthorization_aff);
			$('#link_authorization_delete').click(common.config.authorization.ciAuthorization.deleteAuthorization_aff);
			$('#link_Authorization_save_edit_ok').click(common.config.authorization.ciAuthorization.addAuthorization);
		}
	}
}();
$(document).ready(common.config.authorization.ciAuthorization.init);