$package("common.config.template")
$import('itsm.cim.includes.includes');
$import('itsm.change.includes.includes');
$import('itsm.problem.includes.includes');
$import('itsm.request.includes.includes');
$import('common.knowledge.leftMenu');
/**  
 * @author Wstuo  
 * @constructor 
 * @description 内容模板函数
 * @date 2012-09-04
 * @since version 1.0 
 */ 
common.config.template.templateGrid = function(){
	return {
		/**
		 * 显示内容模板列表
		 */
		showTemplateGrid:function(){
		var _url="template!findPager.action";
		var params = $.extend({},jqGridParams, {
			url: _url,
			colNames:['ID',i18n['template_name'],i18n['type'],'',i18n['operateItems']], 
			colModel:[
						{name:'templateId',width:60,align:'center',sortable:true},
						{name:'templateName',width:180,align:'center'},
						{name:'templateType',width:50,align:'center',formatter:common.config.template.templateGrid.typeFormat},
						{name:'serviceDirId',hidden:true},
						{name:'act', width:100,sortable:false,align:'center',formatter:function(cell,event,data){
							return $('#templateToolbarAct').html().replace(/{templateId}/g,event.rowId);
						}}
					],
			jsonReader:$.extend({},jqGridJsonReader,{id:'templateId'}),
			sortname:'templateId',
			pager:'#templatePager'
		});
		$("#templateGrid").jqGrid(params);
		$("#templateGrid").navGrid('#templatePager',navGridParams);
		$("#t_templateGrid").css(jqGridTopStyles);
		$("#t_templateGrid").append($('#templateToolbar').html());
		$("#templateGrid_loading").attr("style","display:none");
		},
		/**
		 * 类型格式化
		 */
		typeFormat:function(cellvalue, options, rowObject){
			if(cellvalue=='request'){
				return i18n['template_request'];
			}
			else if(cellvalue=='problem'){
				return i18n['template_problem'];
			}
			else if(cellvalue=='change'){
				return i18n['template_change'];
			}
			else if(cellvalue=='configureItem'){
				return i18n['template_configureItem'];
			}
			else{
				return i18n['template_knowledge'];
			}
			
		},
		/**
		 * 打开搜索框
		 */
		openSearch:function(){
			windows('searchTemplate',{width:350,modal: true});
			//禁止回车事件
			$("#searchTemplateName").keydown(function (e) {
	            var curKey = e.which;
	            if (curKey == 13) {
	                return false;
	            }
	        });
		},
		/**
		 * 进行搜索
		 */
		doSearch:function(){
			if($('#templateForm').form('validate')){
				var sdata=$('#searchTemplate form').getForm();
				var postData = $("#templateGrid").jqGrid("getGridParam", "postData");
				$.extend(postData,sdata);  // 将postData中的查询参数覆盖为空值
				$('#templateGrid').trigger('reloadGrid',[{"page":"1"}]);
			}
		},
		/**
		 * 打开编辑内容模板名称框
		 * @param rowId 选中Id
		 */
		edit_templateNameOpt:function(rowId){
			$.post("template!findByTemplateId.action", 'templateDTO.templateId='+rowId, function(data){
				$("#templateId_edit_win").val(data.templateId);
				$("#templateName_edit_win").val(data.templateName);
				windows('template_editName',{width:400,height:140,modal: true});
			});
		},
		/**
		 * 编辑内容模板名称
		 */
		edit_templateName:function(){
			if($('#template_editName form').form('validate')){
				startProcess();
				var param = $('#template_editName form').serialize();
				$.post("template!editTemplateName.action", param, function(data){
					endProcess();
					msgShow(i18n.saveSuccess,'show');
					$('#template_editName').dialog('close');
					$('#templateGrid').trigger('reloadGrid');
				});
			}
		},
		/**
		 * 点击编辑时判断是否选中行
		 */
		edit_template:function(){
			checkBeforeEditGrid('#templateGrid',common.config.template.templateGrid.editTemplate);
		},
		/**
		 * 编辑内容模板
		 * @param rowId 选中Id
		 */
		edit_template_aff:function(rowId){
			var rowData= $("#templateGrid").jqGrid('getRowData',rowId);  // 行数据  
			common.config.template.templateGrid.editTemplate(rowData);
		},
		/**
		 * 编辑内容模板
		 * @param rowData 模板对象
		 */
		editTemplate:function(rowData){
			    var templateId=rowData.templateId;
			    var serviceDirId=rowData.serviceDirId;
				if(rowData.templateType==i18n['template_request']){
					basics.tab.tabUtils.closeTab(i18n["title_request_addRequest"]);
					basics.tab.tabUtils.closeTab(i18n["request_add_templet_title"]);
					basics.tab.tabUtils.refreshTab(i18n["request_edit_templet_title"],'../pages/itsm/request/addRequestFormCustom.jsp?pageType=template&templateId='+templateId+'&serviceDirId='+serviceDirId);
				}
				if(rowData.templateType==i18n['template_problem']){
					basics.tab.tabUtils.closeTab(i18n["problem_add"]);
					basics.tab.tabUtils.closeTab(i18n["problem_add_templet_title"]);
					basics.tab.tabUtils.addTab(i18n['problem_edit_templet_title'],'../pages/itsm/problem/addProblem.jsp?pageType='+"template"+'&templateId='+templateId);
				}
				if(rowData.templateType==i18n['template_change']){
					basics.tab.tabUtils.closeTab(i18n["saveChange"]);
					basics.tab.tabUtils.closeTab(i18n["change_add_templet_title"]);
					basics.tab.tabUtils.reOpenTab('../pages/itsm/change/addChange.jsp?pageType='+"template"+'&templateId='+templateId,i18n['change_edit_templet_title']);
				}
				if(rowData.templateType==i18n['template_configureItem']){
					var ci_s_categoryName_v=encodeURIComponent("");
					basics.tab.tabUtils.closeTab(i18n["ci_addConfigureItem"]);
					basics.tab.tabUtils.closeTab(i18n["ci_add_templet_title"]);
					basics.tab.tabUtils.reOpenTab('ci!configureItemAdd.action?categoryName='+ci_s_categoryName_v+'&categoryType='+""+'&categoryNo='+""+'&pageType='+"template"+'&templateId='+templateId,i18n['ci_edit_templet_title']);
				}
				if(rowData.templateType==i18n['template_knowledge']){
					basics.tab.tabUtils.closeTab(i18n["title_request_newKnowledge"]);
					basics.tab.tabUtils.closeTab(i18n["issue_add_templet_title"]);
					basics.tab.tabUtils.addTab(i18n['issue_edit_templet_title'],'../pages/common/knowledge/addKnowledge.jsp?pageType='+"template"+'&templateId='+templateId);
				}
		},
		/**
		 * @description 删除时判断是否选中.
		 */
		opt_delete:function(){
			checkBeforeDeleteGrid('#templateGrid',common.config.template.templateGrid.opt_deleteInLineMethod);
		},
		/**
		 * @description 确定删除.
		 * @param rowId 选中列Id
		 */
		opt_delete_aff:function(rowId){
			msgConfirm(i18n['msg_msg'],'<br/>'+i18n['msg_confirmDelete'],function(){
				common.config.template.templateGrid.opt_deleteInLineMethod(rowId);
			});
		},
		/**
		 * @description 删除内容模板
		 * @param rowId 选中列Id
		 */
		opt_deleteInLineMethod:function(rowId){
			var param = $.param({'templateIds':rowId},true);
			$.post("template!delTemplate.action", param, function(data){
					$('#templateGrid').trigger('reloadGrid');
					msgShow(i18n['deleteSuccess'],'show');
			});
		},
		/**
		 * 打开保存内容模板框
		 */
		saveTemplate:function(){
			windows('templateTypeDiv',{width:300});
		},
		/**
		 * 跳转到对应模块的内容模板
		 */
		selectTemplateConfirm:function(){
			var _templateType=$("input[name='templateType']:checked").val();
			$('#templateTypeDiv').dialog('close');
			if(_templateType=='request'){
				basics.tab.tabUtils.closeTab(i18n["title_request_addRequest"]);
				basics.tab.tabUtils.closeTab(i18n["request_edit_templet_title"]);
				basics.tab.tabUtils.closeTab(i18n["formDesigner"]);
				basics.tab.tabUtils.refreshTab(i18n["request_add_templet_title"],'../pages/itsm/request/addRequestFormCustom.jsp?pageType='+"template");
			}
			if(_templateType=='problem'){
				basics.tab.tabUtils.closeTab(i18n["problem_add"]);
				basics.tab.tabUtils.closeTab(i18n["problem_edit_templet_title"]);
				basics.tab.tabUtils.refreshTab(i18n['problem_add_templet_title'],'../pages/itsm/problem/addProblem.jsp?pageType='+"template");
			}
			if(_templateType=='change'){
				basics.tab.tabUtils.closeTab(i18n["saveChange"]);
				basics.tab.tabUtils.closeTab(i18n["change_edit_templet_title"]);
				basics.tab.tabUtils.refreshTab(i18n['change_add_templet_title'],'../pages/itsm/change/addChange.jsp?pageType='+"template");
			}
			if(_templateType=='configureItem'){
				var ci_s_categoryName_v=encodeURIComponent("");
				basics.tab.tabUtils.closeTab(i18n["ci_addConfigureItem"]);
				basics.tab.tabUtils.closeTab(i18n["ci_edit_templet_title"]);
				basics.tab.tabUtils.closeTab(i18n["formDesigner"]);
				basics.tab.tabUtils.refreshTab(i18n['ci_add_templet_title'],'ci!configureItemAdd.action?categoryName='+ci_s_categoryName_v+'&categoryType='+""+'&categoryNo='+""+'&pageType='+"template");
			}
			if(_templateType=='knowledge'){
				basics.tab.tabUtils.closeTab(i18n["title_request_newKnowledge"]);
				basics.tab.tabUtils.closeTab(i18n["issue_edit_templet_title"]);
				basics.tab.tabUtils.refreshTab(i18n['issue_add_templet_title'],'../pages/common/knowledge/addKnowledge.jsp?pageType='+"template");
			}
		},
		/**
		 * 初始化
		 * @private
		 */
		init:function(){
			$("#templateGrid_loading").hide();
			$("#templateGrid_content").show();
			common.config.template.templateGrid.showTemplateGrid();
			$('#template_confirm').click(common.config.template.templateGrid.selectTemplateConfirm);//确认选择的类型
			$('#template_common_dosearch').click(common.config.template.templateGrid.doSearch);
		}
	}
}();
$(document).ready(common.config.template.templateGrid.init);
