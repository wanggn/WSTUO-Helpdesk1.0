$package('common.conifg.backup');
 /**  
  * 备份
 * @author Van  
 * @constructor WSTO
 * @description 备份
 * @date 2011-10-24
 * @since version 1.0 
 */
common.config.backup.backupMain = function() {
	return {
		/**
		 * @description 操作项格式化.
		 * @param cell 当前列值
		 * @param event 事件
		 * @param data 行数据
		 * @private
		 */
		actionFormatter:function(cell,event,data){
			return "&nbsp;&nbsp;<a href='backup!download.action?fileName="+data.fileName+"'>"+i18n.common_download+"</a>";
		},
		/**
		 * 加载备份列表.
		 * @private
		 */
		showGrid:function(){
			var params = $.extend({},jqGridParams, {
				url:'backup!showAllBackFiles.action',
				colNames:[i18n.title_backup_fileName,i18n.title_backup_fileSize,i18n.title_backup_createDate,i18n.common_action],
				colModel:[{name:'fileName',width:40,align:'center',sortable:false},
                          {name:'fileSize',width:20,align:'center',sortable:false},
                          {name:'crateDate',width:20,align:'center',sortable:false},
                          {name:'act',width:20,align:'center',sortable:false,formatter:common.config.backup.backupMain.actionFormatter,hidden:database_restoreable}
                         ],
				jsonReader: $.extend(jqGridJsonReader, {id: "fileName"}),
				sortname:'crateDate',
				autowidth:true,
				rowNum:100
				});
				$("#backupGrid").jqGrid(params);			
				$("#t_backupGrid").css(jqGridTopStyles);
				$("#t_backupGrid").append($('#backupGridToolbar').html());
		},
		/**
		 * 删除备份.
		 * @private
		 */
		deleteBackup:function(){
			checkBeforeDeleteGrid('#backupGrid',function(rowIds){
				var _param = $.param({'fileNames':rowIds},true);
				$.post('backup!deleteBackup.action',_param,function(res){				
					msgShow(i18n.msg_backup_backupFileHasBeenSuccessfulDeleted,'show');
					$("#backupGrid").trigger('reloadGrid');
				});
			});
		},
		/**
		 * 创建备份.
		 * @private
		 */
		createBackup:function(){			
			msgConfirm(i18n.tip,i18n.msg_backup_createBackup,function(){
				startProcess();
				$.post('backup!createBackup.action',function(res){				
					endProcess();
					msgShow(i18n.msg_backup_backupFileHasBeenSuccessfulCreated,'show');
					$("#backupGrid").trigger('reloadGrid');
				});
			});
		},
		/**
		 * 初始化加载.
		 * @private
		 */
		init:function(){
			common.config.backup.backupMain.showGrid();
			$('#backupGrid_createBack').click(common.config.backup.backupMain.createBackup);
			$('#backupGrid_deleteBack').click(common.config.backup.backupMain.deleteBackup);
		}
	}
}();
$(document).ready(common.config.backup.backupMain.init);