$package('common.config.category');

common.config.category.CICategoryUtil=function(){
	return {
		/**
		 * @description  加载扩展属性下拉列表.
		 * @param select 扩展属性下拉列表Id
		 */
		loadEavsToSelect:function(select){
			var url = "eav!findEavEntity.action";
			$.post(url,"rows=10000", function(res){
				$(select).html('');
				$('<option value="">--'+i18n.pleaseSelect+'--</option>').appendTo(select);
				if(res!==null && res.data!==null && res.data.length>0){
					for(var i=0;i<res.data.length;i++){
						$("<option value='"+res.data[i].eavNo+"'>"+res.data[i].eavName+"</option>").appendTo(select);
					}
				}
			});
		}
	}
}();