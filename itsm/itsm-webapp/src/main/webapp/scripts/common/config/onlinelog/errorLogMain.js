$package('common.config.onlinelog');
$import('common.report.defaultTimeAndBind');
 /**  
 * @author QXY  
 * @constructor tan
 * @description 错误日志函数
 * @date 2012-01-17
 * @since version 1.0 
 */
common.config.onlinelog.errorLogMain = function() {
	return {
		
		/**
		 * @description 操作项式化.
		 * @param cell 当前列值
		 * @param event 事件
		 * @param data 行数据
		 * @private
		 */
		actionFormatter:function(cell,event,data){
			 return '<a href="javascript:common.config.onlinelog.errorLogMain.downloadErrFile(\''+data.fileName+'\')"><img src="../images/icons/arrow_down_blue.gif"></a>';
		},
		downloadErrFile:function(fileName){
			 location.href="log!downloadErrLog.action?fileName="+fileName;
		 }
		 ,
		/**
		 * 加载错误日志列表.
		 */
		showGrid:function(){
			var params = $.extend({},jqGridParams, {
				url:'log!errLogPage.action',
				colNames:[i18n['title_backup_fileName'],i18n['title_backup_fileSize'],i18n['common_createTime'],i18n['common_action']],
				colModel:[{name:'fileName',width:40,align:'center',sortable:false,formatter:function(cellvalue,options,rowOjbect){
					 return '<a href="javascript:common.config.onlinelog.errorLogMain.downloadErrFile(\''+cellvalue+'\')">'+cellvalue+'</a>';
				 	}},
				          {name:'fileSize',width:20,align:'center',sortable:false},
				          {name:'time',width:20,align:'center',sortable:false}, 
				          {name:'act',width:20,align:'center',sortable:false,formatter:common.config.onlinelog.errorLogMain.actionFormatter}
						  ],
				jsonReader: $.extend(jqGridJsonReader, {id: "crateDate"}),
				sortname:'crateDate',
				sortorder:'desc',
				pager:'#errorLogGridPager'
				});
				$("#errorLogGrid").jqGrid(params);		
				$("#errorLogGrid").navGrid('#errorLogGridPager',navGridParams);
				$("#t_errorLogGrid").css(jqGridTopStyles);
				$("#t_errorLogGrid").append($('#errorLogGridToolbar').html());
			
		},
		/**
		 * 删除错误文件
		 */
		deleteErrorLog:function(){
			checkBeforeDeleteGrid('#errorLogGrid',function(rowIds){
				var _param = $.param({'fileNames':rowIds},true);
				$.post('userErrLog!deleteErrorLog.action',_param,function(res){				
					msgShow(i18n['msg_deleteSuccessful'],'show');
					$("#errorLogGrid").trigger('reloadGrid');
				});
			});
		},
		/**
		 * 删除错误文件
		 */
//		deleteErrorLog:function(){
//			checkBeforeDeleteGrid('#errorLogGrid',function(rowIds){
//				var fileNames = new Array([rowIds.length]);
//				for(var i=0;i<rowIds.length;i++){
//					var rowData=$("#errorLogGrid").getRowData(rowIds[i]);
//					fileNames[i]=rowData.fileName;
//				}
//				var _param = $.param({'fileNames':fileNames},true);
//				$.post('userErrLog!deleteErrorLog.action',_param,function(res){				
//					msgShow(i18n['msg_deleteSuccessful'],'show');
//					$("#errorLogGrid").trigger('reloadGrid');
//				});
//			});
//		},
		/**
		 * 创建错误日志文件
		 */
		exportUserErrLog:function(){		
			$('#exportUserErrLog_msg').text('['+i18n['msg_export_ing']+']');
			$.post('userErrLog!exportUserErrLog.action',function(res){
				$('#exportUserErrLog_msg').text('['+i18n['msg_dc_dataExportSuccessful']+']');
				$("#errorLogGrid").trigger('reloadGrid');
			});
		},
		/**
		 * 创建错误日志文件
		 */
		searchErrorLog:function(){		
			var _url = 'userErrLog!showAllErrLogFiles.action';
			$('#errorLogGrid').jqGrid('setGridParam',{page:1,url:_url,postData:$("#searchErrLogForm").serialize()}).trigger('reloadGrid');
		},
		/**
		 * 打开搜索框
		 */
		openSearch:function(){
			//bindControl('#ErrLog_search_startTime,#ErrLog_search_endTime');
			windows('ErrLog_search',{width:450});
		},
		/**
		 * 初始化
		 * @private
		 */
		init:function(){
			common.config.onlinelog.errorLogMain.showGrid();
			$('#errorLogGrid_createBack').click(common.config.onlinelog.errorLogMain.exportUserErrLog);
			$('#errorLogGrid_deleteBack').click(common.config.onlinelog.errorLogMain.deleteErrorLog);
			$('#errorLogGrid_search').click(common.config.onlinelog.errorLogMain.openSearch);
			$('#ErrLog_searchBtn').click(common.config.onlinelog.errorLogMain.searchErrorLog);
			$("#userOnlineGrid_download").click(function(){
				//bindControl('#errorZipStartTime,#errorZipEndTime');
				windows('zipUserErrorLog',{width:450});
			});
			 $("#userErrorGrid_download").click(function(){
					//bindControl('#errorZipStartTime,#errorZipEndTime');
					windows('zipUserErrorLog',{width:450});
				});
			$("#userErrorGrid_downloadzip").click(function(){
				startProcess();
				var startTime = $("#errorZipStartTime").val();
				var endTime = $("#errorZipEndTime").val();
				$.post("log!multiZip.action","zipType=errorLog&startTime="+startTime+"&endTime="+endTime,function(data){
					if(data){
						location.href="log!downloadMultiZip.action?fileName="+data;
						endProcess();
						$.post("log!deleteNultiZip.action","fileName="+data);
					}
				});
			});
			
		}
	}
}();
$(document).ready(common.config.onlinelog.errorLogMain.init);