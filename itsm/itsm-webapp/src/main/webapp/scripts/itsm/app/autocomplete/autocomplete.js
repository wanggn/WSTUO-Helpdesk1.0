$package('itsm.app.autocomplete'); 
$import("common.tools.tcode.t_code");
/**  
 * @fileOverview 自动补全主函数
 * @author QXY
 * @version 1.0  
 */  
 /**  
 * @author QXY  
 * @constructor 自动补全主函数
 * @description 自动补全主函数
 * @date 2011-10-24
 * @since version 1.0 
 */ 
itsm.app.autocomplete.autocomplete=function(){	
	return {
		/**
		 * 
		 * @param inputId
		 * @param entityName 补全的实体类名
		 * @param propertyName  补全的字段
		 * @param returnLabel 补全下拉显示的内容
		 * @param returnValue 补全文本框显示的内容
		 * @param returnValueType 字段的类型（Long or 其他）
		 * @param hiddenId   需要隐藏id的input文本框ID
		 * @param terms 条件
		 * @param yOrn 是否验证
		 * @returns
		 */
		bindAutoComplete:function(inputId , entityName , propertyName , returnLabel , returnValue ,returnValueType ,hiddenId , terms , yOrn){
			if(terms===null||terms===undefined)
				terms="";
			var auto_param= 'paramDto.entityName='+entityName+
							'&paramDto.propertyName='+propertyName+
							'&paramDto.returnLabel='+returnLabel+
							'&paramDto.returnValue='+returnValue+
							'&paramDto.returnValueType='+returnValueType;
			var hiddenVal=$(hiddenId).val(); //记录表单的默认值
			var inputVal=$(inputId).val();
			
			var term=[];
			//var fName="";
			var n=1;
			$(inputId).autocomplete({
				minLength: 2,
				source: function(request, response) {
					if(terms!==null||terms!==undefined){
						var terms_companyNo = $(terms).val();
						terms_companyNo = terms_companyNo == undefined ? terms : terms_companyNo;
						$.post("autoComplete!autoComplete.action" , auto_param+'&paramDto.queryValue='+request.term+'&paramDto.term='+terms_companyNo, function(data){
							if(data!==null&&data.length>0){
								term=[];
								$.each(data,function(i,v){
									var lab=v.label;
									if(v.lName!==null&&v.lName.length>0){
										if(v.label==null&&v.label.length==0)
											v.label=v.lName;
										lab=v.label+"("+v.lName+","+v.remark1+")";
									}
									//fName=v.label;
									if(inputId=="#selectUser_userGrid_loginName")
										term.push({"label":lab,"value":propertyName==="loginName"||propertyName==='fullName'?v.lName:v.label,"id":v.longValue});
									else
										term.push({"label":lab,"value":v.label,"id":v.longValue});
								});
								response(term);
							}
						});
					}
				},select: function( e, ui ) {
					if(hiddenId!==null&&hiddenId.length>0){
						$(hiddenId).val(ui.item.id);	//给隐藏ID
					}
					if(inputId!==null&&inputId.length>0){
						$(inputId).val(ui.item.value);
						if(yOrn=="false" && inputId=="#selectUser_userGrid_loginName"){
							common.security.userUtil.searchuserRoleFmUser();
						}
					}
					if(hiddenId.indexOf("#request_companyNo") > 0 ){
						$("#request_userName").val("");
						$("#request_userId").val("");
					}
					return false;
				}
			}).blur(function(){
				setTimeout(function(){
					//失去焦点是判断表单内容是否正确
					if(yOrn!=="false"){
						var url = '';
						var param = '';
						//var right=true;
						var hId=$(hiddenId).val();
						var inpId=$(inputId).val();
						
						/*var boo=itsm.app.autocomplete.autocomplete.stringItem(inpId);
						if(boo==true){
							$('#inputId').focus();
							msgShow(i18n['label_string'],'show');
							return;
						}*/
						//var blurNoSelectedFirstInputID = "#selectUser_userGrid_loginName";//失去焦点并且选中第一个值得文本框ID
						if(entityName==="com.wstuo.common.security.entity.User"){//新增请求时判断用户
							if(yOrn==="true" && inputId!=="#selectUser_userGrid_loginName"){
								if(term.length>0){
									$(inputId).val(term[0].value);
									$(hiddenId).val(term[0].id);
								}/*else{
									//msgShow(i18n['autoPrompt'],'show');
									$(inputId).val(inputVal);
									$(hiddenId).val(hiddenVal);
								}*/
							}
							//inpId=fName;
						}else if(entityName==="com.wstuo.itsm.itsop.itsopuser.entity.ITSOPUser"){
							if(inpId===companyName && hId===companyNo)
								return;  //当所属客户是用户自己所在公司时停止验证
							url = 'itsopUser!autoVerification.action';
							param='itsopUserDTO.orgName=';
						}else if(entityName==="com.wstuo.common.security.entity.Organization"){
							if(term.length==0){
								$(inputId).val(inputVal);
								$(hiddenId).val(hiddenVal);
							}else{
								for ( var int2 = 0; int2 < term.length; int2++) {
									if(term[0].value==inpId)
										return false;
								}
								$(inputId).val(inputVal);
								$(hiddenId).val(hiddenVal);
							}
						}
						
						if(url !== '' && inpId!=='' && inpId!==undefined){
							$.post( url , param + inpId, function( data ){
								var ids =data.substring(1,data.length-1).split(",");
								var len=ids.length;
								if(len>1){
									if(hId===''){
										$(hiddenId).val(ids[0]);
										return;
									}
									for ( var _int = 0; _int < ids.length; _int++) {
										if(hId===ids[_int] + ""){
											return;
										}
									}
								}else if(len===1){
									if(ids[0]!==""){
										$(hiddenId).val(ids[0]);
										return;
									}
									else{
										if(yOrn==="ITSOPUser"){
											$(hiddenId).val("-1");
										}
									}
								}else{
									$(inputId).val(inputVal);
									$(hiddenId).val(hiddenVal);
								}
								
							});
						}
					}
				},600);
				$(inputId).removeClass("ui-autocomplete-loading");
			});
		},
		/**
		 * 过滤字符
		 */
		stringItem:function(str){
			 var pattern=new RegExp("[`~%!@#^=?~！@#￥……&——？*]");
			 //[]内输入你要过滤的字符，这里是我的 
			 var rs=pattern.test(str);
			
			 return rs;
		},
		/**
		 *  T_code
		 */
		bindTcodeAutoComplete:function(inputId,data){
			$(inputId).autocomplete({
				minLength: 1,
				source: data,
				select: function(e, ui) {
					$("#t_code_cmd").val(ui.item.value);
					//common.tools.tcode.t_code.docmd(ui.item.value);
					tcode_foucs='true';
				}
			});
		},
		/**
		 *  多个值，ajax返回json
		 */
		bindManyAutocomplete:function(inputId , entityName , propertyName , returnLabel , returnValue ,returnValueType ,hiddenId , terms){
			if(terms===null||terms===undefined)
				terms="";
			var auto_param= 'paramDto.entityName='+entityName+
							'&paramDto.propertyName='+propertyName+
							'&paramDto.term='+terms+
							'&paramDto.returnLabel='+returnLabel+
							'&paramDto.returnValue='+returnValue+
							'&paramDto.returnValueType='+returnValueType;
			$(inputId).bind("keydown", function(event) {
			    if (event.keyCode === $.ui.keyCode.TAB && $(this).autocomplete("instance").menu.active) {
			        event.preventDefault();
			    }// 按Tab键时，取消为输入框设置value
			}).autocomplete({
				minLength: 1,
		        source: function( request, response ) {
		        	$.post("autoComplete!autoComplete.action" , auto_param+'&paramDto.queryValue='+itsm.app.autocomplete.autocomplete.extractLast( request.term ) , function(data){
						if(data!==null&&data.length>0){
							var term=[];
							$.each(data,function(i,v){
								var lab=v.label;
								if(v.lName!==null&&v.lName.length>0)
									lab=v.label+"（"+v.lName+"）";
								term.push({"label":lab,"value":v.lName,"id":v.longValue});
							});
							response(term);
						}
					});
		        	//response( $.ui.autocomplete.filter(data, itsm.app.autocomplete.autocomplete.extractLast( request.term ) ) );
		        },
	            //获得焦点
			    focus:function() {
			        // prevent value inserted on focus
			        return false;
			    },
			    // 从autocomplete弹出菜单选择一个值时，加到输入框最后，并以逗号分隔
			    select: function(event, ui) {
			        var terms = itsm.app.autocomplete.autocomplete.split(this.value);
			        // remove the current input
			        terms.pop();
			        for ( var _int = 0; _int < terms.length; _int++) {
						if(terms[_int]===ui.item.value){
							return false;	//如果有选择的内容已有则返回
						}
					}
			        //console.log(ui.item);
			        // add the selected item
			        terms.push(ui.item.value);
			        // add placeholder to get the comma-and-space at the end
			        terms.push("");
			        this.value = terms.join(";");
			        return false;
			    }
			});
		},
		/**
		 *  按逗号分隔多个值
		 */
		split:function(val) {
		    return val.split(/;\s*/);
		},
		/**
		 * 提取输入的最后一个值
		 */ 
		extractLast:function(term) {
		    return itsm.app.autocomplete.autocomplete.split(term).pop();
		}
		
	};
}();