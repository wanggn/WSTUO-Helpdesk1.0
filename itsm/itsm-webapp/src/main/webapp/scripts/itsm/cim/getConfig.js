$package("itsm.cim");
 /**  
 * @fileOverview 获取当前电脑配置信息
 * @author yctan
 * @version 1.0  
 */  
 /**  
 * @author yctan  
 * @constructor 获取当前电脑配置信息
 * @description 获取当前电脑配置信息
 * @date 2011-4-11
 * @since version 1.0 
 */ 

itsm.cim.getConfig=function(){
	this.computerName='';
	this.OS='';
	this.OSVersion='';
	this.mainboard='';
	this.serialNumber='';
	this.CPU='';
	this.CPUGrade='';
	this.CPUSpeed='';
	this.memorySize='';
	this.memorySize1='';
	this.HDModel='';
	this.HDSize='';
	this.CDDrive='';
	this.show='';
	this.DesktopMonitor='';
	this.NetworkAdapter='';
	this.lastLogin='';
	this.networkDescription='';
	this.networkMacAddress='';
	this.networkIpAddress='';
	this.softwareStr='';
	return {
		/**
		 * @description 获取配置详细信息
		 */
		getConfigInfo:function(){
			
			//本地
			var locator = new ActiveXObject ("WbemScripting.SWbemLocator"); 
			$('#getConfigInfoLoad').text("["+i18n['titil_getLoad']+"]");
			var service = locator.ConnectServer("."); 
			
			//系统信息
			var properties = service.ExecQuery("SELECT * FROM Win32_OperatingSystem"); 
			var e = new Enumerator (properties); 
			for (; !e.atEnd(); e.moveNext ())
			{
				var p = e.item (); 
				computerName=p.CSName; 
				OS=p.Caption;
				OSVersion=p.Version;
			}
			// 主板
			var properties = service.ExecQuery("SELECT * FROM Win32_BaseBoard"); 
			var e = new Enumerator (properties); 
			for (; !e.atEnd(); e.moveNext ())
			{
				var p = e.item (); 
				mainboard=p.Manufacturer ;
				serialNumber=p.SerialNumber
			}
			
			//CPU 
			var properties = service.ExecQuery("SELECT * FROM Win32_Processor"); 
			var e = new Enumerator (properties); 
			for (; !e.atEnd(); e.moveNext ())
			{
				var p = e.item (); 
				CPU=p.Name 
				CPUGrade=p.Stepping
				CPUSpeed=p.MaxClockSpeed
			}
			
			//内存 
			var properties = service.ExecQuery("SELECT * FROM Win32_PhysicalMemory"); 
			var e = new Enumerator (properties); 
			memorySize=0
			memorySize1=0;
			for (; !e.atEnd(); e.moveNext ())
			{
				var p = e.item (); 
				if(p.Capacity!=""){
					memorySize=parseInt(p.Capacity)+parseInt(memorySize)
				}
					
			}
			if(memorySize>=0){
				memorySize1=memorySize/1024/1024
			}
			//硬盘  
			var properties = service.ExecQuery("SELECT * FROM Win32_DiskDrive"); 
			var e = new Enumerator (properties); 
//			for (; !e.atEnd(); e.moveNext ())
//			{
				var p = e.item (); 
				HDModel=p.Model 
				HDSize=p.Size
			//}
			if(HDSize>=0){
				HDSize1=HDSize/1000/1000
			}
			//光驱  
			var properties = service.ExecQuery("SELECT * FROM Win32_CDROMDrive"); 
			var e = new Enumerator (properties); 
			for (; !e.atEnd(); e.moveNext ())
			{
				var p = e.item (); 
				CDDrive=p.Caption 
			}
			//显示卡  
			var properties = service.ExecQuery("SELECT * FROM Win32_VideoController"); 
			var e = new Enumerator (properties); 
			for (; !e.atEnd(); e.moveNext ())
			{
				var p = e.item (); 	
				show=p.name 
			}
			//监视器类型  
			var properties = service.ExecQuery("SELECT * FROM Win32_DesktopMonitor"); 
			var e = new Enumerator (properties); 
			for (; !e.atEnd(); e.moveNext ())
			{
				var p = e.item (); 
				DesktopMonitor=p.name 
			}
			//网络适配器  
			var properties = service.ExecQuery("SELECT * FROM Win32_NetworkAdapter"); 
			var e = new Enumerator (properties); 
			for (; !e.atEnd(); e.moveNext ())
			{
				var p = e.item (); 	
				NetworkAdapter=p.Name 
			}
			var WshNetwork = new ActiveXObject("WScript.Network");
			lastLogin=WshNetwork.UserName
			
			var properties = service.ExecQuery("SELECT * FROM Win32_NetworkAdapterConfiguration"); 
			var e = new Enumerator (properties); 
			for (; !e.atEnd(); e.moveNext ())
			{
				var p = e.item (); 
				networkDescription=p.Description;
				networkMacAddress=p.MACAddress;
				networkIpAddress=p.IPAddress(0);
				
				if(networkMacAddress!=null && networkIpAddress!=null){
					break;
				}
			}
			
			
			//软件安装包  
			var properties = service.ExecQuery("SELECT * FROM Win32_Product"); 
			var e = new Enumerator (properties); 
			
			for (; !e.atEnd(); e.moveNext ())
			{
				var p = e.item (); 	
				softwareStr=p.Name+"=="+p.InstallDate+"<>"+softwareStr;
			}
			var param='hardwareDTO.lastLogin='+lastLogin+
				'&hardwareDTO.computerName='+computerName+
				'&hardwareDTO.serialNumber='+serialNumber+
				'&hardwareDTO.systemName='+OS+
				'&hardwareDTO.systemVersion='+OSVersion+
				'&hardwareDTO.mainboard='+mainboard+
				'&hardwareDTO.cpu='+CPU+
				'&hardwareDTO.cpuGrade='+CPUGrade+
				'&hardwareDTO.cpuSpeed='+CPUSpeed+
				'&hardwareDTO.memorySize='+memorySize1+
				'&hardwareDTO.hardDriveModel='+HDModel+
				'&hardwareDTO.hardDriveSize='+HDSize1+
				'&hardwareDTO.cdDrive='+CDDrive+
				'&hardwareDTO.networkAdapter='+NetworkAdapter+
				'&hardwareDTO.desktopMonitor='+show+
				'&hardwareDTO.monitor='+DesktopMonitor+

				'&hardwareDTO.networkDescription='+networkDescription+
				'&hardwareDTO.networkMacAddress='+networkMacAddress+
				'&hardwareDTO.networkIpAddress='+networkIpAddress+
				
				'&softwareDTO.softwareStr='+softwareStr
			return param;
		},
		/**
		 * 保存配置信息到数据库
		 */
		saveConfigInfo:function(){
			
		    var app=navigator.appName;
		    var verStr=navigator.appVersion;
		    if (app.indexOf('Netscape') != -1) {
		    	
		        msgAlert("友情提示：\n 使用此功能建议您使用 IE4.0 或以上版本。");
		    }else if (app.indexOf('Microsoft') != -1) {
		      if (verStr.indexOf("MSIE 3.0")!=-1 || verStr.indexOf("MSIE 4.0") != -1 || verStr.indexOf("MSIE 5.0") != -1 || verStr.indexOf("MSIE 5.1") != -1)
		          msgAlert("友情提示：\n    您的浏览器版本太低,建议您使用 IE4.0 或以上版本。");
		      else{
		    	  	var postData=itsm.cim.getConfig.getConfigInfo();
			    	$.post('ci!saveCiConfigInfo.action',postData+'&ciEditId='+$('#configureCiInfoId').val(),function(){
						msgShow(i18n['title_getSuccess'],'show');
						itsm.cim.getConfig.showConfigInfo();
						$('#getConfigInfoLoad').text('');
			    	})	
			    }
		    }
		},
		
		configSetInput:function(value,inputId){
			if(value!=null)
				$(inputId).val(value);
			else
				$(inputId).val('');
		},
		
		/**
		 * @description 显示电脑配置信息
		 */
		showConfigInfo:function(){
			$.post('ci!getCiHardware.action','ciEditId='+$('#configureCiInfoId').val(),function(data){
				if(data!=null){
					itsm.cim.getConfig.configSetInput(data.ciHardwareId,'#ciHardwareId');
					itsm.cim.getConfig.configSetInput(data.lastLogin,'#lastLogin');
					itsm.cim.getConfig.configSetInput(data.computerName,'#computerName');
					itsm.cim.getConfig.configSetInput(data.serialNumber,'#serialNumber');
					itsm.cim.getConfig.configSetInput(data.systemName,'#systemName');
					itsm.cim.getConfig.configSetInput(data.systemVersion,'#systemVersion');
					itsm.cim.getConfig.configSetInput(data.mainboard,'#mainboard');
					itsm.cim.getConfig.configSetInput(data.cpu,'#cpu');
					itsm.cim.getConfig.configSetInput(data.cpuGrade,'#cpuGrade');
					itsm.cim.getConfig.configSetInput(data.cpuSpeed,'#cpuSpeed');
					itsm.cim.getConfig.configSetInput(data.memorySize,'#memorySize');
					itsm.cim.getConfig.configSetInput(data.hardDriveModel,'#hardDriveModel');
					itsm.cim.getConfig.configSetInput(data.hardDriveSize,'#hardDriveSize');
					itsm.cim.getConfig.configSetInput(data.cdDrive,'#cdDrive');
					itsm.cim.getConfig.configSetInput(data.networkAdapter,'#networkAdapter');
					itsm.cim.getConfig.configSetInput(data.desktopMonitor,'#desktopMonitor');
					itsm.cim.getConfig.configSetInput(data.monitor,'#monitor');
					itsm.cim.getConfig.configSetInput(data.networkDescription,'#networkDescription');
					itsm.cim.getConfig.configSetInput(data.networkMacAddress,'#networkMacAddress');
					itsm.cim.getConfig.configSetInput(data.networkIpAddress,'#networkIpAddress');
					itsm.cim.getConfig.configSetInput(data.remark,'#config_remark');

				}
			})
		},
		/**
		 * 格式数据
		 */
		dataForma:function(cell){
			if(cell!=null && cell!='' && cell.length>=6 && cell.indexOf('-')==-1){
				return cell.substr(0,4)+'-'+cell.substr(4,2)+'-'+cell.substr(6,4);
			}else{
				if(cell!=null && cell!='' && cell.indexOf('-')>-1)
					return cell;
				else
					return '';
			}
		},
		/**
		 * @description 显示安装软件
		 */
		
		showInstallSoftware:function(){
			var params = $.extend({},jqGridParams,{
				url:'ci!getCiSoftware.action?ciEditId='+$('#configureCiInfoId').val(),
				colNames:['ID',i18n['title_softName'],i18n['title_installDate'],i18n['title_snmp_installState']
							,i18n['title']
							,i18n['label_software_identifyingNumber']
							,i18n['title_snmp_productID']
							,i18n['label_software_regOwner']
							,i18n['label_software_packageCache']
							,i18n['label_software_packageCode']
							,i18n['label_software_assignmentType']
							,i18n['label_software_LocalPackage']
							,i18n['label_software_InstallSource']
							,i18n['label_software_InstallLocation']
							,i18n['title_installDate']
							,i18n['label_software_Vendor']
							,i18n['label_software_PackageName']
							,i18n['label_software_Language']
							,i18n['label_software_regCompany']
							,i18n['label_software_SKUNumber']
						],
				colModel:[
						  {name:'softwareId',index:'id',width:20,sortable:false},
						  {name:'softwareName',width:120,sortable:false,formatter:function(cellvalue, options, rowObject){
								 return rowObject.softwareName; 
						  }},
						  {name:'installDate',width:80,formatter:itsm.cim.getConfig.dataForma,sortable:false},
						  {name:'installState',width:80,align:'center',sortable:false,hidden:true},
						  {name:'caption',width:80,align:'center',sortable:false,hidden:true},
						  {name:'identifyingNumber',width:80,align:'center',sortable:false,hidden:true},
						  {name:'productID',width:80,align:'center',sortable:false,hidden:true},
						  {name:'regOwner',width:80,align:'center',sortable:false,hidden:true},
						  {name:'packageCache',width:80,align:'center',sortable:false,hidden:true},
						  {name:'packageCode',width:80,align:'center',sortable:false,hidden:true},
						  {name:'assignmentType',width:80,align:'center',sortable:false,hidden:true},
						  {name:'localPackage',width:80,align:'center',sortable:false,hidden:true},
						  {name:'installSource',width:80,align:'center',sortable:false,hidden:true},
						  {name:'installLocation',width:80,align:'center',sortable:false,hidden:true},
						  {name:'installDate2',width:80,align:'center',sortable:false,hidden:true},
						  {name:'vendor',width:80,align:'center',sortable:false,hidden:true},
						  {name:'packageName',width:80,align:'center',sortable:false,hidden:true},
						  {name:'softwareLanguage',width:80,align:'center',sortable:false,hidden:true},
						  {name:'regCompany',width:80,align:'center',sortable:false,hidden:true},
						  {name:'skuNumber',width:80,align:'center',sortable:false,hidden:true}
						  ],
				multiselect:true,
				toolbar:[true,'top'],
				jsonReader: $.extend(jqGridJsonReader, {id: "softwareId"}),
				sortname:'softwareId',
				pager:'#installSoftwarePager'
			});
			$("#installSoftwareGrid").jqGrid(params);
			$("#installSoftwareGrid").navGrid('#installSoftwarePager',navGridParams);
			defaultLoadColumn("#installSoftwareGrid");
			//自定义列
			$("#installSoftwareGrid").jqGrid('navButtonAdd','#installSoftwarePager',{
			    caption:"",
			    title:i18n['label_set_column'],
			    onClickButton : function (){
			    	loadColumnChooserItem('installSoftwareGrid');
			    }
			});	
			//列表操作项
			$("#t_installSoftwareGrid").css(jqGridTopStyles);
			$("#t_installSoftwareGrid").append($('#installSoftwareGridToolbar').html());
			
			configureItemInfoGrids.push('#installSoftwareGrid');
		},
		
		
		/**
		 * @description 修改配置信息
		 */
		updateConfigInfo:function(){
			var param=$('#configInfoDiv form').serialize();
			$.post('ci!saveHardware.action',param,function(){
				$.post('ci!getCiHardware.action','ciEditId='+$('#configureCiInfoId').val(),function(data){
					if(data!=null){
						if(data.ciHardwareId!=null){
							$('#ciHardwareId').val(data.ciHardwareId);
						}
					}
				})
				msgShow(i18n['saveSuccess'],'show');
			})
		},
	
		init:function(){
		}
	}
}();
//$(document).ready(itsm.cim.getConfig.init);

