 /**  
 * @author QXY  
 * @constructor 请求审批加签
 * @description 请求审批加签
 * @date 2013-12-07
 * @since version 1.0 
 */
var requestVisit=function(){
	/**
	 * @description 请求审批加签
	 */
	this.requestVisit_win=function(){
		$("#requestVisit_win table").html("");
		$.post('visit!findVisit.action?rows=10000&useStatus=true',function(data){
			if(data==null || data.data==null) return;
			$("#requestVisit_win table").append("<tr><td>"+i18n.label_returnVisit_satisfaction+"</td><td>"+
					"<input type='radio' value='5' name='userReturnVisitDTO.satisfaction' />"+i18n.label_returnVisit_verysatisfied+
					"<input type='radio' value='4' name='userReturnVisitDTO.satisfaction' />"+i18n.label_returnVisit_satisfied+
					"<input type='radio' value='3' name='userReturnVisitDTO.satisfaction' checked='checked' />"+i18n.label_returnVisit_general+
					"<input type='radio' value='2' name='userReturnVisitDTO.satisfaction' />"+i18n.label_returnVisit_dissatisfied+
					"<input type='radio' value='1' name='userReturnVisitDTO.satisfaction' />"+i18n.label_returnVisit_verydissatisfied+
					"</td></tr>");
			var visitItem=data.data;
			for(var i=0;i<visitItem.length;i++){
				if(visitItem[i].useStatus){
					var html="";
				    if(visitItem[i].visitItemType=='radio' && visitItem[i].visitItems!=null){
				    	
						for(var j=0;j<visitItem[i].visitItems.length;j++){
						    html+= "<input type='radio' value='"+visitItem[i].visitItems[j].visitItemName+"' name='visitItemValue"+i+"' ";
						    if (j===0) {
                              html+= "checked ='checked'";
                            }
						    html+= "/>"+visitItem[i].visitItems[j].visitItemName;
						}
				    } else if(visitItem[i].visitItemType=='text' && visitItem[i].visitItems!=null){
				        html="<input  id='visitItemValue"+i+"' />";
				    }else if(visitItem[i].visitItemType=='Lob' && visitItem[i].visitItems!=null){
				    	html="<textarea type='' id='visitItemValue"+i+"' /></textarea>";
				    }else{
				    	html="<input class='easyui-numberbox input' type='' id='visitItemValue"+i+"' />";
				    }
				    $("#requestVisit_win table").append("<tr><td>"+visitItem[i].visitName+"<input type='hidden' value='"+visitItem [i].visitName+"' id='visitItemName"+i+"' /></td><td>"+html+"</td></tr>");
				}
			}
			
		});
	},
	/**
	 * 获取请求参数
	 * @param paras 请求参数Id
	 */
	this.request=function(paras) {
			var url = location.href;  
			var paraString = url.substring(url.indexOf("?")+1,url.length).split("&");  
			var paraObj = {};  
			for (i=0; j=paraString[i]; i++){  
				paraObj[j.substring(0,j.indexOf("=")).toLowerCase()] = j.substring(j.indexOf("=")+1,j.length);
			}  
			var returnValue = paraObj[paras.toLowerCase()];  
			if(typeof(returnValue)=="undefined"){  
				return "";  
			}else{  
				return returnValue; 
			}
	},
	/**
	 * 关闭页面
	 */
	this.closeme=function(){
		var browserName=navigator.appName; 
		if (browserName=="Netscape"){
			window.open('','_parent','');
			window.close();
		}else if(browserName=="Microsoft Internet Explorer"){ 
			window.opener = "whocares";
			window.close();
		}
	},
	/**
	 * 加载加签人信息
	 */
	this.loadFindById=function(){
		var sendUserNum=request("randomNumber1");
		var user_URL="user!findUserByRandomNum.action";
		$.post(user_URL,"userDto.randomId="+sendUserNum,function(datas){
			if(datas == null){
				window.open("../../../pages/errors/403.jsp");
				window.close();
			}else{
				var requestId=request("requestId");
				$.post("historyRecord!findAllHistoryRecordByClient.action","hisotryRecordDto.logTitle="+i18n.lable_request_appor_user_ref+"&hisotryRecordDto.eno="+requestId+"&hisotryRecordDto.eventType=itsm.request",function(datalist){
					if(datalist.length ===0){
						$("#ApporTAB").css("display","block");
						$("#addApporUserDIV").css("display","none");
					}else{
						$("#addApporUserDIV").css("display","block");
						$("#ApporTAB").css("display","none");
					}
				});
			}
		});
	},
	/**
	 * 请求加签
	 */
	this.requestVisit_opt=function(){
		var requestId=request("requestId");
		var sendUserNum=request("randomNumber1");
		var getUserNum=request("randomNumber2");
		var user_url="user!findUserByRandomNum.action";
		startProcess();
		$.post(user_url,"userDto.randomId="+sendUserNum,function(datas){
			if(datas == null ||datas == "null"){
				window.open("../../../pages/errors/403.jsp");
				window.close();
			}else{
				var apporText=$("#apporText").val();
				var url="historyRecord!saveHistoryRecord.action";
				$.post("historyRecord!findAllHistoryRecordByClient.action","hisotryRecordDto.logTitle="+i18n.lable_request_appor_user_ref+"&hisotryRecordDto.eno="+requestId+"&hisotryRecordDto.eventType=itsm.request",function(datalist){
					if(datalist.length ===0){
						var frm="hisotryRecordDto.operator="+datas.loginName+"&hisotryRecordDto.logTitle="+i18n.lable_request_appor_user_ref+"&hisotryRecordDto.logDetails="+apporText+"&hisotryRecordDto.eno="+requestId+"&hisotryRecordDto.eventType=itsm.request";
						$.post(url,frm,function(data){
							if(data == null){
								startProcess();
								$.post("user!findUserByRandomNum.action","userDto.randomId="+getUserNum,function(_data){
									var noticeFrm="noticeRuleDTO.noticeRuleNo=requestPlusSignReplyNotice&templateVariableDTO.eventCode="+requestId+"&noticeRuleDTO.technicianOrEmail="+_data.email;
									var urls="noticeRule!apporCountersign.action";
									$.post(urls,noticeFrm,function(){
										endProcess();
										msgConfirm(i18n.tip,'<br/>'+i18n.lable_request_appor_user_ok,function(){
											closeme();
										});
									});
								});
							}
						});
					}else{
						msgConfirm(i18n.tip,'<br/>'+i18n.lable_request_appor_user_on,function(){
							closeme();
						});
					}
				});
			}
		});
	};
	return {
		/**
		 * 初始化
		 * @private
		 */
		init:function(){
			loadFindById();
			$('#addApporUser').bind("click",function(){
				requestVisit_opt();
			});
		}
	};

}();
$(document).ready(requestVisit.init);