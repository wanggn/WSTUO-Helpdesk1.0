$package('itsm.request.updateRequest');
/**  
 * @author Van  
 * @constructor updateRequest
 * @description 请求过期自动转化成问题
 * @date 2012-02-07
 * @since version 1.0 
 */  
itsm.request.updateRequest.updateRequest = function() {
	return{
		/**
		 * 显示请求过期多久转化成问题
		 */
		find:function(){
			$.post('updateRequest!find.action',function(data){
				if(data!=null){
					$('#edit_update_request_day').val(data.day);
					$('#edit_update_request_hour').val(data.hour);
					$('#edit_update_request_minute').val(data.minute);
					if(data.enable=="1"){
						$('#edit_update_request_enable').attr({"checked":true});
					}else{
						$('#edit_update_request_enable').attr({"checked":false});
					}
					$('#edit_update_request_id').val(data.id);
				}
			});
		},
		/**
		 * 保存请求过期转问题转化时间
		 */
		save:function(){
			var _param=$('#updateRequest_form').serialize();
			$.post('updateRequest!merge.action',_param,function(){
				itsm.request.updateRequest.updateRequest.find();
				msgShow(i18n.saveSuccess,'show');
			});
		}
	};
}();