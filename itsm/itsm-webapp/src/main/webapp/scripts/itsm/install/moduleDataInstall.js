$package('itsm.install');
 /**  
 * @author Wstuo  
 * @constructor moduleDataInstall
 * @description 加载模块数据主函数.
 * @date 2013-03-21
 * @since version 1.0 
 */
itsm.install.moduleDataInstall=function(){
	var lan;
	if(language=='en_US'){
		lan="en";
	}else if(language=='zh_CN'){
		lan="cn";
	}else if(language=='ja_JP'){
		lan="jp";
	}else if(language=='zh_TW'){
		lan="tw";
	}
	return {
		/**
		 * 加载请求基础数据
		 */
		installRequestData:function(){
			startProcess();
			$.post("configData!insatllRequestData.action",{"installType":"request","languageVersion":lan},function(res){
				endProcess();
				if(res=='success'){
					$('#requestInstall_btn').linkbutton({disabled:true});
					msgShow(i18n['label_installRequestDataSuccess'],'show');
				}else if(res=='failure'){
					msgAlert(i18n['install_Failure'],'error');
				}else if(res=='configed'){
					msgAlert(i18n['install_Repeat'],'warning');
				}
				
			});
		},
		/**
		 * 加载问题基础数据
		 */
		installProblemData:function(){
			startProcess();
			$.post("configData!insatllProblemData.action",{"installType":"problem","languageVersion":lan},function(res){
				endProcess();
				if(res=='success'){
					$('#problemInstall_btn').linkbutton({disabled:true});
					msgShow(i18n['label_installProblemDataSuccess'],'show');
				}else if(res=='failure'){
					msgAlert(i18n['install_Failure'],'error');
				}else if(res=='configed'){
					msgAlert(i18n['install_Repeat'],'warning');
				}
			});
		},
		/**
		 * 加载变更基础数据
		 */
		installChangeData:function(){
			startProcess();
			$.post("configData!insatllChangeData.action",{"installType":"change","languageVersion":lan},function(res){
				endProcess();
				if(res=='success'){
					$('#changeInstall_btn').linkbutton({disabled:true});
					msgShow(i18n['label_installChangeDataSuccess'],'show');
				}else if(res=='failure'){
					msgAlert(i18n['install_Failure'],'error');
				}else if(res=='configed'){
					msgAlert(i18n['install_Repeat'],'warning');
				}
			});
		},
		/**
		 * 加载发布基础数据
		 */
		installReleaseData:function(){
			startProcess();
			$.post("configData!insatllReleaseData.action",{"installType":"release","languageVersion":lan},function(res){
				endProcess();
				if(res=='success'){
					$('#releaseInstall_btn').linkbutton({disabled:true});
					msgShow(i18n['label_installReleaseDataSuccess'],'show');
				}else if(res=='failure'){
					msgAlert(i18n['install_Failure'],'error');
				}else if(res=='configed'){
					msgAlert(i18n['install_Repeat'],'warning');
				}
			});
		},
		/**
		 * 加载配置项基础数据
		 */
		installCIData:function(){
			startProcess();
			$.post("configData!insatllCIData.action",{"installType":"configureItem","languageVersion":lan},function(res){
				endProcess();
				if(res=='success'){
					$('#ciInstall_btn').linkbutton({disabled:true});
					msgShow(i18n['label_installCIDataSuccess'],'show');
				}else if(res=='failure'){
					msgAlert(i18n['install_Failure'],'error');
				}else if(res=='configed'){
					msgAlert(i18n['install_Repeat'],'warning');
				}
			});
		},
		/**
		 * 初始化
		 */
		init:function(){
			//绑定日期控件
			$("#ModuleMain_loading").hide();
			$("#ModuleMain_content").show();
			$("#requestInstall_btn").click(itsm.install.moduleDataInstall.installRequestData);
			$("#ciInstall_btn").click(itsm.install.moduleDataInstall.installCIData);
			$("#problemInstall_btn").click(itsm.install.moduleDataInstall.installProblemData);
			$("#changeInstall_btn").click(itsm.install.moduleDataInstall.installChangeData);
			$("#releaseInstall_btn").click(itsm.install.moduleDataInstall.installReleaseData);
			
			setTimeout(function(){
				getUploaderHar('#moduleInstall_file','#moduleInstall_success_attachment','moduleInstallQueId');
			},0)
		}
	}
}();
$(document).ready(itsm.install.moduleDataInstall.init);