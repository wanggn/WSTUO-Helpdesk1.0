$package('itsm.portal');

//$import('itsm.portal.schedule');
//$import('common.config.includes.includes');

itsm.portal.costCommon = function(){
	this.calendarTaskDiv = $('#calendarTaskDiv');
	this._startTime = "";
	this._endTime = "";
	this._taskId = "";
	this.dtoState = "";
	this._cycle = "";
	this._fullView = "";
	this._isWorkManage = "";//否是排班
	var state ="0";
	return {
		costInfoShowByTask:function(eventObject){
			var taskId = eventObject.taskId;
			if (taskId) {
				var startTime = dateFormat(eventObject.start);
				var endTime = dateFormat(eventObject.end);
				$.post("tasks!taskCostByTaskId.action",
						'taskDto.taskId='+taskId+"&taskDto.startTime="+startTime+"&taskDto.endTime="+endTime+"&taskDto.taskCycle="+eventObject.taskCycle,
						function(res){
					if (res.costDTO && res.costDTO.progressId) {
						
					}
				});
			}
		},
		scheduleShow1Init:function(cycle){
			_cycle = cycle;
		},
		scheduleShow2Init:function(fullView){
			_fullView = fullView;
		},
		scheduleShow3Init:function(workManage){
			_isWorkManage = workManage;
		},
		scheduleShow1:function(taskId,start,end){
			_startTime = start;_endTime = end;_taskId = taskId;
			itsm.portal.schedule.historyShow( taskId );
			
			$("#addcalendarTreatmentResultsTr").hide();
			$("#calendarTreatmentResults_tr").hide();
			$("#addcalendarTreatmentResults").val("");
			startLoading();
			$.post("tasks!taskCostByTaskId1.action",
					'taskDto.taskId='+_taskId+"&taskDto.startTime="+_startTime+"&taskDto.endTime="+_endTime
					,function(res){
				$('#calendarTaskId').val(res.taskId);
				$('#calendarTaskCreator').text(res.creator);
				$('#calendarTaskOwner').text(res.owner);
				if(res.dno=='task_personal'){
					$("#taskType").text(i18n.label_task_personal);
				}else if(res.dno=='task_workforce'){
					$("#taskType").text(i18n.label_task_workforce);
				}else{//事件任务
					$("#taskType").text(i18n.lable_taskType);
				}
				if (res.taskCycle === "WEEK") {
					$("#taskType").text( $("#taskType").text() + " ( "+i18n.lable_task_cycle_type+" )");
				}
				
				if(res.title!=null && res.title!="null"){
					$('#calendarTaskTitle').text(res.title.substring(res.title.indexOf(']')+1,res.title.length));
				}else{
					$("#calendarTaskTitle").val("null");
				}
				$('#calendarTaskLocation').text(res.location);
				$('#calendarTaskIntroduction').text(res.introduction);
				//if(dtoState=="dtoState"){
					$('#calendarTaskStartTime').text(res.startTime);
					if(res.endTime===null){
						$('#calendarTaskEndTime').text(res.startTime);
					}else{
						$('#calendarTaskEndTime').text(res.endTime);
					}
				/*}else{
					$('#calendarTaskStartTime').text($.fullCalendar.formatDate(res.start));
					if(res.end===null){
						$('#calendarTaskEndTime').text($.fullCalendar.formatDate(res.start));
					}else{
						$('#calendarTaskEndTime').text($.fullCalendar.formatDate(res.end));
					}
				}*/
				if (res.allDay == 'true' || res.allDay) {
					$('#calendarTaskAllDay').text(i18n.label_basicConfig_deafultCurrencyYes);
				} else {
					$('#calendarTaskAllDay').text(i18n.label_basicConfig_deafultCurrencyNo);
				}

				$('#calendarResults').text(res.treatmentResults);
				$('#calendarRealStartTime').text(res.realStartTime);
				$('#calendarRealEndTime').text(res.realEndTime);
				$('#calendarRealFree').text(Math.floor(res.realFree*1/1440)+i18n.label_slaRule_days+" "+Math.floor(res.realFree*1%1440/60)+i18n.label_slaRule_hours+" "+Math.floor(res.realFree*1%1440%60)+i18n.label_slaRule_minutes);
				$('#schedule_task_delete_but').unbind().click(function(){itsm.portal.schedule.remoreCalendarTask(res.taskId);});
				if (_fullView === 'false') {
					_fullView = '';
				}else{
					$('#schedule_task_edit_but').unbind().click(function(){itsm.portal.schedule.editCalendarTask(res.taskId);});
				}
				$('#schedule_task_process_but').unbind().click(function(){itsm.portal.costCommon.scheduleTaskProcess();});
				$('#schedule_task_remark_but').unbind().click(function(){itsm.portal.costCommon.remarkCalendarTask_opt(res.taskId);});
				$('#schedule_task_closed_but').unbind().click(function(){itsm.portal.costCommon.closedCalendarTask_opt(res.taskId);});
				
				if (res.costDTO && res.costDTO.progressId) {
					var data = res.costDTO;
					$('#calendarRealStartTime').text(timeFormatter(data.startTime,'',''));
					$('#calendarRealEndTime').text(timeFormatter(data.endTime,'',''));
					$('#taskStartToEedTime').text(itsm.portal.costCommon.minute2HM2(data.startToEedTime,'',''));
					$('#calendarRealFree').text(itsm.portal.costCommon.minute2HM2(data.actualTime,'',''));
					$('#calendarResults').text(data.description);
					
					if (res.costDTO.status === 2) {
						$("#schedule_task_closed_but").hide();
						$("#schedule_task_edit_but").hide();
						$('#calendarResults_tr').show();
					}else{
						$("#schedule_task_closed_but").show();
					}
					$("#schedule_task_process_but").hide();
				}else{
					$("#schedule_task_process_but").show();
					$("#schedule_task_closed_but").hide();
					$("#task_cost_show_action").hide();
				}
				if (res.taskStatus === 1) {
					$('#calendarTaskStatus').text(i18n.task_label_pending);
					$("#schedule_task_delete_but").show();
					$('#schedule_task_closed_but').show();
					$("#schedule_task_edit_but").show();
					$('#schedule_task_process_but').hide();
				}else if(res.taskStatus === 2){
					//$("#schedule_task_delete_but").hide();
					$("#schedule_task_edit_but").hide();
					$('#schedule_task_closed_but').hide();
					$('#schedule_task_process_but').hide();
					$('#calendarResults_tr').show();
					
					$('#calendarTaskStatus').text(i18n.lable_complete);
				}else{
					$('#calendarTaskStatus').text(i18n.title_newCreate);
					$("#schedule_task_delete_but").show();
					$('#schedule_task_closed_but').hide();
					$("#schedule_task_edit_but").show();
					$('#schedule_task_process_but').show();
				}
				var taskUserName=res.taskCreator;
				var ownerLoginName = res.ownerLoginName;
				if(ownerLoginName!=userName){
					$('#schedule_task_process_but').hide();
					$('#schedule_task_closed_but').hide();
				}
				if(taskUserName==userName || operationTaskRes){
					$('#task_info_win').show();
				}else{
					$('#task_info_win').show();
					$('#schedule_task_delete_but').hide();
					$('#schedule_task_owner_link').hide();
					$('#schedule_task_edit_but').hide();
				}
				if (res.dataFlag === 99) {
					$("#schedule_task_edit_but").hide();
					$("#schedule_task_delete_but").hide();
				}
				if (_cycle === 'false') {
					$('#task_info_win').hide();
					_cycle = '';
				}
				if( _isWorkManage === 'true' || _isWorkManage === true){
					$('#schedule_task_process_but,#schedule_task_closed_but,#schedule_task_delete_but,#schedule_task_edit_but,#schedule_task_remark_but').hide();
					_isWorkManage = '';
				}
				$("#taskGrid").trigger('reloadGrid');
				$("#requestEventTaskGrid").trigger('reloadGrid');
				$("#problemEventTaskGrid").trigger('reloadGrid');
				$("#changeEventTaskGrid").trigger('reloadGrid');
				endLoading();
				$('.WSTUO-dialog').dialog('close');
				$('#calendarTaskDiv').dialog({title:i18n.title_request_task+':'+res.title,width:'600px',modal: true});
			});
		},
		scheduleTaskProcess:function(){
			//_startTime = start;_endTime = end;_taskId = taskId;
			var _url="tasks!editTaskDetail.action";
			$.post(_url,'taskDto.taskId='+_taskId+"&taskDto.taskStatus="+1+"&taskDto.operator="+userName
					+"&taskDto.startTime="+_startTime+"&taskDto.endTime="+_endTime
					,function(){
				calendarTaskDiv.dialog('close');
				$("#taskGrid").trigger('reloadGrid');
				msgShow(i18n.lable_startProcess,"show");
				if(state=="0"){
					itsm.portal.schedule.addEventSource('taskQueryDto.owner='+userName);
				}else if(state=="1"){
					itsm.portal.schedule.addEventSource('taskQueryDto.owner=');
				}else if(state=="2"){
					itsm.portal.schedule.addEventSource(showOther);
				}
				itsm.portal.costCommon.scheduleShow1( _taskId , _startTime, _endTime);
			});
		},
		closedCalendarTask_opt:function(id){
			$.post("tasks!taskRealTimeDefaultValue.action",
					'taskDto.taskId='+id+"&taskDto.startTime="+_startTime+"&taskDto.endTime="+_endTime
					,function(taskDto){
				calendarTaskDiv.dialog('close');
				resetForm("#realProcessTaskfm");
				$('#schedule_process_taskId').val(id);
				$('#schedule_process_realStartTime').val(taskDto.realStartTime);
				$('#schedule_process_realEndTime').val(taskDto.realEndTime);
				$('#schedule_process_taskCostDay').val(Math.floor(taskDto.realFree*1/1440));
				$('#schedule_process_taskCostHour').val(Math.floor(taskDto.realFree*1%1440/60));
				$('#schedule_process_taskCostMinute').val(Math.floor(taskDto.realFree*1%1440%60));
				$("#schedule_process_realFree").val(taskDto.realFree);
				$('#schedule_link_task_closedTask').unbind().click(function(){itsm.portal.costCommon.closedCalendarTask();});
				windows('realProcessTaskDiv',{width:600});
			});
		},
		closedCalendarTask:function(){
			var _url="tasks!closedTaskDetail.action";
			if ($('#realProcessTaskfm').form('validate')) {
				var _param=$('#realProcessTaskDiv form').serialize();
				_param += "&taskDto.startTime="+_startTime+"&taskDto.endTime="+_endTime+"&taskDto.taskStatus="+2;
				
				if(($('#schedule_process_realEndTime').val()!=="" && $('#schedule_process_realStartTime').val() !== "" && $('#schedule_process_realFree').val()>=1)){
					startProcess();
					$.post(_url,_param,function(){
						calendarTaskDiv.dialog('close');
						$("#taskGrid").trigger('reloadGrid');
						msgShow(i18n.lable_process_closedTask,"show");
						if(state=="0"){
							itsm.portal.schedule.addEventSource('taskQueryDto.owner='+userName);
						}else if(state=="1"){
							itsm.portal.schedule.addEventSource('taskQueryDto.owner=');
						}else if(state=="2"){
							itsm.portal.schedule.addEventSource(showOther);
						}
						$('#realProcessTaskDiv').dialog('close');
						endProcess();
						itsm.portal.costCommon.scheduleShow1( _taskId , _startTime, _endTime);
					});
				}else{
					msgAlert(i18n.labe_taskClosedMsg,'info');
				}
			}
		},
		remarkCalendarTask_opt:function(id){
			$("#calendarTreatmentResults_tr").hide();
			
			var styleDisplay = $("#addcalendarTreatmentResultsTr").css("display");
			if (styleDisplay === "none") {
				$("#addcalendarTreatmentResultsTr").show();
				$('#schedule_link_task_remarkTask').unbind().click(function(){
					itsm.portal.costCommon.remarkCalendarTask(id);
				});
			}else{
				$("#addcalendarTreatmentResultsTr").hide();
			}
		},
		remarkCalendarTask:function(id){
		    startProcess();
			var _url="tasks!remarkTaskDetail.action";
			var treatmentResults = $("#addcalendarTreatmentResults").val();
			$.post(_url,'taskDto.taskId='+id+"&taskDto.treatmentResults="+treatmentResults+"&taskDto.operator="+userName,function(){
				$("#addcalendarTreatmentResultsTr").attr("style","display:none;");
				$("#addcalendarTreatmentResults").val("");
				if(state=="0"){
					itsm.portal.schedule.addEventSource('taskQueryDto.owner='+userName);
				}else if(state=="1"){
					itsm.portal.schedule.addEventSource('taskQueryDto.owner=');
				}else if(state=="2"){
					itsm.portal.schedule.addEventSource(showOther);
				}
				endProcess();
				itsm.portal.costCommon.scheduleShow1( _taskId , _startTime, _endTime);
			});
		},
		
		/**
		 * @description 分钟转为小时和分钟 国际化
		 */
		minute2HM2:function(cell,event,data){
			return Math.floor(cell/60)+i18n.hours+cell%60+i18n.minutes;
		},
		monthFristDay:function(){
			var date = new Date();
			date.setDate(1);
			itsm.portal.schedule.timePickerInitValue(null,'#searchTaskStartTime',null,dateFormat( date ) );
		},
		monthLastDay:function(element,_dayDate){
			var date = new Date();
			date.setMonth( date.getMonth() + 1);
			date.setDate(0);
			date.setDate( date.getDate() );
			itsm.portal.schedule.timePickerInitValue(null,'#searchTaskEndTime',null,dateFormat( date ) );
		},
		cycleTaskDetail:function(taskId){
			startProcess();
			$("#addcalendarTreatmentResultsTr").attr('style','display:none');
			$("#calendarTreatmentResults_tr").attr('style','display:none');
			$(".schedule_taskCycle_tr").show();
			$("#schedule_taskCycle_week_tr").show();
			$.post("tasks!findByTaskId.action",'taskId='+taskId,function(res){
				calendarTaskDiv.dialog('close');
				$('#schedule_link_task_save_edit_ok,#schedule_link_task_save_edit_ok2,#schedule_task_owner_link,#schedule_type,#schedule_task_owner_link').hide();

				$("#schedule_type").val(res.dCode);	
				$("#schedule_type_val").show().val( $("#schedule_type").find("option:selected").text() );
				
				if(res.taskStatus=='0'){
					$('#schedule_new').attr("checked",'true');
				}else if(res.taskStatus=='1'){
					$('#schedule_processing').attr("checked",'true');
				}else{
					$('#schedule_complete').attr("checked",'true');
				}
				if(res.taskType=='task_personal'){
					$("#schedule_type").text(i18n.label_task_personal);
				}
				if(res.taskType=='task_workforce'){
					$("#schedule_type").text(i18n.label_task_workforce);
				}	
				if(res.allDay){
					$('#schedule_taskAllDay').attr("checked",true);
				}else{
					$('#schedule_taskAllDay').attr("checked","");
				}
				$("#schedule_task_taskId").val(res.taskId);
				if( res.title!=null && res.title!="null"){
					$("#schedule_taskTitle").val(res.title.substr(res.title.indexOf(']')+1));
				}else{
					$("#schedule_taskTitle").val("");
				}
				$("#schedule_taskLocation").val(res.location);
				$("#schedule_taskIntroduction").val(res.introduction);
				$("#schedule_taskStartTime_hid").val(res.startTime);
				$('#schedule_realStartTime').val(res.realStartTime);
				$('#schedule_realEndTime').val(res.realEndTime);
				$("#schedule_realFree").val(res.realFree);
				$('#schedule_taskCostDay').val(Math.floor(res.realFree*1/1440));
				$('#schedule_taskCostHour').val(Math.floor(res.realFree*1%1440/60));
				$("#schedule_taskCostMinute").val(Math.floor(res.realFree*1%1440%60));
				$('#schedule_treatmentResults').val(res.treatmentResults);
				$("#schedule_task_creator").val(res.taskCreator);
				$("#schedule_task_owner").val(res.ownerLoginName);
				$("#schedule_task_owner_fullName").val( res.owner );
				if(res.endTime!==null){
					$("#schedule_taskEndTime_hid").val(res.endTime);}
				else{
					$("#schedule_taskEndTime_hid").val(res.startTime);}
				
				if ( res.taskCycle === '' || res.taskCycle == "NO") {//非循环任务
					res.startDate = res.startTime;
					res.endDate = res.endTime;
					$("#schedule_taskCycle").attr('checked',true);
				}else{
					$("#schedule_taskCycleWeek").attr('checked',true);
				}
				itsm.portal.schedule.cycleTaskEditInitForWeek(res);

				$('#schedule_type').removeAttr('disabled');
				if (res.dCode == '' || res.dCode == null || res.dCode < 1) {//事件任务
					$(".schedule_taskCycle_tr").hide();
					$("#schedule_taskCycle_week_tr").hide();
					$(".schedule_taskTime").show();
					itsm.portal.schedule.timePickerInitValue("#schedule_taskEndTime_hid",
							"#schedule_taskEndTime","#schedule_taskEndTime_time",res.endTime);
					itsm.portal.schedule.timePickerInitValue("#schedule_taskStartTime_hid",
							"#schedule_taskStartTime","#schedule_taskStartTime_time",res.startTime);
					$("#schedule_type").attr("disabled",true);
				}

				endProcess();
				$('#scheduleTaskForm').form('validate');
				$('#scheduleAddEditTaskDiv input,#scheduleAddEditTaskDiv textarea').attr('disabled','disabled');
				windows('scheduleAddEditTaskDiv',{title:i18n.title_request_task+':'+res.title,width:'600px',modal: true});
			});
		},
		
		init:function(){
		}
	}
}();
//$(document).ready(itsm.portal.costCommon.init);