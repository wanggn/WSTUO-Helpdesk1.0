function Map() {  
    /** 存放键的数组(遍历用到) */  
    this.keys = new Array();  
    /** 存放数据 */  
    this.data = new Object();  
      
    /**
	 * 放入一个键值对
	 * 
	 * @param {String}
	 *            key
	 * @param {Object}
	 *            value
	 */  
    this.put = function(key, value) {  
        if(this.data[key] == null){  
            this.keys.push(key);  
        }  
        this.data[key] = value;  
    };  
      
    /**
	 * 获取某键对应的值
	 * 
	 * @param {String}
	 *            key
	 * @return {Object} value
	 */  
    this.get = function(key) {  
        return this.data[key];  
    };  
      
    /**
	 * 删除一个键值对
	 * 
	 * @param {String}
	 *            key
	 */  
    this.remove = function(key) {  
        this.keys.remove(key);  
        this.data[key] = null;  
    };  
      
    /**
	 * 遍历Map,执行处理函数
	 * 
	 * @param {Function}
	 *            回调函数 function(key,value,index){..}
	 */  
    this.each = function(fn){  
        if(typeof fn != 'function'){  
            return;  
        }  
        var len = this.keys.length;  
        for(var i=0;i<len;i++){  
            var k = this.keys[i];  
            fn(k,this.data[k],i);  
        }  
    };  
      
    /**
	 * 获取键值数组(类似Java的entrySet())
	 * 
	 * @return 键值对象{key,value}的数组
	 */  
    this.entrys = function() {  
        var len = this.keys.length;  
        var entrys = new Array(len);  
        for (var i = 0; i < len; i++) {  
            entrys[i] = {  
                key : this.keys[i],  
                value : this.data[i]  
            };  
        }  
        return entrys;  
    };  
      
    /**
	 * 判断Map是否为空
	 */  
    this.isEmpty = function() {  
        return this.keys.length == 0;  
    };  
      
    /**
	 * 获取键值对数量
	 */  
    this.size = function(){  
        return this.keys.length;
    };  
      
    /**
	 * 重写toString
	 */  
    this.toString = function(){  
        var s = "{";  
        for(var i=0;i<this.keys.length;i++,s+=','){  
            var k = this.keys[i];  
            s += k+"="+this.data[k];  
        }  
        s+="}";  
        return s;  
    };  
}
var map=new Map();
map.put(i18n.title_request_requestGrid,"requestGrid");
map.put(i18n.problem_grid,"problemGrid");
map.put(i18n.title_changeList,"changeGrid");
map.put(i18n.ci_configureItemAdmin,"configureGrid,configureItemMain_layout");
map.put(i18n.release_grid,"releaseGrid");
map.put(i18n.default_page_sla,"contractGrid");
map.put(i18n.title_request_knowledgeGrid,"knowledgeGrid,knowledgeMain_layout");
map.put(i18n.affiche,"afficheGrid");
map.put(i18n.label_message_manageTitle,"IMjqGrid");
map.put(i18n.default_page_emailMgt,"emailGrid");
map.put(i18n.title_personTask,"taskGrid");
map.put(i18n.request_detail,"requestDetails_layout");
map.put(i18n.title_add_scheuledTask,"addScheduledTask_layout");
map.put(i18n.title_edit_scheuledTask, "editScheduledTask_layout");
map.put(i18n.probelm_detail,"problemDetails_layout");
map.put(i18n.change_detail,"changeDetails_layout");
map.put(i18n.ci_configureItemInfo,"ciDetails_layout");
map.put(i18n.release_details,"releaseDetails_layout");
map.put(i18n.label_request_requestType,"requestCategoryDiv_layout");
map.put(i18n.default_title_problemCategory,"problemCategoryDiv_layout");
map.put(i18n.label_request_changeCategory,"changeCategoryDiv_layout");
map.put(i18n.label_release_releaseCategory,"releaseCategoryDiv_layout");
map.put(i18n.label_knowledge_knowledgeCategory,"knowledgeCategoryDiv_layout");
map.put(i18n.label_software_softwareCategory,"softwareCategoryDiv_layout");
map.put(i18n.title_service_directory,"serviceCategoryDiv_layout");
map.put(i18n.ci_configureItemCategory,"CategoryDiv_layout");
map.put(i18n.default_security_orgSet,"organization_layout");
map.put(i18n.title_flow_property_setting,"flowPropertyMainContent_layout");
map.put(i18n.companyInformation,"company_div_layout");
map.put(i18n.default_security_user,"userGrid");
map.put(i18n.default_security_resourceManager,"resContent_layout");
map.put(i18n.title_request_addRequest, "addRequest_layout");
map.put(i18n.title_request_editRequest, "editRequest_layout");
map.put(i18n.request_add_templet_title, "addRequest_layout");
map.put(i18n.request_edit_templet_title, "editRequest_layout");
map.put(i18n.problem_edit, "editProblem_layout");
map.put(i18n.problem_add, "addProblem_layout");
map.put(i18n.titie_change_edit, "changeEdit_layout");
map.put(i18n.titie_change_add, "changeAdd_layout");
map.put(i18n.ci_addConfigureItem, "configureItemAdd_layout");
map.put(i18n.ci_editConfigureItem, "configureItemEdit_layout");
map.put(i18n.release_add, "addRelease_layout");
map.put(i18n.release_edit, "editRelease_layout");
map.put(i18n.title_request_newKnowledge, "addKnowledge_layout");
map.put(i18n.knowledge_editKnowledge, "editKnowledge_layout");
map.put(i18n.title_sla_slaDetail,"SLAServiceManage_content_layout");
map.put(i18n.default_title_sms,"sendMsgDiv_layout");
map.put(i18n.default_title_groupManage,"technicalGroupGrid");
map.put(i18n.default_title_customerManager,"customerGrid");
map.put(i18n.default_title_supplierManager,"supplierGrid");
map.put(i18n.default_title_customerManager,"ITSOPUserGrid,ITSOPCustomer_userGrid");
map.put(i18n.default_title_role,"roleGrid");
map.put(i18n.default_title_LDAPManager,"ldapGrid");
map.put(i18n.default_title_areaUser,"adGrid");
map.put(i18n.caption_ruleGrid, "rulesGrid");
map.put(i18n.default_security_mailToRequestRule, "rulesGrid");
map.put(i18n.default_security_changeRule, "rulesGrid");
map.put(i18n.default_security_processManage, "processDefinedGrid");
map.put(i18n.flowExample, "historyProcessGrid");
map.put(i18n.default_security_database, "backupGrid");
map.put(i18n.title_scheduled_task_manage, "scheduledTasksGrid");
map.put(i18n.default_security_license, "licenseTable");
map.put(i18n.default_security_emailTemplate, "noticeGrid");
map.put(i18n.default_security_template, "templateGrid");
map.put(i18n.extendedAttributeGroup, "attrGroupTable");
map.put(i18n.extendedAttribute, "eavCiList,eavAttrsList");
map.put(i18n.returnItem, "visitList,visitItemList");
map.put(i18n.escalateLevel, "updateLevelGrid");
map.put(i18n.default_cab, "cabGrid,cabMemberGrid");
map.put(i18n.default_report_single, "singleGroupGrid");
map.put(i18n.label_dynamicReports_kpiGrid, "KPIGrid");
map.put(i18n.label_dynamicReports_crosstabReport, "crosstabGrid");
map.put(i18n.title_dynamicReports_mutilGroupReport, "multiGroupGrid");
map.put(i18n.custom_report_uploadedReport, "customReportGrid");
map.put(i18n.title_rule_addRule, "addRuleDiv_layout");
map.put(i18n.title_rule_editRule, "editRuleDiv_layout");
map.put(i18n.notice_noticeRuleManage, "noticeGrid");
map.put(i18n.label_scan_software, "softwareMainGrid");
map.put(i18n.label_returnVisit_manage, "returnVisitGrid");
map.put(i18n.label_sms_historyRecord, "smsRecordGrid");
map.put(i18n.DataDictionaryItems, "dataDictionaryGroupsGrid");
map.put(i18n.UserOnlineLog, "userOnlineGrid");
map.put(i18n.userOptLog, "userOptGrid");
map.put(i18n.exportUserErrLog, "errorLogGrid");
map.put(i18n.label_dc_ciRelationType, "dictionaryItemGrid_ciRelationType");
map.put(i18n.label_dc_releaseStatus, "dictionaryItemGrid_releaseStatus");
map.put(i18n.label_dc_changeStatus, "dictionaryItemGrid_changeStatus");
map.put(i18n.label_dc_problemStatus, "dictionaryItemGrid_problemStatus");
map.put(i18n.label_dc_requestStatus, "dictionaryItemGrid_requestStatus");
map.put(i18n.label_dc_offmode, "dictionaryItemGrid_offmode");
map.put(i18n.label_dc_systemPlatform, "dictionaryItemGrid_systemPlatform");
map.put(i18n.label_dc_SLAStatus, "dictionaryItemGrid_SLAStatus");
map.put(i18n.label_dc_softwareType, "dictionaryItemGrid_softwareType");
map.put(i18n.label_dc_itsopType, "dictionaryItemGrid_itsopType");
map.put(i18n.label_dc_workloadStatus, "dictionaryItemGrid_workloadStatus");
map.put(i18n.label_dc_brand, "dictionaryItemGrid_brand");
map.put(i18n.label_dc_location, "dictionaryItemGrid_location");
map.put(i18n.label_dc_useStatus, "dictionaryItemGrid_useStatus");
map.put(i18n.label_dc_supplier, "dictionaryItemGrid_supplier");
map.put(i18n.label_dc_effectRange, "dictionaryItemGrid_effectRange");
map.put(i18n.label_dc_seriousness, "dictionaryItemGrid_seriousness");
map.put(i18n.label_dc_level, "dictionaryItemGrid_level");
map.put(i18n.label_dc_priority, "dictionaryItemGrid_priority");
map.put(i18n.label_dc_imode, "dictionaryItemGrid_imode");
map.put(i18n.exportDown,"exportInfoGrid");
map.put(i18n.label_rule_addRequestRule, "addRuleDiv_layout");
map.put(i18n.label_rule_addEmailToRequestRule, "addRuleDiv_layout");
map.put(i18n.label_rule_addChangeRule, "addRuleDiv_layout");
map.put(i18n.label_rule_editRequestRule, "editRuleDiv_layout");
map.put(i18n.label_rule_editEmailToRequestRule, "editRuleDiv_layout");
map.put(i18n.label_rule_editChangeRule, "editRuleDiv_layout");
map.put(i18n.label_tools_workloadStatusStatistics, "workStatisticsGrid");
map.put(i18n.RulePackage, "packagesGrid");
map.put(i18n.notitceRule, "noticeRule_layout");
map.put(i18n.request_process_rule, "rulesGrid");
map.put(i18n.lable_created_adddrl_processing, "addRuleDiv_layout");
map.put(i18n.lable_created_editdrl_processing, "editRuleDiv_layout");
map.put(i18n.formCustom,"formCustom_layout");
map.put(i18n.default_security_user,"userGrid,userMain_layout");
map.put(i18n.editNoticeRule,"editNotice_layout");
map.put(i18n.addNotice,"addNotice_layout");
map.put(i18n.lable_Process_rules_package_management,"packagesGrid");
function resizeCommon(){
	$("#itsmMainTab,#regCenter").height("100%");
	windowResize.resizeItsmMainTab();   // 改变tabs的尺寸
	
	$('#itsmMainTab .tabs:first .tabs-title:visible').each(function(i,n){
        var t = $(n).text();
        if(map.get(t)!=null && map.get(t)!=''){
    		var layOutAndGridName =map.get(t).split(',');
			$.each(layOutAndGridName, function(v, gridId){
				if(gridId.indexOf('layout')>=0){
					redraw(gridId);
				}else{
					if(gridId=='configureGrid'){
						if($('#configureItemDiv').parent().is(":hidden")){
							setGridWidth('#'+gridId,"regCenter",40);
						}else{
							setGridWidth('#'+gridId,"regCenter",223);
						}
					}else if(gridId=='knowledgeGrid'){
						if($('#knowledgeCategoryDiv').parent().is(":hidden")){
							setGridWidth('#'+gridId,"regCenter",40);
						}else{
							setGridWidth('#'+gridId,"regCenter",223);
						}
					}else{
						if(gridId!=null && gridId!=undefined ){
							setGridWidth('#'+gridId,"regCenter",20);
						}else{
							setGridWidth('#'+gridId,"regCenter",20);
						}
					}
				}
			});
    	}
    });
}
function redraw(id){
	$('#'+id).layout('resize');
}

		