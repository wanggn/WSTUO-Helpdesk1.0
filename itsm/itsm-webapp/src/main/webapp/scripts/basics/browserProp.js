$package('browserProp');
/**
 * 浏览器的一些属性
 */
browserProp.browserProp = {
    'getWinWidth' : function() {   // 浏览器窗口宽度
        var winWidth;
        if (window.innerWidth)
            winWidth = window.innerWidth;
        else if ((document.body) && (document.body.clientWidth))
            winWidth = document.body.clientWidth;
        if (document.documentElement && document.documentElement.clientWidth)
            winWidth =  document.documentElement.clientWidth;
        return winWidth;
    },
    'getWinHeight' : function() {  // 浏览器窗口高度
        var winHeight;
        if (window.innerHeight)
            winHeight = window.innerHeight;
        else if ((document.body) && (document.body.clientHeight))
            winHeight = document.body.clientHeight;
        if (document.documentElement  && document.documentElement.clientHeight)
            winHeight = document.documentElement.clientHeight;
        return winHeight;
    },
    'getScrollTop' : function() {    // 滚动条距离顶部
        var scrollTop;
        if (window.pageYOffset)
            scrollTop = window.pageYOffset;
        else if (document.compatMode && document.compatMode != 'BackCompat')
            scrollTop = document.documentElement.scrollTop;
        else if (document.body)
            scrollTop = document.body.scrollTop;
        return scrollTop;
    },
    'getScrollLeft' : function() {   // 滚动条距离左边
        var scrollLeft;
        if (window.pageXOffset)
            scrollLeft = window.pageXOffset;
        else if (document.compatMode && document.compatMode != 'BackCompat')
            scrollLeft = document.documentElement.scrollLeft;
        else if (document.body)
            scrollLeft = document.body.scrollLeft;
        return scrollLeft;
    }
};