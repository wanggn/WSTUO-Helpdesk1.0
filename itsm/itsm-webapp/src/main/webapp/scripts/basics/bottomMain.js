$package('basics');
$import('basics.showChart');
$import('itsm.request.requestStats');
$import('itsm.problem.problemStats');
$import('itsm.change.changeStats');
$import('itsm.release.releaseStats');
$import('itsm.cim.leftMenu');
$import('common.knowledge.leftMenu');
 /**  
 * @author ciel  
 * @description "列表下方饼图显示-flotChart"
 * @date 2014-11
 * @since version 1.0 
 */ 
basics.bottomMain=function(){
	var _module = "";
	var _type = "";
	return {
		/**
		 * 获得字符串实际长度，中文2，英文1
		 * @param str 要获得长度的字符串
		 */
		getRelLength:function(str){
	        var realLength = 0, len = str.length, charCode = -1;
	        for (var i = 0; i < len; i++) {
	            charCode = str.charCodeAt(i);
	            if (charCode >= 0 && charCode <= 128) realLength += 1;
	            else realLength += 2;
	        }
	        return realLength;
		},
		 /** 
	     * js截取字符串，中英文都能用 
	     * @param str：需要截取的字符串 
	     * @param len: 需要截取的长度 
	     */
		cutstr:function(str, len) {
	        var str_length = 0;
	        var str_len = 0;
	        str_cut = new String();
	        str_len = str.length;
	        for (var i = 0; i < str_len; i++) {
	            a = str.charAt(i);
	            str_length++;
	            if (escape(a).length > 4) {
	                //中文字符的长度经编码之后大于4  
	                str_length++;
	            }
	            str_cut = str_cut.concat(a);
		        if (str_length > len+1) {
	                str_cut = str_cut.concat("...");
	                return str_cut;
	            }
	        }

	        
	        return str;
	        
	    },
		/**
		 * 显示饼图
		 * @param div 饼图显示层
		 * @param data 饼图数据
		 * @param module 模块
		 * @param type 查询类型
		 */
		showPieChart:function(div,data,module,type){
			var placeholder = $("#"+div);
			_module = module;
			_type = type;
 			if(placeholder.width()!=null){ //层已被加载
				if(!data || !data.length){
					placeholder.html("");
					placeholder.append("<center><span style='color:red;width:"+placeholder.width()+"px;height:100px;line-height:100px;'>"+i18n.noData+"</span></center>");
				}else{
					placeholder.unbind();
					$.plot(placeholder, data, {
						series: {
							pie: { 
								show: true,
								radius: 0.63,
								offset: {
									top:-40,
									left:0
								},
								label:{
									show: true,
				                    radius: 2/3,
				                    formatter: function(label, series){
				                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:red;">'+series.data[0][1]+'('+Math.round(series.percent)+'%)</div>';
				                    },
				                    threshold: 0.1
								}
							}
						},
						legend: {
							show: true,
							labelFormatter:basics.bottomMain.labelFormatter,
							position:"sw",
							noColumns:2
							
						},
						grid: {
							hoverable: true,
							clickable: true
						}
					});
					placeholder.bind("plothover", function(event, pos, obj) {
						if (!obj) {
							return;
						}
						var percent = parseFloat(obj.series.percent).toFixed(2);
						//$("#hover").html("<span title='"+obj.series.label+"' style='font-weight:bold; color: #000;'>" + obj.series.label + " (" + percent + "%)</span>");
					});
					placeholder.bind("plotclick", function(event, pos, obj) {
						if (!obj) {
							return;
						}
						percent = parseFloat(obj.series.percent).toFixed(2);
						if(module=="request"){
							itsm.request.requestStats.requestCountSearchByPieChart(_type,obj.series.id,obj.series.groupField);
						}else if(module=="problem"){
							itsm.problem.problemStats.problemCountSearchByPieChart(_type,obj.series.id,obj.series.groupField);
						}else if(module=="change"){
							itsm.change.changeStats.changeCountSearchByPieChart(_type,obj.series.id,obj.series.groupField);
						}else if(module=="release"){
							itsm.release.releaseStats.releaseCountSearchByPieChart(_type,obj.series.id,obj.series.groupField);
						}else if(module=="configureItem"){
							itsm.cim.leftMenu.ciCountSearchByPieChart(obj.series.id,obj.series.groupField);
						}
					});
				}
			}
			
		},
		/**
		 * 文本格式化
		 * @param lable 饼图下方的文字
		 * @param series 饼图参数集合
		 */
		labelFormatter:function(label, series) {
			var relLabel = label;
			var _len=7;
			var dataLen = basics.bottomMain.getRelLength("["+series.data[0][1]+ "]");
			_len = _len - dataLen;
			if(_len%2 > 0){
				_len = _len -1;
			}
			if(basics.bottomMain.getRelLength(label)>_len){
				label = basics.bottomMain.cutstr(label,_len);
			}
			if(_module=="request"){
				return "<a title='"+relLabel+"' href=\"javascript:itsm.request.requestStats.requestCountSearchByPieChart('"+_type+"',"+series.id+",'"+series.groupField+"','"+relLabel+"');\">"+label+"["+series.data[0][1]+ "]</a>";
			}else if(_module=="problem"){
				return "<a title='"+relLabel+"' href=\"javascript:itsm.problem.problemStats.problemCountSearchByPieChart('"+_type+"',"+series.id+",'"+series.groupField+"','"+relLabel+"');\">"+label+"["+series.data[0][1]+ "]</a>";
			}else if(_module=="change"){
				return "<a title='"+relLabel+"' href=\"javascript:itsm.change.changeStats.changeCountSearchByPieChart('"+_type+"',"+series.id+",'"+series.groupField+"','"+relLabel+"');\">"+label+"["+series.data[0][1]+ "]</a>";
			}else if(_module=="release"){
				return "<a title='"+relLabel+"' href=\"javascript:itsm.release.releaseStats.releaseCountSearchByPieChart('"+_type+"',"+series.id+",'"+series.groupField+"','"+relLabel+"');\">"+label+"["+series.data[0][1]+ "]</a>";
			}else if(_module=="configureItem"){
				return "<a title='"+relLabel+"' href=\"itsm.cim.leftMenu.ciCountSearchByPieChart("+series.id+",'"+series.groupField+"');\">"+label+"["+series.data[0][1]+ "]</a>";
			}
		},
		/**
		 * 列表自适应
		 * @param module 模块
		 */
		resizeWithChart:function(module){
			$("#"+module+"MainBottom").show();
			var width = browserProp.browserProp.getWinWidth() - ($('#index_body').layout('panel', 'west').panel('options').width + 15)-5;
			if(browserProp.browserProp.getWinWidth()<1024){
				width = 1024 - ($('#index_body').layout('panel', 'west').panel('options').width + 30);
			}
			$("#"+module+"MainBottom").width(width);
			if($("#"+module+"CompanyDiv").is(":hidden")){
				$("#"+module+"StatsDiv").width(width-2);
			}else
				$("#"+module+"StatsDiv").width(width-220-2);
			
	       	var statsDivWidth = $("#"+module+"StatsDiv").width();
	       	$("#"+module+"StatsRightDiv").width(statsDivWidth-190-10);
	       	var pieDivWidth = (statsDivWidth-190-20)/3 - 1;
	       	$("#"+module+"PieDiv1").width(pieDivWidth);
	       	$("#"+module+"PieDiv2").width(pieDivWidth);
	       	$("#"+module+"PieDiv3").width(pieDivWidth);
	       	$("#"+module+"MainBottom .div_ellipsis").width(pieDivWidth*0.5);
	       	
		},
		/**
		 * 配置项列表自适应
		 */
		resizeWithCimChart:function(){
			$('#configureItemMainBottom').show();
			var width =$('#gview_configureGrid').width();
			if(width<450){
				width = 420;
			}
			$('#configureItemMainBottom').width(width);
			if($("#configureItemCompanyDiv").is(":hidden")){
				$("#configureItemStatsDiv").width(width-2);
			}else
				$('#configureItemStatsDiv').width(width-$("#configureItemCompanyDiv").width()-2);
			var cimWidth = $('#configureItemStatsDiv').width()/2;
			$('#configureItemPieDiv1').width(cimWidth-10);
			$('#configureItemPieDiv2').width(cimWidth-10);
		},
		/**
		 * 知识库列表自适应
		 */
		resizeWithKnowledgeChart:function(){
			$('#knowledgeMainBottom .mainBottom').show();
			var width = $('#gview_knowledgeGrid').width();
			if(width<450){
				width = 420;
			}
			$('#knowledgeMainBottom').width(width);
			var bottomWidth = width-16;
			$('#knowledgeMainLeftBottom').width(bottomWidth/3);
			$('#knowledgeMainMidBottom').width(bottomWidth/3);
			$('#knowledgeMainRightBottom').width(bottomWidth/3); 
			
			common.knowledge.leftMenu.showNewKnowledges();
			common.knowledge.leftMenu.showHotKnowledges();
		},
		/**
		 * 根据选择卡是否选中进行饼图自适应
		 * @param value 选项卡名称
		 */
		resizeTabByTitle:function(value){
			var tabModule = "";
			if(value==i18n.title_request_requestGrid){
				tabModule = "request";
			}else if(value==i18n.title_changeList){
				tabModule = "change";
			}else if(value==i18n.problem_grid){
				tabModule = "problem";
			}else if(value==i18n.release_grid){
				tabModule = "release";
			}else if(value==i18n.ci_configureItemAdmin){
				tabModule = "configureItem";
			}else if(value==i18n.title_request_knowledgeGrid){
				tabModule = "knowledge";
			}
			if(tabModule!=""){
				if(tabModule=="configureItem"){
					basics.bottomMain.resizeWithCimChart();
				}else if(tabModule == "knowledge"){
					basics.bottomMain.resizeWithKnowledgeChart();
				}else {
					basics.bottomMain.resizeWithChart(tabModule);
				}
			}
		},
		/**
		 * 显示图表
		 * @param div 饼图显示层
		 * @param data 饼图数据
		 * @param module 模块
		 * @param type 查询类型
		 */
		showTableChart:function(div,data,module,type){
			
			var str ="";
			if(module=="request"){
				str = "<a  href=\"javascript:itsm.request.requestStats.requestCountSearchByPieChart('{0}',{1},'{2}','{4}');\">{3}</a>";
			}else if(module=="problem"){
				str = "<a  href=\"javascript:itsm.problem.problemStats.problemCountSearchByPieChart('{0}',{1},'{2}','{4}');\">{3}</a>";
			}else if(module=="change"){
				str = "<a  href=\"javascript:itsm.change.changeStats.changeCountSearchByPieChart('{0}',{1},'{2}','{4}');\">{3}</a>";
			}else if(module=="release"){
				str = "<a  href=\"javascript:itsm.release.releaseStats.releaseCountSearchByPieChart('{0}',{1},'{2}','{4}');\">{3}</a>";
			}else if(module=="configureItem"){
				basics.bottomMain.resizeWithCimChart();
				str = "<a  href=\"javascript:itsm.cim.leftMenu.ciCountSearchByPieChart({1},'{2}','{4}');\">{3}</a>";
				if($('#'+div).width()>300){
					$('#'+div).parent().width(300);
				}
			}
			$('#'+div).html("");
			if(!data || !data.length){
				$('#'+div).append("<center><span style='color:red;width:"+$('#'+div).width()+"px;height:100px;line-height:100px;'>"+i18n.noData+"</span></center>");
			}else{
				$("<tr>" +
					"<td align='left' ></td>" +
					"<td width='25px' align='left'></td>" +
					"<td width='110px' align='left'></td>" +
				"</tr>").appendTo("#"+div);
				for ( var i = 0; i < data.length; i++) {
					var ch = data[i];
					
					var pic = "";
					var percent = Math.floor((ch.data/ch.total)*100);
					//var width = Math.floor((ch.data/ch.total)*120)+1;
					var chData = ch.data;
					var label = ch.label;
					var picDiv = "<div style =' border-top: 13px solid #3c78b5;display: block;height: 0;overflow: hidden;'></div>";
					var divWidth = $('#'+div).width()*0.5;
					/*				var _len =Math.floor(3+(divWidth*0.5)/20);
					//console.log(divWidth +"=="+ _len);
					var dataLen = basics.bottomMain.getRelLength(label);
					if(dataLen>_len){
						label = basics.bottomMain.cutstr(label,_len);
					}*/
					if(ch.label == "other" ){
						pic = picDiv;
					}else {
						var formtLabel = label.replace('unknown',i18n['label_unknown']);
						label = str.replace('{0}',type).replace('{1}',ch.id).replace('{2}',ch.groupField).replace('{3}',formtLabel).replace('{4}',ch.label);
						pic = str.replace('{0}',type).replace('{1}',ch.id).replace('{2}',ch.groupField).replace('{3}',picDiv).replace('{4}',ch.label);
						chData = str.replace('{0}',type).replace('{1}',ch.id).replace('{2}',ch.groupField).replace('{3}',chData).replace('{4}',ch.label);
					}
					$("<tr width='100%' height='20px'>"+
						"<td title='"+ch.label+"' align='left'><div class='div_ellipsis' style='width:"+divWidth+"px'>"+label+"</div></td>"+
						"<td width='25px' title='"+ch.label+"' align='left'>"+chData+"</td>"+
						"<td width='45%' align='left'>"+
							"<table cellspacing='0' cellpadding='0' border='0' width='auto'>"+
							"<tbody><tr>"+
							"<td width='"+(percent+1)+"%' title='"+ch.label+"' >"+pic+"</td>"+
							"<td width='"+(99-percent)+"%'><span style='padding: 0 0.333em;' title='"+ch.data+""+ch.label+"' >("+percent+"%)</span></td></tr>"+
							"</tbody></table>"+
						"</td>"+
						"</tr>").appendTo("#"+div);
				}
			}
		}
	};
}();
