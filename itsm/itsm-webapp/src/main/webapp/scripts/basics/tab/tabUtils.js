$package('basics.tab');
$import('basics.bottomMain');

/**  
 * @fileOverview 有关Tab操作包
 * @author yctan
 * @version 1.0  
 */  
 /**  
 * @author yctan  
 * @constructor 有关Tab操作包
 * @description 有关Tab操作的所有函数
 * @date 2011-04-16
 * @since version 1.0 
 */  
basics.tab.tabUtils=function(){
	
	return {
		
	    /**
	     * 根据Id和标题打开tab
	     */
	    openTabById: function(id, title) {
	        if ($('#' + id).tabs('exists', title)){
                $('#' + id).tabs('select', title);
	        }
	    },
	    
		/**
		 * @description如果当前TAB存在，则关闭TAB再打开新的TAB,与refreshTab一样，只是参数顺序不一样
		 */
		reOpenTab:function(url,title){
			if ($('#itsmMainTab').tabs('exists',title))
			{
				$('#itsmMainTab').tabs('close',title);
				$('#itsmMainTab').tabs('add',{
					title:title,
					cache:"true",
		 			href:url+'&random='+new Date().getTime(),
					closable:true
				});
			} 
			else 
			{
				$('#itsmMainTab').tabs('add',{
					title:title,
					cache:"true",
		 			href:url+'&random='+new Date().getTime(),
					closable:true
				});
			}
			basics.tab.tabUtils.tabClose();
		},
		/**
		 * @description如果当前TAB存在，则关闭TAB再打开新的TAB,与reOpenTab一样，只是参数顺序不一样(推荐使用)
		 */
		refreshTab:function(title,url){
			if ($('#itsmMainTab').tabs('exists',title)){
				
				$('#itsmMainTab').tabs('close',title);
			}
			// 如果当前tabs标签>10则跳出
			if ($('#itsmMainTab .tabs:first .tabs-title:visible').length > 25) {
			    msgAlert(i18n['title_tabs_tooLarge'],'info');
			    return;
			}
			$('#itsmMainTab').tabs('add',{
				title:title,
				cache:"false",
				href:url,
				closable:true
			});
			basics.tab.tabUtils.tabClose();
		},
		/**
		  * @description 如果tabs存在则转到对应的TAB，如果不存在则添加tabs
		  * */
		addTab:function(title, url, method){
			if ($('#itsmMainTab').tabs('exists', title)){
				$('#itsmMainTab').tabs('select', title);
			} else {
				if( typeof method === 'function' ){
					method();
				}
			    //如果当前tabs标签>50则跳出
	            if ($('#itsmMainTab .tabs:first .tabs-title:visible').length > 25) {
	                msgAlert(i18n['title_tabs_tooLarge'],'info');
	                return;
	            }
				$('#itsmMainTab').tabs('add',{
					title:title,
					cache:"true",
					href:url,
					closable:true
				});
			}
			basics.tab.tabUtils.tabClose();
		},
		/**
		 * 关闭选项卡.
		 * @param title
		 **/
		closeTab:function(title){
			if ($('#itsmMainTab').tabs('exists',title)){
				$('#itsmMainTab').tabs('close',title);
			}
		},
		/**
		 * 
		 * @param title
		 * @param method
		 **/
		selectTab:function(title,method){
			
			if ($('#itsmMainTab').tabs('exists',title)){
				
				$('#itsmMainTab').tabs('select',title);
				
				if(method!=null){
					method();
				}
			} 
		},
		/**
		 * 改变窗口大小时，刷新Tab
		 **/
		refreshWindow:function(){
			
			if(load_shuaxin_flag){
				if($('#itsmMainTab').tabs('getSelected')!=null){
					var obj=$('#itsmMainTab').tabs('getSelected').panel('options');
					$('.WSTUO-dialog').dialog('close');
					$('#itsmMainTab').tabs('getSelected').panel('refresh');
					//改变时需要改变大小
					initHeaderHeight();
				}
			}
			
		},
		/**
		 * 刷新Tab
		 **/
		refreshItsmMainTab:function(){
			if(load_shuaxin_flag){
				if($('#index_refresh').attr('refresh')!=''){
					if($('#itsmMainTab').tabs('getSelected')!=null){
						var obj=$('#itsmMainTab').tabs('getSelected').panel('options');
						$('.WSTUO-dialog').dialog('close');
						$('#itsmMainTab').tabs('getSelected').panel('refresh');
						$('#index_refresh').attr('refresh','');
						setTimeout(function () { //1s后才允许重新点击刷新
							$('#index_refresh').attr('refresh','refresh');
						}, 1000);
						//改变时需要改变大小
						initHeaderHeight();
					}
				}
				
			}
			
		},
		/**
		 * 改变窗口大小时，刷新Tab index
		 **/
		refreshWindowIndex:function(){
			if(reqRenovate){
				reqRenovate=false;
				var obj=$('#itsmMainTab').tabs('getSelected').panel();
				$('.WSTUO-dialog').dialog('close');
				$('#itsmMainTab').tabs('getSelected').panel('refresh');
				//改变时需要改变大小
				initHeaderHeight();
				reqRenovate=true;
			}
		},
		
		
		/**
		 * 打开Tab，存在则选择
		 * @param title
		 * @param url
		 **/
		openTabCallback:function(title,url,callBack){
			
			if ($('#itsmMainTab').tabs('exists',title)){
				
				$('#itsmMainTab').tabs('select',title);
				
				callBack();

			}else {
				
				$('#itsmMainTab').tabs('add',{
					title:title,
					cache:"false",
		 			href:url,
					closable:true
				});
			}

		},
		/**
		 * @description 关闭事件绑定
		 * */
		tabCloseEven:function()
		{
		    //关闭当前
		    $('#mm-tabclose').click(function(){
		        var currtab_title = $('#mm').data("currtab");
		        $('#itsmMainTab').tabs('close',currtab_title);
		    });
		    //全部关闭
		    $('#mm-tabcloseall').click(function(){
		        $('.tabs-closable').each(function(i,n){
		            var t = $(n).text();
		            $('#itsmMainTab').tabs('close',t);
		        });   
		    });
		    //关闭除当前之外的TAB
		    $('#mm-tabcloseother').click(function(){
		        var currtab_title = $('#mm').data("currtab");
		        $('.tabs-closable').each(function(i,n){
		            var t = $(n).text();
		            if(t!=currtab_title)
		                $('#itsmMainTab').tabs('close',t);
		        });   
		    });
		    //关闭当前右侧的TAB
		    $('#mm-tabcloseright').click(function(){
		        var nextall = $('.tabs-selected').nextAll();
		        if(nextall.length==0){
		            //msgShow('系统提示','后边没有啦~~','error');
		            return false;
		        }
		        nextall.each(function(i,n){
		            var t=$('a:eq(0) span',$(n)).text();
		            $('#itsmMainTab').tabs('close',t);
		        });
				  return false;
		    });
		    //关闭当前左侧的TAB
		    $('#mm-tabcloseleft').click(function(){
		        var prevall = $('.tabs-selected').prevAll();
		        if(prevall.length==0){
		            return false;
		        }
		        prevall.each(function(i,n){
		            var t=$('a:eq(0) span',$(n)).text();
		           
		            $('#itsmMainTab').tabs('close',t);
		        });
		        return false;
		    });
		    
		},
		// Build popup menu HTML,创建右键菜单
		createMenu:function(menuData,e) {
			
		    var menu = $('<ul class=contextMenuPlugin><div class=gutterLine></div></ul>')
		      .appendTo(document.body);

		    $.each(menuData.items,function(id,item){
		    	if (item) {
		    		//var row = $('<li><a href="#"><img><span></span></a></li>').appendTo(menu);
		            var row = $('<li><a><span></span></a></li>').appendTo(menu);
		            //row.find('img').attr('src', item.icon);
		            row.find('span').text(item.label);
		            if (item.action) {
		              row.find('a').click(item.action);
		            }
		          } else {
		            $('<li class=divider></li>').appendTo(menu);
		          }
		    });
		    //menu.find('.header').text(menuData.title);
		    menu.show()
		      .css({zIndex:1000001, left:e.pageX + 5 /* nudge to the right, so the pointer is covering the title */, top:e.pageY});
		    
		    var bg = $('<div></div>')
		      .css({left:0, top:0, width:'100%', height:'100%', position:'absolute', zIndex:1000000})
		      .appendTo(document.body)
		      .bind('contextmenu click', function() {
		        // If click or right click anywhere else on page: remove clean up.
		        bg.remove();
		        menu.remove();
		        //return false;
		      });
		    
		    menu.find('a').click(function() {
		        $('.contextMenuPlugin,.divider').remove();
		        bg.remove();
		        menu.remove();
		      });
		  },
		closeTab:function(title){
			if(title!=''){
        		var onetab= $('#itsmMainTab').tabs('getTab',title);
        		if(onetab!=null){
        			var closable = onetab.panel('options').closable;
        			if(closable){
        				$('#itsmMainTab').tabs('close',title);
        			}
        		}
        	}
		},  
		openMenu:function(e){
			$('.contextMenuPlugin').remove();
			basics.tab.tabUtils.createMenu({//创建关闭菜单
		          title:'',
		          items: [
					{label:i18n['refresh'],icon:'',action:function(){
						var currtab_title = $('#mm').data("currtab");
						basics.tab.tabUtils.refreshWindow();
					}},
		            {label:i18n['label_contextMenuClose'],icon:'',action:function(){
		            	var currtab_title = $('#mm').data("currtab");
				        $('#itsmMainTab').tabs('close',currtab_title);
		            }},
		            {label:i18n['lable_closeAll'],icon:'',action:function(){
		            	$('.tabs-closable').each(function(i,n){
				            var t = $(n).text();
				            //我的首页、我的面板、日程表
				            if(basics.tab.tabUtils.excludeNotAllowCloseTab(t)){
				            	basics.tab.tabUtils.closeTab(t);
				            }

				        });  
		            }},
		            {label:i18n['lable_onlyCloseAll'],icon:'',action:function(){
		            	var currtab_title = $('#mm').data("currtab");
				        $('.tabs-closable').each(function(i,n){
				            var t = $(n).text();
				            if(basics.tab.tabUtils.excludeNotAllowCloseTab(t) && t!=currtab_title){
				            	basics.tab.tabUtils.closeTab(t);
				            }
				        }); 
		            }}
		          ]
		    },e);
		},
		/**
		 * @description 绑定右键菜单事件 
		 * */
		tabClose:function()
		{
			/*双击关闭TAB选项卡*/
			$(".tabs-inner").dblclick(function(){
//				var obj=$('#itsmMainTab').tabs('getSelected').panel();
				var subtitle = $(this).children("span").text();
				//我的首页、我的面板、日程表
				if(basics.tab.tabUtils.excludeNotAllowCloseTab(subtitle))
					$('#itsmMainTab').tabs('close',subtitle);
			});

			$(".tabs-inner").bind('contextmenu',function(e){
				
				$('#mm').menu('show',{
					left: e.pageX,
					top: e.pageY
				});
				var subtitle =$(this).children("span").text();
				//我的首页、我的面板、日程表
				if(basics.tab.tabUtils.excludeNotAllowCloseTab(subtitle))
					basics.tab.tabUtils.openMenu(e);
				
				
				$('#mm').data("currtab",subtitle);
				$('#itsmMainTab').tabs('select',subtitle);
				resizeCommon();
				return false;
			});
			

		},
		//排除不允许关闭的Tab
		excludeNotAllowCloseTab:function(title){
			if(
				 title==i18n['label_dashboard_my_panel'] 
				|| title==i18n['title_calendar'] 
				|| title==i18n['label_quick_launch']
				|| title==i18n['title_call_seat']
				|| title==i18n['title_index_shortcuts']
				|| title==i18n['companyInformation']
			){
				return false;
			}else{
				return true;
			}
		},
		
		recordOpenTab:function(){
			//记录最近打开的Tab和关闭时转到最近打开的Tab
			var lastTabs = new Array();
			$('#itsmMainTab').tabs({
				   onBeforeClose:function(title){
					   ckeditorDestroy(title);
				   },
		           onSelect:function(title){
		        	 if(page=="main"){
		        		 $("#requestMain_content,#problemMain_content,#changeManage_panel,#configureItemMain_layout,#releaseGrid_content,#knowledgeMain_layout").parent().parent().height("auto");
		                 $("#requestMain_content,#problemMain_content,#changeManage_panel,#configureItemMain_layout,#releaseGrid_content,#knowledgeMain_layout").parent().height("auto");
		        	 }
		        	 //Tab自适应
		        	 basics.bottomMain.resizeTabByTitle(title);
		        	//刷新
					 if(tobeRefreshGrid.contains(title)){
						 startLoading();
						 setTimeout(function(){  
							endLoading();
						 },300);
						 $('#itsmMainTab').tabs('getSelected').panel('refresh'); 
					 }
					 //加入数组
					 if(clickRefreshTab.toString().indexOf(title) > -1 && tobeRefreshGrid.contains(title)==false){
						 tobeRefreshGrid.push(title);
					 }
					//移除 tt
					lastTabs = $.grep(lastTabs, function(n, i){ return n != title; });
					//重新压入，保证 最新的在最上面
					lastTabs.push(title);
					//更新当前
					currentTabTitle = title;
					
					 var resizeId=map.get(title);
					 if(!!resizeId){
						 var layOutAndGridNames =resizeId.split(',');
						 $.each(layOutAndGridNames, function(v, gridId){
							 if(!!gridId && gridId.indexOf('layout')>=0){//判断点击的tab是否是布局
							     redraw(gridId);
				        	 }
						 });
					 }
					 if(title==i18n['label_dashboard_my_panel']){
						 $('#helpdeskPortalTab').tabs('resize');
						 resizePortalGrid();//自适应宽度
					 }
				},
				onClose: function(tt) {
					//移除 tt
					lastTabs = $.grep(lastTabs, function(n, i) { return n != tt; });
					//重新选择
					$('#itsmMainTab').tabs('select', lastTabs[lastTabs.length - 1]);
				}
			});
		},
		
		/**
		 * @description 隐藏所有菜单项 未使用
		 */
		hideAll : function() {
            $('#itsmMainTab .tabs:first .tabs-title').each(function(i,n){
                var $t = $(n);
                if(!$t.is(':hidden')){
                    if ($t.text() != i18n['label_dashboard_my_panel'] || $t.text() !==i18n['title_calendar'] 
    				|| $t.text() !==i18n['label_quick_launch']
    				|| $t.text() !==i18n['title_call_seat']) {
                        var tab_option = $('#itsmMainTab').tabs('getTab', $t.text()).panel('options').tab;
                        tab_option.hide();
                    }
                }
            });
		},
		
		/**
         * @description 根据数组显示 未使用
         * @param names 存放i18n的数组
         */
		showByArray : function(names) {
		    $('#itsmMainTab .tabs:first .tabs-title').each(function(i,n){
                var $t = $(n);
                
                for (var j = 0; j < names.length; j ++){
                    if (names[j] == $t.text()) {
                        var tab_option = $('#itsmMainTab').tabs('getTab', $t.text()).panel('options').tab;
                        tab_option.show();
                    }
                }
            });
		},
		init:function(){
		}
	};
}();
