﻿/* MarcGrabanski.com v2.5 */
/* Pop-Up Calendar Built from Scratch by Marc Grabanski */
/* Enhanced by Keith Wood (kbwood@iprimus.com.au). */
/* Time picker functionality added by Gregor Petrin*/
/*	Share or Remix it but please Attribute the authors. */
var mypopUpCal = {
    selectedDay: 0,
    selectedMonth: 0, // 0-11
    selectedYear: 0, // 4-digit year
    selectedHour: 0,
    selectedMinute: 0,
    clearText:i18n['calendar_remove'], // Display text for clear link清除
    closeText:i18n['task_label_close'], // Display text for close link关闭
    prevText:i18n['calendar_on_month'], // &lt;Display text for previous month link
    nextText:i18n['calendar_next_month'], // Display text for next month link
    currentText:i18n['calendar_today'], // Display text for current month link今天
    appendText: '', // Display text following the input box, e.g. showing the format
    buttonText: '...', // Text for trigger button
    buttonImage: '', // URL for trigger button image
    buttonImageOnly: false, // True if the image appears alone, false if it appears on a button
    dayNames: [i18n['calendar_sun'],i18n['calendar_mon'],i18n['calendar_tues'],i18n['calendar_webnes'],i18n['calendar_thurs'],i18n['calendar_fri'],i18n['calendar_satur']], // Names of days starting at Sunday
    monthNames: [i18n['calendar_Jan'],i18n['calendar_Feb'],i18n['calendar_Mar'],i18n['calendar_Apr'],
                 i18n['calendar_May'],i18n['calendar_Jun'],i18n['calendar_Jul'],i18n['calendar_Aug'],
                 i18n['calendar_Sep'],i18n['calendar_Oct'],i18n['calendar_Nov'],i18n['calendar_Dec']], // Names of months
    dateFormat: 'YMD-', // First three are day, month, year in the required order, fourth is the separator, e.g. US would be 'MDY/'
    yearRange: '-5:+5', // Range of years to display in drop-down, either relative to current year (-nn:+nn) or absolute (nnnn:nnnn)
    changeMonth: true, // True if month can be selected directly, false if only prev/next
    changeYear: true, // True if year can be selected directly, false if only prev/next
    firstDay: 0, // The first day of the week, Sun = 0, Mon = 1, ...
    changeFirstDay: true, // True to click on day name to change, false to remain as set
    showOtherMonths: false, // True to show dates in other months, false to leave blank
    minDate: null, // The earliest selectable date, or null for no limit
    maxDate: null, // The latest selectable date, or null for no limit
    speed: 'slow', // Speed of display/closure
    autoPopUp: 'focus', // 'focus' for popup on focus, 'button' for trigger button, or 'both' for either
    closeAtTop: false, // True to have the clear/close at the top, false to have them at the bottom
    customDate: null, // Function that takes a date and returns an array with [0] = true if selectable, false if not,
    // [1] = custom CSS class name(s) or '', e.g. mypopUpCal.mynoWeekends
    fieldSettings: null, // Function that takes an input field and returns a set of custom settings for the calendar
    timeSeparators: [' ', ':'], //,'上午','下午'An array of time separators: the first and second strings are obligatory, while the second and third strings specify
    //the AM and PM strings if they are needed; the first parameter separates the date and time fields.
    //timeSeparators:[' ',':'], 	//Enable the time selector without AM/PM
    showDay: true,

    /* Format and display the given date. */
    myformatDate: function (day, month, year, hour, minute) {
        month++; // adjust javascript month
        var dateString = '';
        for (var i = 0; i < this.dateFormat.length - 1; i++) {
            dateString += 
				(this.dateFormat.charAt(i) == 'D' ? (day < 10 ? '0' : '') + day :
				(this.dateFormat.charAt(i) == 'M' ? (month < 10 ? '0' : '') + month :
				(this.dateFormat.charAt(i) == 'Y' ? year : ''))) + this.dateFormat.charAt(this.dateFormat.length - 1);
        }
        dateString=dateString.substr(0, dateString.length - 1)
        //append time info
        if (this.timeSeparators && this.timeSeparators.length > 1 && this.timeSeparators[0] && hour != null && minute != null) {
            dateString += this.timeSeparators[0] + this.myhourString(hour) + this.timeSeparators[1] + ((minute >= 10) ? minute : ('0' + minute));
            if (this.timeSeparators.length == 4 && hour < 12) dateString += this.timeSeparators[2];
            else if (this.timeSeparators.length == 4 && hour >= 12) dateString += this.timeSeparators[3];
        }
        if(dateString.length<=16){
        	return dateString+':00';
        }else{
        	return dateString;
        }
        
    },


    /*Parses a string and returns a Date object*/
    myparseDate: function (Text) {
        var currentYear, currentMonth, currentDay, currentHour, currentMinute;

        if (this.timeSeparators && this.timeSeparators.length > 1) {
            var currentTimeAndDate = Text.split(this.timeSeparators[0]);
           // alert(currentTimeAndDate);
            var index = 0;
            while (index < currentTimeAndDate.length) if (currentTimeAndDate[index]) index++; else currentTimeAndDate.splice(index, 1);
            if (currentTimeAndDate.length > 0) var currentDate = currentTimeAndDate[0].split(this.dateFormat.charAt(3)); else var currentDate = [];
            if (currentTimeAndDate.length > 1) var currentTime = currentTimeAndDate[1].split(this.timeSeparators[1]); else var currentTime = [];
        }else {
            var currentDate = Text.split(this.dateFormat.charAt(3));
            var currentTime = null;
        }
        if (currentDate.length == 3) {

        	var day = new Date(currentDate[0],currentDate[1]-1,currentDate[2]);  
        	currentDay = day.getDate();
            currentMonth =day.getMonth();
            currentYear =day.getFullYear();
//            currentDay = parseInt(this.mytrimNumber(currentDate[this.dateFormat.indexOf('D')]));
//            currentMonth = parseInt(this.mytrimNumber(currentDate[this.dateFormat.indexOf('M')])) - 1;
//            currentYear = parseInt(this.mytrimNumber(currentDate[this.dateFormat.indexOf('Y')]));
        	

        } else {
            currentDay = new Date().getDate();
            currentMonth = new Date().getMonth();
            currentYear = new Date().getFullYear();

        }
        if (this.timeSeparators && this.timeSeparators.length > 1 && this.timeSeparators[0]) {
            if (currentTime != null && currentTime.length == 2) {
            	//alert("you"+currentTime);
            	var time1 = new Date(2011,1,1,currentTime[0],currentTime[1]);
                //currentHour = parseInt(this.mytrimNumber(currentTime[0]));
                currentHour=time1.getHours();
                //alert('go');
                if (this.timeSeparators.length > 2) {
                	//alert('go1');
                    if (currentTime[1].indexOf(this.timeSeparators[2]) == currentTime[1].length - this.timeSeparators[2].length) {
                        if (currentHour == 12) currentHour = 0;
                        currentMinute = parseInt(this.mytrimNumber(currentTime[1].substring(0, currentTime[1].length - this.timeSeparators[2].length)));
                    } else {
                    	//alert('go2');
                        if (currentHour < 12) currentHour += 12;
                        currentMinute = parseInt(this.mytrimNumber(currentTime[1].substring(0, currentTime[1].length - this.timeSeparators[1].length)));
                    }
                }
                else {
                	//alert('go32');
                	currentMinute = parseInt(this.mytrimNumber(currentTime[1]));
                }
                
            } else {
            	//alert("no");
                currentHour = new Date().getHours();
                currentMinute = new Date().getMinutes();
            }
            //alert(new Date(currentYear, currentMonth, currentDay, currentHour, currentMinute));
            return new Date(currentYear, currentMonth, currentDay, currentHour, currentMinute);
        }
        else{
        	return new Date(currentYear, currentMonth, currentDay);
        }	
    },


    /* Parse existing date and initialise calendar. */
    mysetDateFromField: function () {
        var currentDate = this.myparseDate(this.input.val());
        this.currentDay = currentDate.getDate();
        this.currentMonth = currentDate.getMonth();
        this.currentYear = currentDate.getFullYear();
        if (this.timeSeparators && this.timeSeparators.length > 1 && this.timeSeparators[0]) {
            this.currentHour = currentDate.getHours();
            this.currentMinute = currentDate.getMinutes();
        }


        this.selectedDay = this.currentDay;
        this.selectedMonth = this.currentMonth;
        this.selectedYear = this.currentYear;
        if (this.currentHour && this.currentMinute) {
            this.selectedHour = this.currentHour;
            this.selectedMinute = this.currentMinute;
        }
        this.myadjustDate(0, 'D', true);
    },

    /*Translates this hour into the AM/PM number if neccessary*/
    myhourString: function (Hour) {
        if (this.timeSeparators.length == 4) {
            if (Hour == 0) return '' + 12;
            else if (Hour > 12) return '' + (Hour - 12);
            else return '' + Hour;
        }
        else return '' + Hour;
    },

    /* Update the input field with the selected date. */
    /* Edited by Gregor Petrin to allow a custom input field as a parameter  - this can be used to display a date at start*/
    myselectDate: function (Input) {
        if (!Input) var Input = this.input;
        this.myhideCalendar(this.speed);
        //默认显示0分钟
//        if (this.timeSeparators.length > 1) Input.val(this.myformatDate(this.selectedDay, this.selectedMonth, this.selectedYear, this.selectedHour, 0));
        if (this.timeSeparators.length > 1) Input.val(this.myformatDate(this.selectedDay, this.selectedMonth, this.selectedYear, this.selectedHour, this.selectedMinute));
        else Input.val(this.myformatDate(this.selectedDay, this.selectedMonth, this.selectedYear));
        Input.blur();
    },

    /* Construct and display the calendar. */
    myshowCalendar: function () {
        this.popUpShowing = true;
        // build the calendar HTML
        var timeSelect = '';
        if (this.timeSeparators.length > 1) {
            timeSelect += '<div id="my_calendar_time">';
            if (this.timeSeparators.length == 4) {
                timeSelect += ' <select id="calendar_ampm">';
                timeSelect += '<option value="AM"' + ((this.selectedHour < 12) ? ' selected="selected"' : '') + '>' + this.timeSeparators[2] + '</option>';
                timeSelect += '<option value="PM"' + ((this.selectedHour >= 12) ? ' selected="selected"' : '') + '>' + this.timeSeparators[3] + '</option>';
                timeSelect += '</select>';
            }
            timeSelect += "<select id=\"calendar_hour\">";
            if (this.timeSeparators.length == 4) {
                if (this.selectedHour == 12 || this.selectedHour == 0) timeSelect += '<option value="12" selected="selected">12</option>';
                else timeSelect += '<option value="12">12</option>';
                for (var a = 1; a < 12; a++) {
                    if (this.selectedHour == a || (a == 12 && this.selectedHour == 0) || (a + 12 == this.selectedHour)) timeSelect += '<option value="' + a + '" selected="selected">' + a + '</option>';
                    else timeSelect += '<option value="' + a + '">' + a + '</option>';
                }
            } else {
                for (var a = 0; a < 24; a++) {
                    if (this.selectedHour == a) timeSelect += '<option value="' + a + '" selected="selected">' + a + '</option>';
                    else timeSelect += '<option value="' + a + '">' + a + '</option>';
                }
            }

            timeSelect += '</select> ' + this.timeSeparators[1] + ' <select id="calendar_minute">';
            for (var a = 0; a < 60; a++) {
                if (this.selectedMinute == a) timeSelect += '<option value="' + a + '" selected="selected">' + ((a >= 10) ? a : ('0' + a)) + '</option>';
                else timeSelect += '<option value="' + a + '">' + ((a >= 10) ? a : ('0' + a)) + '</option>';
            	//默认为0分钟
//                if (this.selectedMinute == a) timeSelect += '<option value="' + a + '" ">' + ((a >= 10) ? a : ('0' + a)) + '</option>';
//                else timeSelect += '<option value="' + a + '">' + ((a >= 10) ? a : ('0' + a)) + '</option>';
            }
            timeSelect += '</select>';
            timeSelect += '</div>';
        }

        var html = (this.closeAtTop ? '<div id="my_calendar_control">' +
			'<a id="my_calendar_clear">' + this.clearText + '</a>' +
			'<a id="my_calendar_close">' + this.closeText + '</a></div>' : '') +
			'<div id="my_calendar_links"><a id="my_calendar_prev">' + this.prevText + '</a>' +
			'<a id="my_calendar_current">' + this.currentText + '</a>' +
			'<a id="my_calendar_next">' + this.nextText + '</a></div>' +
			'<div id="my_calendar_header">';
        
        if (!this.changeYear) {
            html += this.selectedYear;
        }
        else {
            // determine range of years to display
            var years = this.yearRange.split(':');
            var year = 0;
            var endYear = 0;
            if (years.length != 2) {
                year = this.selectedYear - 10;
                endYear = this.selectedYear + 10;
            }
            else if (years[0].charAt(0) == '+' || years[0].charAt(0) == '-') {
                year = this.selectedYear + parseInt(years[0]);
                endYear = this.selectedYear + parseInt(years[1]);
            }
            else {
                year = parseInt(years[0]);
                endYear = parseInt(years[1]);
            }
            year = (this.minDate ? Math.max(year, this.minDate.getFullYear()) : year);
            endYear = (this.maxDate ? Math.min(endYear, this.maxDate.getFullYear()) : endYear);
            html += '<select id="calendar_newYear">';
            for (; year <= endYear; year++) {
            	//alert(year+"=="+this.selectedYear);
                html += '<option value="' + year + '"' +
					(year == this.selectedYear ? ' selected="selected"' : '') +
					'>' + year + i18n['calendar_years']+'</option>';
            }
            html += '</select>';
        }
        if (!this.changeMonth) {
            html += this.monthNames[this.selectedMonth] + '&nbsp;';
        }
        else {
            var inMinYear = (this.minDate && this.minDate.getFullYear() == this.selectedYear);
            var inMaxYear = (this.maxDate && this.maxDate.getFullYear() == this.selectedYear);
            html += '<select id="calendar_newMonth">';
            for (var month = 0; month < 12; month++) {
                if ((!inMinYear || month >= this.minDate.getMonth()) &&
						(!inMaxYear || month <= this.maxDate.getMonth())) {
                    html += '<option value="' + month + '"' +
						(month == this.selectedMonth ? ' selected="selected"' : '') +
						'>' + this.monthNames[month] + '</option>';
                }
            }
            html += '</select>';
        }

        html += '</div>' + timeSelect;
        if (this.dateFormat.indexOf('D') >= 0) {
            html += '<table id="my_calendar" cellpadding="0" cellspacing="0"><thead>' +
			'<tr class="my_calendar_titleRow">';
            for (var dow = 0; dow < 7; dow++) {
                html += '<td>' + (this.changeFirstDay ? '<a>' : '') +
				this.dayNames[(dow + this.firstDay) % 7] + (this.changeFirstDay ? '</a>' : '') + '</td>';
            }
            html += '</tr></thead><tbody>';
            var daysInMonth = this.mygetDaysInMonth(this.selectedYear, this.selectedMonth);
            this.selectedDay = Math.min(this.selectedDay, daysInMonth);
            var leadDays = (this.mygetFirstDayOfMonth(this.selectedYear, this.selectedMonth) - this.firstDay + 7) % 7;
            var currentDate = new Date(this.currentYear, this.currentMonth, this.currentDay);
            var selectedDate = new Date(this.selectedYear, this.selectedMonth, this.selectedDay);
            var printDate = new Date(this.selectedYear, this.selectedMonth, 1 - leadDays);
            var numRows = Math.ceil((leadDays + daysInMonth) / 7); // calculate the number of rows to generate
            var today = new Date();
            today = new Date(today.getFullYear(), today.getMonth(), today.getDate()); // clear time
            for (var row = 0; row < numRows; row++) { // create calendar rows
                html += '<tr class="my_calendar_daysRow">';
                for (var dow = 0; dow < 7; dow++) { // create calendar days
                    var customSettings = (this.customDate ? this.customDate(printDate) : [true, '']);
                    var otherMonth = (printDate.getMonth() != this.selectedMonth);
                    var unselectable = otherMonth || !customSettings[0] ||
					(this.minDate && printDate < this.minDate) ||
					(this.maxDate && printDate > this.maxDate);
                    html += '<td class="my_calendar_daysCell' +
					((dow + this.firstDay + 6) % 7 >= 5 ? ' my_calendar_weekEndCell' : '') + // highlight weekends
					(otherMonth ? ' calendar_otherMonth' : '') + // highlight days from other months
					(printDate.getTime() == selectedDate.getTime() ? ' my_calendar_daysCellOver' : '') + // highlight selected day
					(unselectable ? ' my_calendar_unselectable' : '') +  // highlight unselectable days
					(!otherMonth || this.showOtherMonths ? ' ' + customSettings[1] : '') + '"' + // highlight custom dates
					(printDate.getTime() == currentDate.getTime() ? ' id="my_calendar_currentDay"' : // highlight current day
					(printDate.getTime() == today.getTime() ? ' id="my_calendar_today"' : '')) + '>' + // highlight today (if different)
					(otherMonth ? (this.showOtherMonths ? printDate.getDate() : '&nbsp;') : // display for other months
					(unselectable ? printDate.getDate() : '<a>' + printDate.getDate() + '</a>')) + '</td>'; // display for this month
                    printDate.setDate(printDate.getDate() + 1);
                }
                html += '</tr>';

            }
            html += '</tbody></table><!--[if lte IE 6.5]><iframe src="javascript:false;" id="my_calendar_cover"></iframe><![endif]-->';
        }
        html += (this.closeAtTop ? '' : '<div id="my_calendar_control"><a id="my_calendar_clear">' + this.clearText + '</a>' + (this.dateFormat.indexOf('D')>=0 ? "" : "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a id=\"calendar_confirm\">确定</a>") +
			'<a id="my_calendar_close">' + this.closeText + '</a></div>');
        // add calendar to element to calendar Div
        $('#mycalendar_div').empty().append(html).show(this.speed);
        this.input[0].focus();
        this.mysetupActions();
    }, // end myshowCalendar

    /* Initialisation. */
    myinit: function () {
        this.popUpShowing = false;
        this.lastInput = null;
        this.disabledInputs = [];
        $('body').append('<div id="mycalendar_div"></div>');
        $(document).mousedown(mypopUpCal.mycheckExternalClick);
        
    },

    /* Pop-up the calendar for a given input field. */
    myshowFor: function (target) {
        var input = (target.nodeName && target.nodeName.toLowerCase() == 'input' ? target : this);
        if (input.nodeName.toLowerCase() != 'input') { // find from button/image trigger
            input = $('../input', input)[0];
        }
        if (mypopUpCal.lastInput == input) { // already here
            return;
        }
        for (var i = 0; i < mypopUpCal.disabledInputs.length; i++) {  // check not disabled
            if (mypopUpCal.disabledInputs[i] == input) {
                return;
            }
        }
        mypopUpCal.input = $(input);
        mypopUpCal.myhideCalendar();
        mypopUpCal.lastInput = input;
        mypopUpCal.mysetDateFromField();
        mypopUpCal.mysetPos(input, $('#mycalendar_div'));
        $.extend(mypopUpCal, (mypopUpCal.fieldSettings ? mypopUpCal.fieldSettings(input) : {}));
        mypopUpCal.myshowCalendar();
    },

    /* Handle keystrokes. */
    mydoKeyDown: function (e) {
        if (mypopUpCal.popUpShowing) {
        	
            switch (e.keyCode) {
                case 9: mypopUpCal.myhideCalendar(); break; // hide on tab out
                case 13: mypopUpCal.myselectDate(); break; // select the value on enter
                case 27: mypopUpCal.myhideCalendar(mypopUpCal.speed); break; // hide on escape
                case 33: mypopUpCal.myadjustDate(-1, (e.ctrlKey ? 'Y' : 'M')); break; // previous month/year on page up/+ ctrl
                case 34: mypopUpCal.myadjustDate(+1, (e.ctrlKey ? 'Y' : 'M')); break; // next month/year on page down/+ ctrl
                case 35: if (e.ctrlKey) $('#my_calendar_clear').click(); break; // clear on ctrl+end
                case 36: if (e.ctrlKey) $('#my_calendar_current').click(); break; // current on ctrl+home
                case 37: if (e.ctrlKey) mypopUpCal.myadjustDate(-1, 'D'); break; // -1 day on ctrl+left
                case 38: if (e.ctrlKey) mypopUpCal.myadjustDate(-7, 'D'); break; // -1 week on ctrl+up
                case 39: if (e.ctrlKey) mypopUpCal.myadjustDate(+1, 'D'); break; // +1 day on ctrl+right
                case 40: if (e.ctrlKey) mypopUpCal.myadjustDate(+7, 'D'); break; // +1 week on ctrl+down
            }
        }
        else if (e.keyCode == 36 && e.ctrlKey) { // display the calendar on ctrl+home
            mypopUpCal.myshowFor(this);
        }
    },

    /* Filter entered characters. */
    mydoKeyPress: function (e) {
        var chr = String.fromCharCode(e.charCode == undefined ? e.keyCode : e.charCode);
        return (chr < ' ' || chr == mypopUpCal.dateFormat.charAt(3) || (chr >= '0' && chr <= '9')); // only allow numbers and separator
    },

    /* Attach the calendar to an input field. */
    myconnectCalendar: function (target) {
        var $input = $(target);
        $input.after('<span class="calendar_append">' + this.appendText + '</span>');
        if (this.autoPopUp == 'focus' || this.autoPopUp == 'both') { // pop-up calendar when in the marked fields
            $input.focus(this.myshowFor);
        }
        if (this.autoPopUp == 'button' || this.autoPopUp == 'both') { // pop-up calendar when button clicked
            $input.wrap('<span class="calendar_wrap"></span>').
				after(this.buttonImageOnly ? '<img class="my_calendar_trigger" src="' +
				this.buttonImage + '" alt="' + this.buttonText + '" title="' + this.buttonText + '"/>' :
				'<button class="my_calendar_trigger">' + (this.buttonImage != '' ?
				'<img src="' + this.buttonImage + '" alt="' + this.buttonText + '" title="' + this.buttonText + '"/>' :
				this.buttonText) + '</button>');
            $((this.buttonImageOnly ? 'img' : 'button') + '.my_calendar_trigger', $input.parent('span')).click(this.myshowFor);
        }
        $input.keydown(this.mydoKeyDown).keypress(this.mydoKeyPress);
    },

    /* Enable the input field(s) for entry. */
    myenableFor: function (inputs) {
        inputs = (inputs.jquery ? inputs : $(inputs));
        inputs.each(function () {
            this.disabled = false;
            $('../button.my_calendar_trigger', this).each(function () { this.disabled = false; });
            $('../img.my_calendar_trigger', this).each(function () { $(this).css('opacity', '1.0'); });
            var $this = this;
            mypopUpCal.disabledInputs = $.map(mypopUpCal.disabledInputs,
				function (value) { return (value == $this ? null : value); }); // delete entry
        });
        return false;
    },

    /* Disable the input field(s) from entry. */
    mydisableFor: function (inputs) {
        inputs = (inputs.jquery ? inputs : $(inputs));
        inputs.each(function () {
            this.disabled = true;
            $('../button.my_calendar_trigger', this).each(function () { this.disabled = true; });
            $('../img.my_calendar_trigger', this).each(function () { $(this).css('opacity', '0.5'); });
            var $this = this;
            mypopUpCal.disabledInputs = $.map(mypopUpCal.disabledInputs,
				function (value) { return (value == $this ? null : value); }); // delete entry
            mypopUpCal.disabledInputs[mypopUpCal.disabledInputs.length] = this;
        });
        return false;
    },

    /* Connect behaviours to the calendar. */
    mysetupActions: function () {
    	var flag="0";
    	document.onclick=function(){
        	var oDiv=document.getElementById("mycalendar_div");
        	var evt=getEvent();
        	
        	var obj=evt.srcElement?event.srcElement:evt.target;
        	if(!!window.find){ 
        		HTMLElement.prototype.contains = function(obj){ 
        			return this.compareDocumentPosition(obj) - 19 > 0 
        		} 
        	} 
            if(obj!=oDiv && !oDiv.contains(obj))
            {	
            	if(flag=="0"){
            		flag="1";
            	}else{
            		mypopUpCal.myhideCalendar(mypopUpCal.speed);
            	}
            	
            } 
        };
    	
        if (this.timeSeparators.length > 1) {
            $('#calendar_hour').change(function () {	//change hour
                mypopUpCal.selecting = false;
                mypopUpCal.selectedHour = this.options[this.selectedIndex].value - 0;
                if (mypopUpCal.timeSeparators.length == 4) {
                    var ampm = $('#calendar_ampm').val();
                    if (ampm == 'AM' && mypopUpCal.selectedHour == 12) mypopUpCal.selectedHour = 0;
                    else if (ampm == 'PM' && mypopUpCal.selectedHour != 12) mypopUpCal.selectedHour += 12;
                }
                mypopUpCal.myadjustDate();
                //默认显示0分钟
//                mypopUpCal.input.val(mypopUpCal.myformatDate(mypopUpCal.selectedDay, mypopUpCal.selectedMonth, mypopUpCal.selectedYear, mypopUpCal.selectedHour, 0));
                mypopUpCal.input.val(mypopUpCal.myformatDate(mypopUpCal.selectedDay, mypopUpCal.selectedMonth, mypopUpCal.selectedYear, mypopUpCal.selectedHour, mypopUpCal.selectedMinute));
            }).click(this.myselectMonthYear);
            $("#calendar_minute").change(function () {	//change minute
                mypopUpCal.selecting = false;
                mypopUpCal.selectedMinute = this.options[this.selectedIndex].value - 0;
                mypopUpCal.myadjustDate();
                //默认显示0分钟
//                mypopUpCal.input.val(mypopUpCal.myformatDate(mypopUpCal.selectedDay, mypopUpCal.selectedMonth, mypopUpCal.selectedYear, mypopUpCal.selectedHour,0));
                mypopUpCal.input.val(mypopUpCal.myformatDate(mypopUpCal.selectedDay, mypopUpCal.selectedMonth, mypopUpCal.selectedYear, mypopUpCal.selectedHour, mypopUpCal.selectedMinute));
            }).click(this.myselectMonthYear); ;
            $("#calendar_ampm").change(function () {		//change am/pm
                mypopUpCal.selecting = false;
                if (mypopUpCal.timeSeparators.length == 4) {
                    var ampm = $('#calendar_ampm').val();
                    if (ampm == 'AM' && mypopUpCal.selectedHour > 12) mypopUpCal.selectedHour -= 12;
                    else if (ampm == 'PM' && mypopUpCal.selectedHour < 12) mypopUpCal.selectedHour += 12;
                    else if (ampm == 'PM' && mypopUpCal.selectedHour == 12) mypopUpCal.selectedHour = 12;
                    else if (ampm == 'AM' && mypopUpCal.selectedHour == 12) mypopUpCal.selectedHour = 0;
                }
                mypopUpCal.myadjustDate();
                //默认显示0分钟
//                mypopUpCal.input.val(mypopUpCal.myformatDate(mypopUpCal.selectedDay, mypopUpCal.selectedMonth, mypopUpCal.selectedYear, mypopUpCal.selectedHour,0));
                mypopUpCal.input.val(mypopUpCal.myformatDate(mypopUpCal.selectedDay, mypopUpCal.selectedMonth, mypopUpCal.selectedYear, mypopUpCal.selectedHour, mypopUpCal.selectedMinute));
            });
            
        }
        $('#my_calendar_clear').click(function () { // clear button link
            mypopUpCal.myclearDate();
        });
        $('#my_calendar_close').click(function () { // close button link
            mypopUpCal.myhideCalendar(mypopUpCal.speed);
        });
        
        
        
        $('#my_calendar_prev').click(function () { // setup navigation links
            mypopUpCal.myadjustDate(-1, 'M');
        });
        $('#my_calendar_next').click(function () {
            mypopUpCal.myadjustDate(+1, 'M');
        });
        $('#my_calendar_current').click(function () { // back to today
            mypopUpCal.selectedDay = new Date().getDate();
            mypopUpCal.selectedMonth = new Date().getMonth();
            mypopUpCal.selectedYear = new Date().getFullYear();
            mypopUpCal.myadjustDate();
        });
        $('#calendar_newMonth').change(function () { // change month
            mypopUpCal.selecting = false;
            mypopUpCal.selectedMonth = this.options[this.selectedIndex].value - 0;
            mypopUpCal.myadjustDate();
        }).click(this.myselectMonthYear);
        $('#calendar_newYear').change(function () { // change year
            mypopUpCal.selecting = false;
            mypopUpCal.selectedYear = this.options[this.selectedIndex].value - 0;
            mypopUpCal.myadjustDate();
        }).click(this.myselectMonthYear);
        $('.my_calendar_titleRow a').click(function () { // change first day of week
            for (var i = 0; i < 7; i++) {
                if (mypopUpCal.dayNames[i] == this.firstChild.nodeValue) {
                    mypopUpCal.firstDay = i;
                }
            }
            mypopUpCal.myshowCalendar();
        });
        //$('.my_calendar_daysRow td a').hover( // highlight current day
        $('.my_calendar_daysRow td').hover( // highlight current day
            function () {
                $(this).addClass('my_calendar_daysCellOver');
            }, function () {
                $(this).removeClass('my_calendar_daysCellOver');
            });
        //$('.my_calendar_daysRow td[a]').click(function() { // select day
        $('.my_calendar_daysRow td a').click(function () { // select day
            //alert("click");
            //mypopUpCal.selectedDay = $("a",this).html();
            mypopUpCal.selectedDay = $(this).html();
            mypopUpCal.myselectDate();
        });
        $('#calendar_confirm').click(function () { // clear button link
            mypopUpCal.selectedDay = $(this).html();
            mypopUpCal.myselectDate();
            
        });
        
        
        
    },

    /* Hide the calendar from view. */
    myhideCalendar: function (speed) {
    	
        if (this.popUpShowing) {
            $('#mycalendar_div').hide(speed);
            this.popUpShowing = false;
            this.lastInput = null;
            $('#'+this.input[0].id).removeClass("validatebox-invalid");
            this.input[0].blur();//关闭后失去焦点
        }
    },

    /* Restore input focus after not changing month/year. */
    myselectMonthYear: function () {
        if (mypopUpCal.selecting) {
            mypopUpCal.input[0].focus();
        }
        mypopUpCal.selecting = !mypopUpCal.selecting;
    },

    /* Erase the input field and hide the calendar. */
    myclearDate: function () {
        this.myhideCalendar(this.speed);
        this.input.val('');
    },

    /* Close calendar if clicked elsewhere. */
    6: function (event) {
        if (!mypopUpCal.popUpShowing) {
            var node = event.target;
            var cal = $('#mycalendar_div')[0];
            while (node && node != cal && node.className != 'my_calendar_trigger') {
                node = node.parentNode;
            }
            if (!node) {
                mypopUpCal.myhideCalendar();
            }
        }
    },

    /* Set as customDate function to prevent selection of weekends. */
    mynoWeekends: function (date) {
        var day = date.getDay();
        return [(day > 0 && day < 6), ''];
    },

    /* Ensure numbers are not treated as octal. */
    mytrimNumber: function (value) {
        if (value == '')
            return '';
        while (value.charAt(0) == ' 0 ' && value.length > 1) {
            value = value.substring(1);
        }
        return value;
    },

    /* Adjust one of the date sub-fields. */
    myadjustDate: function (offset, period, dontShow) {
        if (this.timeSeparators.length > 1) {
            var date = new Date(this.selectedYear + (period == 'Y' ? offset : 0),
			this.selectedMonth + (period == 'M' ? offset : 0),
			this.selectedDay + (period == 'D' ? offset : 0),
			this.selectedHour + (period == 'H' ? offset : 0),
			this.selectedMinute + (period == 'MIN' ? offset : 0));
        } else {
            var date = new Date(this.selectedYear + (period == 'Y' ? offset : 0),
			this.selectedMonth + (period == 'M' ? offset : 0),
			this.selectedDay + (period == 'D' ? offset : 0));
        }
        // ensure it is within the bounds set
        date = (this.minDate && date < this.minDate ? this.minDate : date);
        date = (this.maxDate && date > this.maxDate ? this.maxDate : date);
        this.selectedDay = date.getDate();
        this.selectedMonth = date.getMonth();
        this.selectedYear = date.getFullYear();
        if (this.timeSeparators.length > 1) {
            this.selectedHour = date.getHours();
            this.selectedMinute = date.getMinutes();
        }

        if (!dontShow) {
            this.myshowCalendar();
        }
    },

    /* Find the number of days in a given month. */
    mygetDaysInMonth: function (year, month) {
        return 32 - new Date(year, month, 32).getDate();
    },

    /* Find the day of the week of the first of a month. */
    mygetFirstDayOfMonth: function (year, month) {
        return new Date(year, month, 1).getDay();
    },

    /* Set an object's position on the screen. */
    mysetPos: function (targetObj, moveObj) {
        var coords = this.myfindPos(targetObj);
        var top0 = $("#"+targetObj.id).offset().top;
        var left0 = $("#"+targetObj.id).offset().left;
//        alert(targetObj.offsetHeight);
        var window_height = $(document).height(); //浏览器时下窗口可视区域高度 
        var window_width = $(document).width(); //浏览器时下窗口可视区域宽度 
        var _left = left0;
        var _top = top0;
        //日期窗口宽度190,高170
        if((_top+175)>window_height){
        	_top = _top-175;
        }
        if((_left+195)>window_width){
        	_left = _left-195;
        }
        moveObj.css('position', 'absolute').css('left', _left + 'px').
			css('top', (_top + targetObj.offsetHeight) + 'px');
    },

    /* Find an object's position on the screen. */
    myfindPos: function (obj) {
        var curleft = curtop = 0;
        if (obj.offsetParent) {
            curleft = obj.offsetLeft;
            curtop = obj.offsetTop;
            while (obj = obj.offsetParent) {
                var origcurleft = curleft;
                curleft += obj.offsetLeft;
                if (curleft < 0) {
                    curleft = origcurleft;
                }
                curtop += obj.offsetTop;
            }
        }
        return [curleft, curtop];
    }
};

/* Attach the calendar to a jQuery selection. */
$.fn.mycalendar = function (settings) {
	
    // customise the calendar object
    $.extend(mypopUpCal, settings || {});
    // attach the calendar to each nominated input element
    return this.each(function () {
        if (this.nodeName.toLowerCase() == 'input') {
        	mypopUpCal.myconnectCalendar(this);
        }
    });
};

function bindControl(calendarId){
	 $(calendarId).mycalendar({ dateFormat: 'YMD-'//格式为yyyy-MM-dd HH:mm:ss
     });//日历控件
}


function myfunc()
{
    var evt=getEvent();
    var element=evt.srcElement || evt.target;

}

function getEvent()
{
    if(document.all)
    {
    return window.event;//如果是ie
    }
func=getEvent.caller;
while(func!=null)
{
    var arg0=func.arguments[0];
    if(arg0)
    {
        if((arg0.constructor==Event || arg0.constructor ==MouseEvent)
        ||(typeof(arg0)=="object" && arg0.preventDefault && arg0.stopPropagation))
        {
        return arg0;
        }
    }
        func=func.caller;
    }
    return null;
}

/* Initialise the calendar. */
$(document).ready(function () {
	mypopUpCal.myinit();
	
    
});
