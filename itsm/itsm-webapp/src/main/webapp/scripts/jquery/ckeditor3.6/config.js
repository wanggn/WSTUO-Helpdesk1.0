﻿/*
Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.editorConfig = function( config )
{
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	config.filebrowserBrowseUrl = '../scripts/jquery/ckeditor3.6/uploader/browse.jsp';  
    config.filebrowserImageBrowseUrl = '../scripts/jquery/ckeditor3.6/uploader/browse.jsp?type=Images';  
    config.filebrowserFlashBrowseUrl = '../scripts/jquery/ckeditor3.6/uploader/browse.jsp?type=Flashs';  
    config.filebrowserUploadUrl = 'upload.jsp';  
    config.filebrowserImageUploadUrl = 'upload.jsp?type=Images';  
    config.filebrowserFlashUploadUrl = 'upload.jsp?type=Flashs';
    
};
