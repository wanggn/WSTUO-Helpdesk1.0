package com.wstuo.common.sla.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.wstuo.common.config.updatelevel.entity.UpdateLevel;
import com.wstuo.common.entity.BaseEntity;
import com.wstuo.common.security.entity.User;

/**
 * Promote Rule Entity
 * @author QXY
 *
 */
@SuppressWarnings({ "serial", "rawtypes" })
@Entity
public class PromoteRule extends BaseEntity {


	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long ruleNo;
	@Column(nullable=false)
	private String ruleName;
	private Boolean beforeOrAfter=false;
	private Long ruleTime; // TODO 关键字
	private Boolean isIncludeHoliday=false;
	@ManyToOne
	@JoinColumn(name = "levelNo")
	private User updateLevel;
	@ManyToOne
	@JoinColumn(name = "contractNo")
	private SLAContract slaContract;
	private String referType;//参考类型 请求响应/处理完成
	private String ruleType;//自动升级/SLA到期通知请求人


	public String getRuleType() {
		return ruleType;
	}



	public void setRuleType(String ruleType) {
		this.ruleType = ruleType;
	}

	public User getUpdateLevel() {
		return updateLevel;
	}



	public void setUpdateLevel(User updateLevel) {
		this.updateLevel = updateLevel;
	}



	public Long getRuleNo() {
		return ruleNo;
	}



	public void setRuleNo(Long ruleNo) {
		this.ruleNo = ruleNo;
	}



	public String getRuleName() {
		return ruleName;
	}



	public void setRuleName(String ruleName) {
		this.ruleName = ruleName;
	}



	public Boolean getBeforeOrAfter() {
		return beforeOrAfter;
	}



	public void setBeforeOrAfter(Boolean beforeOrAfter) {
		this.beforeOrAfter = beforeOrAfter;
	}

	public Long getRuleTime() {
        return ruleTime;
    }



    public void setRuleTime(Long ruleTime) {
        this.ruleTime = ruleTime;
    }



    public Boolean getIsIncludeHoliday() {
		return isIncludeHoliday;
	}



	public void setIsIncludeHoliday(Boolean isIncludeHoliday) {
		this.isIncludeHoliday = isIncludeHoliday;
	}




	public SLAContract getSlaContract() {
		return slaContract;
	}



	public void setSlaContract(SLAContract slaContract) {
		this.slaContract = slaContract;
	}



	public String getReferType() {
		return referType;
	}



	public void setReferType(String referType) {
		this.referType = referType;
	}

	
	
	
}