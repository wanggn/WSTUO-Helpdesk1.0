package com.wstuo.common.sla.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.wstuo.common.entity.BaseEntity;
/**
 * sla发送邮件的参数
 * @author QXY
 *
 */
@SuppressWarnings({ "serial", "rawtypes" })
@Entity
public class SLAEmailControl extends BaseEntity{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@Column(nullable=false)
	private Long eno;
	private Long ruleNo;
	private Long updateLevelUserId;
	private String referType;
	private String ruleType;
	private Long upgradeTime;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getEno() {
		return eno;
	}
	public void setEno(Long eno) {
		this.eno = eno;
	}
	public Long getRuleNo() {
		return ruleNo;
	}
	public void setRuleNo(Long ruleNo) {
		this.ruleNo = ruleNo;
	}
	public Long getUpdateLevelUserId() {
		return updateLevelUserId;
	}
	public void setUpdateLevelUserId(Long updateLevelUserId) {
		this.updateLevelUserId = updateLevelUserId;
	}
	public String getReferType() {
		return referType;
	}
	public void setReferType(String referType) {
		this.referType = referType;
	}
	public String getRuleType() {
		return ruleType;
	}
	public void setRuleType(String ruleType) {
		this.ruleType = ruleType;
	}
	public Long getUpgradeTime() {
		return upgradeTime;
	}
	public void setUpgradeTime(Long upgradeTime) {
		this.upgradeTime = upgradeTime;
	}
	
}
