package com.wstuo.common.sla.dao;

import com.wstuo.common.sla.dto.SLAContractQueryDTO;
import com.wstuo.common.sla.entity.SLAContract;
import com.wstuo.common.dao.IEntityDAO;
import com.wstuo.common.dto.PageDTO;


/**
 * SLA服务协议DAO类接口.
 * @author QXY
 *
 */
public interface ISLAContractDAO extends IEntityDAO<SLAContract> {

	/**
	 * 分页查找数据.
	 * @param queryDTO 查询DTO
	 * @param start 开始数据行
	 * @param limit 每页数据条数
	 * @return PageDTO 分页数据
	 */
    PageDTO findPager(SLAContractQueryDTO queryDTO, int start,
        int limit,String sidx,String sord);
    
    /**
     * 根据规则包名查找SLA信息.
     * @param rulePackageNo 规则包No
     * @return SLAContract 
     */
    SLAContract findByRulePackageNo(Long rulePackageNo);
     
    /**
     * 根据服务机构和被服务机构查找SLA
     * @param servicesNo 服务机构No
     * @param byServicesNo 被服务机构 No
     * @return SLAContract
     */
    SLAContract findByServicesNo(Long servicesNo,Long byServicesNo);
    
	/**
	 * 根据服务机构查找SLA.
	 * @param serviceOrgName 服务机构名称
	 * @return SLAContract
	 */
    SLAContract findByServiceOrgName(String serviceOrgName);
    
    /**
     * 设置值.
     * @param latesSLANo 最后一个sla编号
     */
    void setLatesSLANo(Long latesSLANo);
    
    /**
     * 取得下一个编号.
     * @return Long sla协议编号
     */
    Long getNextResNo();
    
    /**
     * 查询服务机构是否存在SLA。
     * @param orgNo 机构No
     * @return  Boolean 
     */
    Boolean isHasSmaeServiceOrg(Long orgNo,Long slaNo);
    
    
}