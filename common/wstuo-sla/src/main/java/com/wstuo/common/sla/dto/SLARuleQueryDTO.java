package com.wstuo.common.sla.dto;

import com.wstuo.common.rules.dto.RuleQueryDTO;

/**
 * SLA Rule Query DTO
 * @author QXY
 *
 */
public class SLARuleQueryDTO extends RuleQueryDTO {

    private Long contractNo;

    public Long getContractNo() {

        return contractNo;
    }

    public void setContractNo(Long contractNo) {

        this.contractNo = contractNo;
    }
}