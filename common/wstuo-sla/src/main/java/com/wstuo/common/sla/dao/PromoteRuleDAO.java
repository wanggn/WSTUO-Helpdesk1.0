package com.wstuo.common.sla.dao;


import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import com.wstuo.common.sla.dto.PromoteRuleQueryDTO;
import com.wstuo.common.sla.entity.PromoteRule;
import com.wstuo.common.dao.BaseDAOImplHibernate;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.util.DaoUtils;

/**
 * 自动升级规则DAO类.
 * @author QXY
 *
 */
public class PromoteRuleDAO extends BaseDAOImplHibernate<PromoteRule>
    implements IPromoteRuleDAO {

	/**
	 * 分页查找自动升级列表.
	 * @param qdto 查询DTO
	 * @param start 开始数据行
	 * @param limit 每页数据条数
	 * @return PageDTO 分页数据
	 */
    public PageDTO findPager(PromoteRuleQueryDTO qdto, int start, int limit,String sidx,String sord) {

        DetachedCriteria dc = DetachedCriteria.forClass(PromoteRule.class);
      
        if(qdto!=null&&qdto.getContractNo() != null){
            dc.createAlias("slaContract", "contract")
              .add(Restrictions.eq("contract.contractNo",
                    qdto.getContractNo()))
                    .add(Restrictions.like("ruleType", qdto.getRuleType(),MatchMode.ANYWHERE));
        }
        //排序
        dc = DaoUtils.orderBy(sidx, sord, dc);
        return super.findPageByCriteria(dc, start, limit);
    }
    
    /**
     * find PromoteRule By ruleType
     * @param ruleType
     * @return List<PromoteRule>
     */
    @SuppressWarnings("unchecked")
    public List<PromoteRule> findPromoteRuleByruleType(String ruleType){
    	final DetachedCriteria dc = DetachedCriteria.forClass(PromoteRule.class);
    	dc.add(Restrictions.eq("ruleType", ruleType));
    	return super.getHibernateTemplate().findByCriteria(dc);
    }

    
}