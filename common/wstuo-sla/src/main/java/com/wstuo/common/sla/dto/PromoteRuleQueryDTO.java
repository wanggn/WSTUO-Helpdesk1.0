package com.wstuo.common.sla.dto;

import com.wstuo.common.rules.dto.RuleDTO;

/**
 * Promote Rule Query DTO
 * @author QXY
 *
 */
public class PromoteRuleQueryDTO extends RuleDTO {
	
	
	public Long contractNo;

	private String ruleType;

	public String getRuleType() {
		return ruleType;
	}

	public void setRuleType(String ruleType) {
		this.ruleType = ruleType;
	}

	public Long getContractNo() {
		return contractNo;
	}

	public void setContractNo(Long contractNo) {
		this.contractNo = contractNo;
	}
	

	
}