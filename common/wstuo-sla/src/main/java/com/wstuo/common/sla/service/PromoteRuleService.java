package com.wstuo.common.sla.service;

import java.util.ArrayList;
import java.util.List;

import com.wstuo.common.security.dao.IUserDAO;
import com.wstuo.common.security.entity.User;
import com.wstuo.common.sla.dao.IPromoteRuleDAO;
import com.wstuo.common.sla.dao.ISLAContractDAO;
import com.wstuo.common.sla.dto.PromoteRuleDTO;
import com.wstuo.common.sla.dto.PromoteRuleQueryDTO;
import com.wstuo.common.sla.entity.PromoteRule;
import com.wstuo.common.config.updatelevel.dao.IUpdateLevelDAO;
import com.wstuo.common.config.updatelevel.entity.UpdateLevel;
import com.wstuo.common.dto.PageDTO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 * 自动升级规则业务类.
 * @author QXY
 *
 */
public class PromoteRuleService implements IPromoteRuleService {

	@Autowired
    private IPromoteRuleDAO promoteRuleDAO;
	@Autowired
    private ISLAContractDAO slaContractDAO;
	@Autowired
	private IUpdateLevelDAO updateLevelDAO;
    @Autowired
    private IUserDAO userDAO;

	/**
	 * 分页查找自动升级列表.
	 * @param qdto 查询DTO
	 * @param start 开始数据行
	 * @param limit 每页数据条数
	 * @return PageDTO 分页数据
	 */
	@SuppressWarnings("unchecked")
	@Transactional
	public PageDTO findPromoteRuleByContractNo(PromoteRuleQueryDTO qdto, int start,
        int limit,String sidx,String sord) {

        PageDTO p = promoteRuleDAO.findPager(qdto, start, limit,sidx,sord);

        List<PromoteRule> entities = p.getData();
        List<PromoteRuleDTO> dtos = new ArrayList<PromoteRuleDTO>();

        for (PromoteRule entity : entities) {

        	PromoteRuleDTO dto = new PromoteRuleDTO();
        	PromoteRuleDTO.entity2dto(entity, dto);


            if(entity.getUpdateLevel()!=null){

            	dto.setAssigneeName(entity.getUpdateLevel().getLoginName());
            	dto.setUpdateLevelNo(entity.getUpdateLevel().getUserId());
        	}
            
            if(entity.getSlaContract()!=null){
            	
            	dto.setContractNo(entity.getSlaContract().getContractNo());
            }

            dtos.add(dto);
        }

        p.setData(dtos);

        return p;
    }

	/**
	 * 根据ID查找自动升级信息.
	 * @param no 自动升级编号
	 * @return PromoteRule 自动升级数据
	 */
    @Transactional
    public PromoteRule findPromoteRuleById(Long no) {
    	return promoteRuleDAO.findById(no);
    }

    
	/**
	 * 修改自动升级信息.
	 * @param promoteRuleDTO 数据DTO
	 * @return PromoteRule
	 */
    @Transactional
    public PromoteRule mergePromoteRuleEntity(PromoteRuleDTO promoteRuleDTO) {
    	
    	PromoteRule entity=promoteRuleDAO.findById(promoteRuleDTO.getRuleNo());
    	
    	PromoteRuleDTO.dto2entity(promoteRuleDTO, entity);
    	
    	
    	/**
    	 * 升级级别
    	 */
    	if(promoteRuleDTO.getUpdateLevelNo()!=null){
    		
    		User updateLevel=userDAO.findById(promoteRuleDTO.getUpdateLevelNo());
    		
    		if(updateLevel!=null){
    			
    			entity.setUpdateLevel(updateLevel);
    		}
    	}
    	
    	
    	
    	
    	return promoteRuleDAO.merge(entity);
        
    }
    
    
	/**
	 * 根据ID删除自动升级.
	 * @param no 自动升级ID
	 */
    @Transactional
    public void removePromoteRule(Long no) {

    	promoteRuleDAO.delete(promoteRuleDAO.findById(no));
    }

	/**
	 * 批量删除自动升级信息.
	 * @param nos 自动升级ID数组
	 */
    @Transactional
    public void removePromoteRules(Long[] nos) {

    	promoteRuleDAO.deleteByIds(nos);
    }

	/**
	 * 保存自动升级.
	 * @param promoteRuleDTO promoteRule
	 */
    @Transactional
    public void savePromoteRuleEntity(PromoteRuleDTO promoteRuleDTO) {
    	PromoteRule entity=new PromoteRule();
    	PromoteRuleDTO.dto2entity(promoteRuleDTO, entity);
    	
    	/**
    	 * 升级级别
    	 */
    	if(promoteRuleDTO.getUpdateLevelNo()!=null){
    		
    		User updateLevel=userDAO.findById(promoteRuleDTO.getUpdateLevelNo());
    		
    		if(updateLevel!=null){
    			
    			entity.setUpdateLevel(updateLevel);
    		}
    	}
    	
    	if(promoteRuleDTO.getContractNo()!=null){
    		
    		entity.setSlaContract(
    		slaContractDAO.findById(promoteRuleDTO.getContractNo())
    		);
    		
    	}
    	

        promoteRuleDAO.save(entity);
        promoteRuleDTO.setRuleNo(entity.getRuleNo());
    }
    
    
	/**
	 * 根据ID查找自动升级信息DTO.
	 * @param no 自动升级编号
	 * @return PromoteRuleDTO 自动升级数据
	 */
    @Transactional
    public PromoteRuleDTO findPromoteRuleDTOById(Long no) {
    	PromoteRuleDTO dto=new PromoteRuleDTO();
    	PromoteRule promoteRule=promoteRuleDAO.findById(no);
    	PromoteRuleDTO.entity2dto(promoteRule, dto);
    	if(promoteRule!=null && promoteRule.getUpdateLevel()!=null){
        	dto.setAssigneeName(promoteRule.getUpdateLevel().getLoginName());
        	dto.setUpdateLevelNo(promoteRule.getUpdateLevel().getUserId());
    	}
    	if(promoteRule!=null && promoteRule.getSlaContract()!=null){
    		dto.setContractNo(promoteRule.getSlaContract().getContractNo());
    	}
    	return dto;
    }
    
}