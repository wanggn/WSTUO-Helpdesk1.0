package com.wstuo.itsm.knowledge.dto;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.struts2.json.annotations.JSON;

import com.wstuo.common.config.attachment.entity.Attachment;
import com.wstuo.common.config.category.dto.EventCategoryDTO;
import com.wstuo.common.config.onlinelog.dto.ADTO;
import com.wstuo.itsm.knowledge.entity.KnowledgeInfo;
import com.wstuo.common.dto.BaseDTO;
/**
 * 知识库信息DTO类
 * @author Eileen
 */
@SuppressWarnings("serial")
@ADTO(id="kid")
public class KnowledgeDTO extends BaseDTO{

    private Long kid;
    private String title;
    private String categoryName;
    private Long categoryNo;
    private String permission;
    private String content;
    private List<Attachment> attachments;
    private String attachmentStr;
    private String keyWords;
    private String remark;
    private String description;
    private Date addTime=new Date();
    private Long clickRate = 0L;
    private Byte dataFlag;
    private String knowledgeStatus;//知识状态True正常，False审核中
    private String statusDesc;//状态描述，即审核失败消息.
    private Date lastUpdateTime;
    
    //服务目录项
    private String serviceDirectoryItemName;
    private Long serviceDirectoryItemNo;
    private Long [] kids;
    private String creator;
    private String creatorFullName;
    
    private List<Long> knowledServiceNo;
    
    private String stars;//星级
    
    private Long count;

    private Long knowledgeNo;
	

	//Attachment aids
    private Long[] aids;
    //服务目录项树
    private String knowledgeServiceName;
    private String knowledgeAppDescRole;//提交审核commt是审核人还是知识创建者
    
    private List<EventCategoryDTO> listEvent=new ArrayList<EventCategoryDTO>();
	
	
	public Long getKnowledgeNo() {
		return knowledgeNo;
	}

	public void setKnowledgeNo(Long knowledgeNo) {
		this.knowledgeNo = knowledgeNo;
	}
    
    public Long getCount() {
		return count;
	}

	public void setCount(Long count) {
		this.count = count;
	}

	public String getStars() {
		return stars;
	}

	public void setStars(String stars) {
		this.stars = stars;
	}
	public String getKnowledgeAppDescRole() {
		return knowledgeAppDescRole;
	}

	public void setKnowledgeAppDescRole(String knowledgeAppDescRole) {
		this.knowledgeAppDescRole = knowledgeAppDescRole;
	}

	public List<EventCategoryDTO> getListEvent() {
		return listEvent;
	}

	public void setListEvent(List<EventCategoryDTO> listEvent) {
		this.listEvent = listEvent;
	}

	public String getKnowledgeServiceName() {
		return knowledgeServiceName;
	}

	public void setKnowledgeServiceName(String knowledgeServiceName) {
		this.knowledgeServiceName = knowledgeServiceName;
	}


	public List<Long> getKnowledServiceNo() {
		return knowledServiceNo;
	}

	public void setKnowledServiceNo(List<Long> knowledServiceNo) {
		this.knowledServiceNo = knowledServiceNo;
	}

	public String getServiceDirectoryItemName() {
		return serviceDirectoryItemName;
	}

	public void setServiceDirectoryItemName(String serviceDirectoryItemName) {
		this.serviceDirectoryItemName = serviceDirectoryItemName;
	}

	public Long getServiceDirectoryItemNo() {
		return serviceDirectoryItemNo;
	}

	public void setServiceDirectoryItemNo(Long serviceDirectoryItemNo) {
		this.serviceDirectoryItemNo = serviceDirectoryItemNo;
	}

	public String getShortTitle() {
		if(title!=null){
			if(title.trim().length()>12){
				return title.trim().substring(0,12)+"..";
			}else{
				return title.trim();
			}
		}
		return null;
	}
	public Long getKid() {
		return kid;
	}
	public void setKid(Long kid) {
		this.kid = kid;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public Long getCategoryNo() {
		return categoryNo;
	}
	public void setCategoryNo(Long categoryNo) {
		this.categoryNo = categoryNo;
	}
	public String getPermission() {
		return permission;
	}
	public void setPermission(String permission) {
		this.permission = permission;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public List<Attachment> getAttachments() {
		return attachments;
	}
	public void setAttachments(List<Attachment> attachments) {
		this.attachments = attachments;
	}
	public String getAttachmentStr() {
		return attachmentStr;
	}
	public void setAttachmentStr(String attachmentStr) {
		this.attachmentStr = attachmentStr;
	}
	public String getKeyWords() {
		return keyWords;
	}
	public void setKeyWords(String keyWords) {
		this.keyWords = keyWords;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	@JSON(format ="yyyy-MM-dd HH:mm:ss")
	public Date getAddTime() {
		return addTime;
	}
	public void setAddTime(Date addTime) {
		this.addTime = addTime;
	}
	public Long getClickRate() {
		return clickRate;
	}
	public void setClickRate(Long clickRate) {
		this.clickRate = clickRate;
	}
	public Byte getDataFlag() {
		return dataFlag;
	}
	public void setDataFlag(Byte dataFlag) {
		this.dataFlag = dataFlag;
	}

	public Long[] getAids() {
		return aids;
	}

	public void setAids(Long[] aids) {
		this.aids = aids;
	}

	public KnowledgeDTO(){
		
	}
	
	public KnowledgeDTO(Long kid, String title, String categoryName,
			Long categoryNo, String permission, String content,
			List<Attachment> attachments, String attachmentStr,
			String keyWords, String remark, String description, Date addTime,
			Long clickRate, Byte dataFlag) {
		super();
		this.kid = kid;
		this.title = title;
		this.categoryName = categoryName;
		this.categoryNo = categoryNo;
		this.permission = permission;
		this.content = content;
		this.attachments = attachments;
		this.attachmentStr = attachmentStr;
		this.keyWords = keyWords;
		this.remark = remark;
		this.description = description;
		this.addTime = addTime;
		this.clickRate = clickRate;
		this.dataFlag = dataFlag;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((addTime == null) ? 0 : addTime.hashCode());
		result = prime * result + Arrays.hashCode(aids);
		result = prime * result
				+ ((attachmentStr == null) ? 0 : attachmentStr.hashCode());
		result = prime * result
				+ ((attachments == null) ? 0 : attachments.hashCode());
		result = prime * result
				+ ((categoryName == null) ? 0 : categoryName.hashCode());
		result = prime * result
				+ ((categoryNo == null) ? 0 : categoryNo.hashCode());
		result = prime * result
				+ ((clickRate == null) ? 0 : clickRate.hashCode());
		result = prime * result + ((content == null) ? 0 : content.hashCode());
		result = prime * result + ((creator == null) ? 0 : creator.hashCode());
		result = prime * result
				+ ((creatorFullName == null) ? 0 : creatorFullName.hashCode());
		result = prime * result
				+ ((dataFlag == null) ? 0 : dataFlag.hashCode());
		result = prime * result
				+ ((description == null) ? 0 : description.hashCode());
		result = prime * result
				+ ((keyWords == null) ? 0 : keyWords.hashCode());
		result = prime * result + ((kid == null) ? 0 : kid.hashCode());
		result = prime * result + Arrays.hashCode(kids);
		result = prime
				* result
				+ ((knowledServiceNo == null) ? 0 : knowledServiceNo.hashCode());
		result = prime
				* result
				+ ((knowledgeAppDescRole == null) ? 0 : knowledgeAppDescRole
						.hashCode());
		result = prime
				* result
				+ ((knowledgeServiceName == null) ? 0 : knowledgeServiceName
						.hashCode());
		result = prime * result
				+ ((knowledgeStatus == null) ? 0 : knowledgeStatus.hashCode());
		result = prime * result
				+ ((lastUpdateTime == null) ? 0 : lastUpdateTime.hashCode());
		result = prime * result
				+ ((listEvent == null) ? 0 : listEvent.hashCode());
		result = prime * result
				+ ((permission == null) ? 0 : permission.hashCode());
		result = prime * result + ((remark == null) ? 0 : remark.hashCode());
		result = prime
				* result
				+ ((serviceDirectoryItemName == null) ? 0
						: serviceDirectoryItemName.hashCode());
		result = prime
				* result
				+ ((serviceDirectoryItemNo == null) ? 0
						: serviceDirectoryItemNo.hashCode());
		result = prime * result
				+ ((statusDesc == null) ? 0 : statusDesc.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		KnowledgeDTO other = (KnowledgeDTO) obj;
		if (addTime == null) {
			if (other.addTime != null)
				return false;
		} else if (!addTime.equals(other.addTime))
			return false;
		if (!Arrays.equals(aids, other.aids))
			return false;
		if (attachmentStr == null) {
			if (other.attachmentStr != null)
				return false;
		} else if (!attachmentStr.equals(other.attachmentStr))
			return false;
		if (attachments == null) {
			if (other.attachments != null)
				return false;
		} else if (!attachments.equals(other.attachments))
			return false;
		if (categoryName == null) {
			if (other.categoryName != null)
				return false;
		} else if (!categoryName.equals(other.categoryName))
			return false;
		if (categoryNo == null) {
			if (other.categoryNo != null)
				return false;
		} else if (!categoryNo.equals(other.categoryNo))
			return false;
		if (clickRate == null) {
			if (other.clickRate != null)
				return false;
		} else if (!clickRate.equals(other.clickRate))
			return false;
		if (content == null) {
			if (other.content != null)
				return false;
		} else if (!content.equals(other.content))
			return false;
		if (creator == null) {
			if (other.creator != null)
				return false;
		} else if (!creator.equals(other.creator))
			return false;
		if (creatorFullName == null) {
			if (other.creatorFullName != null)
				return false;
		} else if (!creatorFullName.equals(other.creatorFullName))
			return false;
		if (dataFlag == null) {
			if (other.dataFlag != null)
				return false;
		} else if (!dataFlag.equals(other.dataFlag))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (keyWords == null) {
			if (other.keyWords != null)
				return false;
		} else if (!keyWords.equals(other.keyWords))
			return false;
		if (kid == null) {
			if (other.kid != null)
				return false;
		} else if (!kid.equals(other.kid))
			return false;
		if (!Arrays.equals(kids, other.kids))
			return false;
		if (knowledServiceNo == null) {
			if (other.knowledServiceNo != null)
				return false;
		} else if (!knowledServiceNo.equals(other.knowledServiceNo))
			return false;
		if (knowledgeAppDescRole == null) {
			if (other.knowledgeAppDescRole != null)
				return false;
		} else if (!knowledgeAppDescRole.equals(other.knowledgeAppDescRole))
			return false;
		if (knowledgeServiceName == null) {
			if (other.knowledgeServiceName != null)
				return false;
		} else if (!knowledgeServiceName.equals(other.knowledgeServiceName))
			return false;
		if (knowledgeStatus == null) {
			if (other.knowledgeStatus != null)
				return false;
		} else if (!knowledgeStatus.equals(other.knowledgeStatus))
			return false;
		if (lastUpdateTime == null) {
			if (other.lastUpdateTime != null)
				return false;
		} else if (!lastUpdateTime.equals(other.lastUpdateTime))
			return false;
		if (listEvent == null) {
			if (other.listEvent != null)
				return false;
		} else if (!listEvent.equals(other.listEvent))
			return false;
		if (permission == null) {
			if (other.permission != null)
				return false;
		} else if (!permission.equals(other.permission))
			return false;
		if (remark == null) {
			if (other.remark != null)
				return false;
		} else if (!remark.equals(other.remark))
			return false;
		if (serviceDirectoryItemName == null) {
			if (other.serviceDirectoryItemName != null)
				return false;
		} else if (!serviceDirectoryItemName
				.equals(other.serviceDirectoryItemName))
			return false;
		if (serviceDirectoryItemNo == null) {
			if (other.serviceDirectoryItemNo != null)
				return false;
		} else if (!serviceDirectoryItemNo.equals(other.serviceDirectoryItemNo))
			return false;
		if (statusDesc == null) {
			if (other.statusDesc != null)
				return false;
		} else if (!statusDesc.equals(other.statusDesc))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}
	
	
	/**
	 * 
	 * @param entity
	 * @param dto
	 */
	public void entity2dto(KnowledgeInfo entity, KnowledgeDTO dto){
		
		super.entity2dto(entity, dto);
		if(entity != null && entity.getCategory()!=null){
        	dto.setCategoryName( entity.getCategory().getEventName() );
			dto.setCategoryNo( entity.getCategory().getEventId() );
        }
		
		//服务
        /*if(entity.getServiceDirectoryItem()!=null){
        	
        	dto.setServiceDirectoryItemName(entity.getServiceDirectoryItem().getSubServiceName());
        	dto.setServiceDirectoryItemNo(entity.getServiceDirectoryItem().getSubServiceId());
        }*/
		/*if(entity.getServiceLists()!=null){
			List<EventCategory> li=entity.getServiceLists();
			for(EventCategory ev:li){
				dto.setKnowledgeServiceName(ev.getEventName());
			}
		}*/
		
		
	}

	public String getKnowledgeStatus() {
		return knowledgeStatus;
	}

	public void setKnowledgeStatus(String knowledgeStatus) {
		this.knowledgeStatus = knowledgeStatus;
	}

	public String getStatusDesc() {
		return statusDesc;
	}

	public void setStatusDesc(String statusDesc) {
		this.statusDesc = statusDesc;
	}

	public Long[] getKids() {
		return kids;
	}

	public void setKids(Long[] kids) {
		this.kids = kids;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public Date getLastUpdateTime() {
		return lastUpdateTime;
	}

	public void setLastUpdateTime(Date lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}

	public String getCreatorFullName() {
		return creatorFullName;
	}

	public void setCreatorFullName(String creatorFullName) {
		this.creatorFullName = creatorFullName;
	}

	@Override
	public String toString() {
		return "KnowledgeDTO [kid=" + kid + ", title=" + title
				+ ", categoryName=" + categoryName + ", categoryNo="
				+ categoryNo + ", permission=" + permission + ", content="
				+ content + ", attachments=" + attachments + ", attachmentStr="
				+ attachmentStr + ", keyWords=" + keyWords + ", remark="
				+ remark + ", description=" + description + ", addTime="
				+ addTime + ", clickRate=" + clickRate + ", dataFlag="
				+ dataFlag + ", knowledgeStatus=" + knowledgeStatus
				+ ", statusDesc=" + statusDesc + ", lastUpdateTime="
				+ lastUpdateTime + ", serviceDirectoryItemName="
				+ serviceDirectoryItemName + ", serviceDirectoryItemNo="
				+ serviceDirectoryItemNo + ", kids=" + Arrays.toString(kids)
				+ ", creator=" + creator + ", creatorFullName="
				+ creatorFullName + ", knowledServiceNo=" + knowledServiceNo
				+ ", aids=" + Arrays.toString(aids) + ", knowledgeServiceName="
				+ knowledgeServiceName + ", knowledgeAppDescRole="
				+ knowledgeAppDescRole + ", listEvent=" + listEvent + "]";
	}

	
}