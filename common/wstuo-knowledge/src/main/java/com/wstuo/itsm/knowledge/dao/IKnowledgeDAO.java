package com.wstuo.itsm.knowledge.dao;

import com.wstuo.common.config.customfilter.entity.CustomFilter;

import com.wstuo.itsm.knowledge.dto.KnowledgeQueryDTO;
import com.wstuo.itsm.knowledge.entity.KnowledgeInfo;
import com.wstuo.common.dao.IEntityDAO;
import com.wstuo.common.dto.PageDTO;

import java.util.List;

/**
 * 知识库DAO接口.
 * @author QXY
 */
public interface IKnowledgeDAO
    extends IEntityDAO<KnowledgeInfo>
{
    /**
     * 分页查找知识信息.
     * @param querydto
     * @param start
     * @param limit
     * @return PageDTO
     */
    PageDTO findAllKnowledgeInfos( KnowledgeQueryDTO querydto, int start, int limit, String sidx, String sord );
    
    /**
	 * 查找热门知识/最新知识.
	 * @param queryType
	 * @param categoryIds
	 */
	List<KnowledgeInfo> findKnowledgeByIds(String queryType,Long [] categoryIds);
	/**
	 * 根据条件统计
	 * @param qdto
	 * @return
	 */
	Integer countChangeByType(KnowledgeQueryDTO qdto);
	List<KnowledgeInfo> findListKnowledgeInfos(KnowledgeQueryDTO querydto,
			String sidx, String sord);
	/**
     * 通过过滤器查询实体数据
     * @param filter
     * @return PageDTO
     * 
     */
	PageDTO findPageByCustomFilter(CustomFilter filter,KnowledgeQueryDTO queryDTO,
			List<Long> liid, int start , int limit,String sidx,String sord);
	
	List<KnowledgeInfo> findListByCustomFilter(CustomFilter filter,Long [] companyNos,List<Long> liid,String sidx,String sord);
    
	/**
	 * 服务目录查询
	 * @param filter
	 * @return
	 */
	List<Object> findByServiceDirectoryFilter(CustomFilter filter);

}
