package com.wstuo.itsm.knowledge.service;



public interface ITableUpdate {
	/**
	 * 查询原服务目录信息
	 * */
	boolean subServiceList();
	/**
	 * 查询原子服务目录
	 * */
	boolean serviceDirectoryList();
}
