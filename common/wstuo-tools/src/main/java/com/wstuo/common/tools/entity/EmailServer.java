package com.wstuo.common.tools.entity;

import com.wstuo.common.entity.BaseEntity;

import javax.persistence.Cacheable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * ServicesEmail Entity class.
 * @author QXY
 */
@SuppressWarnings( {"serial",
    "rawtypes"
} )
@Entity
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class EmailServer
    extends BaseEntity
{
    /**
     * ServicesEmail number
     */
	@Id
    @GeneratedValue( strategy = GenerationType.AUTO )
    private Long emailServerId;

    /**
     * server address
     */
    private String smtpServerAddress;

    /**
    * server port
    */
    private String smtpServerPort;

    /**
     * server address
     */
    private String pop3ServerAddress;

    /**
     * server port
     */
    private String pop3ServerPort;

    /**
     * User name
     */
    private String userName;

    /**
     * Password
     */
    private String password;

    /**
     * Attestation;
     */
    private Boolean attestation = false;
    
    private String emailType;//邮件类型(exchange)
    private String emailVersion;//邮件版本
    
    private String exchangeHostName;//服务器
    
    
    private String exchangeUserName;//用户名
    
    private String exchangePassword;//密码
    
    private String domain;//域
    
    private String specifyConnectionUrl;//指定连接的URL
    
    private Boolean useHttps=true;//使用HTTPS链接

    private String exchangeEmailAccount;//邮件账号
    
    /**发件人姓名*/
    private String personal;
    /**发件人邮件地址 */
    private String personalEmailAddress;
    
    
    public Long getEmailServerId(  )
    {
        return emailServerId;
    }

    public void setEmailServerId( Long emailServerId )
    {
        this.emailServerId = emailServerId;
    }

    public String getSmtpServerAddress(  )
    {
        return smtpServerAddress;
    }

    public void setSmtpServerAddress( String smtpServerAddress )
    {
        this.smtpServerAddress = smtpServerAddress;
    }

    public String getSmtpServerPort(  )
    {
        return smtpServerPort;
    }

    public void setSmtpServerPort( String smtpServerPort )
    {
        this.smtpServerPort = smtpServerPort;
    }

    public String getPop3ServerAddress(  )
    {
        return pop3ServerAddress;
    }

    public void setPop3ServerAddress( String pop3ServerAddress )
    {
        this.pop3ServerAddress = pop3ServerAddress;
    }

    public String getPop3ServerPort(  )
    {
        return pop3ServerPort;
    }

    public void setPop3ServerPort( String pop3ServerPort )
    {
        this.pop3ServerPort = pop3ServerPort;
    }

    public String getUserName(  )
    {
        return userName;
    }

    public void setUserName( String userName )
    {
        this.userName = userName;
    }

    public String getPassword(  )
    {
        return password;
    }

    public void setPassword( String password )
    {
        this.password = password;
    }


	public String getEmailType() {
		return emailType;
	}

	public void setEmailType(String emailType) {
		this.emailType = emailType;
	}

	public String getExchangeHostName() {
		return exchangeHostName;
	}

	public void setExchangeHostName(String exchangeHostName) {
		this.exchangeHostName = exchangeHostName;
	}

	public String getExchangeUserName() {
		return exchangeUserName;
	}

	public void setExchangeUserName(String exchangeUserName) {
		this.exchangeUserName = exchangeUserName;
	}

	public String getExchangePassword() {
		return exchangePassword;
	}

	public void setExchangePassword(String exchangePassword) {
		this.exchangePassword = exchangePassword;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public String getSpecifyConnectionUrl() {
		return specifyConnectionUrl;
	}

	public void setSpecifyConnectionUrl(String specifyConnectionUrl) {
		this.specifyConnectionUrl = specifyConnectionUrl;
	}
	public String getExchangeEmailAccount() {
		return exchangeEmailAccount;
	}

	public void setExchangeEmailAccount(String exchangeEmailAccount) {
		this.exchangeEmailAccount = exchangeEmailAccount;
	}

	public String getPersonal() {
		return personal;
	}

	public void setPersonal(String personal) {
		this.personal = personal;
	}

	public String getPersonalEmailAddress() {
		return personalEmailAddress;
	}

	public void setPersonalEmailAddress(String personalEmailAddress) {
		this.personalEmailAddress = personalEmailAddress;
	}

	public Boolean getAttestation() {
		return attestation;
	}

	public void setAttestation(Boolean attestation) {
		this.attestation = attestation;
	}

	public Boolean getUseHttps() {
		return useHttps;
	}

	public void setUseHttps(Boolean useHttps) {
		this.useHttps = useHttps;
	}

	public String getEmailVersion() {
		return emailVersion;
	}

	public void setEmailVersion(String emailVersion) {
		this.emailVersion = emailVersion;
	}
	
}
