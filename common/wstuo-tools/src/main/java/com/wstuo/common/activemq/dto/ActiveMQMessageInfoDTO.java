package com.wstuo.common.activemq.dto;


import com.wstuo.common.dto.BaseDTO;

/**
 * 消息队列消息详细信息DTO
 * @author WSTUO
 *
 */
@SuppressWarnings("serial")
public class ActiveMQMessageInfoDTO extends BaseDTO{
	private String messageID;
	private Integer priority;
	private Boolean redelivered = false;
	private String timestamp;
	private String mqTitle;//MQ执行的主题
    private String mqContent;//MQ执行的内容
    private String mqCreator;//MQ创建者
	
    
	public String getMqTitle() {
		return mqTitle;
	}
	public void setMqTitle(String mqTitle) {
		this.mqTitle = mqTitle;
	}
	public String getMqContent() {
		return mqContent;
	}
	public void setMqContent(String mqContent) {
		this.mqContent = mqContent;
	}
	public String getMqCreator() {
		return mqCreator;
	}
	public void setMqCreator(String mqCreator) {
		this.mqCreator = mqCreator;
	}
	public String getMessageID() {
		return messageID;
	}
	public void setMessageID(String messageID) {
		this.messageID = messageID;
	}
	public Integer getPriority() {
		return priority;
	}
	public void setPriority(Integer priority) {
		this.priority = priority;
	}
	public Boolean getRedelivered() {
		return redelivered;
	}
	public void setRedelivered(Boolean redelivered) {
		this.redelivered = redelivered;
	}
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	
	
}
