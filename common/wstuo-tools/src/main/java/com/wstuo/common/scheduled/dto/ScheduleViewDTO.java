package com.wstuo.common.scheduled.dto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 人员行程显示DTO
 * @author WSTUO
 *
 */
public class ScheduleViewDTO {
	//全部行程最大的时间
	private String minimumTime;
	//全部行程最大的时间
	private String maximumTime;
	//行程数据
	private List<ScheduleShowDataDTO> scheduleShowDataDTO = new ArrayList<ScheduleShowDataDTO>();
	//进展及成本关闭的请求信息
	private Map<String, ScheduleEventDTO> events = new HashMap<String, ScheduleEventDTO>();
	public String getMinimumTime() {
		return minimumTime;
	}
	public void setMinimumTime(String minimumTime) {
		this.minimumTime = minimumTime;
	}
	public String getMaximumTime() {
		return maximumTime;
	}
	public void setMaximumTime(String maximumTime) {
		this.maximumTime = maximumTime;
	}
	public List<ScheduleShowDataDTO> getScheduleShowDataDTO() {
		return scheduleShowDataDTO;
	}
	public void setScheduleShowDataDTO(List<ScheduleShowDataDTO> scheduleShowDataDTO) {
		this.scheduleShowDataDTO = scheduleShowDataDTO;
	}
	public Map<String, ScheduleEventDTO> getEvents() {
		return events;
	}
	public void setEvents(Map<String, ScheduleEventDTO> events) {
		this.events = events;
	}
	
	
}
