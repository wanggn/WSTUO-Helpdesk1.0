package com.wstuo.common.tools.action;

import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;
import com.wstuo.common.tools.dto.ExportInfoDTO;
import com.wstuo.common.tools.service.IExportInfoService;
import com.wstuo.common.dto.PageDTO;

/**
 * 导出信息Action
 * @author WSTUO
 *
 */
@SuppressWarnings("serial")
public class ExportInfoAction extends ActionSupport{
	private ExportInfoDTO exportInfoDTO;
	@Autowired
	private IExportInfoService exportInfoService;
	private Long[] ids;
	private int page = 1;
    private int rows = 10;
    private String sord;
    private String sidx;
    private PageDTO pageDTO;
    
    
	public Long[] getIds() {
		return ids;
	}
	public void setIds(Long[] ids) {
		this.ids = ids;
	}
	public ExportInfoDTO getExportInfoDTO() {
		return exportInfoDTO;
	}
	public void setExportInfoDTO(ExportInfoDTO exportInfoDTO) {
		this.exportInfoDTO = exportInfoDTO;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public int getRows() {
		return rows;
	}
	public void setRows(int rows) {
		this.rows = rows;
	}
	public String getSord() {
		return sord;
	}
	public void setSord(String sord) {
		this.sord = sord;
	}
	public String getSidx() {
		return sidx;
	}
	public void setSidx(String sidx) {
		this.sidx = sidx;
	}
	public PageDTO getPageDTO() {
		return pageDTO;
	}
	public void setPageDTO(PageDTO pageDTO) {
		this.pageDTO = pageDTO;
	}
    
	/**
	 * 获取所有导出信息
	 * @return pageDTO
	 */
	public String findExportInfoPager(){
		int start = (page - 1) * rows;
		pageDTO=exportInfoService.findExportInfoPager(exportInfoDTO, start, rows, sord, sidx);
		pageDTO.setRows(rows);
		pageDTO.setPage(page);
		return SUCCESS;
	}
	/**
	 * 删除导出信息
	 * @return null
	 */
	public String deleteExportInfos(){
		exportInfoService.deleteExportInfo(ids);
		return SUCCESS;
	}
}
