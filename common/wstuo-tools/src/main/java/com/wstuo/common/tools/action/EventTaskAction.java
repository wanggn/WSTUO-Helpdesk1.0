package com.wstuo.common.tools.action;

import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;
import com.wstuo.common.tools.dto.EventTaskDTO;
import com.wstuo.common.tools.dto.EventTaskQueryDTO;
import com.wstuo.common.tools.service.IEventTaskService;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.exception.ApplicationException;

/**
 * 事件任务Action
 * @author WSTUO
 *
 */
@SuppressWarnings("serial")
public class EventTaskAction extends ActionSupport {
	@Autowired
	private IEventTaskService eventTaskService;
    private PageDTO pageDTO;
    private EventTaskDTO eventTaskDTO;
    private Long eventTaskId;
    private Long[] ids;
    private int page = 1;
    private int rows = 10;
    private EventTaskQueryDTO queryDTO;
    private boolean result;
    private String sord;
    private String sidx;
	public PageDTO getPageDTO() {
		return pageDTO;
	}
	public void setPageDTO(PageDTO pageDTO) {
		this.pageDTO = pageDTO;
	}
	public EventTaskDTO getEventTaskDTO() {
		return eventTaskDTO;
	}
	public void setEventTaskDTO(EventTaskDTO eventTaskDTO) {
		this.eventTaskDTO = eventTaskDTO;
	}
	public Long getEventTaskId() {
		return eventTaskId;
	}
	public void setEventTaskId(Long eventTaskId) {
		this.eventTaskId = eventTaskId;
	}
	public Long[] getIds() {
		return ids;
	}
	public void setIds(Long[] ids) {
		this.ids = ids;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public int getRows() {
		return rows;
	}
	public void setRows(int rows) {
		this.rows = rows;
	}
	
	public EventTaskQueryDTO getQueryDTO() {
		return queryDTO;
	}
	public void setQueryDTO(EventTaskQueryDTO queryDTO) {
		this.queryDTO = queryDTO;
	}

	public boolean isResult() {
		return result;
	}
	public void setResult(boolean result) {
		this.result = result;
	}
	/**
	 * 分页查询事件任务
	 * @return SUCCESS
	 */
	public String findPagerChangeTask(){
		
		int start = ( page - 1 ) * rows;
		if(queryDTO!=null){
			queryDTO.setSidx(sidx);
			queryDTO.setSord(sord);
		}
		pageDTO =eventTaskService.findPagerEventTask(queryDTO, start, rows);
		pageDTO.setPage( page );
		pageDTO.setRows( rows );
		
		return SUCCESS;
	}
	
	/**
	 * 保存任务
	 * @return String
	 */
	public String saveEventTask(){
		eventTaskService.saveEventTask(eventTaskDTO);
		return SUCCESS;
	}
	
	/**
	 * 编辑任务
	 * @return String
	 */
	public String editEventTask(){
		eventTaskService.editEventTask(eventTaskDTO);
		return SUCCESS;
	}
	
	
	/**
	 * 保存任务
	 * @return String
	 */
	public String deleteEventTask(){
		try {
			eventTaskService.deleteEventTask(ids);
		} catch (Exception e) {
			// TODO: handle exception
			throw new ApplicationException("ERROR_DATA_CAN_NOT_DELETE",e);
		}
		
		return SUCCESS;
	}
	
	
	/**
	 * 查询指定时间段任务
	 * @return String
	 */
	public String timeConflict(){
		result=eventTaskService.timeConflict(eventTaskDTO);
		return "result";
	}
	
	/**
	 *  根据ID查找
	 * @return eventTaskDTO
	 */
	public String findById(){
		eventTaskDTO = eventTaskService.findEventTaskById(eventTaskId);
		return "eventTaskDTO";
	}
	
	/**
	 * 根据ID查询任务是否存在
	 * @return result
	 */
	public String findTaskByIds(){
		result = eventTaskService.findTaskByIds(ids);
		return "result";
	}
	
	
	public String getSord() {
		return sord;
	}
	public void setSord(String sord) {
		this.sord = sord;
	}
	public String getSidx() {
		return sidx;
	}
	public void setSidx(String sidx) {
		this.sidx = sidx;
	}
    
}
