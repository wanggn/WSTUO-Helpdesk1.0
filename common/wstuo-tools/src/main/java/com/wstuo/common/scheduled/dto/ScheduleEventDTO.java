package com.wstuo.common.scheduled.dto;

/**
 * 人员行程 事件DTO
 * @author Administrator
 *
 */
public class ScheduleEventDTO {
	//Ticket No
	private String eventCode;
	//标题
	private String title;
	//分类
	private String category;
	//状态
	private String status;
	public String getEventCode() {
		return eventCode;
	}
	public void setEventCode(String eventCode) {
		this.eventCode = eventCode;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
}
