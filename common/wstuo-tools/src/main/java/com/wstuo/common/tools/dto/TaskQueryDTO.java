package com.wstuo.common.tools.dto;

import java.net.URLDecoder;
import java.util.Date;

import com.wstuo.common.util.TimeUtils;
/**
 * 任务查询DTO
 * @author QXY
 *
 */
public class TaskQueryDTO {
	private String introduction;
	private Date startTime;
	private Date endTime;
	private Long taskStatus;
	private Long taskType;
	private String owner;
	private String title;
	private String location;
	private String creator;
	private Date realStartTime;
	private Date realEndTime;
	private Double realFree;
	private Long ownerNo;
	private String treatmentResults;//处理结果
	private String typeDno;//类型编码
	private String sord;
    private String sidx;
    private Long dcode;//类型编号
    private boolean cycle = true;
    private String noOwner;
	

	public String getNoOwner() {
		return noOwner;
	}
	public void setNoOwner(String noOwner) {
		this.noOwner = noOwner;
	}
	public Long getDcode() {
		return dcode;
	}
	public void setDcode(Long dcode) {
		this.dcode = dcode;
	}
	public String getSord() {
		return sord;
	}
	public void setSord(String sord) {
		this.sord = sord;
	}
	public String getSidx() {
		return sidx;
	}
	public void setSidx(String sidx) {
		this.sidx = sidx;
	}
	public String getTypeDno() {
		return typeDno;
	}
	public void setTypeDno(String typeDno) {
		this.typeDno = typeDno;
	}
	public Long getOwnerNo() {
		return ownerNo;
	}
	public void setOwnerNo(Long ownerNo) {
		this.ownerNo = ownerNo;
	}
	public String getTreatmentResults() {
		return treatmentResults;
	}
	public void setTreatmentResults(String treatmentResults) {
		this.treatmentResults = treatmentResults;
	}
	public Date getRealStartTime() {
		return realStartTime;
	}
	public void setRealStartTime(Date realStartTime) {
		this.realStartTime = realStartTime;
	}
	public Date getRealEndTime() {
		return realEndTime;
	}
	public void setRealEndTime(Date realEndTime) {
		this.realEndTime = realEndTime;
	}
	public Double getRealFree() {
		return realFree;
	}
	public void setRealFree(Double realFree) {
		this.realFree = realFree;
	}
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
	public String getIntroduction() {
		return introduction;
	}
	public void setIntroduction(String introduction) {
		this.introduction = introduction;
	}
	public Date getStartTime() {
		return startTime;
	}
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public Long getTaskStatus() {
		return taskStatus;
	}
	public void setTaskStatus(Long taskStatus) {
		this.taskStatus = taskStatus;
	}
	public Long getTaskType() {
		return taskType;
	}
	public void setTaskType(Long taskType) {
		this.taskType = taskType;
	}
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		if (title != null ) {//由于前台的日程表控件不方便以Post方式提交；
			try {
				title = URLDecoder.decode( title , "utf-8");
			} catch (Exception e) {	}
		}
		this.title = title;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public boolean isCycle() {
		return cycle;
	}
	public void setCycle(boolean cycle) {
		this.cycle = cycle;
	}
	public TaskQueryDTO(Date startTime, Date endTime) {
		super();
		this.startTime = startTime;
		this.endTime = endTime;
	}
	public TaskQueryDTO() {
		super();
	}
	
	public static TaskQueryDTO chenkDTO( TaskQueryDTO dto ) {
		if (dto == null ) {
			dto = new TaskQueryDTO();
		}
		if (dto.getStartTime() == null && dto.getEndTime() == null) {
			dto.setStartTime( TimeUtils.getMonthFirstDay( null ) );
			dto.setEndTime( TimeUtils.getMonthLastDay( null ) );
		}
		return dto;
	}
}
