package com.wstuo.common.tools.dto;

import com.wstuo.common.dto.AbstractValueObject;
/**
 * MQ导出发送信息DTO
 * @author will
 *
 */
@SuppressWarnings("serial")
public class ExportQueryDTO extends AbstractValueObject{

	private String type;//导出类型
	private Object queryDTO;//导出查询条件DTO
	private String tenantId;
	private String path;
	
	
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Object getQueryDTO() {
		return queryDTO;
	}
	public void setQueryDTO(Object queryDTO) {
		this.queryDTO = queryDTO;
	}
	public String getTenantId() {
		return tenantId;
	}
	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	} 
	
	
}
