package com.wstuo.common.noticeRule.dto;

import com.wstuo.common.dto.BaseDTO;

/**
 * 模板变更DTO
 * @author WSTUO
 *
 */
@SuppressWarnings("serial")
public class TemplateVariableDTO extends BaseDTO {
	private String eno;
	//编码
    private String eventCode;
    private String edesc;
    //指派技术员
    private String assigneeName;
    private String operator;//操作者
    private String url;
    private String randomParmone;
    private String randomParmtwo;
    private String tenantId;
    
	public String getEno() {
		return eno;
	}
	public void setEno(String eno) {
		this.eno = eno;
	}
	public String getEventCode() {
		return eventCode;
	}
	public void setEventCode(String eventCode) {
		this.eventCode = eventCode;
	}
	public String getEdesc() {
		return edesc;
	}
	public void setEdesc(String edesc) {
		this.edesc = edesc;
	}
	public String getAssigneeName() {
		return assigneeName;
	}
	public void setAssigneeName(String assigneeName) {
		this.assigneeName = assigneeName;
	}
	public String getOperator() {
		return operator;
	}
	public void setOperator(String operator) {
		this.operator = operator;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getRandomParmone() {
		return randomParmone;
	}
	public void setRandomParmone(String randomParmone) {
		this.randomParmone = randomParmone;
	}
	public String getRandomParmtwo() {
		return randomParmtwo;
	}
	public void setRandomParmtwo(String randomParmtwo) {
		this.randomParmtwo = randomParmtwo;
	}
	public String getTenantId() {
		return tenantId;
	}
	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}
    
}
