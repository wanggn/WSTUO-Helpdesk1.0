package com.wstuo.common.tools.service;

import java.util.List;

import com.wstuo.common.tools.dto.ExportInfoDTO;
import com.wstuo.common.tools.entity.ExportInfo;
import com.wstuo.common.dto.PageDTO;

public interface IExportInfoService {
	/**
	 * csv文件的扩展名常量
	 */
	public static final String FILE_EXTENSION = ".csv";
	/**
	 * 导出的文件的表头字符串的分隔符
	 */
	public static final String STRING_SEPARATOR = "--";
	/**
	 *分页获取下载信息
	 * @param exportInfoDTO
	 * @param start
	 * @param limit
	 * @param sord
	 * @param sidx
	 * @return PageDTO
	 */
	PageDTO findExportInfoPager(ExportInfoDTO exportInfoDTO, int start, int limit,String sord, String sidx);
	/**
	 * 删除导出信息
	 * @param ids
	 */
	void deleteExportInfo(Long[] ids);
	/**
	 * 保存导出信息
	 * @param type
	 * @return ExportInfo
	 */
	ExportInfo saveExportInfoReturnEntity(String type);
	
	/**
	 * 导出CSV公共方法
	 * @param data
	 * @param entity
	 */
	void exportCommonCSV(List<String[]> data,ExportInfo entity,String path);
	/**
	 * 导出xls公共方法
	 * @param data
	 * @param colNames
	 * @param filePath
	 * @param sheetName
	 * @param fileName
	 */
	//void exportCommonXls(List<String[]> data,String[] colNames,String sheetName,String filePath,String fileName);
	/**
	 * 导出普通CSV文件；
	 * @param path 导出路径
	 * @param data 导出的数据
	 */
	void exportCSV(String path,List<String[]> data);
}
