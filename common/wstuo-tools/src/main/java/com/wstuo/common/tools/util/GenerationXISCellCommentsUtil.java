package com.wstuo.common.tools.util;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFComment;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;

public class GenerationXISCellCommentsUtil {
	/**
	 * 将批注添加到单元格对象中
	 * @param row
	 * @param cellstyle
	 * @param col
	 * @param val
	 * @param comment
	 */
	@SuppressWarnings("deprecation")
	public static void createCell(HSSFRow row, HSSFCellStyle cellstyle,short col, String val, HSSFComment comment) {
		HSSFCell cell = row.createCell(col);
		cell.setCellStyle(cellstyle);
		// 将批注添加到单元格对象中
		cell.setCellComment(comment);
		HSSFRichTextString cellString = new HSSFRichTextString(val);
		cell.setCellValue(cellString);
	}
}
