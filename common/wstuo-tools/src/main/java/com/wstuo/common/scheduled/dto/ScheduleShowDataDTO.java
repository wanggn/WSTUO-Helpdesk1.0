package com.wstuo.common.scheduled.dto;

import java.util.ArrayList;
import java.util.List;


/**
 * 人员行程任务DTO Class
 * @author WSTUO
 *
 */
public class ScheduleShowDataDTO {
	//Project、Tickets、Task
	private String type;
	//顾问工号
	private String loginName;
	private Long userId;
	//顾问姓名
	private String technician;
	private String category;
	private String module;
	//人员行程管理显示DTO
	private List<ScheduleDTO> data = new ArrayList<ScheduleDTO>();

	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public String getTechnician() {
		return technician;
	}
	public void setTechnician(String technician) {
		this.technician = technician;
	}

	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}

	public List<ScheduleDTO> getData() {
		return data;
	}
	public void setData(List<ScheduleDTO> data) {
		this.data = data;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getModule() {
		return module;
	}
	public void setModule(String module) {
		this.module = module;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	
}
