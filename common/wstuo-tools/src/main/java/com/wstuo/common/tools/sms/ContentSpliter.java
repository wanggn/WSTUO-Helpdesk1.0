package com.wstuo.common.tools.sms;

/**
 * 短信内容长度截取
 * @author QXY
 *
 */
public class ContentSpliter {
	
	
	private static final Integer SMS_LENGTH=64;//短信长度限制
	private static final Integer SMS_PHONES=1000;//每次发送数量限制
	
	
	/**
	 * 根据字取得分批发送的手机号码.
	 * @param str
	 * @return String []
	 */
	public static String [] getMobileArrFromString(String str){
		
		String [] mobilePath;

		//分批发送
		String [] mobiles=str.split(",");
		if(mobiles.length>SMS_PHONES){
			int length=mobiles.length/SMS_PHONES;
			if(mobiles.length%SMS_PHONES>0){
				length=length+1;
			}
			mobilePath=new String[length];
			for(int i=0;i<length;i++){
				StringBuffer sbMob=new StringBuffer();
				int jj=(i+1)*SMS_PHONES;
				if(jj>mobiles.length){
					jj=mobiles.length;
				}
				for(int j=i*SMS_PHONES;j<jj;j++){
					sbMob.append(mobiles[j]);
					if(j<jj-1){
						sbMob.append(",");
					}
				}
				mobilePath[i]=sbMob.toString();        
			}
		}else{
			mobilePath=new String[1];
			mobilePath[0]=str;
		}
		
		return mobilePath;
	}
	
	/**
	 * 根据短信内容取得分批发送的短信内容。
	 * @param msg
	 * @return String []
	 */
	public static String [] getMsgArrFromString(String msg){
		
		
		String [] msgArr;
		
		if(msg.length()>SMS_LENGTH){
			int length=msg.length()/SMS_LENGTH;
			if(msg.length()%SMS_LENGTH!=0){
				length=length+1;
			}
			msgArr=new String[length];
			for(int i=0;i<length;i++){
				int ii=(i+1)*SMS_LENGTH;
				if(ii>msg.length()){
					ii=msg.length();
				}
				msgArr[i]=msg.substring(i*SMS_LENGTH,ii);
			}
		}else{
			msgArr=new String[1];
			msgArr[0]=msg;
		}
		return msgArr;
	}
	
	
	

}
