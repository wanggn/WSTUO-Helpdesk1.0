package com.wstuo.common.tools.service;

import java.io.File;

import com.wstuo.common.tools.dto.FileInfoDTO;

public interface IUploadFile {
	public final static String ATTACHMENT_TYPE_IMAGE = "jpg,gif,png,bmp";
	
	/**
	 * 文件上传
	 */
	public FileInfoDTO uploadFile(FileInfoDTO fileInfo,String filedataFileName,File filedata,String fileExt,String flag);
	
	/**
	 * 文件删除
	 * @return
	 */
	 public void deleteFile(String deleteFileName);
}
