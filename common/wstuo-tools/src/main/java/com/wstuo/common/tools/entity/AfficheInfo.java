package com.wstuo.common.tools.entity;

import com.wstuo.common.entity.BaseEntity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;

/**
 * 公告信息的实体类
 * 
 * @author brain date 2010/9/10
 * **/
@Entity
@SuppressWarnings({ "serial", "rawtypes" })
public class AfficheInfo extends BaseEntity {
	/*
	 * Announces the information serial number *
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long affId;

	/*
	 * Announces the title *
	 */
	private String affTitle;

	/*
	 * Notice launch time *
	 */
	private Date affStart;

	/*
	 * Notice the end of time *
	 */
	private Date affEnd;

	/*
	 * Who created *
	 */
	private String affCreator;

	/*
	 * Created
	 * 
	 * *
	 */
	private Date affCreaTime;

	/*
	 * Notice Content *
	 */
	@Lob
	private String affContents;

	/*
	 * Visible state *
	 */
	private Integer visibleState;

	/*
	 * Notice by email *
	 */
	private Boolean noticeByEmal = false;

	public Long getAffId() {
		return affId;
	}

	public void setAffId(Long affId) {
		this.affId = affId;
	}

	public String getAffTitle() {
		return affTitle;
	}

	public void setAffTitle(String affTitle) {
		this.affTitle = affTitle;
	}

	public Date getAffStart() {
		return affStart;
	}

	public void setAffStart(Date affStart) {
		this.affStart = affStart;
	}

	public Date getAffEnd() {
		return affEnd;
	}

	public void setAffEnd(Date affEnd) {
		this.affEnd = affEnd;
	}

	public String getAffCreator() {
		return affCreator;
	}

	public void setAffCreator(String affCreator) {
		this.affCreator = affCreator;
	}

	public Date getAffCreaTime() {
		return affCreaTime;
	}

	public void setAffCreaTime(Date affCreaTime) {
		this.affCreaTime = affCreaTime;
	}

	public String getAffContents() {
		return affContents;
	}

	public void setAffContents(String affContents) {
		this.affContents = affContents;
	}

	public Integer getVisibleState() {
		return visibleState;
	}

	public void setVisibleState(Integer visibleState) {
		this.visibleState = visibleState;
	}

	public Boolean getNoticeByEmal() {
		return noticeByEmal;
	}

	public void setNoticeByEmal(Boolean noticeByEmal) {
		this.noticeByEmal = noticeByEmal;
	}

}
