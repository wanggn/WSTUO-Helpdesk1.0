package com.wstuo.common.tools.util;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFPalette;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.springframework.transaction.annotation.Transactional;

public class StructuralStyleUtil {
	/**
	 * IT Tickets背景样式
	 * @param wb
	 * @param palette
	 * @return
	 */
	@Transactional
	public static HSSFCellStyle getItstyle(HSSFWorkbook wb,HSSFPalette palette,HSSFFont font){
		// IT Tickets
		HSSFCellStyle itstyle = wb.createCellStyle();
		itstyle.setFillBackgroundColor(HSSFCellStyle.ALT_BARS);
		itstyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		palette.setColorAtIndex(HSSFColor.DARK_RED.index, (byte) 13,
				(byte) 131, (byte) 230);
		itstyle.setFillForegroundColor(HSSFColor.DARK_RED.index);
		itstyle.setFont(font);
		
		return itstyle;
	}
	/**
	 * 计划人天背景样式
	 * @param wb
	 * @param palette
	 * @return
	 */
	@Transactional
	public static HSSFCellStyle getTstyle(HSSFWorkbook wb,HSSFPalette palette,HSSFFont font){
		// 计划人天
		HSSFCellStyle tstyle = wb.createCellStyle();
		tstyle.setFillBackgroundColor(HSSFCellStyle.ALT_BARS);
		tstyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		palette.setColorAtIndex(HSSFColor.BLUE_GREY.index, (byte) 228,
				(byte) 109, (byte) 10);
		tstyle.setFillForegroundColor(HSSFColor.BLUE_GREY.index);
		tstyle.setFont(font);
		
		return tstyle;
	}
	/**
	 * AMS人天背景样式
	 * @param wb
	 * @param palette
	 * @return
	 */
	@Transactional
	public static HSSFCellStyle getAstyle(HSSFWorkbook wb,HSSFPalette palette,HSSFFont font){
		// AMS人天
		HSSFCellStyle astyle = wb.createCellStyle();
		astyle.setFillBackgroundColor(HSSFCellStyle.ALT_BARS);
		astyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		palette.setColorAtIndex(HSSFColor.BROWN.index, (byte) 0, (byte) 176,
				(byte) 80);
		astyle.setFillForegroundColor(HSSFColor.BROWN.index);
		astyle.setFont(font);
		
		return astyle;
	}
	/**
	 * 项目人天背景样式
	 * @param wb
	 * @param palette
	 * @return
	 */
	@Transactional
	public static HSSFCellStyle getPstyle(HSSFWorkbook wb,HSSFPalette palette,HSSFFont font){
		// 项目人天
		HSSFCellStyle pstyle = wb.createCellStyle();
		pstyle.setFillBackgroundColor(HSSFCellStyle.ALT_BARS);
		pstyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		palette.setColorAtIndex(HSSFColor.BLUE.index, (byte) 55, (byte) 96,
				(byte) 145);
		pstyle.setFillForegroundColor(HSSFColor.BLUE.index);
		pstyle.setFont(font);
		
		return pstyle;
	}
	/**
	 * 周六周日背景样式
	 * @param wb
	 * @param palette
	 * @return
	 */
	@Transactional
	public static HSSFCellStyle getweekendStyle(HSSFWorkbook wb,HSSFPalette palette){
		// 周六周日背景样式
		HSSFCellStyle weekendStyle = wb.createCellStyle();
		weekendStyle.setFillBackgroundColor(HSSFCellStyle.ALT_BARS);
		weekendStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		palette.setColorAtIndex(HSSFColor.BLACK.index, (byte) 216, (byte) 216,
				(byte) 216);
		weekendStyle.setFillForegroundColor(HSSFColor.BLACK.index);
		
		return weekendStyle;
	}
}
