package com.wstuo.common.tools.action;

import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;
import com.wstuo.common.tools.dto.SMSAccountDTO;
import com.wstuo.common.tools.dto.SMSMessageDTO;
import com.wstuo.common.tools.service.ISMSAccountService;
import com.wstuo.common.tools.service.IBaseTools;
import com.wstuo.common.tools.sms.SMSSenderHelper;
/**
 * 短信Action类
 * @author QXY
 */
@SuppressWarnings("serial")
public class SMSAction extends ActionSupport{
	private SMSMessageDTO message=new SMSMessageDTO();
	private ISMSAccountService smsAccountService;
	private SMSAccountDTO smsAccountDTO=new SMSAccountDTO();
	private String mobiles;
	private String content;
	private String bookTime;
	private Boolean testResult;
	
	@Autowired
	private SMSSenderHelper smsSenderHelper;
	@Autowired
	private IBaseTools baseTools;
	
	
	public SMSMessageDTO getMessage() {
		return message;
	}
	public void setMessage(SMSMessageDTO message) {
		this.message = message;
	}
	public String getMobiles() {
		return mobiles;
	}
	public void setMobiles(String mobiles) {
		this.mobiles = mobiles;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getBookTime() {
		return bookTime;
	}
	public void setBookTime(String bookTime) {
		this.bookTime = bookTime;
	}
	public ISMSAccountService getSmsAccountService() {
		return smsAccountService;
	}
	public void setSmsAccountService(ISMSAccountService smsAccountService) {
		this.smsAccountService = smsAccountService;
	}
	public SMSAccountDTO getSmsAccountDTO() {
		return smsAccountDTO;
	}
	public void setSmsAccountDTO(SMSAccountDTO smsAccountDTO) {
		this.smsAccountDTO = smsAccountDTO;
	}
	public Boolean getTestResult() {
		return testResult;
	}
	public void setTestResult(Boolean testResult) {
		this.testResult = testResult;
	}
	/**
	 * 查找短信账户.
	 * @return String
	 */
	public String findSMSAccount(){
		smsAccountDTO=smsAccountService.findSMSAccount();
		baseTools.encodeDTO(smsAccountDTO);//密码编码
		return "SMSAccount";
	}
	
	
	/**
	 * 测试账户信息
	 * @return String
	 */
	public String testSMSAccount(){
		baseTools.decodeDTO(smsAccountDTO);//密码解码
		testResult=smsSenderHelper.testConnection(smsAccountDTO);
		return "testResult";
	}
	/**
	 * 测试账户信息
	 * @return String
	 */
	public String testfindSMSAccount(){
		testResult=smsAccountService.testConnection();
		return "testResult";
	}
	
	/**
	 * 保存短信账户.
	 * @return String
	 */
	public String saveOrUpdateSMSAccount(){
		baseTools.decodeDTO(smsAccountDTO);//密码解码
		smsAccountService.saveOrUpdateSMSAccount(smsAccountDTO);
		return SUCCESS;
		
	}
	/**
	 * 发送短信.
	 * @return String
	 */
	public String sendSMS(){
		dealMoblesStr();
		message=smsSenderHelper.SendSms(mobiles,content,false,true);//用户发送短信
		return SUCCESS;
	}
	public String sendsms_reg(){
		dealMoblesStr();
		if("validate".equals(content)){
			int code = (int) (Math.random() * 100000);
			String codes = String.valueOf(code);
			content="您好，您此次验证码为："+codes+"，1小时内有效。";
			HttpSession session =ServletActionContext.getRequest().getSession();  
			session.setAttribute("validateCode", codes);
		}
		message=smsSenderHelper.SendSms(mobiles,content,false,true);//用户发送短信
		return "message";
	}
	private void dealMoblesStr() {
		mobiles=mobiles.replace(";", ",").trim();
		if(",".equals(mobiles.substring(mobiles.length()-1,mobiles.length()))){
			mobiles=mobiles.substring(0,mobiles.length()-1);
		}
		System.out.println("SMSAction.dealMoblesStr()"+mobiles);
	}
	
	
	/**
	 * 查询余额.
	 * @return String
	 */
	public String queryMoney(){
		message=smsSenderHelper.queryMoney();
		return SUCCESS;
	}

}
