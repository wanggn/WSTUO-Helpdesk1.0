package com.wstuo.common.noticeRule.service;

import java.io.File;
import java.util.Map;

import com.wstuo.common.bpm.dto.FlowTaskDTO;
import com.wstuo.common.noticeRule.dto.NoticeInfoDTO;
import com.wstuo.common.noticeRule.dto.NoticeRuleDTO;
import com.wstuo.common.noticeRule.dto.TemplateVariableDTO;
import com.wstuo.common.noticeRule.entity.NoticeRule;
import com.wstuo.common.tools.dto.NoticeSendDTO;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.security.entity.User;

/**
 * 通知规则Service接口类
 * 
 * @author QXY
 * 
 */
public interface INoticeRuleService {

	// 变更
	/**
	 * 变更创建成功通知
	 */
	public final static String CHANGE_NEW_CHANGE_NOTICE = "newChangeNotice";

	/**
	 * 变更指派通知
	 */
	public final static String CHANGE_ASSIGN_NOTICE = "changeAssignNotice";

	/**
	 * 变更被关闭时通知技术员
	 */
	public final static String CHANGE_CLOSE_NOTICE_TECHNICIAN = "changeCloseNoticeTechnician";

	/**
	 * 变更被关闭时通知CMDB
	 */
	public final static String CHANGE_CLOSE_NOTICE_CMDB = "changeCloseNoticeCMDB";

	/**
	 * 同一配置项在同一天有多个变更冲突通知
	 */
	public final static String CHANGE_THE_CI_TODAY_MULTIPLE_CHANGE_NOTICE = "theCiTodayMultipleChangeNotice";
	/**
	 * 变更流程动作通知
	 */
	public final static String CHANGE_FLOW_ACTION_NOTICE = "changeFlowActionNotice";
	/**
	 * 变更进行审批动作时通知CAB
	 */
	public final static String CHANGE_APPROVE_NOTICE_CAB = "changeApproveNoticeCAB";
	// 配置项
	/**
	 * 配置项生命周期到期通知
	 */
	public final static String CIM_LIFE_CYCLE_EXPIRY_NOTICE = "lifeCycleExpiryNotice";
	/**
	 * 配置项保修期到期通知
	 */
	public final static String CIM_WARRANTY_EXPIRY_NOTICE = "warrantyexpirynotice";
	/**
	 * 配置项预警时间到期通知
	 */
	public final static String CIM_ALERT_CYCLE_EXPIRY_NOTICE = "alertCycleExpiryNotice";

	// 问题
	/**
	 * 问题创建成功通知
	 */
	public final static String PROBLEM_CARETE_SUCCES_SNOTICE = "problemCareteSuccessNotice";
	/**
	 * 问题指派通知
	 */
	public final static String PROBLEM_ASSIGN_NOTICE = "problemAssignNotice";
	/**
	 * 问题标记为已知错误通知
	 */
	public final static String PROBLEM_SIGNKNOWN_ERROR_NOTICE = "problemSignKnownErrorNotice";
	/**
	 * 问题取消已知错误标记通知
	 */
	public final static String PROBLEM_REMOVE_SIGNKNOWN_ERROR_NOTICE = "problemRemoveSignKnownErrorNotice";
	/**
	 * 问题关闭通知
	 */
	public final static String PROBLEM_CLOSE_NOTICE = "problemCloseNotice";
	/**
	 * 问题创建成功通知
	 */
	public final static String PROBLEM_FLOWACTION_NOTICE = "problemFlowActionNotice";
	// 发布
	/**
	 * 发布创建成功通知
	 */
	public final static String RELEASE_NEW_RELEASE_NOTICE = "newReleaseNotice";
	/**
	 * 发布指派通知
	 */
	public final static String RELEASE_ASSIGN_NOTICE = "releaseAssignNotice";
	/**
	 * 发布流程动作通知
	 */
	public final static String RELEASE_FLOW_ACTION_NOTICE = "releaseFlowActionNotice";
	// 请求
	/**
	 * 服务台接收到邮件自动回复请求人
	 */
	public final static String REQUEST_RECEIVE_REQUEST_REPLY_REQUESTER = "receiveRequestReplyRequester";
	/**
	 * 自助创建请求通知,
	 */
	public final static String REQUEST_SELF_CREATE_REQUEST_NOTICE = "selfCreateRequestNotice";
	/**
	 * 有新的请求通知服务台,
	 */
	public final static String REQUEST_NEW_REQUEST_NOTICE_HELPDESK = "newRequestNoticeHelpdesk";
	/**
	 * 请求指派通知,
	 */
	public final static String REQUEST_ASSIGN_NOTICE = "requestAssignNotice";
	/**
	 * 请求流程重新开启通知,
	 */
	public final static String REQUEST_REOPEN_NOITCE = "requestReOpenNoitce";
	/**
	 * 请求关闭通知,
	 */
	public final static String REQUEST_CLOSE_NOTICE = "requestCloseNotice";
	/**
	 * 请求更新通知,
	 */
	public final static String REQUEST_UPDATE_NOTICE = "requestUpdateNotice";
	/**
	 * 请求升级通知,
	 */
	public final static String REQUEST_UPGRADE_NOTICE = "requestUpgradeNotice";
	/**
	 * 请求挂起通知,
	 */
	public final static String REQUEST_HANG_NOTICE = "requestHangNotice";
	/**
	 * 请求解除挂起通知,
	 */
	public final static String REQUEST_REMOVE_HANG_NOTICE = "requestRemoveHangNotice";
	/**
	 * 请求响应时通知
	 */
	public final static String REQUEST_RESPONSES_NOTIC = "requestResponsesNotice";
	/**
	 * 请求自助解决通知,
	 */
	public final static String REQUEST_SELF_SOLVE_NOTICE = "requestSelfSolveNotice";
	/**
	 * SLA逾期通知,
	 */
	public final static String REQUEST_SLA_OVER_DUE_NOTICE = "slaOverdueNotice";
	/**
	 * 请求流程动作通知
	 */
	public final static String REQUEST_FLOW_ACTION_NOTICE = "requestFlowActionNotice";
	/**
	 * 请求回访通知
	 */
	public final static String REQUEST_VISIT_NOTICE = "requestVisitNotice";
	/**
	 * 请求处理备注通知
	 */
	public final static String REQUEST_ADD_COMMENT_NOTICE = "requestAddCommentNotice";
	/**
	 * 请求描述不全退回通知,
	 */
	public final static String REQUEST_NOT_COMPREHEN_SIVE_NOTSUBMITTED_NOTICE = "notComprehensiveNotSubmittedNotice";
	/**
	 * 请求重新提交,
	 */
	public final static String REQUEST_RESUBMIT_NOTICE = "reSubmitNotice";
	/**
	 * 发送请求工单,
	 */
	public final static String REQUEST_SEND_REQUES_TORDER = "sendRequestOrder";

	/**
	 * 通知规则分页查询
	 * 
	 * @param noticeRuleDTO
	 *            :条件查询DTO
	 * @param start
	 * @param limit
	 * @param sord
	 * @param sidx
	 * @return PageDTO
	 */
	PageDTO findPagerNoticeRule(NoticeRuleDTO noticeRuleDTO, int start,
			int limit, String sord, String sidx);

	/**
	 * 更新通知规则信息
	 * 
	 * @param useStatuses
	 * @param noticeRuleNos
	 * @param url
	 */
	void updateNoticeRule(Map<String, Boolean> useStatuses,
			Map<String, String> noticeRuleNos, String url);

	/**
	 * 通知
	 * 
	 * @param noticeInfoDto
	 *            通知信息DTO
	 */
	void commonNotice(NoticeInfoDTO noticeInfoDto);

	/**
	 * 邮件发送
	 * 
	 * @param noticeSendDTO
	 *            变量DTO
	 */
	void sendEmail(NoticeSendDTO noticeSendDTO);
	/**
	 * 消息发送
	 * @param noticeSendDTO
	 */
	void sendIM(NoticeSendDTO noticeSendDTO);
	
	/**
	 * 发送信息.
	 * @param noticeSendDTO 
	 */
	void sendSms(NoticeSendDTO noticeSendDTO);
	
	/**
	 * 发送信息.
	 * @param noticeSendDTO 
	 */
	void sendPush(NoticeSendDTO noticeSendDTO);


	/**
	 * 模板预览
	 * 
	 * @param templatesType
	 * @param tempContent
	 * @return templates html content
	 */
	String templatePreview(String templatesType, String tempContent);

	/**
	 * 获取模板文件内容
	 * 
	 * @param fileName
	 * @param module
	 * @param templatesType
	 * @return String
	 */
	String getTempplatesFileContent(String fileName, String module,
			String templatesType);

	/**
	 * writer to template file
	 * 
	 * @param filename
	 * @param module
	 * @param templatesType
	 * @param content
	 * @return boolean
	 */
	boolean writeToFile(String filename, String module, String templatesType,
			String content);

	/**
	 * import notice data
	 * 
	 * @param importFile
	 * @return import result
	 */
	String importNotice(File importFile);

	/**
	 * verifi return visit
	 * 
	 * @return boolean
	 */
	boolean verifiReturnVisit();

	/**
	 * 新建一个通知规则文件
	 * 
	 * @param dto
	 *            NoticeRuleDTO
	 */
	void addNoticesRule(NoticeRuleDTO dto);

	/**
	 * 根据Id查找NoticeRuleDTO
	 * 
	 * @param id
	 * @return NoticeRuleDTO
	 */
	NoticeRuleDTO findById(Long id);

	/**
	 * 根据Id查找NoticeRuleDTO
	 * 
	 * @param id
	 * @return NoticeRule
	 */
	NoticeRule findEntityById(Long id);

	/**
	 * update notice rule
	 * 
	 * @param dto
	 *            NoticeRuleDTO
	 */
	void editNoticeMethod(NoticeRuleDTO dto);

	/**
	 * 删除通知规则
	 * 
	 * @param nos
	 */
	void delsNoticeRules(Long[] nos);

	/**
	 * 根据通知编码查询实体
	 * 
	 * @param noticeNo
	 *            通知编码
	 * @return NoticeEntity
	 */
	NoticeRule findEntityByNoticeNo(String noticeNo);

	/**
	 * 获取模板路径
	 * 
	 * @param fileName
	 * @param module
	 * @param templatesType
	 * @return String
	 */
	String getFilePath(String fileName, String module, String templatesType);


	/**
	 * 获取模板路径
	 * 
	 * @param fileName
	 * @param module
	 * @param templatesType
	 * @return template file path
	 */
	String getTemplateFilePath(String fileName, String module,
			String templatesType);

	/**
	 * update notice rule
	 * 
	 * @param dto
	 * @param variableDTO
	 * @return boolean
	 */
	boolean apporCountersign(NoticeRuleDTO dto, TemplateVariableDTO variableDTO);

	/**
	 * 发送邮件处理流程
	 * 
	 * @param assignee
	 * @param templateVariableDTOList
	 * @param flowTaskDto
	 * @param pid
	 * @param moduleType
	 */
	void sendMailHandlingProcess(User assignee,
			Object variables, FlowTaskDTO flowTaskDto,String moduleType);


	/**
	 * 根据模板文件名称查询邮件模板
	 * 
	 * @param noticeRuleDTO
	 * @return NoticeRuleDTO
	 */
	NoticeRuleDTO findByFileName(Long noticeRuleId);

	/**
	 * 取得模板内容
	 * 
	 * @param noticeRuleDTO
	 * @param fileName
	 * @param module
	 * @return NoticeRuleDTO
	 */
	NoticeRuleDTO findByNoticesModel(NoticeRuleDTO noticeRuleDTO,
			String fileName, String module);
	/**消息推送的ClientId绑定
	 * @return pushBind
	 */
	String pushUserBind(String userName,String clientId);
	/**消息数量修改
	 * @return pushBind
	 */
	int pushCount(String userName,int count);
}
