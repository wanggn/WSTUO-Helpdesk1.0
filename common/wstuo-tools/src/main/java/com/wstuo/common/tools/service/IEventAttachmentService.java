package com.wstuo.common.tools.service;

import java.util.List;

import com.wstuo.common.config.attachment.dto.AttachmentDTO;
import com.wstuo.common.tools.dto.EventAttachmentDTO;
/**
 * Event Attachment Service interface class
 * @author WSTUO
 *
 */
public interface IEventAttachmentService {
	/**
	 * 查询全部
	 * @param dto EventAttachmentDTO
	 * @return List<EventAttachment>
	 */
    List<EventAttachmentDTO> findAllEventAttachment(EventAttachmentDTO dto);
   
    /**
     * 保存附件
     * @param createor
     * @param aids
     * @param attachmentStr
     * @param eno
     * @param eventType
     */
    void saveEventAttachment(String createor,Long[] aids,String attachmentStr,Long eno,String eventType);
    void saveEventAttachment(AttachmentDTO attachment,Long[] aids,String attachmentStr,Long eno);
  
    /**
    * 删除附件
    * @param ids
    * @param eno
    */
    void deleteEventAttachment(final Long[] ids,Long eno);
    
    /**
     * 保存附件
     * @param attachmentStr
     * @param eno
     * @param eventType
     */
    @Deprecated
    void saveEventAttachment(String attachmentStr,Long eno,String eventType);
    
    /**
     * 删除附件
     * @param aid
     * @return EventAttachment
     */
    String deleteEventAttachmentByAid(Long aid,String filePath);
}
