package com.wstuo.common.tools.action;



import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;
import com.wstuo.common.exchange.IWebDavService;
import com.wstuo.common.noticeRule.service.IEmailTemplateService;
import com.wstuo.common.tools.dto.EmailConnectionDTO;
import com.wstuo.common.tools.dto.EmailDTO;
import com.wstuo.common.tools.dto.EmailMessageDTO;
import com.wstuo.common.tools.dto.EmailMessageQueryDTO;
import com.wstuo.common.tools.dto.EmailServerDTO;
import com.wstuo.common.tools.service.IBaseTools;
import com.wstuo.common.tools.service.IEmailServerService;
import com.wstuo.common.tools.service.IEmailService;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.security.utils.AppConfigUtils;
import com.wstuo.common.security.utils.AppContext;

/**
 * 邮件Action
 * @author QXY
 */
@SuppressWarnings("serial")
public class EmailAction extends ActionSupport {
	@Autowired
	private IEmailServerService emailServerService;
	@Autowired
	private IEmailTemplateService emailTemplateService;
	@Autowired
	private AppContext appctx;
	private EmailConnectionDTO emailConnectionDto = new EmailConnectionDTO();
	private EmailDTO emailDto = new EmailDTO();
	private EmailMessageDTO emailMessageDto = new EmailMessageDTO();
	private boolean result = false;
	private boolean isSend = true;
	private int testErrorEncode = 0;
	private PageDTO emailDtos;
	private int page = 1;
	private int rows = 10;
	private long eid = 0;
	private Long[] ids;
	private EmailMessageQueryDTO emailMessageQueryDto;
	private String sidx;
	private String sord;
	private String loginName;
	private String title;
	private String content;
	private EmailServerDTO emailServerDTO;
	@Autowired
	private IWebDavService webDavService;
	/* service */
	@Autowired
	private IEmailService emailService;
	@Autowired
	private IBaseTools baseTools;

	public boolean isSend() {
		return isSend;
	}
	public void setSend(boolean isSend) {
		this.isSend = isSend;
	}

	public String getSidx() {
		return sidx;
	}

	public void setSidx(String sidx) {
		this.sidx = sidx;
	}

	public String getSord() {
		return sord;
	}

	public void setSord(String sord) {
		this.sord = sord;
	}

	public IEmailService getEmailService() {
		return emailService;
	}

	public void setEmailService(IEmailService emailService) {
		this.emailService = emailService;
	}
	
	public PageDTO getEmailDtos() {
		return emailDtos;
	}

	public void setEmailDtos(PageDTO emailDtos) {
		this.emailDtos = emailDtos;
	}

	public EmailConnectionDTO getEmailConnectionDto() {
		return emailConnectionDto;
	}

	public void setEmailConnectionDto(EmailConnectionDTO emailConnectionDto) {
		this.emailConnectionDto = emailConnectionDto;
	}

	public EmailDTO getEmailDto() {
		return emailDto;
	}

	public void setEmailDto(EmailDTO emailDto) {
		this.emailDto = emailDto;
	}

	public boolean isResult() {
		return result;
	}

	public void setResult(boolean result) {
		this.result = result;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getRows() {
		return rows;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}

	public int getTestErrorEncode() {
		return testErrorEncode;
	}

	public void setTestErrorEncode(int testErrorEncode) {
		this.testErrorEncode = testErrorEncode;
	}

	public EmailMessageDTO getEmailMessageDto() {
		return emailMessageDto;
	}

	public void setEmailMessageDto(EmailMessageDTO emailMessageDto) {
		this.emailMessageDto = emailMessageDto;
	}

	public long getEid() {
		return eid;
	}

	public void setEid(long eid) {
		this.eid = eid;
	}

	public EmailMessageQueryDTO getEmailMessageQueryDto() {
		return emailMessageQueryDto;
	}

	public void setEmailMessageQueryDto(
			EmailMessageQueryDTO emailMessageQueryDto) {
		this.emailMessageQueryDto = emailMessageQueryDto;
	}

	public Long[] getIds() {
		return ids;
	}

	public void setIds(Long[] ids) {
		this.ids = ids;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	public EmailServerDTO getEmailServerDTO() {
		return emailServerDTO;
	}

	public void setEmailServerDTO(EmailServerDTO emailServerDTO) {
		this.emailServerDTO = emailServerDTO;
	}

	/**
	 * 获取邮件
	 * @return String
	 */
	public String findEmail() {
		int start = (page - 1) * rows;
		emailDtos = emailService.getEmailMessage(emailMessageQueryDto, start,
				rows, sidx, sord);
		emailDtos.setPage(page);
		emailDtos.setRows(rows);
		return "emailDtos";
	}
	/**
	 * 邮件找回密码
	 * @return
	 * @throws Exception
	 */
	public String findPasswordToEmail() throws Exception{
		String pathDir =AppConfigUtils.getInstance().getConfigPathByTenantId("templateFile", "emailTemplates",appctx.getCurrentTenantId());
		String subject = emailTemplateService.template(emailDto,pathDir, "findPasswordToEmail_title.ftl","findPasswordToEmail");
		String content = emailTemplateService.template(emailDto,pathDir, "findPasswordToEmail_email.ftl","findPasswordToEmail");
		emailDto.setSubject(subject);
		emailDto.setContent(content);
		result = emailService.sendMail(emailDto);
		return "result";
	}

	/**
	 * to email 写Email
	 * @return String
	 * @throws Exception
	 */
	public String toEmail() throws Exception {
		result = emailService.sendMail(emailDto);
		return "result";
	}

	/**
	 * 邮件发送 
	 * @return send result
	 * @throws Exception
	 */
	public String sendEmail() throws Exception {
		result = emailService.sendMail(emailDto);
		return "result";
	}

	/**
	 * 邮件连接测试
	 * @return String
	 * @throws Exception
	 */
	public String emailConnTest() {
		baseTools.decodeDTO(emailConnectionDto);//密码解码
		testErrorEncode = emailService.emailConnTest(emailConnectionDto);
		return "testErrorEncode";
	}

	/**
	 * 用户手动扫描邮件
	 * @return String
	 * @throws Exception
	 */

	public String scanEmailMessagesByUser() {
		result = emailService.scanEmailMessagesByUserService(emailConnectionDto);
		return "result";
	}

	/**
	 * 邮件发送测试
	 * @return null
	 * @throws Exception
	 */
	public String testSendEmail() throws Exception {
		emailService.sendMail(emailDto);
		return "result";
	}
	public String testEmail() throws Exception {
		result = emailService.testEmail(null,isSend);
		return "result";
	}

	/**
	 * 获取与请求相关的邮件
	 * @return String
	 * @throws Exception
	 */
	public String findEmailAboutRequest() {
		int start = (page - 1) * rows;
		emailDtos = emailService.getEmailMessage(emailMessageQueryDto, start,
				rows, sidx, sord);
		emailDtos.setPage(page);
		emailDtos.setRows(rows);
		return "emailDtos";
	}

	/**
	 * 根据ID查找邮件信息.
	 * @return EmailMessageDTO 邮件信息DTO�?
	 */
	public String showEmailDetail() {
		emailMessageDto = emailService.findById(eid);
		return "showEmailDetail";
	}

	/**
	 * 根据ID查邮件消息
	 * @return emailMessageDto
	 */
	public String findEmailMessageById() {
		emailMessageDto = emailService.findById(eid);
		return "emailMessageDto";
	}

	/**
	 * 删除邮件消息
	 * @return null
	 */
	public String deleteEmailMessage() {
		emailService.deleteEmailMessage(ids);
		return "emailDtos";
	}

	/**
	 * 根据登录名发送邮件
	 * @return String
	 */
	public String sendMailByLoginName() {

		result = emailService.sendMailByLoginName(loginName, title, content);
		return "result";
	}

	/**
	 * Exchange连接测试
	 * @return result
	 */
	public String exchangeEmailConnTest() {
		baseTools.decodeDTO(emailServerDTO);//密码解码
		result = webDavService.emailConnTest(emailServerDTO);
		return "result";
	}

	/**
	 * 查邮件服务器配置
	 * @return emailServerDTO
	 */
	public String findServiceEmail(){
		
		emailServerDTO= emailServerService.findEmail();
		baseTools.encodeDTO(emailServerDTO);//密码编码
		return "findServiceEmail";
	}
}
