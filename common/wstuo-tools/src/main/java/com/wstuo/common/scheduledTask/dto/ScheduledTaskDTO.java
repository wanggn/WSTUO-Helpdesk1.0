package com.wstuo.common.scheduledTask.dto;

import java.util.Date;

import com.wstuo.common.dto.BaseDTO;

/**
 * 定期任务DTO
 * @author WSTUO
 *
 */
@SuppressWarnings("serial")
public class ScheduledTaskDTO extends BaseDTO {
	private Long scheduledTaskId;//ID
	private String timeType;//时间类型：时间类型：day、日计划;weekly、周计划;month、月计划;cycle：周期性计划；on_off、一次性计划
	private Long taskHour;//小时
	private Long taskMinute;//分
	private Date taskDate;//具体时间(开始时间)
	private Date taskEndDate;//结束时间
	private String weekWeeks;//周计划：每周星期几
	private String monthMonths;//月计划：每月
	private String monthDay;//月计划：每月的日期
	private Long cyclicalDay;//周期性计划：每过多少天执行一次
	private ScheduledTaskRequestDTO requestDTO=new ScheduledTaskRequestDTO();//任务详细DTO
	private String[] weekWeek;
	private String[] monthMonth;
	private String beanId;//job beanId
	private String scheduledTaskType;
	private Long cyclicalMinute;//周期性计划：每过多少分钟执行一次
	//对应的报表编号
	private Long reportId;
	private Long formId;
	
	public Long getFormId() {
		return formId;
	}
	public void setFormId(Long formId) {
		this.formId = formId;
	}
	public Long getScheduledTaskId() {
		return scheduledTaskId;
	}
	public void setScheduledTaskId(Long scheduledTaskId) {
		this.scheduledTaskId = scheduledTaskId;
	}
	public String getTimeType() {
		return timeType;
	}
	public void setTimeType(String timeType) {
		this.timeType = timeType;
	}

	public Long getTaskHour() {
		return taskHour;
	}
	public void setTaskHour(Long taskHour) {
		this.taskHour = taskHour;
	}
	public Long getTaskMinute() {
		return taskMinute;
	}
	public void setTaskMinute(Long taskMinute) {
		this.taskMinute = taskMinute;
	}
	public Date getTaskDate() {
		return taskDate;
	}
	public void setTaskDate(Date taskDate) {
		this.taskDate = taskDate;
	}
	public String getWeekWeeks() {
		return weekWeeks;
	}
	public void setWeekWeeks(String weekWeeks) {
		this.weekWeeks = weekWeeks;
	}
	public String getMonthMonths() {
		return monthMonths;
	}
	public void setMonthMonths(String monthMonths) {
		this.monthMonths = monthMonths;
	}
	public String getMonthDay() {
		return monthDay != null ? monthDay.trim() : monthDay ;
	}
	public void setMonthDay(String monthDay) {
		this.monthDay = monthDay;
	}
	public Long getCyclicalDay() {
		return cyclicalDay;
	}
	public void setCyclicalDay(Long cyclicalDay) {
		this.cyclicalDay = cyclicalDay;
	}
	public ScheduledTaskRequestDTO getRequestDTO() {
		return requestDTO;
	}
	public void setRequestDTO(ScheduledTaskRequestDTO requestDTO) {
		this.requestDTO = requestDTO;
	}
	public String[] getWeekWeek() {
		return weekWeek;
	}
	public void setWeekWeek(String[] weekWeek) {
		this.weekWeek = weekWeek;
	}
	public String[] getMonthMonth() {
		return monthMonth;
	}
	public void setMonthMonth(String[] monthMonth) {
		this.monthMonth = monthMonth;
	}
	public String getBeanId() {
		return beanId;
	}
	public void setBeanId(String beanId) {
		this.beanId = beanId;
	}
	public Date getTaskEndDate() {
		return taskEndDate;
	}
	public void setTaskEndDate(Date taskEndDate) {
		this.taskEndDate = taskEndDate;
	}
	public String getScheduledTaskType() {
		return scheduledTaskType;
	}
	public void setScheduledTaskType(String scheduledTaskType) {
		this.scheduledTaskType = scheduledTaskType;
	}
	public Long getCyclicalMinute() {
		return cyclicalMinute;
	}
	public void setCyclicalMinute(Long cyclicalMinute) {
		this.cyclicalMinute = cyclicalMinute;
	}
	public Long getReportId() {
		return reportId;
	}
	public void setReportId(Long reportId) {
		this.reportId = reportId;
	}

	
	
}
