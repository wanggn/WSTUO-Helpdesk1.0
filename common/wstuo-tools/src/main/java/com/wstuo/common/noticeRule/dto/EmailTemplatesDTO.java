package com.wstuo.common.noticeRule.dto;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.wstuo.common.bpm.dto.FlowTaskDTO;
import com.wstuo.common.tools.dto.HistoryRecordDTO;
import com.wstuo.common.dto.BaseDTO;

/**
 * 邮件模块内容DTO
 * @author WSTUO
 *
 */
@SuppressWarnings("serial")
public class EmailTemplatesDTO extends BaseDTO{
	
	private TemplateVariableDTO templateVariableDTO=new TemplateVariableDTO();
    private List<HistoryRecordDTO> historyRecordDTO = new ArrayList<HistoryRecordDTO>();
    private String tempDir;
    private String emailTitleTemp;
    private String visitUrl;
	private String userName;
	private String module;//模块
	private String emailAddress;//接收的邮件地址(分号隔开)
	private String url;
	private List<String> loginName;
	private String mqTitle;//MQ执行的主题
    private String mqContent;//MQ执行的内容
    private String mqCreator;//MQ创建者
    //邮件接收的地址
	private Set<String> emailReceiveAddress = new HashSet<String>();
	//IM接收的用户
	private Set<String> imReceiveUser = new HashSet<String>();
	//短信接收的手机号码
	private Set<String> smsReceiveNumber = new HashSet<String>();
	//规则编码
	private String noticeRuleNo;
	//模板
	private String templatesType;
	//流程动作
	private List<String> outcome;
	private FlowTaskDTO flowTaskDTO;
	private String pid;
	//模块类型
	private String moduleType;
	private String userRandomId;
	//通知模板变量
	private Object variables;
	public String getUserRandomId() {
		return userRandomId;
	}
	public void setUserRandomId(String userRandomId) {
		this.userRandomId = userRandomId;
	}
	public String getModuleType() {
		return moduleType;
	}
	public void setModuleType(String moduleType) {
		this.moduleType = moduleType;
	}
	public String getPid() {
		return pid;
	}
	public void setPid(String pid) {
		this.pid = pid;
	}
	public FlowTaskDTO getFlowTaskDTO() {
		return flowTaskDTO;
	}
	public void setFlowTaskDTO(FlowTaskDTO flowTaskDTO) {
		this.flowTaskDTO = flowTaskDTO;
	}
	public List<String> getOutcome() {
		return outcome;
	}
	public void setOutcome(List<String> outcome) {
		this.outcome = outcome;
	}
	public String getMqTitle() {
		return mqTitle;
	}
	public void setMqTitle(String mqTitle) {
		this.mqTitle = mqTitle;
	}
	public String getMqContent() {
		return mqContent;
	}
	public void setMqContent(String mqContent) {
		this.mqContent = mqContent;
	}
	public String getMqCreator() {
		return mqCreator;
	}
	public void setMqCreator(String mqCreator) {
		this.mqCreator = mqCreator;
	}
	public List<String> getLoginName() {
		return loginName;
	}
	public void setLoginName(List<String> loginName) {
		this.loginName = loginName;
	}
	public TemplateVariableDTO getTemplateVariableDTO() {
		return templateVariableDTO;
	}
	public void setTemplateVariableDTO(TemplateVariableDTO templateVariableDTO) {
		this.templateVariableDTO = templateVariableDTO;
	}
	public List<HistoryRecordDTO> getHistoryRecordDTO() {
		return historyRecordDTO;
	}
	public void setHistoryRecordDTO(List<HistoryRecordDTO> historyRecordDTO) {
		this.historyRecordDTO = historyRecordDTO;
	}
	public String getTempDir() {
		return tempDir;
	}
	public void setTempDir(String tempDir) {
		this.tempDir = tempDir;
	}
	public String getVisitUrl() {
		return visitUrl;
	}
	public void setVisitUrl(String visitUrl) {
		this.visitUrl = visitUrl;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getEmailTitleTemp() {
		return emailTitleTemp;
	}
	public void setEmailTitleTemp(String emailTitleTemp) {
		this.emailTitleTemp = emailTitleTemp;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getModule() {
		return module;
	}
	public void setModule(String module) {
		this.module = module;
	}
	public Set<String> getEmailReceiveAddress() {
		return emailReceiveAddress;
	}
	public void setEmailReceiveAddress(Set<String> emailReceiveAddress) {
		this.emailReceiveAddress = emailReceiveAddress;
	}
	public Set<String> getImReceiveUser() {
		return imReceiveUser;
	}
	public void setImReceiveUser(Set<String> imReceiveUser) {
		this.imReceiveUser = imReceiveUser;
	}
	public Set<String> getSmsReceiveNumber() {
		return smsReceiveNumber;
	}
	public void setSmsReceiveNumber(Set<String> smsReceiveNumber) {
		this.smsReceiveNumber = smsReceiveNumber;
	}
	public String getNoticeRuleNo() {
		return noticeRuleNo;
	}
	public void setNoticeRuleNo(String noticeRuleNo) {
		this.noticeRuleNo = noticeRuleNo;
	}
	public String getTemplatesType() {
		return templatesType;
	}
	public void setTemplatesType(String templatesType) {
		this.templatesType = templatesType;
	}
	public Object getVariables() {
		return variables;
	}
	public void setVariables(Object variables) {
		this.variables = variables;
	}
	
}
