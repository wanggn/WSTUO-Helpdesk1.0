package com.wstuo.common.activemq.dao;

import com.wstuo.common.activemq.entity.QueueConfigure;
import com.wstuo.common.dao.IEntityDAO;

/**
 * 消息队列配置DAO接口类
 * @author WSTUO
 *
 */
public interface IQueueConfigureDAO extends IEntityDAO<QueueConfigure>{

}
