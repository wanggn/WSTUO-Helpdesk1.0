package com.wstuo.common.activemq.dto;

import com.wstuo.common.dto.BaseDTO;

/**
 * 消息队列详细信息DTO
 * @author WSTUO
 *
 */
@SuppressWarnings("serial")
public class ActiveMQInfoDTO extends BaseDTO{
	
	private String queueName;//队列名称
	private Long consumerCount;//消费者个数
	private Long dequeueCount;//出队数
	private Long enqueueCount;//排队数
	private Long dispatchCount;//发送个数
	private Long expiredCount;//过期 个数
	private Long maxEnqueueTime;//最大排队时间
	private Long producerCount;//生产者个数
	private Integer memoryPercentUsage;//内存使用百分比
	private Long memoryLimit;//内存限制
	
	
	public String getQueueName() {
		return queueName;
	}
	public void setQueueName(String queueName) {
		this.queueName = queueName;
	}
	public Long getConsumerCount() {
		return consumerCount;
	}
	public void setConsumerCount(Long consumerCount) {
		this.consumerCount = consumerCount;
	}
	public Long getDequeueCount() {
		return dequeueCount;
	}
	public void setDequeueCount(Long dequeueCount) {
		this.dequeueCount = dequeueCount;
	}
	public Long getEnqueueCount() {
		return enqueueCount;
	}
	public void setEnqueueCount(Long enqueueCount) {
		this.enqueueCount = enqueueCount;
	}
	public Long getDispatchCount() {
		return dispatchCount;
	}
	public void setDispatchCount(Long dispatchCount) {
		this.dispatchCount = dispatchCount;
	}
	public Long getExpiredCount() {
		return expiredCount;
	}
	public void setExpiredCount(Long expiredCount) {
		this.expiredCount = expiredCount;
	}
	public Long getMaxEnqueueTime() {
		return maxEnqueueTime;
	}
	public void setMaxEnqueueTime(Long maxEnqueueTime) {
		this.maxEnqueueTime = maxEnqueueTime;
	}
	public Long getProducerCount() {
		return producerCount;
	}
	public void setProducerCount(Long producerCount) {
		this.producerCount = producerCount;
	}
	public Integer getMemoryPercentUsage() {
		return memoryPercentUsage;
	}
	public void setMemoryPercentUsage(Integer memoryPercentUsage) {
		this.memoryPercentUsage = memoryPercentUsage;
	}
	public Long getMemoryLimit() {
		return memoryLimit;
	}
	public void setMemoryLimit(Long memoryLimit) {
		this.memoryLimit = memoryLimit;
	}
}
