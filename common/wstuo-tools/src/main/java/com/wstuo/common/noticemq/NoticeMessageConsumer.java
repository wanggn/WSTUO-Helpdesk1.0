package com.wstuo.common.noticemq;

import org.hibernate.internal.SessionFactoryImpl;
import org.springframework.context.ApplicationContext;

import com.wstuo.common.activemq.consumer.IMessageConsumer;
import com.wstuo.common.tools.dto.NoticeSendDTO;
import com.wstuo.common.util.AppliactionBaseListener;

/**
 * 消息消费类
 * @author will
 *
 */

public class NoticeMessageConsumer{
	private SessionFactoryImpl sessionFactory;
	
	public SessionFactoryImpl getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactoryImpl sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	
	public void noticeConsumer(NoticeSendDTO sendDTO) {
/*		TransactionSynchronizationManager.unbindResourceIfPossible(sessionFactory);
		TenantIdResolver tenantIdResolver = (TenantIdResolver)sessionFactory.getCurrentTenantIdentifierResolver();
		if(tenantIdResolver!=null){
			tenantIdResolver.setTenantId(sendDTO.getTenantId());
		}
*/
		ApplicationContext ctx=AppliactionBaseListener.ctx;
		IMessageConsumer consumer=(IMessageConsumer) ctx.getBean(sendDTO.getSendType()+"MessageConsumer");
		consumer.messageConsumer(sendDTO);
	}
}
