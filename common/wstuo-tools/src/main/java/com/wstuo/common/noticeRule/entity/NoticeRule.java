package com.wstuo.common.noticeRule.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToMany;

import com.wstuo.common.entity.BaseEntity;
import com.wstuo.common.security.entity.User;


/**
 * 通知规则实体类
 * @author QXY
 *
 */
@SuppressWarnings({ "serial", "rawtypes" })
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class NoticeRule extends BaseEntity {

	
    //规则ID
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private Long noticeRuleId;
	//通知规则编码
	@Column(unique=true)
	private String noticeRuleNo;
	//通知方式
	@Column(nullable=true)
	private String noticeWay;
	//通知对象(请求人或报告人-requester、指派的技术员-technician、指派的服务机构-serviceOrganization、指派的技术组-technicalGroup、任务指派组和技术员-taskGroupAndTechnician)
	@Column(nullable=true)
	private String noticeRuleType;
	//规则名称
	@Column(nullable=false)
	private String noticeRuleName;
	//启动状态
	private Boolean useStatus=false;
	//要通知的用户
	@ManyToMany
	private List<User> users;
	//要通知的电邮地址
	@Column(nullable=true)
	private String emailAddress;
	//邮件内容模板
	@Column(nullable=true)
	private String emailTemp;
	//邮件标题模板
	@Column(nullable=true)
	private String emailTitleTemp;
	//标识是技术员还是指定的电子邮件
	@Column(nullable=false)
	private String technicianOrEmail;//0为技术员 1为指定的邮箱
	//短信通知
	private Boolean smsNotice=false;
	//IM通知
	private Boolean imNotice = false;
	//邮箱通知
	private Boolean mailNotice=false;
	//推送通知
	private Boolean pushNotice = false;
	/**系统网络路径*/
	private String url;
	//所属模块(request,problem,change,release,cim)
	private String module;
	//短信模板
	private String smsTemp;
	//IM消息模板
	private String imTemp;
	//消息推送模板
	private String pushTemp;
	
	public String getPushTemp() {
		return pushTemp;
	}
	public void setPushTemp(String pushTemp) {
		this.pushTemp = pushTemp;
	}
	public Boolean getPushNotice() {
		return pushNotice;
	}
	public void setPushNotice(Boolean pushNotice) {
		this.pushNotice = pushNotice;
	}
	public Boolean getMailNotice() {
		return mailNotice;
	}
	public void setMailNotice(Boolean mailNotice) {
		this.mailNotice = mailNotice;
	}
	public String getTechnicianOrEmail() {
		return technicianOrEmail;
	}
	public void setTechnicianOrEmail(String technicianOrEmail) {
		this.technicianOrEmail = technicianOrEmail;
	}
	public Boolean getSmsNotice() {
		return smsNotice;
	}
	public void setSmsNotice(Boolean smsNotice) {
		this.smsNotice = smsNotice;
	}
	public Long getNoticeRuleId() {
		return noticeRuleId;
	}
	public void setNoticeRuleId(Long noticeRuleId) {
		this.noticeRuleId = noticeRuleId;
	}
	public String getNoticeRuleNo() {
		return noticeRuleNo;
	}
	public void setNoticeRuleNo(String noticeRuleNo) {
		this.noticeRuleNo = noticeRuleNo;
	}

	public String getNoticeWay() {
		return noticeWay;
	}
	public void setNoticeWay(String noticeWay) {
		this.noticeWay = noticeWay;
	}
	public String getNoticeRuleType() {
		return noticeRuleType;
	}
	public void setNoticeRuleType(String noticeRuleType) {
		this.noticeRuleType = noticeRuleType;
	}
	public String getNoticeRuleName() {
		return noticeRuleName;
	}
	public void setNoticeRuleName(String noticeRuleName) {
		this.noticeRuleName = noticeRuleName;
	}
	public Boolean getUseStatus() {
		return useStatus;
	}
	public void setUseStatus(Boolean useStatus) {
		this.useStatus = useStatus;
	}
	
	public List<User> getUsers() {
		return users;
	}
	public void setUsers(List<User> users) {
		this.users = users;
	}
	public String getEmailTemp() {
		return emailTemp;
	}
	public void setEmailTemp(String emailTemp) {
		this.emailTemp = emailTemp;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public String getEmailTitleTemp() {
		return emailTitleTemp;
	}
	public void setEmailTitleTemp(String emailTitleTemp) {
		this.emailTitleTemp = emailTitleTemp;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public Boolean getImNotice() {
		return imNotice;
	}
	public void setImNotice(Boolean imNotice) {
		this.imNotice = imNotice;
	}
	public String getModule() {
		return module;
	}
	public void setModule(String module) {
		this.module = module;
	}
	public String getSmsTemp() {
		return smsTemp;
	}
	public void setSmsTemp(String smsTemp) {
		this.smsTemp = smsTemp;
	}
	public String getImTemp() {
		return imTemp;
	}
	public void setImTemp(String imTemp) {
		this.imTemp = imTemp;
	}
	
}
