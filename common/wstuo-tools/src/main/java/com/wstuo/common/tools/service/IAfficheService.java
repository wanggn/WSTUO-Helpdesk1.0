package com.wstuo.common.tools.service;


import com.wstuo.common.tools.dto.AfficheDTO;
import com.wstuo.common.tools.dto.AfficheQueryDTO;
import com.wstuo.common.dto.PageDTO;

import java.io.File;
import java.util.List;


/**
 * 公告Service接口类
 * @author brain
 * date 2010/9/10
 * **/
public interface IAfficheService
{
	 /**
	 * 查询全部公告信息
	 * @return List<AfficheDTO>
	 */
    public List<AfficheDTO> findAllAffiche(  );

    /**
     * 公告分页查询
     * @param dto
     * @param start
     * @param limit
     * @param sidx
     * @param sord
     * @return PageDTO
     */
    public PageDTO findPagerAffiche( AfficheQueryDTO dto, int start, int limit,String sidx,String sord );

    /**
	 * 保存公告信息
	 * @param dto 公告信息DTO
	 */
    public void saveAffiche( AfficheDTO dto );

    /**
	 * 删除公告信息
	 * @param affId 要删除的公告信息ID
	 */
    public void removeAffiche( Long affId );

    /**
	 * 批量删除公告信息
	 * @param affIds 要批量删除的公告信息数组ID
	 */
    public void removeAffiches( Long[] affIds );

    /**
	 * 修改公告信息
	 * @param dto 公告信息DTO
	 */
    public void mergeAffiche( AfficheDTO dto );

    /**
	 * 根据标题查询公告信息
	 * @param affTitle 公告信息标题
	 * @return List<AfficheDTO>：
	 */
    public List<AfficheDTO> findAfficheByName( String affTitle );

    /**
     * 更新公告信息
     * @param dto AfficheDTO
     * */
    public void updateAfficheInfo( AfficheDTO dto );
    
    /**
     * 根据公告ID获取公告信息
     * @param affId
     * @return AfficheDTO
     */
    public AfficheDTO findById(Long affId );
    /**
	 * 导入公告业务数据
	 * 
	 * @param importFile
	 */
	public void importAffiche (File importFile);
}
