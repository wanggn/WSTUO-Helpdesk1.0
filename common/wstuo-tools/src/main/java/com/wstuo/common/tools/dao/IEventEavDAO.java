package com.wstuo.common.tools.dao;

import com.wstuo.common.tools.entity.EventEav;
import com.wstuo.common.dao.IEntityDAO;

/**
 * Event Eav DAO Interface
 * @author WSTUO
 *
 */
public interface IEventEavDAO extends IEntityDAO<EventEav>{

}
