package com.wstuo.common.tools.dao;

import com.wstuo.common.tools.dto.ExportInfoDTO;
import com.wstuo.common.tools.entity.ExportInfo;
import com.wstuo.common.dao.IEntityDAO;
import com.wstuo.common.dto.PageDTO;

/**
 * 导出信息DAO接口
 * @author WSTUO
 *
 */
public interface IExportInfoDAO extends IEntityDAO<ExportInfo>{
	/**
	 * 分页查询下载信息
	 * @param dto
	 * @param start
	 * @param limit
	 * @param sord
	 * @param sidx
	 * @return PageDTO
	 */
	PageDTO findExportInfoPager(ExportInfoDTO dto,int start, int limit,String sord,String sidx);
	/**
	 * 获取文件大小
	 * @param urlFile
	 * @return long
	 */
	long getFileLength(String urlFile);
}
