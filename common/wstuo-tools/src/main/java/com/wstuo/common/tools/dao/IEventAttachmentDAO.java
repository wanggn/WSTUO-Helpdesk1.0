package com.wstuo.common.tools.dao;

import java.util.List;


import com.wstuo.common.tools.dto.EventAttachmentDTO;
import com.wstuo.common.tools.entity.EventAttachment;
import com.wstuo.common.dao.IEntityDAO;

/**
 * Event Attachment DAO interface class
 * @author QXY
 */
public interface IEventAttachmentDAO extends IEntityDAO<EventAttachment>{
	
	/**
	 * 查询全部
	 * @param dto EventAttachmentDTO
	 * @return List<EventAttachment>
	 */
    List<EventAttachment> findAllEventAttachment(EventAttachmentDTO dto); 
    /**
	 * 根据ID查询
	 * @param ids
	 * @param eno
	 * @return List<EventAttachment>
	 */
    List<EventAttachment> findByEventAttachment(Long[] ids,Long eno);
}
