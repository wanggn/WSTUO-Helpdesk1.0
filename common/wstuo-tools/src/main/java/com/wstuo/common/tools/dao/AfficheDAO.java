package com.wstuo.common.tools.dao;

import com.wstuo.common.tools.dto.AfficheQueryDTO;
import com.wstuo.common.tools.entity.AfficheInfo;
import com.wstuo.common.dao.BaseDAOImplHibernate;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.util.StringUtils;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import java.util.Date;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * 公告管理DAO
 * 
 * @author QXY date 2010/9/10
 * **/
public class AfficheDAO extends BaseDAOImplHibernate<AfficheInfo> implements IAfficheDAO {
	/**
	 * 公告分页查询
	 * 
	 * @param dto
	 *            查询条件DTO
	 * @param start
	 *            查询第几页
	 * @param limit
	 *            每页行数
	 * @return PageDTO
	 */
	public PageDTO findPager(AfficheQueryDTO dto, int start, int limit, String sidx, String sord) {
		final DetachedCriteria dc = DetachedCriteria.forClass(AfficheInfo.class);

		if (dto != null) {
			/*
			 * According to the announcement of the start time and end time *
			 */

			if ("portal".equals(dto.getQueryFlag())) {

				Calendar c1 = new GregorianCalendar();
				c1.setTime(new Date());
				c1.set(Calendar.HOUR_OF_DAY, 0);
				c1.set(Calendar.MINUTE, 0);
				c1.set(Calendar.SECOND, 0);
				//发布公告的开始时间要小于等于当天的凌晨
				//dc.add(Restrictions.le("affStart", c1.getTime()));
				//发布公告的结束时间要大于昨天23点59分59秒
				Calendar c2 = new GregorianCalendar();
				c2.setTime(new Date());
				c2.set(Calendar.DATE, c2.get(Calendar.DATE) - 1);
				c2.set(Calendar.HOUR_OF_DAY, 23);
				c2.set(Calendar.MINUTE, 59);
				c2.set(Calendar.SECOND, 59);
				
				dc.add(Restrictions.gt("affEnd", c2.getTime()));
			}

			// 时间搜索
			if (dto.getAffstart() != null && dto.getAffEnd() == null) {

				// 开始时间大于搜索时间或结束时间大于搜索时间
				dc.add(Restrictions.ge("affStart", dto.getAffstart()));

			}
			if (dto.getAffstart() == null && dto.getAffEnd() != null) {

				dc.add(Restrictions.ge("affEnd", dto.getAffEnd()));
			}

			if (dto.getAffstart() != null && dto.getAffEnd() != null) {

				// Calendar endTimeCl=new GregorianCalendar();
				// endTimeCl.setTime(dto.getAffEnd());
				// endTimeCl.set(Calendar.DATE, endTimeCl.get(Calendar.DATE)+1);
				//
				// Calendar startTimeCl=new GregorianCalendar();
				// startTimeCl.setTime(dto.getAffstart());
				// startTimeCl.set(Calendar.DATE, startTimeCl.get(Calendar.DATE)-1);

				dc.add(Restrictions.and(Restrictions.ge("affStart", dto.getAffstart()), Restrictions.le("affEnd", dto.getAffEnd())));
			}

			/*
			 * Notice the title page according to the fuzzy query *
			 */
			if (StringUtils.hasText(dto.getAffTitle())) {
				dc.add(Restrictions.like("affTitle", dto.getAffTitle(), MatchMode.ANYWHERE));
			}

			if (dto.getVisibleState() == 1) {
				dc.add(Restrictions.eq("visibleState", dto.getVisibleState()));
			}

			if (dto.getCompanyNo() != null) {// 所属客户
				dc.add(Restrictions.eq("companyNo", dto.getCompanyNo()));
			}
		}


        //排序
        if ( StringUtils.hasText(sord) &&   StringUtils.hasText(sidx) )
        {
            if ( sord.equals("desc")){
                dc.addOrder( Order.desc( sidx ) );
            } else{
                dc.addOrder( Order.asc( sidx ) );
            }
        }

		return super.findPageByCriteria(dc, start, limit);
	}

	/**
	 * 根据公告标题查询
	 * 
	 * @param affTitle
	 *            公告标题
	 * @return List<AfficheInfo> date 2010/9/10
	 * **/
	@SuppressWarnings("unchecked")
	public List<AfficheInfo> findByName(String affTitle) {
		return (List<AfficheInfo>) getHibernateTemplate().find("from AfficheInfo where affTitle like ?", "%" + affTitle + "%");
	}
}
