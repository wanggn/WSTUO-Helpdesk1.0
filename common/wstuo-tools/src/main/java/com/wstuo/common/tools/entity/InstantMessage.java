package com.wstuo.common.tools.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;

import com.wstuo.common.entity.BaseEntity;

/**
 * 即时信息实体类
 * @author spring
 * date 2010-10-11
 */
@SuppressWarnings({ "serial", "rawtypes" })
@Entity
public class InstantMessage  extends BaseEntity
{
    /**
     * imId
     */
	@Id
    @GeneratedValue( strategy = GenerationType.AUTO )
    private Long imId;

    /**
     * title
     */
    private String title;

    /**
     * sendUsers
     */
    private String sendUsers;

    /**
     * receiveUsers
     */
    private String receiveUsers;

    /**
     * content
     */
    @Lob
    private String content;

    /**
     * status
     */
    private String status;

    /**
     * remarks
     */
    private String remarks;


    /**
     * sendTime
     */
    private Date sendTime;
    
    private String receiverFullName;
    private String senderFullName;
    private String imType;
    private Long requestEno;
    private String dwrStatus;//0为未推送   1为已推送
    private String isReadRequest;//0为未查看请求详细  1未已查看请求详细
    private String eventType;//所属模块
    
    
    
    public String getEventType() {
		return eventType;
	}

	public void setEventType(String eventType) {
		this.eventType = eventType;
	}

	public String getIsReadRequest() {
		return isReadRequest;
	}

	public void setIsReadRequest(String isReadRequest) {
		this.isReadRequest = isReadRequest;
	}

	public String getDwrStatus() {
		return dwrStatus;
	}

	public void setDwrStatus(String dwrStatus) {
		this.dwrStatus = dwrStatus;
	}

	public String getImType() {
		return imType;
	}

	public void setImType(String imType) {
		this.imType = imType;
	}

	public Long getRequestEno() {
		return requestEno;
	}

	public void setRequestEno(Long requestEno) {
		this.requestEno = requestEno;
	}

	public Long getImId(  )
    {
        return imId;
    }

    public void setImId( Long imId )
    {
        this.imId = imId;
    }

    public String getTitle(  )
    {
        return title;
    }

    public void setTitle( String title )
    {
        this.title = title;
    }

    public String getSendUsers(  )
    {
        return sendUsers;
    }

    public void setSendUsers( String sendUsers )
    {
        this.sendUsers = sendUsers;
    }

    public String getReceiveUsers(  )
    {
        return receiveUsers;
    }

    public void setReceiveUsers( String receiveUsers )
    {
        this.receiveUsers = receiveUsers;
    }

    public String getContent(  )
    {
        return content;
    }

    public void setContent( String content )
    {
        this.content = content;
    }

    public String getStatus(  )
    {
        return status;
    }

    public void setStatus( String status )
    {
        this.status = status;
    }

    public String getRemarks(  )
    {
        return remarks;
    }

    public void setRemarks( String remarks )
    {
        this.remarks = remarks;
    }


	public Date getSendTime() {
		return sendTime;
	}

	public void setSendTime(Date sendTime) {
		this.sendTime = sendTime;
	}

	public String getReceiverFullName() {
		return receiverFullName;
	}

	public void setReceiverFullName(String receiverFullName) {
		this.receiverFullName = receiverFullName;
	}

	public String getSenderFullName() {
		return senderFullName;
	}

	public void setSenderFullName(String senderFullName) {
		this.senderFullName = senderFullName;
	}
    
}
