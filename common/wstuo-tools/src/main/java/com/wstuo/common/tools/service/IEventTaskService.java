package com.wstuo.common.tools.service;


import com.wstuo.common.tools.dto.EventTaskDTO;
import com.wstuo.common.tools.dto.EventTaskQueryDTO;
import com.wstuo.common.dto.PageDTO;


/**
 * 事件任务Service interface class
 * @author WSTUO
 *
 */
public interface IEventTaskService {

	/**
	 * 分页查询任务
	 * @param queryDto
	 * @param start
	 * @param limit
	 * @return PageDTO
	 */
	PageDTO findPagerEventTask(EventTaskQueryDTO queryDto,int start,int limit);
	
	/**
	 * 保存事件任务
	 * @param dto
	 */
	void saveEventTask(EventTaskDTO dto);
	/**
	 * 编辑事件任务
	 * @param dto
	 */
	void editEventTask(EventTaskDTO dto);
	/**
	 * 删除事件任务
	 * @param ids
	 */
	void deleteEventTask(final Long[] ids);
	
	/**
	 * 查询指定时间段任务
	 * @param eventTaskDTO
	 * @return boolean
	 */
	boolean timeConflict(EventTaskDTO eventTaskDTO);
	
	/**
	 * 根据ID查找EventTask
	 * @param taskId
	 * @return EventTaskDTO
	 */
	EventTaskDTO findEventTaskById(Long taskId);
	
	/**
	 * 判断是否有删除权限
	 * @param ids
	 * @return boolean
	 */
	boolean findTaskByIds(Long[] ids);
	
}
