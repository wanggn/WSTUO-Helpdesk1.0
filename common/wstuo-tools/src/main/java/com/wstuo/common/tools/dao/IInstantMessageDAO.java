package com.wstuo.common.tools.dao;


import com.wstuo.common.tools.dto.InstantMessageQueryDTO;
import com.wstuo.common.tools.entity.InstantMessage;
import com.wstuo.common.dao.IEntityDAO;
import com.wstuo.common.dto.PageDTO;

import java.util.List;

/**
 * 即时信息DAO接口类
 * @author QXY
 */
public interface IInstantMessageDAO
    extends IEntityDAO<InstantMessage>
{
    /**
     * 查询全部即时信息
     * @return List<InstantMessage>
     */
    List<InstantMessage> findInstantMessage(  );

    /**
     * find 根据关键字查询
     * @param keyWord
     * @return List<InstantMessage>
     */
    List<InstantMessage> findByKeyWord( String keyWord );

   /**
    * 即时信息分页查询
    * @param instantmessageQueryDTO
    * @param start
    * @param limit
    * @param sidx
    * @param sord
    * @return PageDTO
    */
    PageDTO findPager( final InstantMessageQueryDTO instantmessageQueryDTO,
    		int start, int limit,String sidx,String sord );

    /**
     * 统计未读的即时信息
     * @return int
     */
    int findUnread(  );
}
