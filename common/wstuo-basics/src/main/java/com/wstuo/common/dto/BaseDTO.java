package com.wstuo.common.dto;


import org.apache.commons.beanutils.BeanUtils;

import com.wstuo.common.exception.ApplicationException;

/**
 * Base class for dtos.
 * 
 * @author wstuo.com
 */
@SuppressWarnings("serial")
public class BaseDTO extends AbstractValueObject {

	protected Long companyNo;
	protected String companyName;
	protected String lastUpdater;
	protected Byte dataFlag = 0;

	public Long getCompanyNo() {
		return companyNo;
	}

	public void setCompanyNo(Long companyNo) {
		this.companyNo = companyNo;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getLastUpdater() {
		return lastUpdater;
	}

	public void setLastUpdater(String lastUpdater) {
		this.lastUpdater = lastUpdater;
	}

	public Byte getDataFlag() {
		return dataFlag;
	}

	public void setDataFlag(Byte dataFlag) {
		this.dataFlag = dataFlag;
	}

	/**
	 * Transform dto to entity.
	 * 
	 * @param dto
	 *            DTO object
	 * @param entity
	 *            domain entity object
	 */
	public static void dto2entity(Object dto, Object entity) {

		try {
			if (dto != null) {
				BeanUtils.copyProperties(entity, dto);
			}
		} catch (Exception ex) {

			throw new ApplicationException(ex);
		}
	}

	/**
	 * Transform entity to dto.
	 * 
	 * @param entity
	 *            doamin entity object
	 * @param dto
	 *            dto object
	 */
	public static void entity2dto(Object entity, Object dto) {

		try {
			if (entity != null) {
				BeanUtils.copyProperties(dto, entity);
			}
		} catch (Exception ex) {
			throw new ApplicationException(ex);
		}
	}
}