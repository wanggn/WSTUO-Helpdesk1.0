package com.wstuo.common.dto;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
/**
 * ADTO interface
 * @author wstuo.com
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface AnnotationPropertyDTO {
    String id();
}