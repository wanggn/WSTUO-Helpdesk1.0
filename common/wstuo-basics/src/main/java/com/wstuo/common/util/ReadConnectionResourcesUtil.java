package com.wstuo.common.util;

import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

/**
 * 
 * @author wstuo.com
 *
 */
public class ReadConnectionResourcesUtil {

	/**
	 * 获取属性文件的值
	 */
	private static final Logger logger = Logger
			.getLogger(ReadConnectionResourcesUtil.class);
	private Properties properties;
	
	public ReadConnectionResourcesUtil(String fileName) {
		// 加载属性文件
		InputStream inputStream = null;
		try {
			inputStream = ReadConnectionResourcesUtil.class.getClassLoader()
					.getResourceAsStream(fileName);
			try {
				properties = new Properties();
				properties.load(inputStream);
			} catch (Exception e) {
				logger.error(e.getMessage());
			} finally {
				if (inputStream != null) {
					inputStream.close();
				}
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}

	public String getProperty(String key) {
		return properties.getProperty(key);
	}
}
