package com.wstuo.common.util;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;

public class DaoUtils {
	public static DetachedCriteria orderBy(String sidx, String sord, DetachedCriteria dc) {
		// 排序
		if (StringUtils.hasText(sord) && StringUtils.hasText(sidx)) {
			if ("desc".equals(sord))
				dc.addOrder(Order.desc(sidx));
			else
				dc.addOrder(Order.asc(sidx));
		}
		return dc;
	}
}
