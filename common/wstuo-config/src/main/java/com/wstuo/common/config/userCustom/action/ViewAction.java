package com.wstuo.common.config.userCustom.action;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import com.opensymphony.xwork2.ActionSupport;
import com.wstuo.common.config.userCustom.dto.UserCustomDTO;
import com.wstuo.common.config.userCustom.dto.ViewDTO;
import com.wstuo.common.config.userCustom.dto.ViewQueryDTO;
import com.wstuo.common.config.userCustom.service.IViewService;
import com.wstuo.common.dto.PageDTO;

/**
 * View action
 * @author Mark
 *
 */
@SuppressWarnings("serial")
public class ViewAction extends ActionSupport {
	
	@Autowired
	private IViewService viewService;
	private ViewDTO viewdto;
	private Long[] ids;
	private List<ViewDTO> list = new ArrayList<ViewDTO>();
	private PageDTO pageDTO=new PageDTO();
	private ViewQueryDTO queryDTO = new ViewQueryDTO();
	private UserCustomDTO userCustomDTO = new UserCustomDTO();
	private Boolean result;
	private String sidx;
	private String sord;
	private int page = 1;
    private int rows = 10;
    private Long viewId;
    
	public UserCustomDTO getUserCustomDTO() {
		return userCustomDTO;
	}
	public void setUserCustomDTO(UserCustomDTO userCustomDTO) {
		this.userCustomDTO = userCustomDTO;
	}
	
	public Boolean getResult() {
		return result;
	}
	public void setResult(Boolean result) {
		this.result = result;
	}
	public ViewDTO getViewdto() {
		return viewdto;
	}
	public void setViewdto(ViewDTO viewdto) {
		this.viewdto = viewdto;
	}
	public Long[] getIds() {
		return ids;
	}
	public void setIds(Long[] ids) {
		this.ids = ids;
	}
	public List<ViewDTO> getList() {
		return list;
	}
	public void setList(List<ViewDTO> list) {
		this.list = list;
	}
	public PageDTO getPageDTO() {
		return pageDTO;
	}
	public void setPageDTO(PageDTO pageDTO) {
		this.pageDTO = pageDTO;
	}
	public ViewQueryDTO getQueryDTO() {
		return queryDTO;
	}
	public void setQueryDTO(ViewQueryDTO queryDTO) {
		this.queryDTO = queryDTO;
	}
	public String getSidx() {
		return sidx;
	}
	public void setSidx(String sidx) {
		this.sidx = sidx;
	}
	public String getSord() {
		return sord;
	}
	public void setSord(String sord) {
		this.sord = sord;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public int getRows() {
		return rows;
	}
	public void setRows(int rows) {
		this.rows = rows;
	}
	
	public Long getViewId() {
		return viewId;
	}
	public void setViewId(Long viewId) {
		this.viewId = viewId;
	}
	
	/**
	 *  分页查找视图
	 * @return String
	 */
	public String findPageView(){
		int start = ( page - 1 ) * rows;
		queryDTO.setStart(start);
		queryDTO.setLimit(rows);
		queryDTO.setSidx(sidx);
		queryDTO.setSord(sord);
		pageDTO = viewService.findPageView(queryDTO);
		return "pageDTO";
	}
	
	/**
	 *  查询所有
	 * @return String
	 */
	public String findAllSystemView(){
		list = viewService.findAllSystemView();
		return SUCCESS;
	}
	
	/**
	 *  查询所有视图
	 * @return String
	 */
	public String findAllView(){
		list = viewService.findAllView(queryDTO);
		return "findSuccess";
	}
	
	/**
	 *  查询所有视图 和用户视图
	 * @return String
	 */
	public String findAllViewAndUserView(){
		userCustomDTO = viewService.findAllViewAndUserView(queryDTO);
		return "findSuccess";
	}
	
	/**
	 *  查询面板视图
	 * @return String
	 */
	public String findAllT_View(){
		userCustomDTO=viewService.findAllViewAndUserView(queryDTO);
		return "userCustomDTO";
	}
	
	/**
	 *  查询登录用户的视图
	 * @return String
	 */
	public String findMyView(){
		userCustomDTO=viewService.findAllViewAndUserView(queryDTO);
		return "userCustomDTO";
	}
	
	/**
	 * 查询视图位置
	 * @return String
	 * */
	public String findallViewAndUserViewTwo(){
		userCustomDTO = viewService.findAllViewAndUserView(queryDTO);
		return "userCustomDTO";
	}
	
	/**
	 *  检查该用户是否已经存在视图
	 * @return String
	 */
	public String checkOutViewIsShow(){
		result = viewService.checkOutViewIsshow(queryDTO);
		return "result";
	}
	
	/**
	 * 保存视图
	 * @return String
	 */
	public String saveView(){
		viewService.saveView(viewdto);
		return SUCCESS;
	}
	
	/**
	 *  编辑视图
	 * @return String
	 */
	public String editView(){
		viewService.updateView(viewdto);
		return SUCCESS;
	}
	
	/**
	 * 删除视图
	 * @return String
	 */
	public String deleteView(){
		result = viewService.deleteView(ids);
		return "result";
	}
	/**
	 * 更新viewCode
	 * @return String
	 */
	public String upateViewCode(){
		viewService.upateViewCode();
		System.err.println("================更新成功！================");
		return SUCCESS;
	}
}
