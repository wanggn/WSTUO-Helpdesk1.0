package com.wstuo.common.config.customfilter.dto;

/**
 * 查询DTO of CustomFilter 
 * @author Mars
 * */
public class CustomFilterQueryDTO {
	private Long filterId;

	private String userName;
	
	private String filterCategory;

	public Long getFilterId() {
		return filterId;
	}

	public void setFilterId(Long filterId) {
		this.filterId = filterId;
	}
	
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getFilterCategory() {
		return filterCategory;
	}

	public void setFilterCategory(String filterCategory) {
		this.filterCategory = filterCategory;
	}

}
