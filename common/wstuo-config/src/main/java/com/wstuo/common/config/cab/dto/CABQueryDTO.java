package com.wstuo.common.config.cab.dto;

import com.wstuo.common.dto.BaseDTO;
/**
 * 变更委员会查询DTO
 * @author WSTUO
 *
 */
public class CABQueryDTO extends BaseDTO {
	private Long cabId;
	private String cabName;
	private String cabDesc;
	
    private String sidx;
    private String sord;
    
	
	
	public String getSidx() {
		return sidx;
	}
	public void setSidx(String sidx) {
		this.sidx = sidx;
	}
	public String getSord() {
		return sord;
	}
	public void setSord(String sord) {
		this.sord = sord;
	}
	public Long getCabId() {
		return cabId;
	}
	public void setCabId(Long cabId) {
		this.cabId = cabId;
	}
	public String getCabName() {
		return cabName;
	}
	public void setCabName(String cabName) {
		this.cabName = cabName;
	}
	public String getCabDesc() {
		return cabDesc;
	}
	public void setCabDesc(String cabDesc) {
		this.cabDesc = cabDesc;
	}
	
}
