package com.wstuo.common.config.onlinelog.dto;

import com.wstuo.common.dto.BaseDTO;
import com.wstuo.common.security.entity.Operation;

import java.util.Date;

/***
 * UserOptLogDTO class.
 *
 * @author spring date 2010-10-11
 */
public class UserOptLogDTO extends BaseDTO
{
    /**
     * UserOptLog ID
     */
    private Long id;

    /**
     * UserOptLog UserName
     */
    private String userName;

    /**
     * UserOptLog Operation
     */
    private Operation operation;

    /**
     * UserOptLog ResourceId
     */
    private Long resourceId;

    /**
     * the resource name which the user was in operation.
     */
    private String resourceName;
    private Long execTime;
    
    private String userFullName;

    public Long getExecTime(  )
    {
        return execTime;
    }

    public void setExecTime( Long execTime )
    {
        this.execTime = execTime;
    }

    public String getResourceName(  )
    {
        return resourceName;
    }

    public void setResourceName( String resourceName )
    {
        this.resourceName = resourceName;
    }

    /**
     * UserOptLog MethodName
     */
    private String methodName;

    /**
     * UserOptLog Time
     */
    private Date time;

    public Long getId(  )
    {
        return id;
    }

    public void setId( Long id )
    {
        this.id = id;
    }

    public String getUserName(  )
    {
        return userName;
    }

    public void setUserName( String userName )
    {
        this.userName = userName;
    }

    public Operation getOperation(  )
    {
        return operation;
    }

    public void setOperation( Operation operation )
    {
        this.operation = operation;
    }

    public Long getResourceId(  )
    {
        return resourceId;
    }

    public void setResourceId( Long resourceId )
    {
        this.resourceId = resourceId;
    }

    public Date getTime(  )
    {
        return time;
    }

    public void setTime( Date time )
    {
        this.time = time;
    }

    public String getMethodName(  )
    {
        return methodName;
    }

    public void setMethodName( String methodName )
    {
        this.methodName = methodName;
    }

	public String getUserFullName() {
		return userFullName;
	}

	public void setUserFullName(String userFullName) {
		this.userFullName = userFullName;
	}
    
    
    
}
