package com.wstuo.common.config.onlinelog.service;

import com.wstuo.common.config.onlinelog.dto.UserOptLogDTO;
import com.wstuo.common.config.onlinelog.dto.UserOptLogQueryDTO;
import com.wstuo.common.config.onlinelog.entity.UserOptLog;
import com.wstuo.common.dto.PageDTO;


/**
 * 用户操作业务类接口.
 * @author QXY date 2011-02-11
 *
 */
public interface IUserOptLogServiceUtil {

   
    /**
     * 保存操作日志.
     * @param entity
     */
    void saveUserOptLog(UserOptLog entity);

    /**
     * 保存操作日志.
     * @param dto
     */
    void saveUserOptLog(UserOptLogDTO dto);

	/**
	 * 分页查询用户操作日志.
	 * @param useroptlogQueryDTO 查询DTO
	 * @param start 开始数据行
	 * @param limit 每页数据条数
	 * @return PageDTO 分页数据
	 */
    PageDTO findUserOptLogByPager(final UserOptLogQueryDTO useroptlogQueryDTO,int start, int limit);
}