package com.wstuo.common.config.basicConfig.dto;

import com.wstuo.common.dto.BaseDTO;
/**
 * 时区DTO
 * @author Will
 *
 */
@SuppressWarnings("serial")
public class TimeZoneDTO extends BaseDTO{

	private Long tid;
	private String centerTimeZone;
	private String enduserTimeZone;
	
	public Long getTid() {
		return tid;
	}
	public void setTid(Long tid) {
		this.tid = tid;
	}

	
	public String getCenterTimeZone() {
		return centerTimeZone;
	}
	public void setCenterTimeZone(String centerTimeZone) {
		this.centerTimeZone = centerTimeZone;
	}
	public String getEnduserTimeZone() {
		return enduserTimeZone;
	}
	public void setEnduserTimeZone(String enduserTimeZone) {
		this.enduserTimeZone = enduserTimeZone;
	}
	
	public TimeZoneDTO(){
		
	}
	
	public TimeZoneDTO(Long tid,String centerTimeZone,String enduserTimeZone){
		super();
		this.tid = tid;
		this.centerTimeZone = centerTimeZone;
		this.enduserTimeZone = enduserTimeZone;
	}
	
	
	
	
}
