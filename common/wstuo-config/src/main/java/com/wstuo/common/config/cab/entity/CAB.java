package com.wstuo.common.config.cab.entity;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.wstuo.common.entity.BaseEntity;

/**
 * 变更审批委员会实体类
 * @author WSTUO
 *
 */
@SuppressWarnings( {"serial","rawtypes"})
@Entity
public class CAB extends BaseEntity {
	@Id
	@GeneratedValue( strategy = GenerationType.AUTO )
	private Long cabId;
	@Column(nullable=false)
	private String cabName;
	@Column(nullable=true)
	private String cabDesc;
	@OneToMany
	private List<CABMember> cabMember;
	public Long getCabId() {
		return cabId;
	}
	public void setCabId(Long cabId) {
		this.cabId = cabId;
	}
	public String getCabName() {
		return cabName;
	}
	public void setCabName(String cabName) {
		this.cabName = cabName;
	}
	public String getCabDesc() {
		return cabDesc;
	}
	public void setCabDesc(String cabDesc) {
		this.cabDesc = cabDesc;
	}
	public List<CABMember> getCabMember() {
		return cabMember;
	}
	public void setCabMember(List<CABMember> cabMember) {
		this.cabMember = cabMember;
	}
	
	
}
