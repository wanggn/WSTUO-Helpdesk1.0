package com.wstuo.common.config.category.dto;

import java.util.Arrays;

import com.wstuo.common.dto.BaseDTO;

/**
 * event category DTO
 * 
 * @version 0.1
 * @author Jet date 2010-9-10
 */
@SuppressWarnings("serial")
public class EventCategoryDTO extends BaseDTO {
	/**
	 * Event type ID
	 */
	private Long eventId;

	/**
	 * Event type Name
	 */
	private String eventName;
	
	private String prentFullName;

	private Integer scores = 0;
	/**
	 * Event type description
	 */
	private String eventDescription;

	/**
	 * Parent node ID
	 */
	private Long parentEventId;

	/**
	 * Parent node Name
	 */
	private String parentEventName;

	private String categoryRoot;

	private String categoryCodeRule;// 配置项分类编号规则

	private Long eavId;// 扩展属性ID
	public Long[] eventIds;
	private String sord;
	private String sidx;
	private Integer start;
	private Integer limit;
	private String  categoryLocation;
	
	private Long formId;//自定义表单Id

	/**
	 * =============================Package field==========================
	 */
	
	
	public Long getEventId() {
		return eventId;
	}

	public String getCategoryLocation() {
		return categoryLocation;
	}

	public void setCategoryLocation(String categoryLocation) {
		this.categoryLocation = categoryLocation;
	}

	public Long getFormId() {
		return formId;
	}

	public void setFormId(Long formId) {
		this.formId = formId;
	}

	public String getSord() {
		return sord;
	}

	public void setSord(String sord) {
		this.sord = sord;
	}

	public String getSidx() {
		return sidx;
	}

	public void setSidx(String sidx) {
		this.sidx = sidx;
	}

	public Long[] getEventIds() {
		return eventIds;
	}

	public void setEventIds(Long[] eventIds) {
		this.eventIds = eventIds;
	}

	public void setEventId(Long eventId) {
		this.eventId = eventId;
	}

	public Integer getScores() {
		return scores;
	}

	public void setScores(Integer scores) {
		this.scores = scores;
	}

	public Integer getStart() {
		return start;
	}

	public void setStart(Integer start) {
		this.start = start;
	}

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public String getEventDescription() {
		return eventDescription;
	}

	public void setEventDescription(String eventDescription) {
		this.eventDescription = eventDescription;
	}

	public Long getParentEventId() {
		return parentEventId;
	}

	public void setParentEventId(Long parentEventId) {
		this.parentEventId = parentEventId;
	}

	public String getParentEventName() {
		return parentEventName;
	}

	public void setParentEventName(String parentEventName) {
		this.parentEventName = parentEventName;
	}

	public Long getEavId() {
		return eavId;
	}

	public void setEavId(Long eavId) {
		this.eavId = eavId;
	}

	public String getCategoryRoot() {
		return categoryRoot;
	}

	public void setCategoryRoot(String categoryRoot) {
		this.categoryRoot = categoryRoot;
	}

	public String getCategoryCodeRule() {
		return categoryCodeRule;
	}

	public void setCategoryCodeRule(String categoryCodeRule) {
		this.categoryCodeRule = categoryCodeRule;
	}

	public String getPrentFullName() {
		return prentFullName;
	}

	public void setPrentFullName(String prentFullName) {
		this.prentFullName = prentFullName;
	}

	public EventCategoryDTO() {

	}

	public EventCategoryDTO(Long eventId, String eventName, String eventDescription, Long parentEventId, String parentEventName, Long eavId, String categoryRoot) {
		super();
		this.eventId = eventId;
		this.eventName = eventName;
		this.eventDescription = eventDescription;
		this.parentEventId = parentEventId;
		this.parentEventName = parentEventName;
		this.eavId = eavId;
		this.categoryRoot = categoryRoot;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((categoryCodeRule == null) ? 0 : categoryCodeRule.hashCode());
		result = prime * result
				+ ((categoryRoot == null) ? 0 : categoryRoot.hashCode());
		result = prime * result + ((eavId == null) ? 0 : eavId.hashCode());
		result = prime
				* result
				+ ((eventDescription == null) ? 0 : eventDescription.hashCode());
		result = prime * result + ((eventId == null) ? 0 : eventId.hashCode());
		result = prime * result + Arrays.hashCode(eventIds);
		result = prime * result
				+ ((eventName == null) ? 0 : eventName.hashCode());
		result = prime * result + ((limit == null) ? 0 : limit.hashCode());
		result = prime * result
				+ ((parentEventId == null) ? 0 : parentEventId.hashCode());
		result = prime * result
				+ ((parentEventName == null) ? 0 : parentEventName.hashCode());
		result = prime * result + ((scores == null) ? 0 : scores.hashCode());
		result = prime * result + ((sidx == null) ? 0 : sidx.hashCode());
		result = prime * result + ((sord == null) ? 0 : sord.hashCode());
		result = prime * result + ((start == null) ? 0 : start.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EventCategoryDTO other = (EventCategoryDTO) obj;
		if (categoryCodeRule == null) {
			if (other.categoryCodeRule != null)
				return false;
		} else if (!categoryCodeRule.equals(other.categoryCodeRule))
			return false;
		if (categoryRoot == null) {
			if (other.categoryRoot != null)
				return false;
		} else if (!categoryRoot.equals(other.categoryRoot))
			return false;
		if (eavId == null) {
			if (other.eavId != null)
				return false;
		} else if (!eavId.equals(other.eavId))
			return false;
		if (eventDescription == null) {
			if (other.eventDescription != null)
				return false;
		} else if (!eventDescription.equals(other.eventDescription))
			return false;
		if (eventId == null) {
			if (other.eventId != null)
				return false;
		} else if (!eventId.equals(other.eventId))
			return false;
		if (!Arrays.equals(eventIds, other.eventIds))
			return false;
		if (eventName == null) {
			if (other.eventName != null)
				return false;
		} else if (!eventName.equals(other.eventName))
			return false;
		if (limit == null) {
			if (other.limit != null)
				return false;
		} else if (!limit.equals(other.limit))
			return false;
		if (parentEventId == null) {
			if (other.parentEventId != null)
				return false;
		} else if (!parentEventId.equals(other.parentEventId))
			return false;
		if (parentEventName == null) {
			if (other.parentEventName != null)
				return false;
		} else if (!parentEventName.equals(other.parentEventName))
			return false;
		if (scores == null) {
			if (other.scores != null)
				return false;
		} else if (!scores.equals(other.scores))
			return false;
		if (sidx == null) {
			if (other.sidx != null)
				return false;
		} else if (!sidx.equals(other.sidx))
			return false;
		if (sord == null) {
			if (other.sord != null)
				return false;
		} else if (!sord.equals(other.sord))
			return false;
		if (start == null) {
			if (other.start != null)
				return false;
		} else if (!start.equals(other.start))
			return false;
		return true;
	}

}
