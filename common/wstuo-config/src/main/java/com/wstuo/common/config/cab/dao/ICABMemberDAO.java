package com.wstuo.common.config.cab.dao;

import com.wstuo.common.config.cab.dto.CABMemberQueryDTO;
import com.wstuo.common.config.cab.entity.CABMember;
import com.wstuo.common.dao.IEntityDAO;
import com.wstuo.common.dto.PageDTO;

/**
 * 变更委员会成员DAO接口类
 * @author WSTUO
 *
 */
public interface ICABMemberDAO extends IEntityDAO<CABMember> {
	/**
	 * 变更委员会成员分页查询
	 * @param cabMemberQueryDto
	 * @param start
	 * @param limit
	 * @return PageDTO
	 */
	PageDTO findPagerCABMember(CABMemberQueryDTO cabMemberQueryDto,int start,int limit);
}
