package com.wstuo.common.config.cab.dto;


/**
 * CABMember DTO
 * @author WSTUO
 *
 */
public class CABMemberDTO {
	private Long cabMemberId;
	private Long approvalMember; 
	private Long delegateMember;
	private String desc;
	private Long cabId;
	public Long getCabMemberId() {
		return cabMemberId;
	}
	public void setCabMemberId(Long cabMemberId) {
		this.cabMemberId = cabMemberId;
	}
	public Long getApprovalMember() {
		return approvalMember;
	}
	public void setApprovalMember(Long approvalMember) {
		this.approvalMember = approvalMember;
	}
	public Long getDelegateMember() {
		return delegateMember;
	}
	public void setDelegateMember(Long delegateMember) {
		this.delegateMember = delegateMember;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public Long getCabId() {
		return cabId;
	}
	public void setCabId(Long cabId) {
		this.cabId = cabId;
	}
	
}
