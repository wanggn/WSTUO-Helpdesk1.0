package com.wstuo.common.config.onlinelog.action;


import com.opensymphony.xwork2.ActionSupport;
import com.wstuo.common.config.onlinelog.dto.UserOnlineLogQueryDTO;
import com.wstuo.common.config.onlinelog.service.IUserOnlineLogService;
import com.wstuo.common.dto.PageDTO;

import org.springframework.beans.factory.annotation.Autowired;


/**
 * 用户在线日志Action.
 * @author QXY.
 */
@SuppressWarnings("serial")
public class UserOnlineLogAction extends ActionSupport {

    @Autowired
    private IUserOnlineLogService useronlinelogService;
    private UserOnlineLogQueryDTO useronlinelogQueryDTO;
    private PageDTO pageDTO;
    private int page = 1;
    private int rows = 10;
    private String sidx;
    private String sord;

    public String getSidx() {
		return sidx;
	}

	public void setSidx(String sidx) {
		this.sidx = sidx;
	}

	public String getSord() {
		return sord;
	}

	public void setSord(String sord) {
		this.sord = sord;
	}

	public IUserOnlineLogService getUseronlinelogService() {

        return useronlinelogService;
    }

    public void setUseronlinelogService(
        IUserOnlineLogService useronlinelogService) {

        this.useronlinelogService = useronlinelogService;
    }

    public UserOnlineLogQueryDTO getUseronlinelogQueryDTO() {

        return useronlinelogQueryDTO;
    }

    public void setUseronlinelogQueryDTO(
        UserOnlineLogQueryDTO useronlinelogQueryDTO) {

        this.useronlinelogQueryDTO = useronlinelogQueryDTO;
    }

    public PageDTO getPageDTO() {

        return pageDTO;
    }

    public void setPageDTO(PageDTO pageDTO) {

        this.pageDTO = pageDTO;
    }

    public int getPage() {

        return page;
    }

    public void setPage(int page) {

        this.page = page;
    }

    public int getRows() {

        return rows;
    }

    public void setRows(int rows) {

        this.rows = rows;
    }


    /**
     * 分页查询用户在线列表.
     * @return SUCCESS
     */
    public String findPager() {
    	useronlinelogQueryDTO.setSidx(sidx);
    	useronlinelogQueryDTO.setSord(sord);
        int start = (page - 1) * rows;
        pageDTO = useronlinelogService.findUserOnlineByPager(useronlinelogQueryDTO,start, rows);
        pageDTO.setPage(page);
        pageDTO.setRows(rows);
        return SUCCESS;
    }
}