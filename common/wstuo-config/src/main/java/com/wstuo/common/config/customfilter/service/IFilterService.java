package com.wstuo.common.config.customfilter.service;

import java.util.List;
import java.util.Map;

import com.wstuo.common.config.customfilter.dto.CustomExpressionDTO;
import com.wstuo.common.config.customfilter.dto.CustomFilterDTO;
import com.wstuo.common.config.customfilter.dto.CustomFilterQueryDTO;
import com.wstuo.common.dto.PageDTO;

public interface IFilterService {
	

	/**
	 * 根据过滤器查询数据
	 * @param filterId
	 */
	PageDTO findPageByFilter(Long filterId,int start,int limit,String sidx,String sord);
	
	/**
	 * 根据过滤器查询数据
	 * @param filterId
	 * @param start
	 * @param limit
	 * @param sidx
	 * @param sord
	 * @return
	 */
	List findByFilter(Long filterId,int start,int limit,String sidx,String sord);
	/**
	 * 根据类名获取表字段的信息
	 * @param className
	 * @return Map
	 */
	Map getClassMetaDataByClassName(String className);
	
	
	/**
	 * 根据用户名获取用户的过滤器
	 * @param queryDTO
	 */
	List<CustomFilterDTO> findFiltersByUserName(CustomFilterQueryDTO queryDTO);
	
	/**
	 * 根据过滤器ID查找表达式
	 * @param filterId
	 */
	List<CustomExpressionDTO> findExpressionsByFilterId(Long filterId);
	
	/**
	 * 添加过滤器
	 * @param dto
	 * @param orgs
	 */
	void save(CustomFilterDTO dto,String orgs);
	
	/**
	 * 更新过滤器
	 * @param dto
	 * @param orgs
	 */
	void update(CustomFilterDTO dto,String orgs);
	
	/**
	 * 删除过滤器
	 * @param ids
	 */
	void delete(Long[] ids);
	
	/**
	 * 根据过滤器ID获取过滤器信息
	 * @param filterId
	 */
	CustomFilterDTO findById(Long filterId);
	
	/**
	 * 根据过滤器查询数据
	 * @param filterId
	 */
     PageDTO findPageByFilter(Long filterId,Long [] companyNos,Long[] categorys,int start,int limit,String sidx,String sord);
     
     
     
     /**
 	 * 查询我的私有过滤器.
 	 * @param queryDTO
 	 * @return List<CustomFilterDTO>
 	 */
 	List<CustomFilterDTO> findMyFilters(CustomFilterQueryDTO queryDTO);
     
     
     
 	/**
     * 根据服务机构查找SLA
     * @param filterId
     * @return PageDTO
     */
    PageDTO findShareGroups(Long filterId);
    /**
     * 添加请求新建过滤器
     */
    void importNewRequestFilter();
   
    Long importFilter();
    
    Long getFilterViewId();
}
