package com.wstuo.common.config.category.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.wstuo.common.entity.BaseEntity;

/**
 * event category entity Class
 * 
 * @version 0.1
 * @author Wstuo
 *  date 2010-9-10
 */
@SuppressWarnings({ "serial", "rawtypes" })
@Entity
@Table(name = "EventCategory")
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class EventCategory extends BaseEntity {
	@Id
	@Column(name = "EventId")
	private Long eventId;//Event type ID
	@Column(name = "EventName")
	private String eventName;//Event type Name
	private String categoryRoot;
	private Integer scores = 0;
	@Column(name = "EventDescription", length = 200)
	private String eventDescription;//Event type description
	@ManyToOne
	@JoinColumn(name = "ParentEventNo")
	private EventCategory parentEvent;//Parent Category
	@OneToMany(mappedBy = "parentEvent", cascade = CascadeType.REMOVE)
	private List<EventCategory> parentChildren = new ArrayList<EventCategory>();//All the child nodes
	private String categoryCodeRule;// 分类编码规则
	private Long eavId;// 扩展属性ID
	private String path;//路径 
	private String pathAlias;//路径
	private Long formId;//自定义表单Id
	
	public Long getFormId() {
		return formId;
	}

	public void setFormId(Long formId) {
		this.formId = formId;
	}

	public Long getEventId() {
		return eventId;
	}

	public void setEventId(Long eventId) {
		this.eventId = eventId;
	}

	public Integer getScores() {
		return scores;
	}

	public void setScores(Integer scores) {
		this.scores = scores;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public Long getEavId() {
		return eavId;
	}

	public void setEavId(Long eavId) {
		this.eavId = eavId;
	}

	public String getEventDescription() {
		return eventDescription;
	}

	public void setEventDescription(String eventDescription) {
		this.eventDescription = eventDescription;
	}

	public EventCategory getParentEvent() {
		return parentEvent;
	}

	public void setParentEvent(EventCategory parentEvent) {
		this.parentEvent = parentEvent;
	}

	public List<EventCategory> getParentChildren() {
		return parentChildren;
	}

	public void setParentChildren(List<EventCategory> parentChildren) {
		this.parentChildren = parentChildren;
	}

	public String getCategoryRoot() {
		return categoryRoot;
	}

	public void setCategoryRoot(String categoryRoot) {
		this.categoryRoot = categoryRoot;
	}

	public String getCategoryCodeRule() {
		return categoryCodeRule;
	}

	public void setCategoryCodeRule(String categoryCodeRule) {
		this.categoryCodeRule = categoryCodeRule;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getPathAlias() {
		return pathAlias;
	}

	public void setPathAlias(String pathAlias) {
		this.pathAlias = pathAlias;
	}

}
