package com.wstuo.common.config.dictionary.entity;

import com.wstuo.common.entity.BaseEntity;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * entity of DataDictionaryItems
 * @author QXY
 * */
@SuppressWarnings( {"serial",
    "rawtypes"
} )
@Entity
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class DataDictionaryItems
    extends BaseEntity
{
    /**
     * DataDictionaryItems no */
	@Id
    @GeneratedValue( strategy = GenerationType.AUTO )
    private Long dcode;

    /**
     * DataDictionaryItems name*/
    private String dname;

    /**
     * DataDictionaryItems flag*/
    private String dflag;

    /**
     * DataDictionaryItems color*/
    private String color;

    /**
     * DataDictionaryItems icon*/
    private String icon;

    /**
     * DataDictionaryGroup */
    /**
     * get&set Method*/
    @ManyToOne( cascade = CascadeType.PERSIST)
    @JoinColumn( name = "groupNo" )
    private DataDictionaryGroup datadicGroup;
    
    private String dno;
   
    public DataDictionaryGroup getDatadicGroup(  )
    {
        return datadicGroup;
    }

    public void setDatadicGroup( DataDictionaryGroup datadicGroup )
    {
        this.datadicGroup = datadicGroup;
    }

    public Long getDcode(  )
    {
        return dcode;
    }

    public void setDcode( Long dcode )
    {
        this.dcode = dcode;
    }

    public String getDname(  )
    {
        return dname;
    }

    public void setDname( String dname )
    {
        this.dname = dname;
    }

    public String getDflag(  )
    {
        return dflag;
    }

    public void setDflag( String dflag )
    {
        this.dflag = dflag;
    }

    public String getColor(  )
    {
        return color;
    }

    public void setColor( String color )
    {
        this.color = color;
    }

    public String getIcon(  )
    {
        return icon;
    }

    public void setIcon( String icon )
    {
        this.icon = icon;
    }

	public String getDno() {
		return dno;
	}

	public void setDno(String dno) {
		this.dno = dno;
	}
    
    
	public DataDictionaryItems(){
		
	}
	
	public DataDictionaryItems(String dname,String dno,DataDictionaryGroup gp){
		
		this.dno=dno;
		this.dname=dname;
		this.dataFlag=1;
		this.datadicGroup=gp;
	}
	
}
