package com.wstuo.common.config.cab.dao;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;

import com.wstuo.common.config.cab.dto.CABMemberQueryDTO;
import com.wstuo.common.config.cab.entity.CABMember;
import com.wstuo.common.dao.BaseDAOImplHibernate;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.util.StringUtils;

/**
 * 变更审批委员会委员DAO类
 * @author WSTUO
 *
 */
public class CABMemberDAO extends BaseDAOImplHibernate<CABMember> implements ICABMemberDAO {
	/**
	 * 变更委员会成员分页查询
	 * @param cabMemberQueryDto
	 * @param start
	 * @param limit
	 * @return PageDTO
	 */
	public PageDTO findPagerCABMember(CABMemberQueryDTO cabMemberQueryDto,int start,int limit){
	 final DetachedCriteria dc = DetachedCriteria.forClass(CABMember.class);
	 
	 
	 	if(cabMemberQueryDto!=null){
	 		
	 		 //排序
	        if(StringUtils.hasText(cabMemberQueryDto.getSord())&&StringUtils.hasText(cabMemberQueryDto.getSidx())){

	        	if("desc".equals(cabMemberQueryDto.getSord())){
	        		
	        		 dc.addOrder(Order.desc(cabMemberQueryDto.getSidx()));
	        		 
	        	}else{
	        		 dc.addOrder(Order.asc(cabMemberQueryDto.getSidx()));
	        	}
	        }else{
	        
	        	dc.addOrder(Order.desc("cabMemberId"));
	        }

	 		
	 	}
	 
        
        return super.findPageByCriteria(dc, start, limit);
	};
}
