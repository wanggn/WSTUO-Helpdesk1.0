package com.wstuo.common.config.basicConfig.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.wstuo.common.config.basicConfig.dao.ITimeZoneDAO;
import com.wstuo.common.config.basicConfig.dto.TimeZoneDTO;
import com.wstuo.common.config.basicConfig.entity.TimeZone;
import com.wstuo.common.util.StringUtils;

/**
 * 时区Service实现类
 * @author will
 *
 */
public class TimeZoneService implements ITimeZoneService {

	@Autowired
	private ITimeZoneDAO timeZoneDAO;
	
	/**
	 * 查询时区
	 */
	@Transactional
	public TimeZoneDTO findTimeZone(){
		TimeZoneDTO dto=new TimeZoneDTO();
		if(timeZoneDAO.findAll().size()>0){
			TimeZoneDTO.entity2dto(timeZoneDAO.findAll().get(0), dto);
		}
		return dto;
	}
	
	/**
	 * 保存修改时区
	 */
	@Transactional
	public void saveOrUpdateTomeZone(TimeZoneDTO dto){
		dto.setCenterTimeZone(dto.getCenterTimeZone().replaceAll("&amp;", "&"));
		dto.setEnduserTimeZone(dto.getEnduserTimeZone().replaceAll("&amp;", "&"));
		TimeZone timeZone=null;
		if(dto.getTid()!=null && dto.getTid()!=0){
			timeZone=timeZoneDAO.findById(dto.getTid());
		}else{
			timeZone=new TimeZone();
		}
		TimeZoneDTO.dto2entity(dto, timeZone);
		timeZoneDAO.merge(timeZone);
	}

	/**
	 * 获取默认的时区设置
	 */
	@Transactional
	public java.util.TimeZone getDefaultTimeZone(String type) {
		TimeZoneDTO dto= findTimeZone();
		java.util.TimeZone timeZone = java.util.TimeZone.getDefault();
		if(dto!=null){
			if("centerTimeZone".equals(type) && StringUtils.hasText(dto.getCenterTimeZone())){
				String tzId = dto.getCenterTimeZone().substring(0, 4);
				timeZone = java.util.TimeZone.getTimeZone(TIME_ZONE_GMT+tzId);
			}else if("enduserTimeZone".equals(type) && StringUtils.hasText(dto.getEnduserTimeZone())){
				String tzId = dto.getEnduserTimeZone().substring(0, 4);
				timeZone = java.util.TimeZone.getTimeZone(TIME_ZONE_GMT+tzId);
			}
		}
		return timeZone;
	}
	
}
