package com.wstuo.common.config.comment.dao;

import com.wstuo.common.config.comment.entity.Comment;
import com.wstuo.common.dao.IEntityDAO;

public interface ICommentDAO extends IEntityDAO<Comment>{

}
