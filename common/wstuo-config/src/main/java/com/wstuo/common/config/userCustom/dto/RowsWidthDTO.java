package com.wstuo.common.config.userCustom.dto;

import com.wstuo.common.dto.BaseDTO;
/**
 * RowsWidth DTO
 * @author Will
 *
 */
public class RowsWidthDTO extends BaseDTO {
	private static final long serialVersionUID = 1L;
	private Long widthId;//宽度id
	private String oneRows;//第一列
	private String twoRows;//第二列
	private String threeRows;//第三列
	private String fourRows;//第四列
	private String loginName;//登录名
	public Long getWidthId() {
		return widthId;
	}
	public void setWidthId(Long widthId) {
		this.widthId = widthId;
	}
	public String getOneRows() {
		return oneRows;
	}
	public void setOneRows(String oneRows) {
		this.oneRows = oneRows;
	}
	
	public String getTwoRows() {
		return twoRows;
	}
	public void setTwoRows(String twoRows) {
		this.twoRows = twoRows;
	}
	public String getThreeRows() {
		return threeRows;
	}
	public void setThreeRows(String threeRows) {
		this.threeRows = threeRows;
	}
	public String getFourRows() {
		return fourRows;
	}
	public void setFourRows(String fourRows) {
		this.fourRows = fourRows;
	}
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	
	
	
}
