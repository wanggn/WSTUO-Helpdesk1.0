package com.wstuo.common.config.historyData.dao;

import com.wstuo.common.config.historyData.dto.HistoryDataQueryDTO;
import com.wstuo.common.config.historyData.entity.HistoryData;
import com.wstuo.common.dao.IEntityDAO;
import com.wstuo.common.dto.PageDTO;
/**
 * 历史数据DAO接口
 * @author Will
 *
 */
public interface IHistoryDataDAO extends IEntityDAO<HistoryData>{
	
	
	/**
	 * 分页查询历史数据.
	 * @param qdto
	 * @return PageDTO
	 */
	PageDTO findHistoryDataPager(HistoryDataQueryDTO qdto);

}
