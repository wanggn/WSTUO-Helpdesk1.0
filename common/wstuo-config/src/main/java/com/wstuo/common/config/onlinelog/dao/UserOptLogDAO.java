package com.wstuo.common.config.onlinelog.dao;


import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.wstuo.common.config.onlinelog.dto.UserOptLogQueryDTO;
import com.wstuo.common.config.onlinelog.entity.UserOptLog;
import com.wstuo.common.dao.BaseDAOImplHibernate;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.util.StringUtils;


/**
 * 用户操作日志DAO类.
 * @author QXY date 2011-02-11
 *
 */
public class UserOptLogDAO extends BaseDAOImplHibernate<UserOptLog>
    implements IUserOptLogDAO {

  
	/**
	 * 分页查询数据.
	 * @param useroptlogQueryDTO 查询DTO
	 * @param start 开始数据行
	 * @param limit 每页数据行
	 */
    public PageDTO findPager(UserOptLogQueryDTO useroptlogQueryDTO,
        int start, int limit) {

        final DetachedCriteria dc = DetachedCriteria.forClass(UserOptLog.class);
        

        if (useroptlogQueryDTO != null) {

            if (useroptlogQueryDTO.getTimeStart() != null) {

                dc.add(Restrictions.gt("time",
                        useroptlogQueryDTO.getTimeStart()));
            }

            if (useroptlogQueryDTO.getTimeEnd() != null) {

                dc.add(Restrictions.lt("time",
                        useroptlogQueryDTO.getTimeEnd()));
            }

            if (StringUtils.hasText(useroptlogQueryDTO.getUserName())) {

                dc.add(Restrictions.like("userName",
                        useroptlogQueryDTO.getUserName(),
                        MatchMode.ANYWHERE));
            }

            if (StringUtils.hasText(useroptlogQueryDTO.getMethodName())) {

                dc.add(Restrictions.like("methodName",
                        useroptlogQueryDTO.getMethodName(),
                        MatchMode.ANYWHERE));
            }
            if (StringUtils.hasText(useroptlogQueryDTO.getResourceName())) {

                dc.add(Restrictions.like("resourceName",
                        useroptlogQueryDTO.getResourceName(),
                        MatchMode.ANYWHERE));
            }
            

            if(useroptlogQueryDTO.getResourceId()!=null){
            	dc.add(Restrictions.eq("resourceId", useroptlogQueryDTO.getResourceId()));
            }
            
            if(useroptlogQueryDTO.getCompanyNo()!=null){
          	  dc.add(Restrictions.eq("companyNo", useroptlogQueryDTO.getCompanyNo()));
            }
            
            
            //排序
            if(StringUtils.hasText(useroptlogQueryDTO.getSord())&&StringUtils.hasText(useroptlogQueryDTO.getSidx())){
                if(useroptlogQueryDTO.getSord().equals("desc"))
                	dc.addOrder(Order.desc(useroptlogQueryDTO.getSidx()));
                else
                	dc.addOrder(Order.asc(useroptlogQueryDTO.getSidx()));
            }else{
            	
            	dc.addOrder(Order.desc( "time" ));
            }
            
        }
        
        
        
        
        return super.findPageByCriteria(dc, start, limit);
    }
}