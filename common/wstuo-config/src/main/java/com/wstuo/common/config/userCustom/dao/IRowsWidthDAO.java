package com.wstuo.common.config.userCustom.dao;

import com.wstuo.common.config.userCustom.entity.RowsWidth;
import com.wstuo.common.dao.IEntityDAO;
/**
 * Rows Width interface DAO
 * @author Will
 *
 */
public interface IRowsWidthDAO extends IEntityDAO<RowsWidth> {

}
