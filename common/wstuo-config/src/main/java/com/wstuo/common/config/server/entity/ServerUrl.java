package com.wstuo.common.config.server.entity;

import javax.persistence.Cacheable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
/**
 * ServerUrl entity
 * @author Will
 *
 */
@SuppressWarnings("serial")
@Entity
@Table(name="serverUrl")
@Cacheable
public class ServerUrl {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long Id ;
	
	private String urlName;

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public String getUrlName() {
		return urlName;
	}

	public void setUrlName(String urlName) {
		this.urlName = urlName;
	}


	

}
