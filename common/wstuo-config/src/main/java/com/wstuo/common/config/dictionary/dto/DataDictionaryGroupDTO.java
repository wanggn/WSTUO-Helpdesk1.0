package com.wstuo.common.config.dictionary.dto;

import com.wstuo.common.dto.BaseDTO;


/**
 * DTO of datadictionary Group.
 * @author QXY
 * */
@SuppressWarnings( "serial" )
public class DataDictionaryGroupDTO extends BaseDTO
{
    /**
     * DataDictionaryGroup no*/
    private Long groupNo;

    /**
     * DataDictionaryGroup name*/
    private String groupName;
    
    private Byte dataFlag;
    
    private String groupCode;

    private String groupType;
    
    
    public String getGroupType() {
		return groupType;
	}

	public void setGroupType(String groupType) {
		this.groupType = groupType;
	}

	/**
     * get&set Method*/
    public Long getGroupNo(  )
    {
        return groupNo;
    }

    public void setGroupNo( Long groupNo )
    {
        this.groupNo = groupNo;
    }

    public String getGroupName(  )
    {
        return groupName;
    }

    public void setGroupName( String groupName )
    {
        this.groupName = groupName;
    }

    public Byte getDataFlag() {
		return dataFlag;
	}

	public void setDataFlag(Byte dataFlag) {
		this.dataFlag = dataFlag;
	}
	
	public String getGroupCode() {
		return groupCode;
	}

	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}
	
	public DataDictionaryGroupDTO(){
		
	}

	
    public DataDictionaryGroupDTO(Long groupNo, String groupName,
			String groupCode) {
		super();
		this.groupNo = groupNo;
		this.groupName = groupName;
		this.groupCode = groupCode;
	}
    
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((dataFlag == null) ? 0 : dataFlag.hashCode());
		result = prime * result
				+ ((groupCode == null) ? 0 : groupCode.hashCode());
		result = prime * result
				+ ((groupName == null) ? 0 : groupName.hashCode());
		result = prime * result + ((groupNo == null) ? 0 : groupNo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DataDictionaryGroupDTO other = (DataDictionaryGroupDTO) obj;
		if (dataFlag == null) {
			if (other.dataFlag != null)
				return false;
		} else if (!dataFlag.equals(other.dataFlag))
			return false;
		if (groupCode == null) {
			if (other.groupCode != null)
				return false;
		} else if (!groupCode.equals(other.groupCode))
			return false;
		if (groupName == null) {
			if (other.groupName != null)
				return false;
		} else if (!groupName.equals(other.groupName))
			return false;
		if (groupNo == null) {
			if (other.groupNo != null)
				return false;
		} else if (!groupNo.equals(other.groupNo))
			return false;
		return true;
	}
}
