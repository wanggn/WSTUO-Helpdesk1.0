package com.wstuo.common.config.dictionary.service;
import java.io.File;
import java.io.InputStream;
import java.util.List;

import com.wstuo.common.config.dictionary.dto.DataDictionaryItemsDTO;
import com.wstuo.common.config.dictionary.dto.DataDictionaryQueryDTO;
import com.wstuo.common.dto.PageDTO;



/**
 * 数据字典项业务类接口.
 * @author QXY
 */
public interface IDataDictionaryItemsService
{

    /**
     * 保存数据字典
     * @param dto 
     */
    void saveDataDictionaryItems( DataDictionaryItemsDTO dto );

    /**
     * 删除数据字典
     * @param no 数据字典项编号
     */
    void removeDataDictionaryItems( Long no );

    /**
     * 批量删除数据字典
     * @param nos 数据字典编号数组
     */
    boolean removeDataDictionaryItemses(Long[] nos );

    /**
     * 保存编辑数据字典
     * @param dto
     * @return DataDictionaryItemsDTO
     */
    DataDictionaryItemsDTO mergeDataDictionaryItems( DataDictionaryItemsDTO dto );

    /**
     * 保存数据字典
     * @param dto
     */
    void updataDataDictionaryItems( DataDictionaryItemsDTO dto );

    /**
     * 批量修改数据字典
     * @param dtos 数据字典DTO集合
     */
    void mergeAllDataDictionaryItem( List<DataDictionaryItemsDTO> dtos );

    /**
     * 根据编号查找数据字典
     * @param no 数据字典编号
     * @return DataDictionaryItemsDTO
     */
    DataDictionaryItemsDTO findDataDictionaryItemById( Long no );

    /**
     * 根据数据字典分组名称查找数据字典
     * @param name 数据字典分组名称
     * @return DataDictionaryItemsDTO
     */
    DataDictionaryItemsDTO findDataDictionaryItemByName( String name );

    /**
     * 分页查询数据字典
     * @param dataDictionaryQueryDTO 查询DTO
     * @return PageDTO
     */
    PageDTO findDictionaryItemsByPage( DataDictionaryQueryDTO dataDictionaryQueryDTO,
    		int start, int limit, String sidx, String sord );

    /**
     * 根据数据字典分组编号查找对应项字典项列表
     * @param no 分组编号
     * @return List<DataDictionaryItemsDTO> 字典项集合
     */
    List<DataDictionaryItemsDTO> findDataDictionaryItemByGroupNo( Long no );
    
    /**
     * 根据数据字典分组编码查数据字典项
     * @param code 字典分组编码
     * @return List<DataDictionaryItemsDTO> 字典项集合
     */
    List<DataDictionaryItemsDTO> findDictionaryItemByGroupCode(String code);
    
    /**
     * 导出数据字典到EXCEL.
     * @return InputStream 输出流
     */
    InputStream exportDataDictionaryItems(DataDictionaryQueryDTO dataDictionaryQueryDTO, 
    		int start, int limit, String sidx, String sord);
    
    /**
     * 从CSV文件导入数据.根据编码
     * @param importCSVFile
     * @param code
     * @return String
     */
    String importDataDictionaryItems(File importCSVFile,String code);
    /**
     * 从CSV文件导入数据.
     * @param importCSVFile
     * @return String
     */
    String importDataDictionaryItems(File importCSVFile);
    
    /**
     * 根据编号查询数据字典信息
     * @param no
     * @return DataDictionaryItemsDTO
     */
    DataDictionaryItemsDTO findByDcode(Long no);
    
    /**
     * 根据编号查找是否为系统数据
     * @param nos
     * @return 1是0 不是；
     */
    int findByDcodeIsSystemData(Long[] nos);
    
    
    
}
