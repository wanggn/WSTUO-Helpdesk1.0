package com.wstuo.common.config.userCustom.dao;

import java.util.List;

import com.wstuo.common.config.userCustom.dto.DashboardQueryDTO;
import com.wstuo.common.config.userCustom.entity.Dashboard;
import com.wstuo.common.dao.IEntityDAO;
import com.wstuo.common.dto.PageDTO;

/**
 * 面板接口DAO
 * @author WSTUO
 *
 */
public interface IDashboardDAO extends IEntityDAO<Dashboard> {
	/**
	 * 查询全部面板
	 * @return List<Dashboard>
	 */
	List<Dashboard> findDashboardAll();
	
	/**
	 * 分页查询
	 * @param queryDTO
	 * @return PageDTO
	 */
	PageDTO findPageDashboard(DashboardQueryDTO queryDTO);
}
