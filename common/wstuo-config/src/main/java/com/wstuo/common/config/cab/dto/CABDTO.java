package com.wstuo.common.config.cab.dto;

import java.util.List;
import com.wstuo.common.config.cab.entity.CABMember;
import com.wstuo.common.dto.BaseDTO;

/**
 * 变更审批委员会DTO类
 * @author WSTUO
 *
 */
public class CABDTO extends BaseDTO {
	private Long cabId;
	private String cabName;
	private String cabDesc;
	private List<CABMember> cabMember;
	public Long getCabId() {
		return cabId;
	}
	public void setCabId(Long cabId) {
		this.cabId = cabId;
	}
	public String getCabName() {
		return cabName;
	}
	public void setCabName(String cabName) {
		this.cabName = cabName;
	}
	public String getCabDesc() {
		return cabDesc;
	}
	public void setCabDesc(String cabDesc) {
		this.cabDesc = cabDesc;
	}
	public List<CABMember> getCabMember() {
		return cabMember;
	}
	public void setCabMember(List<CABMember> cabMember) {
		this.cabMember = cabMember;
	}

	
}
