package com.wstuo.common.config.template.dao;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import com.wstuo.common.config.template.dto.TemplateDTO;
import com.wstuo.common.config.template.entity.Template;
import com.wstuo.common.dao.BaseDAOImplHibernate;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.util.DaoUtils;
import com.wstuo.common.util.StringUtils;


/**
 * 模板DAO
 * @author WSTUO
 *
 */
public class TemplateDAO extends BaseDAOImplHibernate<Template> implements ITemplateDAO{
	/**
	 * 分页查找内容模板列表.
	 * @param dto 查询DTO
	 * @param start 开始数据行
	 * @param limit 每页数据条数
	 * @param sord 排序规则
	 * @param sidx 排序的属性
	 * @return PageDTO 分页数据
	 */
	public PageDTO findTemplatePager(TemplateDTO dto,int start, int limit,String sord,String sidx) {
        DetachedCriteria dc = DetachedCriteria.forClass(Template.class);
        if (dto != null) {
        	if(StringUtils.hasText(dto.getTemplateName())){
        		dc.add(Restrictions.like("templateName", dto.getTemplateName(),MatchMode.ANYWHERE));
        	}
        	if(StringUtils.hasText(dto.getTemplateType())){
        		dc.add(Restrictions.like("templateType", dto.getTemplateType(),MatchMode.ANYWHERE));
        	}
        }
        dc = DaoUtils.orderBy(sidx, sord, dc);
        return super.findPageByCriteria(dc, start, limit);
    }
	
	@SuppressWarnings("unchecked")
	public List<Template> findAllTemplate(TemplateDTO dto){
		DetachedCriteria dc = DetachedCriteria.forClass(Template.class);
        if (dto != null) {
        	if(StringUtils.hasText(dto.getTemplateName())){
        		dc.add(Restrictions.like("templateName", dto.getTemplateName(),MatchMode.ANYWHERE));
        	}
        	if(StringUtils.hasText(dto.getTemplateType())){
        		dc.add(Restrictions.like("templateType", dto.getTemplateType(),MatchMode.ANYWHERE));
        	}
        	if(dto.getFormId()!=null){
        		dc.add(Restrictions.eq("formId", dto.getFormId()));
        	}
        	if(dto.getServiceDirId()!=null){
        		dc.add(Restrictions.eq("serviceDirId", dto.getServiceDirId()));
        	}
        	if(dto.getCategoryNo()!=null){
        		dc.add(Restrictions.eq("categoryNo", dto.getCategoryNo()));
        	}
        	
        	Disjunction dis1 = Restrictions.disjunction(); 
        	dc.add(dis1); 
    		for (String companyNoJson : dto.getTemplateCompanyNoJson()) {
    			dis1.add(Restrictions.like("templateJson", companyNoJson ,MatchMode.ANYWHERE));
			}
    		Disjunction dis2 = Restrictions.disjunction(); 
        	dc.add(dis2); 
    		for (String categoryNoJson : dto.getTemplateCategoryNoJson()) {
    			dis2.add(Restrictions.like("templateJson", categoryNoJson ,MatchMode.ANYWHERE));
			}
    		
        	
        }
        return getHibernateTemplate().findByCriteria(dc);
	}
	
	/**
	 * 查询关联表单的数量
	 * @param formId
	 * @return
	 */
	public Integer findTemplateNumByFormId(Long[] formIds){
		Integer num = 0;
		DetachedCriteria dc = DetachedCriteria.forClass(Template.class);
		if(formIds!=null && formIds.length >0 ){
			dc.add(Restrictions.in("formId", formIds));
		}
		List list = super.getHibernateTemplate().findByCriteria(dc);
		if(list!=null){
			num = list.size();
		}
		return num;
	}
}
