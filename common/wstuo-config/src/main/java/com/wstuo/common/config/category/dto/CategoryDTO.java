package com.wstuo.common.config.category.dto;

import java.util.List;

import com.wstuo.common.config.category.entity.Category;
import com.wstuo.common.dto.BaseDTO;


/**
 * Classification of knowledge classification and configuration
 * @author spring
 *  date 2010-9-28
 */
public class CategoryDTO extends BaseDTO
{
    /**
    *  No
    */
    private Long cno;

    /**
     *  Name
     */
    private String cname;

    /**
     *  imagePath
     */
    private String iconPath;

    /**
     *  Description
     */
    private String description;

    /**
     *  ParentNo
     */
    private Long parentNo;

    /**
     *  ParentName
     */
    private String parentName;
    
    private Long categoryType;

    private String categoryRoot;
    
    private List<Category> children;
    
    private Byte dataFlag;
    private String categoryCodeRule;//配置项分类编号规则
    private Long formId;
    
    
    
    public Long getFormId() {
		return formId;
	}

	public void setFormId(Long formId) {
		this.formId = formId;
	}

	public CategoryDTO(){
    	
    }

    public CategoryDTO(Long cno,String description,String cname,String categoryRoot,Long parentNo,String categoryCodeRule){
    	
    	this.cno=cno;
    	this.description=description;
    	this.cname=cname;
    	this.categoryRoot=categoryRoot;
    	this.parentNo=parentNo;
    	this.categoryCodeRule=categoryCodeRule;
    }


    public Long getCno(  )
    {
        return cno;
    }

    public void setCno( Long cno )
    {
        this.cno = cno;
    }

    public String getCname(  )
    {
        return cname;
    }

    public void setCname( String cname )
    {
        this.cname = cname;
    }

    public String getIconPath(  )
    {
        return iconPath;
    }

    public void setIconPath( String iconPath )
    {
        this.iconPath = iconPath;
    }

    public String getDescription(  )
    {
        return description;
    }

    public void setDescription( String description )
    {
        this.description = description;
    }

    public Long getParentNo(  )
    {
        return parentNo;
    }

    public void setParentNo( Long parentNo )
    {
        this.parentNo = parentNo;
    }

    public String getParentName(  )
    {
        return parentName;
    }

    public void setParentName( String parentName )
    {
        this.parentName = parentName;
    }


	public Long getCategoryType() {
		return categoryType;
	}

	public void setCategoryType(Long categoryType) {
		this.categoryType = categoryType;
	}

	public String getCategoryRoot() {
		return categoryRoot;
	}

	public void setCategoryRoot(String categoryRoot) {
		this.categoryRoot = categoryRoot;
	}

	
	public List<Category> getChildren() {
		return children;
	}

	public void setChildren(List<Category> children) {
		this.children = children;
	}

	public Byte getDataFlag() {
		return dataFlag;
	}

	public void setDataFlag(Byte dataFlag) {
		this.dataFlag = dataFlag;
	}
	
	public String getCategoryCodeRule() {
		return categoryCodeRule;
	}

	public void setCategoryCodeRule(String categoryCodeRule) {
		this.categoryCodeRule = categoryCodeRule;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((categoryCodeRule == null) ? 0 : categoryCodeRule.hashCode());
		result = prime * result
				+ ((categoryRoot == null) ? 0 : categoryRoot.hashCode());
		result = prime * result
				+ ((categoryType == null) ? 0 : categoryType.hashCode());
		result = prime * result
				+ ((children == null) ? 0 : children.hashCode());
		result = prime * result + ((cname == null) ? 0 : cname.hashCode());
		result = prime * result + ((cno == null) ? 0 : cno.hashCode());
		result = prime * result
				+ ((dataFlag == null) ? 0 : dataFlag.hashCode());
		result = prime * result
				+ ((description == null) ? 0 : description.hashCode());
		result = prime * result
				+ ((iconPath == null) ? 0 : iconPath.hashCode());
		result = prime * result
				+ ((parentName == null) ? 0 : parentName.hashCode());
		result = prime * result
				+ ((parentNo == null) ? 0 : parentNo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CategoryDTO other = (CategoryDTO) obj;
		if (categoryCodeRule == null) {
			if (other.categoryCodeRule != null)
				return false;
		} else if (!categoryCodeRule.equals(other.categoryCodeRule))
			return false;
		if (categoryRoot == null) {
			if (other.categoryRoot != null)
				return false;
		} else if (!categoryRoot.equals(other.categoryRoot))
			return false;
		if (categoryType == null) {
			if (other.categoryType != null)
				return false;
		} else if (!categoryType.equals(other.categoryType))
			return false;
		if (children == null) {
			if (other.children != null)
				return false;
		} else if (!children.equals(other.children))
			return false;
		if (cname == null) {
			if (other.cname != null)
				return false;
		} else if (!cname.equals(other.cname))
			return false;
		if (cno == null) {
			if (other.cno != null)
				return false;
		} else if (!cno.equals(other.cno))
			return false;
		if (dataFlag == null) {
			if (other.dataFlag != null)
				return false;
		} else if (!dataFlag.equals(other.dataFlag))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (iconPath == null) {
			if (other.iconPath != null)
				return false;
		} else if (!iconPath.equals(other.iconPath))
			return false;
		if (parentName == null) {
			if (other.parentName != null)
				return false;
		} else if (!parentName.equals(other.parentName))
			return false;
		if (parentNo == null) {
			if (other.parentNo != null)
				return false;
		} else if (!parentNo.equals(other.parentNo))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CategoryDTO [cno=" + cno + ", cname=" + cname + ", iconPath="
				+ iconPath + ", description=" + description + ", parentNo="
				+ parentNo + ", parentName=" + parentName + ", categoryType="
				+ categoryType + ", categoryRoot=" + categoryRoot
				+ ", children=" + children + ", dataFlag=" + dataFlag
				+ ", categoryCodeRule=" + categoryCodeRule + ", formId="
				+ formId + "]";
	}

    
    
}
