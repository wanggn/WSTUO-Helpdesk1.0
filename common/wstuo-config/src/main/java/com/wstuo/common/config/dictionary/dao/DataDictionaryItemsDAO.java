package com.wstuo.common.config.dictionary.dao;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.util.StringUtils;

import com.wstuo.common.config.dictionary.dto.DataDictionaryQueryDTO;
import com.wstuo.common.config.dictionary.entity.DataDictionaryItems;
import com.wstuo.common.dao.BaseDAOImplHibernate;
import com.wstuo.common.dto.PageDTO;

/**
 * 数据字典DAO
 * @author QXY
 */
public class DataDictionaryItemsDAO extends
		BaseDAOImplHibernate<DataDictionaryItems> implements
		IDataDictionaryItemsDAO {

	/**
	 * 分页查询数据字典信息.
	 * @param qdto 查询DTO
	 * @return PageDTO 分页数据
	 */
	public PageDTO findPager(DataDictionaryQueryDTO qdto, 
			int start, int limit , String sidx, String sord) {
		
		final DetachedCriteria dc = DetachedCriteria.forClass(DataDictionaryItems.class);

		
		if (qdto != null) {
			
			if(StringUtils.hasText(qdto.getGroupCode())){
				
				dc.createAlias("datadicGroup", "gp").add(Restrictions.eq("gp.groupCode", qdto.getGroupCode()));
			}
			
			if(qdto.getGroupNo()!=null && qdto.getGroupNo()!=0){
				
				dc.createAlias("datadicGroup", "gp").add(Restrictions.eq("gp.groupNo", qdto.getGroupNo()));
			}
			
			if(StringUtils.hasText(qdto.getDname())){
				dc.add(Restrictions.like("dname", qdto.getDname(),MatchMode.ANYWHERE));
			}

			if(StringUtils.hasText(qdto.getDescription())){
				dc.add(Restrictions.like("description", qdto.getDescription(),MatchMode.ANYWHERE));
			}

		}
		 //排序
        if(StringUtils.hasText(sord)&&StringUtils.hasText(sidx)){
            if(sord.equals("desc"))
            	dc.addOrder(Order.desc(sidx));
            else
            	dc.addOrder(Order.asc(sidx));
        }else{
        	
        	dc.addOrder(Order.desc("dcode"));
        }

		return super.findPageByCriteria(dc, start, limit);
	}
	/**
	 * 根据分组搜索Items
	 * @param groupCode
	 * @return List<DataDictionaryItems>
	 */
	public List<DataDictionaryItems> findItemByGroupCode(String groupCode){
		final DetachedCriteria dc = DetachedCriteria.forClass(DataDictionaryItems.class);
		dc.createAlias("datadicGroup", "gp").add(Restrictions.eq("gp.groupCode",groupCode));
		return getHibernateTemplate().findByCriteria(dc);
	}
	
	/**
	 * 根据分组和名称搜索Items
	 * @param groupCode 分 组
	 * @param name 名称
	 * @return List<DataDictionaryItems>
	 */
	public List<DataDictionaryItems> findItemByGroupCodeAndName(String groupCode,String name){
		final DetachedCriteria dc = DetachedCriteria.forClass(DataDictionaryItems.class);
		dc.createAlias("datadicGroup", "gp").add(Restrictions.eq("gp.groupCode",groupCode));
		dc.add(Restrictions.eq("dname", name));
		return getHibernateTemplate().findByCriteria(dc);
	}
	
	/**
	 * 根据分组编码查询唯一的对象，如果未查询到返回null；
	 * @param groupName
	 * @param name
	 * @return
	 */
	public DataDictionaryItems findUniqueByGroupCodeAndName(String groupCode,String name){
		List<DataDictionaryItems> dictionaryItems = findItemByGroupCodeAndName(groupCode, name);
		if( dictionaryItems != null && dictionaryItems.size() > 0){
			return dictionaryItems.get(0);
		}
		return null;
	}
}
