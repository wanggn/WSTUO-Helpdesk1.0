package com.wstuo.common.config.onlinelog.entity;

import javax.persistence.Entity;
import javax.persistence.Lob;

/**
 * User errlog entity
 * @author will
 *
 */
@Entity
public class UserErrLog extends UserOptLog {
	@Lob
	private String errMsg;
	@Lob
	private String errCause;
	public String getErrMsg() {
		return errMsg;
	}
	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}
	public String getErrCause() {
		return errCause;
	}
	public void setErrCause(String errCause) {
		this.errCause = errCause;
	}

}
