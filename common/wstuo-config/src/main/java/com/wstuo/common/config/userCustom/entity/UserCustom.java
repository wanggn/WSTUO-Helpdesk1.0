package com.wstuo.common.config.userCustom.entity;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.wstuo.common.entity.BaseEntity;

/**
 * 用户自定义项Entity
 * @author WSTUO
 *
 */
@SuppressWarnings({ "serial", "rawtypes" })
@Entity
@Table(name="usercustom")
public class UserCustom extends BaseEntity{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long userCustomId;//ID
	private String loginName;//登录名
	private Long customType;//自定义类型.1:门户模块,2:自定义列
	private String eventType;//事件类型
	@Column(length=1500,nullable=true)
	private String customResult;//自定义结果(用于列的保存结果)
	@ManyToMany
	private List<Dashboard> dashboard;//要显示的面板
	private String layoutType="default";//布局类型
	@ManyToMany
	private List<View> views; //要显示的视图
	@Lob
	private String viewIdStr;  //面板显示ID排序组合字符串
	@Lob
	private String viewRowsStr;//面板显示ID的列排序记录,与id排序组合字符串对应
	
	public String getViewIdStr() {
		return viewIdStr;
	}
	public void setViewIdStr(String viewIdStr) {
		this.viewIdStr = viewIdStr;
	}
	public Long getUserCustomId() {
		return userCustomId;
	}
	public void setUserCustomId(Long userCustomId) {
		this.userCustomId = userCustomId;
	}
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public Long getCustomType() {
		return customType;
	}
	public void setCustomType(Long customType) {
		this.customType = customType;
	}
	public String getEventType() {
		return eventType;
	}
	public void setEventType(String eventType) {
		this.eventType = eventType;
	}
	public String getCustomResult() {
		return customResult;
	}
	public void setCustomResult(String customResult) {
		this.customResult = customResult;
	}
	public List<Dashboard> getDashboard() {
		return dashboard;
	}
	public void setDashboard(List<Dashboard> dashboard) {
		this.dashboard = dashboard;
	}
	public String getLayoutType() {
		return layoutType;
	}
	public void setLayoutType(String layoutType) {
		this.layoutType = layoutType;
	}
	public List<View> getViews() {
		return views;
	}
	public void setViews(List<View> views) {
		this.views = views;
	}
	public String getViewRowsStr() {
		return viewRowsStr;
	}
	public void setViewRowsStr(String viewRowsStr) {
		this.viewRowsStr = viewRowsStr;
	}
	
	
	
}
