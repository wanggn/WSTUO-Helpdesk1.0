package com.wstuo.common.config.category.dao;

import com.wstuo.common.config.category.dto.CategoryDTO;
import com.wstuo.common.config.category.entity.CICategory;
import com.wstuo.common.config.category.entity.Category;
import com.wstuo.common.dao.IEntityDAO;

import java.util.List;

/**
 * ConfigurationItemsDAO Interface 2010.9.13
 *
 * @author spring
 *
 */
public interface ICICategoryDAO
    extends IEntityDAO<CICategory>
{
    /**
     * find all ConfigurationItems
     *
     * @return List<ConfigurationItems>
     */
    List<CICategory> findConfigurationItems( String dtype );
    
    
    CICategory findConfigureItemTree(String dtype);
    
    CICategory findConfigureItemTreeByParentEventId(Long parentEventId);
    /**
     * 设置值.
     * @param latesCICategoryNo
     */
	void setLatesCICategoryNo(Long latesCICategoryNo);
	
	/**
     * 取得下一个编号.
     * @return Long
     */
    Long getNextCICategoryNo();
    
    /**
     * 判断分类是否存在
     * @param ec
     * @return
     */
    public boolean isCategoryExisted(CategoryDTO dto);
    
    public List<CICategory> findSubCategoryByPath(String path);
    /**
     * 获取扩展名称
     * @return Long
     */
    String getFindEavEntity(String eavNo);
    /**
     * 查询子节点id集合
     * @param entity
     * @param cnolist
     */
    void findChildrenCno(Category entity,List<Long> cnolist);
    /**
 	 * 查找分类，如果该分类不存在，则使用默认分类：配置项分类
 	 * @param no
 	 * @return
 	 */
    CICategory findCICategoryById(final Long no );
    /**
     * 根据eavNos查询分类
     */
    List<CICategory> findEavEntity(Long[] eavNos);
}
