package com.wstuo.common.config.updatelevel.dao;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;


import com.wstuo.common.config.updatelevel.dto.UpdateLevelQueryDTO;
import com.wstuo.common.config.updatelevel.entity.UpdateLevel;
import com.wstuo.common.dao.BaseDAOImplHibernate;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.util.StringUtils;
/**
 * UpdateLevel DAO class
 * @author QXY
 *
 */
public class UpdateLevelDAO extends BaseDAOImplHibernate<UpdateLevel>
implements IUpdateLevelDAO{

	/**
	 * 分页查询升级级别
	 * @param qdto
	 * @return PageDTO
	 */
    public PageDTO findPager(UpdateLevelQueryDTO qdto) {

        final DetachedCriteria dc = DetachedCriteria.forClass(UpdateLevel.class);
        int start = 0;
        int limit = 0;

        if (qdto != null) {

            start = qdto.getStart();
            limit = qdto.getLimit();

            if (qdto.getApprovalNo()!=null && qdto.getApprovalNo()!=0) {

                dc.createAlias("approval", "ap");
                dc.add(Restrictions.eq("ap.userId", qdto.getApprovalNo()));
            }

            if (StringUtils.hasText(qdto.getUlName())) {

                dc.add(Restrictions.like("ulName", qdto.getUlName(),
                        MatchMode.ANYWHERE));
            }
            
            
            //排序
            if(StringUtils.hasText(qdto.getSord())&&StringUtils.hasText(qdto.getSidx())){

            	if("desc".equals(qdto.getSord())){
            		
            		 dc.addOrder(Order.desc(qdto.getSidx()));
            		 
            	}else{
            		 dc.addOrder(Order.asc(qdto.getSidx()));
            	}
            }else{
            
            	 dc.addOrder(Order.desc("ulId"));
            }
            
        }
        
        

       
        
        return super.findPageByCriteria(dc, start, limit);
    }
	
}
