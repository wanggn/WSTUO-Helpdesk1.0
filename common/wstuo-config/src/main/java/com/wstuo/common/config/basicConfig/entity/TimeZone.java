package com.wstuo.common.config.basicConfig.entity;

import javax.persistence.Cacheable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.wstuo.common.entity.BaseEntity;

/**
 * 时区实体类
 * @author Will
 *
 */
@SuppressWarnings({ "serial", "rawtypes" })
@Entity
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class TimeZone extends BaseEntity{
	@Id
    @GeneratedValue( strategy = GenerationType.AUTO )
	private Long tid;
	private String centerTimeZone;
	private String enduserTimeZone;
	
	public Long getTid() {
		return tid;
	}
	public void setTid(Long tid) {
		this.tid = tid;
	}

	public String getCenterTimeZone() {
		return centerTimeZone;
	}
	public void setCenterTimeZone(String centerTimeZone) {
		this.centerTimeZone = centerTimeZone;
	}
	public String getEnduserTimeZone() {
		return enduserTimeZone;
	}
	public void setEnduserTimeZone(String enduserTimeZone) {
		this.enduserTimeZone = enduserTimeZone;
	}
	public TimeZone(){
		
	}
	public TimeZone(Long tid,String centerTimeZone,String enduserTimeZone){
		super();
		this.tid = tid;
		this.centerTimeZone = centerTimeZone;
		this.enduserTimeZone = enduserTimeZone;
	}
}
