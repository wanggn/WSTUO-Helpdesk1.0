package com.wstuo.common.config.template.service;

import java.util.List;

import com.wstuo.common.config.template.dto.TemplateDTO;
import com.wstuo.common.config.template.entity.Template;
import com.wstuo.common.dto.PageDTO;
/**
 * 内容模板业务接口
 * @author QXY
 *
 */
public interface ITemplateService {
	/**
	 * 查询内容模板所有
	 * @param templateDTO 内容模板
	 * @param start 开始页数
	 * @param limit 开始记录数
	 * @param sord 排序规则
	 * @param sidx 排序的属性
	 * @return 分页DTO
	 */
	PageDTO findTemplatePager(TemplateDTO templateDTO, int start, int limit,String sord, String sidx);
	/**
	 * 查询所有内容模板
	 * @param templateDTO
	 */
	List<TemplateDTO> findAllTemplate(TemplateDTO templateDTO);
	/**
	 * 新增内容模板
	 * @param dto 模板ＤＴＯ
	 * @param obj 对象
	 */
	void saveTemplate(Object obj,TemplateDTO dto);
	/**
	 * 根据内容模板类型查询
	 * @param type 模板类型
	 * @return 内容模板实体
	 */
	List<Template> findByTemplateType(String type);
	/**
	 * 根据内容模板ID查询出内容详细信息
	 * @param id 内容模板ID
	 * @return 模板DTO
	 */
	TemplateDTO findByTemplateId(Long id);
	/**
	 * 根据ID删除内容模板
	 * @param templateId 内容模板ID
	 */
	void delTemplate(Long[] templateId);
	
	/**
	 * 编辑内容模板名称
	 * @param dto 内容模板DTO
	 */
	void editTemplateName(TemplateDTO dto);
	
	/**
	 * 查询关联表单的数量
	 * @param formId
	 * @return
	 */
	Integer findTemplateNumByFormId(Long[] formIds);
}
