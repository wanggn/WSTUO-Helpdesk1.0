package com.wstuo.common.config.onlinelog.service;

import java.util.List;

import com.wstuo.common.config.onlinelog.dto.UserOnlineLogDTO;
import com.wstuo.common.config.onlinelog.dto.UserOnlineLogQueryDTO;
import com.wstuo.common.dto.PageDTO;

/**
 * 用户在线日志业务类接口.
 * @author QXY date 2011-02-11
 *
 */
public interface IUserOnlineLogService {

	/**
	 * 根据用户代理名查询公司编号
	 * @param userName
	 * @return Long
	 */
	Long findCompanyNoByUserName(String userName);
	
	/**
	 * 根据会话编号查询用户名
	 * @param sessionId 会话编号
	 * @return String
	 */
	String getUserName(String sessionId);
	
	/**
	 * 添加用户在线记录
	 * @param dto 
	 */
	void online(UserOnlineLogDTO dto);

	/**
	 * 添加用户离线记录.
	 * @param sessionId 会话编号
	 * @param loginName 登录名
	 */
	void offline(String sessionId,String loginName);
	/**
	 * 更新用户在线记录
	 * @param dtos 
	 */
	void updateOnline(List<UserOnlineLogDTO> dtos);
	/**
	 * 分页查询用户在线日志.
	 * @param useronlinelogQueryDTO 查询DTO
	 * @param start 开始数据行
	 * @param limit 每页数据条数
	 * @return PageDTO 分页数据
	 */
	PageDTO findUserOnlineByPager(UserOnlineLogQueryDTO useronlinelogQueryDTO,int start, int limit);
}