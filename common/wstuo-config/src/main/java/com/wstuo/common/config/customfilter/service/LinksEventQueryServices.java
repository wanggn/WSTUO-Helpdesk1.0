package com.wstuo.common.config.customfilter.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.wstuo.common.config.customfilter.dao.IFilterDAO;
import com.wstuo.common.config.customfilter.dto.KeyTransferDTO;
import com.wstuo.common.config.customfilter.entity.CustomFilter;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.security.dao.IOrganizationDAO;
import com.wstuo.common.security.entity.Organization;

public class LinksEventQueryServices implements ILinksEventQueryServices {
	@Autowired
	private IOrganizationDAO organizationDAO;
	@Autowired
	private IFilterDAO filterDAO;

	@Transactional
	public PageDTO finddynamic(KeyTransferDTO ktd,Long[] companyNos,int start, int rows, String sidx, String sord) {
		if (ktd.getRowKey().equals("companyNo")) {
			Organization org = organizationDAO.findUniqueBy("orgName", ktd.getRowValue());
			ktd.setRowValue(org.getOrgNo().toString());
		}
		if (ktd.getColKey().equals("companyNo")) {
			Organization org = organizationDAO.findUniqueBy("orgName", ktd.getColValue());
			ktd.setColValue(org.getOrgNo().toString());
		}
		CustomFilter filter = filterDAO.findById(Long.parseLong(ktd.getCustomFilterNo()));
		if (filter == null) {
			filter = new CustomFilter();
			filter.setEntityClass(ktd.getEntityClass());
			filter.setFilterCategory(ktd.getFilterCategory());
		}
		PageDTO pageDTO = filterDAO.findPageByCustomFilter(filter, companyNos, start, rows, sidx, sord, ktd);
		return pageDTO;
	}
}
