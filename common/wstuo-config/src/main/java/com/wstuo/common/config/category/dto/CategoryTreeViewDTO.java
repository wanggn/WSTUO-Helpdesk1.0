package com.wstuo.common.config.category.dto;


import com.wstuo.common.config.category.entity.EventCategory;
import com.wstuo.common.dto.BaseDTO;
import com.wstuo.common.exception.ApplicationException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Category tree class
 * 
 * @version 0.1
 * @author Jet date 2010-9-10
 */
@SuppressWarnings("serial")
public class CategoryTreeViewDTO extends BaseDTO {
	/**
	 * Node name
	 */
	private String data;

	/**
	 * Switch state
	 */
	private String state;

	private String categoryRoot;

	/**
	 * The properties of the current node
	 */
	private Map<String, String> attr = new HashMap<String, String>();

	/**
	 * All nodes
	 */
	private List<CategoryTreeViewDTO> children = new ArrayList<CategoryTreeViewDTO>();

	/**
	 * =======================================Package field===========================================
	 */

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Map<String, String> getAttr() {
		return attr;
	}

	public void setAttr(Map<String, String> attr) {
		this.attr = attr;
	}

	public List<CategoryTreeViewDTO> getChildren() {
		return children;
	}

	public void setChildren(List<CategoryTreeViewDTO> children) {
		this.children = children;
	}

	public String getCategoryRoot() {
		return categoryRoot;
	}

	public void setCategoryRoot(String categoryRoot) {
		this.categoryRoot = categoryRoot;
	}

	/**
	 * Entities into DTO
	 * 
	 * @param entity
	 * @param dto
	 */
	public static void entity2dto(EventCategory entity, CategoryTreeViewDTO dto, List<Long> viewAbleIds) {

		if (entity.getDataFlag() != 99) {
			try {
				// Display Name
				dto.setData(entity.getEventName());
				// Set the map's key and value, a value of entity class
				dto.getAttr().put("id", "" + entity.getEventId());
				dto.getAttr().put("cname", entity.getEventName());
				dto.getAttr().put("description", entity.getEventDescription());
				dto.getAttr().put("categoryRoot", entity.getCategoryRoot());
				// Determine whether the collection is empty
				if (!entity.getParentChildren().isEmpty()) {
					// Off
					// Traverse the node set
					for (EventCategory ecy : entity.getParentChildren()) {

						if (ecy != null && viewAbleIds.contains(ecy.getEventId())) {

							CategoryTreeViewDTO ctDTO = new CategoryTreeViewDTO();

							// Converted to DTO
							CategoryTreeViewDTO.entity2dto(ecy, ctDTO, viewAbleIds);

							// DTO added to the collection of this class
							dto.getChildren().add(ctDTO);

						}
					}
				}
			} catch (Exception ex) {
				throw new ApplicationException("Exception caused while converting Entity into DTO: " + ex.getMessage(), ex);
			}

		}
	}

}
