package com.wstuo.common.config.historyData.dao;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.util.StringUtils;

import com.wstuo.common.config.historyData.dto.HistoryDataQueryDTO;
import com.wstuo.common.config.historyData.entity.HistoryData;
import com.wstuo.common.dao.BaseDAOImplHibernate;
import com.wstuo.common.dto.PageDTO;

/**
 * 历史数据DAO类
 * @author QXY.
 *
 */
public class HistoryDataDAO  extends BaseDAOImplHibernate<HistoryData> implements IHistoryDataDAO{

	
	
	/**
	 * 分页查询历史数据.
	 * @param qdto
	 * @return PageDTO
	 */
	public PageDTO findHistoryDataPager(HistoryDataQueryDTO qdto) {
		
		final DetachedCriteria dc = DetachedCriteria.forClass(HistoryData.class);
		
		int start=0;
		int limit=10;

		if(qdto!=null){
			
			start=qdto.getStart();
			limit=qdto.getLimit();
			
			
			if(StringUtils.hasText(qdto.getEntityName())){
				dc.add(Restrictions.eq("entityName", qdto.getEntityName()));
			}
			
			  //排序
	        if(StringUtils.hasText(qdto.getSord())&&StringUtils.hasText(qdto.getSidx())){

	        	if("desc".equals(qdto.getSord())){
	        		
	        		 dc.addOrder(Order.desc(qdto.getSidx()));
	        		 
	        	}else{
	        		 dc.addOrder(Order.asc(qdto.getSidx()));
	        	}
	        }else{
	        
	        	dc.addOrder(Order.desc("historyNo"));
	        }
			
		}

		

		return super.findPageByCriteria(dc, start, limit);
	}

}
