package com.wstuo.common.config.updatelevel.dto;

import com.wstuo.common.dto.BaseDTO;
/**
 * DTO of update level 
 * @author QXY
 *
 */
public class UpdateLevelDTO extends BaseDTO{

	private Long ulId;
	private String ulName;
	private Long approvalNo;
	private String approvalName;
	
	public Long getUlId() {
		return ulId;
	}
	public void setUlId(Long ulId) {
		this.ulId = ulId;
	}
	public String getUlName() {
		return ulName;
	}
	public void setUlName(String ulName) {
		this.ulName = ulName;
	}
	public Long getApprovalNo() {
		return approvalNo;
	}
	public void setApprovalNo(Long approvalNo) {
		this.approvalNo = approvalNo;
	}
	public String getApprovalName() {
		return approvalName;
	}
	public void setApprovalName(String approvalName) {
		this.approvalName = approvalName;
	}
	
	public UpdateLevelDTO(){
		
	}
	
	public UpdateLevelDTO(Long ulId, String ulName, Long approvalNo,
			String approvalName) {
		super();
		this.ulId = ulId;
		this.ulName = ulName;
		this.approvalNo = approvalNo;
		this.approvalName = approvalName;
	}
	
	

	
}
