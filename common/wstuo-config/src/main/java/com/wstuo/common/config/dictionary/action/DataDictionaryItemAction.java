package com.wstuo.common.config.dictionary.action;

import com.opensymphony.xwork2.ActionSupport;
import com.wstuo.common.config.dictionary.dto.DataDictionaryGroupDTO;
import com.wstuo.common.config.dictionary.dto.DataDictionaryItemsDTO;
import com.wstuo.common.config.dictionary.dto.DataDictionaryQueryDTO;
import com.wstuo.common.config.dictionary.service.IDataDictionaryItemsService;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.exception.ApplicationException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.InputStream;
import java.util.ArrayList;

import java.util.List;

/**
 * 数据字典项Action类.
 * 
 * @author QXY date 2011-02-11
 * 
 */
@SuppressWarnings("serial")
public class DataDictionaryItemAction extends ActionSupport {
	public static final Logger LOGGER = Logger.getLogger(DataDictionaryItemAction.class);
	@Autowired
	private IDataDictionaryItemsService dataDictionaryItemsService;
	private DataDictionaryItemsDTO dataDictionaryItemsDto = new DataDictionaryItemsDTO();
	private DataDictionaryQueryDTO dataDictionaryQueryDto = new DataDictionaryQueryDTO();
	private DataDictionaryGroupDTO dataDictionaryGroupDto = new DataDictionaryGroupDTO();
	private List<DataDictionaryItemsDTO> dataDictionaryItems = new ArrayList<DataDictionaryItemsDTO>();
	private List<DataDictionaryGroupDTO> group = new ArrayList<DataDictionaryGroupDTO>();
	private Long[] dcode;
	private int page = 1;
	private int rows = 10;
	private PageDTO dataDictionarys;
	private Boolean result = false;
	private String effect = "";
	private InputStream exportStream;
	private String fileName = "";
	private String sidx;
	private String sord;
	private List<DataDictionaryItemsDTO> dictionaryItems;
	private Long groupNo;
	private int dataResult;
	
	
	
	
	public int getDataResult() {
		return dataResult;
	}

	public void setDataResult(int dataResult) {
		this.dataResult = dataResult;
	}

	public DataDictionaryItemsDTO getDataDictionaryItemsDto() {
		return dataDictionaryItemsDto;
	}

	public void setDataDictionaryItemsDto(DataDictionaryItemsDTO dataDictionaryItemsDto) {
		this.dataDictionaryItemsDto = dataDictionaryItemsDto;
	}

	public DataDictionaryQueryDTO getDataDictionaryQueryDto() {
		return dataDictionaryQueryDto;
	}

	public void setDataDictionaryQueryDto(DataDictionaryQueryDTO dataDictionaryQueryDto) {
		this.dataDictionaryQueryDto = dataDictionaryQueryDto;
	}

	public DataDictionaryGroupDTO getDataDictionaryGroupDto() {
		return dataDictionaryGroupDto;
	}

	public void setDataDictionaryGroupDto(DataDictionaryGroupDTO dataDictionaryGroupDto) {
		this.dataDictionaryGroupDto = dataDictionaryGroupDto;
	}

	public List<DataDictionaryItemsDTO> getDataDictionaryItems() {
		return dataDictionaryItems;
	}

	public void setDataDictionaryItems(List<DataDictionaryItemsDTO> dataDictionaryItems) {
		this.dataDictionaryItems = dataDictionaryItems;
	}

	public List<DataDictionaryGroupDTO> getGroup() {
		return group;
	}

	public void setGroup(List<DataDictionaryGroupDTO> group) {
		this.group = group;
	}

	public Long[] getDcode() {
		return dcode;
	}

	public void setDcode(Long[] dcode) {
		this.dcode = dcode;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getRows() {
		return rows;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}

	public PageDTO getDataDictionarys() {
		return dataDictionarys;
	}

	public void setDataDictionarys(PageDTO dataDictionarys) {
		this.dataDictionarys = dataDictionarys;
	}

	public Boolean getResult() {
		return result;
	}

	public void setResult(Boolean result) {
		this.result = result;
	}

	public String getEffect() {
		return effect;
	}

	public void setEffect(String effect) {
		this.effect = effect;
	}

	public InputStream getExportStream() {
		return exportStream;
	}

	public void setExportStream(InputStream exportStream) {
		this.exportStream = exportStream;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getSidx() {
		return sidx;
	}

	public void setSidx(String sidx) {
		this.sidx = sidx;
	}

	public String getSord() {
		return sord;
	}

	public void setSord(String sord) {
		this.sord = sord;
	}

	public List<DataDictionaryItemsDTO> getDictionaryItems() {
		return dictionaryItems;
	}

	public void setDictionaryItems(List<DataDictionaryItemsDTO> dictionaryItems) {
		this.dictionaryItems = dictionaryItems;
	}

	public Long getGroupNo() {
		return groupNo;
	}

	public void setGroupNo(Long groupNo) {
		this.groupNo = groupNo;
	}

	/**
	 * 分页查询数据字典项.
	 * 
	 * @return SUCCESS
	 */
	public String find() {
		int start = (page - 1) * rows;

		dataDictionarys = dataDictionaryItemsService.findDictionaryItemsByPage(dataDictionaryQueryDto, start, rows, sidx, sord);
		dataDictionarys.setPage(page);
		dataDictionarys.setRows(rows);
		return SUCCESS;
	}

	/**
	 * 根据数据字典分组Key查找数据字典项.
	 * 
	 * @return String
	 */
	public String findByCode() {

		dataDictionaryItems = dataDictionaryItemsService.findDictionaryItemByGroupCode(dataDictionaryQueryDto.getGroupCode());
		return "dataDictionaryItems";
	}
	
	
	/**
	 * 根据数据字典分组Key查找数据字典项.
	 * 
	 * @return String
	 */
	public String findByCodeJsonp() {
		fileName = "success_jsonpCallback([{name:'John'}])";
		return "fileName";
	}

	/**
	 * 根据数据字典分组Key查找数据字典项.
	 * 
	 * @return String
	 */
	public String findByCodeByClient() {

		dataDictionaryItems = dataDictionaryItemsService.findDictionaryItemByGroupCode(dataDictionaryQueryDto.getGroupCode());
		return "dataDictionaryItems";
	}

	/**
	 * 根据数据字典分组Key查找数据字典项.
	 * 
	 * @return String
	 */
	public String findByGroupNo() {

		dataDictionaryItems = dataDictionaryItemsService.findDataDictionaryItemByGroupNo(dataDictionaryQueryDto.getGroupNo());
		return "dataDictionaryItems";
	}

	/**
	 * 根据dcode查询数据字典项
	 * 
	 * @return String
	 */

	public String findByDcode() {
		dataDictionaryItemsDto = dataDictionaryItemsService.findDataDictionaryItemById(groupNo);
		return "dataDictionaryItemsDto";
	}

	/**
	 * 保存数据字典项.
	 * 
	 * @return SUCCESS
	 */
	public String save() {

		dataDictionaryItemsService.saveDataDictionaryItems(dataDictionaryItemsDto);

		return SUCCESS;
	}

	/**
	 * 删除数据字典项.
	 * 
	 * @return SUCCESS
	 */
	public String delete() {

		try {

			result = dataDictionaryItemsService.removeDataDictionaryItemses(dcode);

		} catch (Exception ex) {

			throw new ApplicationException("ERROR_DATA_CAN_NOT_DELETE", ex);
		}

		return "result";
	}

	/**
	 * 修改数据字典项.
	 * 
	 * @return SUCCESS
	 */
	public String merge() {
		dataDictionaryItemsService.mergeDataDictionaryItems(dataDictionaryItemsDto);

		return SUCCESS;
	}

	/**
	 * 额外加入的方方法
	 * 
	 * @return String
	 */
	public String findItsmByGroup() {
		find();

		return "showDataDictionaryItems";
	}

	/**
	 * 导出数据.
	 * 
	 * @return String
	 */
	public String exportDictionaryItemData() {
		fileName = dataDictionaryQueryDto.getGroupCode() + ".csv";
		exportStream = dataDictionaryItemsService.exportDataDictionaryItems(dataDictionaryQueryDto, 0, 10000, sidx, sord);
		return "exportFileSuccessful";
	}

	/**
	 * 导入数据
	 * 
	 * @return String
	 */
	public String importDataDictionaryItemData() {
		effect = dataDictionaryItemsService.importDataDictionaryItems(dataDictionaryQueryDto.getImportFile(), dataDictionaryItemsDto.getGroupCode());
		return "importResult";
	}
	/**
	 * 是否为系统配置数据
	 * @return
	 */
	public String findByDcodeIsSystemData(){
		dataResult=dataDictionaryItemsService.findByDcodeIsSystemData(dcode);
		return "dataResult";
	}

}
