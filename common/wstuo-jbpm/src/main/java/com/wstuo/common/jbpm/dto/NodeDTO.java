package com.wstuo.common.jbpm.dto;

import java.util.Map;

import com.wstuo.common.dto.BaseDTO;
/**
 * 流程任务DTO
 * @author WSTUO
 *
 */
@SuppressWarnings("serial")
public class NodeDTO extends BaseDTO {
	private Integer index;
	private String nodeTag;//标签
	private String nodeName;//节点名
	private Map<String,String> nodeAttribute;//节点属性
	public String getNodeName() {
		return nodeName;
	}
	public void setNodeName(String nodeName) {
		this.nodeName = nodeName;
	}
	public Map<String, String> getNodeAttribute() {
		return nodeAttribute;
	}
	public void setNodeAttribute(Map<String, String> nodeAttribute) {
		this.nodeAttribute = nodeAttribute;
	}
	public Integer getIndex() {
		return index;
	}
	public void setIndex(Integer index) {
		this.index = index;
	}
	public String getNodeTag() {
		return nodeTag;
	}
	public void setNodeTag(String nodeTag) {
		this.nodeTag = nodeTag;
	}
	
	
}
