package com.wstuo.common.jbpm.dto;

import java.util.List;

import com.wstuo.common.security.entity.Organization;
import com.wstuo.common.security.entity.User;

public class AssignDTO {
	private Organization group;
	private User assignee;
	private List<String> candidateGroupsNo;
	private List<String> candidateUsersNo;
	
	public List<String> getCandidateGroupsNo() {
		return candidateGroupsNo;
	}
	public void setCandidateGroupsNo(List<String> candidateGroupsNo) {
		this.candidateGroupsNo = candidateGroupsNo;
	}
	public List<String> getCandidateUsersNo() {
		return candidateUsersNo;
	}
	public void setCandidateUsersNo(List<String> candidateUsersNo) {
		this.candidateUsersNo = candidateUsersNo;
	}
	public Organization getGroup() {
		return group;
	}
	public void setGroup(Organization group) {
		this.group = group;
	}
	public User getAssignee() {
		return assignee;
	}
	public void setAssignee(User assignee) {
		this.assignee = assignee;
	}
	
}
