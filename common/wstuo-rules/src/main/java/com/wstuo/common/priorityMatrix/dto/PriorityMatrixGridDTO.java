package com.wstuo.common.priorityMatrix.dto;

/**
 * 优先级矩阵DTO类
 * @author WSTUO
 *
 */
public class PriorityMatrixGridDTO {
	private Long urgencyId ;//紧急度
	private String urgencyName ;
	private Long affectId;//影响范围
	private String affectName;
	private Long priorityId;//优先级
	private String priorityName;
	public Long getUrgencyId() {
		return urgencyId;
	}
	public void setUrgencyId(Long urgencyId) {
		this.urgencyId = urgencyId;
	}
	public Long getAffectId() {
		return affectId;
	}
	public void setAffectId(Long affectId) {
		this.affectId = affectId;
	}
	public Long getPriorityId() {
		return priorityId;
	}
	public void setPriorityId(Long priorityId) {
		this.priorityId = priorityId;
	}
	public String getUrgencyName() {
		return urgencyName;
	}
	public void setUrgencyName(String urgencyName) {
		this.urgencyName = urgencyName;
	}
	public String getAffectName() {
		return affectName;
	}
	public void setAffectName(String affectName) {
		this.affectName = affectName;
	}
	public String getPriorityName() {
		return priorityName;
	}
	public void setPriorityName(String priorityName) {
		this.priorityName = priorityName;
	}

}
