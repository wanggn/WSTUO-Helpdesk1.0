package com.wstuo.common.rules.dto;

import com.wstuo.common.dto.AbstractValueObject;

/**
 * the Query DTO of Rule PageDTO
 * 
 * @author <a href="mailto:376890523@qq.com">jeff</a>
 * @version 0.1 2010-9-29
 * */
public class RuleQueryDTO extends AbstractValueObject {
	/**
	 * rule name
	 */
	private String ruleName;

	/**
	 * description
	 */
	private String description;

	/**
	 * start
	 */
	private Integer start;

	/**
	 * limit
	 */
	private Integer limit;

	/**
	 * rule package no
	 */
	private Long rulePackageNo;
	private String packageName;
	private String flagName;

	public String getFlagName() {
		return flagName;
	}

	public void setFlagName(String flagName) {
		this.flagName = flagName;
	}

	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	public String getRuleName() {
		return ruleName;
	}

	public void setRuleName(String ruleName) {
		this.ruleName = ruleName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getStart() {
		return start;
	}

	public void setStart(Integer start) {
		this.start = start;
	}

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	public Long getRulePackageNo() {
		return rulePackageNo;
	}

	public void setRulePackageNo(Long rulePackageNo) {
		this.rulePackageNo = rulePackageNo;
	}
}
