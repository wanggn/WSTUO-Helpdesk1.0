package com.wstuo.common.rules;

import java.io.Serializable;

/**
 * 事件
 * @author QXY
 *
 */
public class Incident implements Serializable{
    private Long ino;
    private String title;
    private String content;
    private String owner;
    private Integer priority;
    private String assignee;
    private String status;

    public Incident(  )
    {
        super(  );

        // TODO Auto-generated constructor stub
    }

    public Incident( Long ino, String title, String content, String owner, Integer priority, String assignee )
    {
        super(  );
        this.ino = ino;
        this.title = title;
        this.content = content;
        this.owner = owner;
        this.priority = priority;
        this.assignee = assignee;
    }

    public String getStatus(  )
    {
        return status;
    }

    public void setStatus( String status )
    {
        this.status = status;
    }

    public Long getIno(  )
    {
        return ino;
    }

    public void setIno( Long ino )
    {
        this.ino = ino;
    }

    public String getTitle(  )
    {
        return title;
    }

    public void setTitle( String title )
    {
        this.title = title;
    }

    public String getContent(  )
    {
        return content;
    }

    public void setContent( String content )
    {
        this.content = content;
    }

    public String getOwner(  )
    {
        return owner;
    }

    public void setOwner( String owner )
    {
        this.owner = owner;
    }

    public Integer getPriority(  )
    {
        return priority;
    }

    public void setPriority( Integer priority )
    {
        this.priority = priority;
    }

    public String getAssignee(  )
    {
        return assignee;
    }

    public void setAssignee( String assignee )
    {
        this.assignee = assignee;
    }
}
