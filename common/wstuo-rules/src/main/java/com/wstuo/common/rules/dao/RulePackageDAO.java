package com.wstuo.common.rules.dao;

import com.wstuo.common.rules.dto.RulePackageDTO;
import com.wstuo.common.rules.entity.RulePackage;
import com.wstuo.common.dao.BaseDAOImplHibernate;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.util.DaoUtils;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.util.StringUtils;

/**
 * dao of rule package
 * 
 * @author <a href="mailto:376890523@qq.com">jeff</a>
 * @version 0.1 2010.9.28
 * */
public class RulePackageDAO extends BaseDAOImplHibernate<RulePackage> implements IRulePackageDAO {
	/**
	 * 查询所有规则包
	 * 
	 * @return List<RulePackage>
	 */
	@SuppressWarnings("unchecked")
	public List<RulePackage> findRules() {
		String hql = " from RulePackage rp where rp.ruleFlag='rules'";
		return getHibernateTemplate().find(hql);
	}

	/**
	 * 分页查询规则包
	 * 
	 * @param packageDTO
	 * @param sidx
	 * @param sord
	 * @param start
	 * @param rows
	 * @return PageDTO
	 */
	public PageDTO findPager(RulePackageDTO packageDTO, String sidx, String sord, int start, int rows) {
		DetachedCriteria dc = DetachedCriteria.forClass(RulePackage.class);
		if(packageDTO!=null && packageDTO.getModule()!=null && packageDTO.getModule().equals("request")){
			String[] flag=new String[]{"requestProce","saveRequest"};
			dc.add(Restrictions.in("flagName", flag));
		}else if(packageDTO!=null && packageDTO.getModule()!=null && packageDTO.getModule().equals("change")){
			String[] flag=new String[]{"changeProce","saveChange"};
			dc.add(Restrictions.in("flagName", flag));
		}else{
			String[] flag=new String[]{"requestProce","changeProce","saveRequest","saveChange"};
			dc.add(Restrictions.in("flagName", flag));
		}
		if (StringUtils.hasText(packageDTO.getFlagName())) {
			dc.add(Restrictions.eq("flagName", packageDTO.getFlagName()));
		}
		if (StringUtils.hasText(packageDTO.getPackageName())) {
				dc.add(Restrictions.like("packageName", packageDTO.getPackageName(), MatchMode.ANYWHERE));
		}
		if (StringUtils.hasText(packageDTO.getRulePackageRemarks())) {
			dc.add(Restrictions.like("rulePackageRemarks", packageDTO.getRulePackageRemarks(), MatchMode.ANYWHERE));
		}
		// 排序
		dc = DaoUtils.orderBy(sidx, sord, dc);
		return super.findPageByCriteria(dc, start, rows);
	}
}
