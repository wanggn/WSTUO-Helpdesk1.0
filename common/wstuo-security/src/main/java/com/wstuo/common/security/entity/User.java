package com.wstuo.common.security.entity;

import java.util.Date;
import java.util.Set;
import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import com.wstuo.common.entity.BaseEntity;
import com.wstuo.common.security.utils.FullNameFactory;
import com.wstuo.common.sha.JDKMessageDigest;
import com.wstuo.common.util.StringUtils;

/**
 * Entity class User
 * 
 */
@SuppressWarnings({ "serial", "rawtypes" })
@Entity
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Table(name = "T_User")
public class User extends BaseEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long userId;
	@Column(unique = true)
	private String loginName; // 登录帐号
	private String password; // 密码
	private String firstName; // 姓名
	private String lastName; // 名字
	private String fullName;// 真实名称
	private String email; // 邮件
	private String moblie; // 手机
	private String phone; // 电话
	private String officePhone; // 办公电话
	@ManyToOne
	@JoinColumn(name = "orgNo")
	private Organization orgnization; // 组织(机构)
	@ManyToMany(fetch = FetchType.LAZY)
	private Set<Role> roles; // 角色
	private Boolean userState = false;// 状态
	private Float userCost =(float) 0;// 成本
	private String job; // 职称
	private String fax; // 传真
	private String msn;// msn
	private String officeAddress; // 办公地址
	private String pinyin;// 姓名拼音
	private Date birthday;// 生日
	private String position;// 职务
	private String icCard;// 证件号
	private Boolean sex = false;// 性别
	private String lcallCodeOper;// 小灵呼语音卡工号
	private String domain;// 用户所在的域名
	private String distinguishedName;// 用户所在域的详细位置
	private String extension;// 分机号

	private String fullNameCode = "ChineseNameCode";// 真实姓名显示方式
	private String remoteHost;// 小灵呼语音卡远程主机
	private String remotePort;// 小灵呼语音卡远程端口
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_tcGroup")
	private Set<TechnicalGroup> tcGroup;
	private Boolean holidayStatus = false;// 休假状态
	@ManyToMany(fetch = FetchType.LAZY)
	private Set<Organization> belongsGroup;// 所属组
	private String randomId;
	@OneToOne(cascade=CascadeType.ALL,fetch=FetchType.LAZY)//,orphanRemoval=true
	private UserInfoClient userInfoClient;
	private String endUserStyle;//终端用户打开页面的方式
	/**
	 * 取得真实姓名
	 */

	public String getFullName() {

		if (fullName != null && !"".equals(fullName)) {

			return fullName;

		} else {
			if (firstName != null && lastName != null && fullNameCode != null && (fullNameCode.equals("ChineseNameCode") || fullNameCode.equals("WesternNameCode"))) {
				return FullNameFactory.getInstance(fullNameCode).getFullName(firstName, lastName);
			} else {
				return loginName;
			}
		}
	}

	public String getRandomId() {
		return randomId;
	}

	public void setRandomId(String randomId) {
		this.randomId = randomId;
	}

	public void setFullName(String fullName) {
		if (fullName != null && !"".equals(fullName)) {

			this.fullName = fullName;

		} else {
			if ((firstName != null || lastName != null)&& fullNameCode != null && (fullNameCode.equals("ChineseNameCode") || fullNameCode.equals("WesternNameCode"))) {
				this.fullName = FullNameFactory.getInstance(fullNameCode).getFullName(firstName, lastName);
			} else {
				this.fullName = loginName;
			}
		}
	}

	public Set<TechnicalGroup> getTcGroup() {
		return tcGroup;
	}

	public void setTcGroup(Set<TechnicalGroup> tcGroup) {
		this.tcGroup = tcGroup;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		if (password.length() != 40) {
			JDKMessageDigest sha = new JDKMessageDigest();
			this.password = sha.Encryption(password);
		} else {
			this.password = password;
		}
	}

	public String getEmail() {
		return email;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMoblie() {
		return moblie;
	}

	public void setMoblie(String moblie) {
		this.moblie = moblie;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getOfficePhone() {
		return officePhone;
	}

	public void setOfficePhone(String officePhone) {
		this.officePhone = officePhone;
	}

	public Organization getOrgnization() {
		return orgnization;
	}

	public void setOrgnization(Organization orgnization) {
		this.orgnization = orgnization;
	}

	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	public String getJob() {
		return job;
	}

	public void setJob(String job) {
		this.job = job;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getMsn() {
		return msn;
	}

	public void setMsn(String msn) {
		this.msn = msn;
	}

	public String getOfficeAddress() {
		return officeAddress;
	}

	public void setOfficeAddress(String officeAddress) {
		this.officeAddress = officeAddress;
	}

	public String getPinyin() {
		return pinyin;
	}

	public void setPinyin(String pinyin) {
		this.pinyin = pinyin;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getIcCard() {
		return icCard;
	}

	public void setIcCard(String icCard) {
		this.icCard = icCard;
	}

	public String getLcallCodeOper() {
		return lcallCodeOper;
	}

	public void setLcallCodeOper(String lcallCodeOper) {
		this.lcallCodeOper = lcallCodeOper;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public String getDistinguishedName() {
		return distinguishedName;
	}

	public void setDistinguishedName(String distinguishedName) {
		this.distinguishedName = distinguishedName;
	}

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	public String getFullNameCode() {
		return fullNameCode;
	}

	public void setFullNameCode(String fullNameCode) {
		
		if(StringUtils.hasText(fullNameCode)){
			
			this.fullNameCode = fullNameCode;
		} else {

			this.fullNameCode = "ChineseNameCode";
		}

	}

	public String getRemoteHost() {
		return remoteHost;
	}

	public void setRemoteHost(String remoteHost) {
		this.remoteHost = remoteHost;
	}

	public String getRemotePort() {
		return remotePort;
	}

	public void setRemotePort(String remotePort) {
		this.remotePort = remotePort;
	}

	public Set<Organization> getBelongsGroup() {
		return belongsGroup;
	}

	public void setBelongsGroup(Set<Organization> belongsGroup) {
		this.belongsGroup = belongsGroup;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Boolean getUserState() {
		return userState;
	}

	public void setUserState(Boolean userState) {
		this.userState = userState;
	}

	public Float getUserCost() {
		return userCost;
	}

	public void setUserCost(Float userCost) {
		this.userCost = userCost;
	}

	public Boolean getSex() {
		return sex;
	}

	public void setSex(Boolean sex) {
		this.sex = sex;
	}

	public Boolean getHolidayStatus() {
		return holidayStatus;
	}

	public void setHolidayStatus(Boolean holidayStatus) {
		this.holidayStatus = holidayStatus;
	}

	public UserInfoClient getUserInfoClient() {
		return userInfoClient;
	}

	public void setUserInfoClient(UserInfoClient userInfoClient) {
		this.userInfoClient = userInfoClient;
	}

	public String getEndUserStyle() {
		return endUserStyle;
	}

	public void setEndUserStyle(String endUserStyle) {
		this.endUserStyle = endUserStyle;
	}
	
}
