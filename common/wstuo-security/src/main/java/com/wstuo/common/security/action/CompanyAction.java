package com.wstuo.common.security.action;

import java.io.File;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;
import com.wstuo.common.security.service.ICompanyService;

/**
 * 公司Action类
 * @author Administrator
 *
 */
@SuppressWarnings("serial")
public class CompanyAction  extends ActionSupport{
	@Autowired
	private ICompanyService companyService;
	private String effect;
	private File importFile;
	private InputStream exportFile;
	private String paName;
	private String paLogo;
	
	
	public String getPaName() {
		return paName;
	}

	public void setPaName(String paName) {
		this.paName = paName;
	}

	public String getPaLogo() {
		return paLogo;
	}

	public void setPaLogo(String paLogo) {
		this.paLogo = paLogo;
	}

	public String getEffect() {
		return effect;
	}

	public void setEffect(String effect) {
		this.effect = effect;
	}

	public File getImportFile() {
		return importFile;
	}

	public void setImportFile(File importFile) {
		this.importFile = importFile;
	}

	public InputStream getExportFile() {
		return exportFile;
	}

	public void setExportFile(InputStream exportFile) {
		this.exportFile = exportFile;
	}

	/**
	 * 导入公司信息.
	 * @return import result
	 */
	public String importCompanyInfo(){
		effect=companyService.importCompanyInfo(importFile.getAbsolutePath());
		return "importResult";
	}
	
	/**
	 * 导出公司信息.
	 * @return export inputStream
	 */
	public String exportCompanyInfo(){
		try {
			paName=java.net.URLDecoder.decode(paName, "utf-8");
			exportFile=companyService.exportCompanyInfo(paName,paLogo);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "exportFile";
	}
}
