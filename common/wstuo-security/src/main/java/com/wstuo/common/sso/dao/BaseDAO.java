package com.wstuo.common.sso.dao;

import com.wstuo.common.sso.util.utilproper;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class BaseDAO
{
    public Connection conn = null;
    public PreparedStatement ps = null;
    public ResultSet rs = null;
    public String hql=null;
    public BaseDAO(  )
    {
        runlink(  );
    }

    public void runlink(  )
    {
        try
        {
            this.conn =
                DriverManager.getConnection( utilproper.getInstance(  ).getUrl(  ),
                                             utilproper.getInstance(  ).getusername(  ),
                                             utilproper.getInstance(  ).getpassword(  ) );
            this.hql=utilproper.getInstance(  ).getHql();
        } catch ( SQLException e )
        {
            System.out.println( "连接失败" );
            e.printStackTrace(  );
        }
    }

    public void stoplink(  )
    {
        try
        {
            if ( this.rs != null )
            {
                this.rs.close(  );
                this.rs = null;
            }

            if ( this.ps != null )
            {
                this.ps.close(  );
                this.ps = null;
            }

            if ( ( this.conn != null ) && ( ! this.conn.isClosed(  ) ) )
            {
                this.conn.close(  );
                this.conn = null;
            }
        } catch ( SQLException ex )
        {
            System.err.println( "释放资源失败" );
            ex.printStackTrace(  );
        }
    }

    static
    {
        try
        {
            Class.forName( utilproper.getInstance(  ).getdriverName(  ) );
        } catch ( ClassNotFoundException e )
        {
            System.err.println( "加载驱动失败" );
            e.printStackTrace(  );
        }
    }
}
