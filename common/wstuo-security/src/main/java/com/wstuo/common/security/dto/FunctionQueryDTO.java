package com.wstuo.common.security.dto;

import com.wstuo.common.dto.BaseDTO;

/**
 * Class FunctionQueryDTO.
 * 
 * @author will
 * 
 */
public class FunctionQueryDTO extends BaseDTO {
	private Long resNo;
	private String resCode;
	private Long parentFunctionNo;
	private String resName;
	private String resIcon;
	private String resUrl;
	private Integer start;
	private Integer limit;
	private String sord;
	private String sidx;

	public String getSord() {
		return sord;
	}

	public void setSord(String sord) {
		this.sord = sord;
	}

	public String getSidx() {
		return sidx;
	}

	public void setSidx(String sidx) {
		this.sidx = sidx;
	}

	public FunctionQueryDTO() {
		super();
	}

	public Long getResNo() {
		return resNo;
	}

	public void setResNo(Long resNo) {
		this.resNo = resNo;
	}

	public String getResCode() {
		return resCode;
	}

	public void setResCode(String resCode) {
		this.resCode = resCode;
	}

	public Long getParentFunctionNo() {
		return parentFunctionNo;
	}

	public void setParentFunctionNo(Long parentFunctionNo) {
		this.parentFunctionNo = parentFunctionNo;
	}

	public String getResName() {
		return resName;
	}

	public void setResName(String resName) {
		this.resName = resName;
	}

	public String getResIcon() {
		return resIcon;
	}

	public void setResIcon(String resIcon) {
		this.resIcon = resIcon;
	}

	public String getResUrl() {
		return resUrl;
	}

	public void setResUrl(String resUrl) {
		this.resUrl = resUrl;
	}

	public Integer getStart() {
		return start;
	}

	public void setStart(Integer start) {
		this.start = start;
	}

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

}
