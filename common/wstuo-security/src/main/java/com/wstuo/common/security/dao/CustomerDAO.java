package com.wstuo.common.security.dao;

import com.wstuo.common.dao.BaseDAOImplHibernate;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.security.dto.CustomerQueryDTO;
import com.wstuo.common.security.entity.Customer;
import com.wstuo.common.util.DaoUtils;
import com.wstuo.common.util.StringUtils;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * 客户信息DAO方法.
 * @author will
 */
@SuppressWarnings( "unchecked" )
public class CustomerDAO
    extends BaseDAOImplHibernate<Customer>
    implements ICustomerDAO
{
	
	@Autowired
	private IOrganizationDAO organizationDAO;
	
    /**
     * 分页查找公司分页信息.
     * @param qdto
     * @param sidx
     * @param sord
     * @return 分页信息 PageDTO
     */
    public PageDTO findPager( CustomerQueryDTO qdto,String sidx, String sord )
    {
        DetachedCriteria dc = DetachedCriteria.forClass( Customer.class );
        int start = 0;
        int limit = 0;

        if ( qdto != null )
        {
            start = qdto.getStart(  );
            limit = qdto.getLimit(  );
            
            
            //数据隔离
            if(qdto.getCompanyNo()!=null){
            	
            	 dc.add(Restrictions.eq("companyNo", qdto.getCompanyNo()));
            }

            if ( qdto.getCompanyNo(  ) != null )
            {
                dc.createAlias( "parentOrg", "par" );
                dc.add( Restrictions.eq( "par.orgNo",
                                         qdto.getCompanyNo(  ) ) );
            }

            if ( StringUtils.hasText( qdto.getOrgName(  ) ) )
            {
                dc.add( Restrictions.like( "orgName",
                                           qdto.getOrgName(  ),
                                           MatchMode.ANYWHERE ) );
            }

            if ( StringUtils.hasText( qdto.getOfficePhone(  ) ) )
            {
                dc.add( Restrictions.like( "officePhone",
                                           qdto.getOfficePhone(  ),
                                           MatchMode.ANYWHERE ) );
            }

            if ( StringUtils.hasText( qdto.getEmail(  ) ) )
            {
                dc.add( Restrictions.like( "email",
                                           qdto.getEmail(  ),
                                           MatchMode.ANYWHERE ) );
            }

            if ( StringUtils.hasText( qdto.getAddress(  ) ) )
            {
                dc.add( Restrictions.like( "address",
                                           qdto.getAddress(  ),
                                           MatchMode.ANYWHERE ) );
            }
        }

      //排序
        dc = DaoUtils.orderBy(sidx, sord, dc);
        
        return super.findPageByCriteria( dc, start, limit );
    }

    /**
     * 根据公司信息查找客户列表.
     * @param companyNo 公司编号 Long companyNo
     * @return 客户列表  List<Customer>
     */
    public List<Customer> findByCompanyNo( Long companyNo )
    {
        List<Customer> customers =
            super.getHibernateTemplate(  ).find( " from Customer cus where cus.parentOrg.orgNo=" + companyNo );

        return customers;
    }
    
    
    
    
    /**
     * 重写SAVE方法.
     * @param entity
     */
     @Override
     public void save(Customer entity){
     	if(entity.getOrgNo()==null || entity.getOrgNo()==0){
     		entity.setOrgNo(organizationDAO.getLatestOrganizationNo());
     	}else{
     		if(entity.getOrgNo()>organizationDAO.getLatestOrganizationNo()){
     			organizationDAO.setLatesOrganizationNo(entity.getOrgNo());
     		}
     	}
     	super.save(entity);
     	organizationDAO.increment();//自增1.
     }
    
     /**
      * 重写Merge方法
      * @param entity
      * @return Customer
      */
     @Override
     public Customer merge(Customer entity){
    	if(entity.getOrgNo()!=null){
      		if(entity.getOrgNo()>=organizationDAO.getLatestOrganizationNo()){
      			organizationDAO.setLatesOrganizationNo(entity.getOrgNo()+1);
      		}
      	}else{
      		organizationDAO.increment();//自增1.
    	}
    	 
    	 return super.merge(entity);
     }
    
}
