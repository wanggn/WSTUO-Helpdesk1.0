package com.wstuo.common.security.dto;

import com.wstuo.common.dto.AbstractValueObject;

/**
 * class OperationDTO.
 * 
 * @author will
 * 
 */
@SuppressWarnings("serial")
public class OperationQueryDTO extends AbstractValueObject {
	private String resCode;
	private String resName;
	private String resIcon;
	private Integer sequence = 0;
	private String resUrl;
	private Integer start;
	private Integer limit;
	private Long functionNo;
	private String sord;
	private String sidx;
	private String functionResCode;// tan add 20120309

	public String getSord() {
		return sord;
	}

	public void setSord(String sord) {
		this.sord = sord;
	}

	public String getSidx() {
		return sidx;
	}

	public void setSidx(String sidx) {
		this.sidx = sidx;
	}

	public OperationQueryDTO() {
		super();
	}

	public Long getFunctionNo() {
		return functionNo;
	}

	public void setFunctionNo(Long functionNo) {
		this.functionNo = functionNo;
	}

	public Integer getStart() {
		return start;
	}

	public void setStart(Integer start) {
		this.start = start;
	}

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	public String getResCode() {
		return resCode;
	}

	public void setResCode(String resCode) {
		this.resCode = resCode;
	}

	public String getResName() {
		return resName;
	}

	public void setResName(String resName) {
		this.resName = resName;
	}

	public String getResIcon() {
		return resIcon;
	}

	public void setResIcon(String resIcon) {
		this.resIcon = resIcon;
	}

	public Integer getSequence() {
		return sequence;
	}

	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}

	public String getResUrl() {
		return resUrl;
	}

	public void setResUrl(String resUrl) {
		this.resUrl = resUrl;
	}

	public String getFunctionResCode() {
		return functionResCode;
	}

	public void setFunctionResCode(String functionResCode) {
		this.functionResCode = functionResCode;
	}

}
