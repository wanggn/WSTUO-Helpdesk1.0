package com.wstuo.common.security.utils;

import org.apache.log4j.Logger;

/**
 * 姓名显示工厂
 * @author Administrator
 *
 */
public abstract class FullNameFactory {
	private static final Logger LOGGER = Logger.getLogger(FullNameFactory.class );
	  public static FullName getInstance(String className) {
	    	FullName nc = null;  
	        try {  
	        	nc = (FullName) Class.forName("com.wstuo.common.security.utils."+className).newInstance();  
	        } catch (Exception e) {  
	        	LOGGER.error(e.getMessage());
	        }  
	        return nc;  
	    }  
	  
}
