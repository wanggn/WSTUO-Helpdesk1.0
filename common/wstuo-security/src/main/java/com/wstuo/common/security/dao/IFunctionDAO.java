package com.wstuo.common.security.dao;

import com.wstuo.common.dao.IEntityDAO;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.security.dto.FunctionQueryDTO;
import com.wstuo.common.security.entity.Function;

import java.util.List;

/**
 * 功能的DAO的接口
 * @author will
 */
public interface IFunctionDAO
    extends IEntityDAO<Function>
{
    /**
     * 分页查找功能.
     * @param qdto 查询DTO FunctionQueryDTO 
     * @return 分页数据 PageDTO
     */
    PageDTO findPager( FunctionQueryDTO qdto );

    /**
     * 验证功能相关属性是否存在.
     * @param qdto FunctionQueryDTO 
     * @return Boolean true表示存在，false表示不存在
     */
    Boolean findExist( FunctionQueryDTO qdto );

    /**
     * 根据父功能查找关联的子功能列表
     * @param parentNo 父功能编号Long 
     * @return 子功能列表 List<Function>
     */
    List<Function> findByParentNo( Long parentNo );
    
    /**
     * 根据资源Code查功能
     * @param resCode
     * @return List<Function>
     */
    List<Function> findRoots(String resCode);
}
