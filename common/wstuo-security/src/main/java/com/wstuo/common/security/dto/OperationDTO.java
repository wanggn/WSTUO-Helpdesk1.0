package com.wstuo.common.security.dto;

import com.wstuo.common.dto.BaseDTO;

/**
 * class OperationDTO.
 * @author will
 */
@SuppressWarnings("serial")
public class OperationDTO extends BaseDTO {
	private Long resNo;
	private String resCode;
	private String resName;
	private String resIcon;
	private Integer sequence = 0;
	private String resUrl;
	private Long functionNo;
	private Byte dataFlag;
	private String functionResCode;// tan add 20110721

	public OperationDTO(Long resNo, String resCode, String resName,
			String resUrl, Long functionNo) {

		this.resNo = resNo;
		this.resName = resName;
		this.resCode = resCode;
		this.resUrl = resUrl;
		this.functionNo = functionNo;
	}

	public OperationDTO() {
		super();
	}

	public Long getResNo() {
		return resNo;
	}

	public void setResNo(Long resNo) {
		this.resNo = resNo;
	}

	public String getResCode() {
		return resCode;
	}

	public void setResCode(String resCode) {
		this.resCode = resCode;
	}

	public String getResName() {
		return resName;
	}

	public void setResName(String resName) {
		this.resName = resName;
	}

	public String getResIcon() {
		return resIcon;
	}

	public void setResIcon(String resIcon) {
		this.resIcon = resIcon;
	}

	public Integer getSequence() {
		return sequence;
	}

	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}

	public String getResUrl() {
		return resUrl;
	}

	public void setResUrl(String resUrl) {
		this.resUrl = resUrl;
	}

	public Long getFunctionNo() {
		return functionNo;
	}

	public void setFunctionNo(Long functionNo) {
		this.functionNo = functionNo;
	}

	public Byte getDataFlag() {
		return dataFlag;
	}

	public void setDataFlag(Byte dataFlag) {
		this.dataFlag = dataFlag;
	}

	public String getFunctionResCode() {
		return functionResCode;
	}

	public void setFunctionResCode(String functionResCode) {
		this.functionResCode = functionResCode;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((dataFlag == null) ? 0 : dataFlag.hashCode());
		result = prime * result
				+ ((functionNo == null) ? 0 : functionNo.hashCode());
		result = prime * result
				+ ((functionResCode == null) ? 0 : functionResCode.hashCode());
		result = prime * result + ((resCode == null) ? 0 : resCode.hashCode());
		result = prime * result + ((resIcon == null) ? 0 : resIcon.hashCode());
		result = prime * result + ((resName == null) ? 0 : resName.hashCode());
		result = prime * result + ((resNo == null) ? 0 : resNo.hashCode());
		result = prime * result + ((resUrl == null) ? 0 : resUrl.hashCode());
		result = prime * result
				+ ((sequence == null) ? 0 : sequence.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OperationDTO other = (OperationDTO) obj;
		if (dataFlag == null) {
			if (other.dataFlag != null)
				return false;
		} else if (!dataFlag.equals(other.dataFlag))
			return false;
		if (functionNo == null) {
			if (other.functionNo != null)
				return false;
		} else if (!functionNo.equals(other.functionNo))
			return false;
		if (functionResCode == null) {
			if (other.functionResCode != null)
				return false;
		} else if (!functionResCode.equals(other.functionResCode))
			return false;
		if (resCode == null) {
			if (other.resCode != null)
				return false;
		} else if (!resCode.equals(other.resCode))
			return false;
		if (resIcon == null) {
			if (other.resIcon != null)
				return false;
		} else if (!resIcon.equals(other.resIcon))
			return false;
		if (resName == null) {
			if (other.resName != null)
				return false;
		} else if (!resName.equals(other.resName))
			return false;
		if (resNo == null) {
			if (other.resNo != null)
				return false;
		} else if (!resNo.equals(other.resNo))
			return false;
		if (resUrl == null) {
			if (other.resUrl != null)
				return false;
		} else if (!resUrl.equals(other.resUrl))
			return false;
		if (sequence == null) {
			if (other.sequence != null)
				return false;
		} else if (!sequence.equals(other.sequence))
			return false;
		return true;
	}

}
