package com.wstuo.common.util;

import java.io.IOException;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

/* 
* @version 1.00 2016-01-07
* @author <a href="mailto:will.qiu@WSTUO.com">Will Qiu</a>
*/
public abstract class EncryptUtils {
	
	private static final String DES_KEY="!@WSTUO#";
	private static final String RC4_KEY="gysc22";
	protected EncryptUtils() {
		super();
	}
	
	public static String encrypt(String data){
		String str="";
		try {
			BASE64Encoder b = new BASE64Encoder();
			str=b.encode(data.getBytes());
			
			str=DesUtil.encrypt(str, DES_KEY);
			
			str=RC4.encry_RC4_string(str, RC4_KEY);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return str;
	}
	public static String decrypt(String data){
		String str="";
		try {
			str=RC4.decry_RC4(data, RC4_KEY);
			
			str=DesUtil.decrypt(str, DES_KEY);
			
			BASE64Decoder bd = new BASE64Decoder();
			str=new String(bd.decodeBuffer(str));
			
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return str;
	}
}
