package com.wstuo.common.acl.entity;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * ACL SID ENTITY CALSS
 * @author Administrator
 *
 */
//@Entity(name="acl_sid")
public class ACL_SID {
	@Id
    @GeneratedValue( strategy = GenerationType.AUTO )
	private Long id;
	private Byte principal;
	private String sid;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Byte getPrincipal() {
		return principal;
	}
	public void setPrincipal(Byte principal) {
		this.principal = principal;
	}
	public String getSid() {
		return sid;
	}
	public void setSid(String sid) {
		this.sid = sid;
	}
	
}
