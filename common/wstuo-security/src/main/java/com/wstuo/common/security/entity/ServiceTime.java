package com.wstuo.common.security.entity;

import javax.persistence.Cacheable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.wstuo.common.entity.BaseEntity;

/**
 * 
 * Entity class ServiceTime
 * 
 */
@Entity
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ServiceTime extends BaseEntity {
	public final static long DAY_MILLISECONDS = 86400000L;
	public final static long HOUR_MILLISECONDS = 3600000L;
	public final static long MINU_MILLISECONDS = 60000L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long sid;
	private Boolean allday = false;
	private Integer startHour = 0;
	private Integer startMinute = 0;
	private Integer endHour = 0;
	private Integer endMinute = 0;
	private Integer startNoonHour = 0;
	private Integer startNoonMinute = 0;
	private Integer endNoonHour = 0;
	private Integer endNoonMinute = 0;
	private Boolean monday = false;
	private Boolean tuesday = false;
	private Boolean wednesday = false;
	private Boolean thursday = false;
	private Boolean friday = false;
	private Boolean saturday = false;
	private Boolean sunday = false;
	@OneToOne(optional = true)
	@JoinColumn(unique = true, nullable = false, updatable = false)
	private Organization organization;

	@Transient
	public Long getStartTime() {
		return ((startHour) * HOUR_MILLISECONDS) + (startMinute * MINU_MILLISECONDS);
	}

	@Transient
	public Long getEndTime() {
		return ((endHour) * HOUR_MILLISECONDS) + (endMinute * MINU_MILLISECONDS);
	}

	@Transient
    public Long getStartNoonTime(  )
    {
        return ( startNoonHour * HOUR_MILLISECONDS ) + ( startNoonMinute * MINU_MILLISECONDS );
    }

    @Transient
    public Long getEndNoonTime(  )
    {
        return ( endNoonHour * HOUR_MILLISECONDS ) + ( endNoonMinute * MINU_MILLISECONDS );
    }
	public Integer getStartNoonHour() {
		return startNoonHour;
	}

	public void setStartNoonHour(Integer startNoonHour) {
		this.startNoonHour = startNoonHour;
	}

	public Integer getStartNoonMinute() {
		return startNoonMinute;
	}

	public void setStartNoonMinute(Integer startNoonMinute) {
		this.startNoonMinute = startNoonMinute;
	}

	public Integer getEndNoonHour() {
		return endNoonHour;
	}

	public void setEndNoonHour(Integer endNoonHour) {
		this.endNoonHour = endNoonHour;
	}

	public Integer getEndNoonMinute() {
		return endNoonMinute;
	}

	public void setEndNoonMinute(Integer endNoonMinute) {
		this.endNoonMinute = endNoonMinute;
	}

	public Long getSid() {
		return sid;
	}

	public void setSid(Long sid) {
		this.sid = sid;
	}

	public Boolean getAllday() {
		return allday;
	}

	public void setAllday(Boolean allday) {
		this.allday = allday;
	}

	public Integer getStartHour() {
		return startHour;
	}

	public void setStartHour(Integer startHour) {
		this.startHour = startHour;
	}

	public Integer getStartMinute() {
		return startMinute;
	}

	public void setStartMinute(Integer startMinute) {
		this.startMinute = startMinute;
	}

	public Integer getEndHour() {
		return endHour;
	}

	public void setEndHour(Integer endHour) {
		this.endHour = endHour;
	}

	public Integer getEndMinute() {
		return endMinute;
	}

	public void setEndMinute(Integer endMinute) {
		this.endMinute = endMinute;
	}

	public Boolean getMonday() {
		return monday;
	}

	public void setMonday(Boolean monday) {
		this.monday = monday;
	}

	public Boolean getTuesday() {
		return tuesday;
	}

	public void setTuesday(Boolean tuesday) {
		this.tuesday = tuesday;
	}

	public Boolean getWednesday() {
		return wednesday;
	}

	public void setWednesday(Boolean wednesday) {
		this.wednesday = wednesday;
	}

	public Boolean getThursday() {
		return thursday;
	}

	public void setThursday(Boolean thursday) {
		this.thursday = thursday;
	}

	public Boolean getFriday() {
		return friday;
	}

	public void setFriday(Boolean friday) {
		this.friday = friday;
	}

	public Boolean getSaturday() {
		return saturday;
	}

	public void setSaturday(Boolean saturday) {
		this.saturday = saturday;
	}

	public Boolean getSunday() {
		return sunday;
	}

	public void setSunday(Boolean sunday) {
		this.sunday = sunday;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public boolean isAlreadySetWorkWeek() {
		if (this.friday || this.monday || this.saturday || this.sunday || this.thursday || this.tuesday || this.wednesday) {
			return true;
		} else {
			return false;
		}
	}

}
