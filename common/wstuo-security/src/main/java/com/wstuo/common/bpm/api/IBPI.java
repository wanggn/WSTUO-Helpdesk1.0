package com.wstuo.common.bpm.api;

import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.wstuo.common.bpm.api.ICustomAssign;
import com.wstuo.common.bpm.dto.ActivityCoordinatesDTO;
import com.wstuo.common.bpm.dto.FlowActivityDTO;
import com.wstuo.common.bpm.dto.FlowPropertyDTO;
import com.wstuo.common.bpm.dto.HistoryTaskDTO;
import com.wstuo.common.bpm.dto.ProcessAssignDTO;
import com.wstuo.common.bpm.dto.ProcessDetailDTO;
import com.wstuo.common.bpm.dto.ProcessHandleDTO;
import com.wstuo.common.bpm.dto.ProcessHistoriesDTO;
import com.wstuo.common.bpm.dto.ProcessHistoriesQueryDTO;
import com.wstuo.common.bpm.dto.TaskQueryDTO;
import com.wstuo.common.dto.PageDTO;

/**
 * business process instance interface class
 * @author wst
 *
 */
public interface IBPI {
	
	
	/**
	 * 根据定义ID分页查找流程实例
	 * @param processDefinitionId
	 * @param start
	 * @param limit
	 * @return PageDTO
	 */
	PageDTO findPageProcessInstance(String processDefinitionId, int start, int limit);
	
	/**
	 * 分页查找历史流程实例
	 * @param dto
	 * @param start
	 * @param limit
	 * @return PageDTO
	 */
	PageDTO findPageHistoryProcessInstance(ProcessHistoriesQueryDTO dto,int start, int limit);
	
	
	/**
	 * 根据流程定义ID获取流程全部活动
	 * @param deploymentId
	 * @return FlowPropertyDTO
	 */
	FlowPropertyDTO findProcessDefinitionActivity(String deploymentId);
	
	/**
	 * 根据流程实例ID获取当前流程下的Activity、Task、Outcomes
	 * @param processInstanceId 流程实例ID
	 * @return List<ProcessDetailDTO>
	 */
	List<ProcessDetailDTO> findProcessInstanceDetail(String processInstanceId);
	
	/**
	 * 查询历史流程实例
	 * @param processInstanceId 流程实例ID
	 * @return ProcessHistoriesDTO
	 */
	ProcessHistoriesDTO findHistoryProcessInstanceByPid(String processInstanceId);
	
	/**
	 * 流程启动(不会自动流转下一任务)
	 * @param id 
	 * @param variables
	 * @return String
	 */
	String startProcessById(String id,Map<String, Object> variables);
	
	/**
	 * 流程启动(不会自动流转下一任务)
	 * @param key
	 * @param variables
	 * @return String
	 */
	String startProcessByKey(String key,Map<String, Object> variables);
	
	/**
	 * 流程启动(自动流转下一任务)
	 * @param id
	 * @param variables
	 * @return String
	 */
	String startProcessByIdAndNextActivity(String id,Map<String, Object> variables);
	
	/**
	 * 流程启动(自动流转下一任务)
	 * @param key
	 * @param variables
	 * @return String
	 */
	String startProcessByKeyAndNextActivity(String key,Map<String, Object> variables);
	
	/**
	 * 结束流程实例ID
	 * @param pid
	 */
	void endProcessInstance(String pid);
	
	/**
	 * 判断流程实例是否已结束
	 * @param pid
	 * @return boolean
	 */
	boolean isEnded(String pid);
	
	/**
	 * 根据流程实例ID获取流程跟踪图
	 * @param processDefinitionId
	 * @return InputStream
	 */
	InputStream getProcessImage(String processDefinitionId);
	
	/**
	 * 根据流程实例ID获取流程XML
	 * @param processDefinitionId
	 * @return InputStream
	 */
	String getProcessXml(String processDefinitionId);
	
	/**
	 * 根据流程布署ID获取流程跟踪图
	 * @param processInstanceId
	 * @return InputStream
	 */
	InputStream getProcessImageByDeploymentId(String processInstanceId);
	
	/**
	 * 根据流程实例ID获取当前活动坐标
	 * @param processInstanceId
	 * @return ActivityCoordinatesDTO
	 */
	ActivityCoordinatesDTO getActivityCoordinates(String processInstanceId);
	/**
	 * 根据流程实例ID获取历史活动坐标
	 * @param pid
	 * @return List<ActivityCoordinatesDTO>
	 */
	List<ActivityCoordinatesDTO> getHistoryActivityCoordinates(String pid);
	
	/**
	 * 根据PID获取流程出口
	 * @param pid
	 * @return
	 */
	Set<String> getOutcomesByPid(String pid);
	
	/**
	 * 根据流程实例ID获取流程定义ID
	 * @param processInstanceId
	 * @return String
	 */
	String getProcessDefinitionIdByPid(String processInstanceId);
	
	/**
	 * 根据流程实例ID获取当前活动名称
	 * @param processInstanceId
	 * @return String
	 */
	String getActivityNameByPid(String processInstanceId);
	
	
	/**
	 * 分页查询指派给组的任务
	 * @param userId String type
	 * @param start
	 * @param limit
	 * @return PageDTO
	 */
	PageDTO findPageGroupTasks(String userId,int page,int limit);
	
	/**
	 * 分页查询指派给个人的任务
	 * @param userId
	 * @param start
	 * @param limit
	 * @return PageDTO
	 */
	PageDTO findPagePersonalTasks(String userId,int start,int limit);
	
	/**
	 * 分页查找待处理的任务
	 * @param queryDTO
	 * @param sidx
	 * @param sord
	 * @param start
	 * @param rows
	 * @return PageDTO
	 */
	PageDTO findJbpmTaskPager(TaskQueryDTO queryDTO,String sidx,String sord,int start,int rows);
	
	/**
	 * 根据流程实例获取历史任务
	 * @param processInstanceId
	 * @param assignee
	 * @param start
	 * @param limit
	 * @return PageDTO
	 */
	PageDTO findHistoryTask(String processInstanceId,String assignee,int start,int limit);
	
	/**
	 * 查找当前实例最后处理完成的历史任务
	 * @param processInstanceId 实例ID
	 * @return 最后处理完成的历史任务
	 */
	HistoryTaskDTO findLastCompleteHistoryActivityInstance(String processInstanceId);
	
	/**
	 * 统计某用户组下的任务
	 * @param userId
	 * @return Integer
	 */
	Integer countGroupTasks(String userId);
	
	/**
	 * 统计我待处理的任务
	 * @param userId
	 * @return Integer
	 */
	Integer countPersonalTasks(String userId);
	
	
	/**
	 * 统计我挂起的任务
	 * @param userId
	 * @return Integer
	 */
	Integer countGetPendingTasks(String userId);
	
	/**
	 * 统计我已处理的任务
	 * @param userId
	 * @return Integer
	 */
	Integer countMyProcessedTask(String userId);
	
	
	
	/**
	 * 任务指派
	 * @param taskId
	 * @param username
	 */
	void assignTask(String taskId,String username);
	
	/**
	 * 指派代理组
	 * @param taskId
	 * @param groupId
	 * @param participationType
	 */
	void addTaskParticipatingGroup(String taskId,String groupId, String participationType);
	
	/**
	 * 任务指派(根据PID)
	 * @param processInstanceId
	 * @param username
	 */
	void assignTaskByPid(String processInstanceId,String username);
	
	/**
	 * 指派代理组(根据PID)
	 * @param processInstanceId
	 * @param groupId
	 */
	void addTaskParticipatingGroupByPid(String processInstanceId,String groupId);
	
	
	/**
	 * 任务完成
	 * @param taskId
	 * @param next
	 * @param variables
	 */
	void completeTask(String taskId, String next, Map<String, Object> variables);
	
	/**
	 * 任务完成(如果完成未指派的任务，则先提取再完成)
	 * @param userId
	 * @param taskId
	 * @param next
	 * @param variables
	 */
	void completeTask(String userId,String taskId, String next, Map<String, Object> variables);
	/**
	 * 任务完成
	 * @param taskId
	 * @param next
	 */
	void completeTask(String taskId, String next);
	
	/**
	 * 任务完成
	 * @param taskId
	 */
	void completeTask(String taskId);
	/**
	 * 任务完成
	 * @param userId
	 * @param taskId
	 * @param next
	 */
	void completeTask(String userId,String taskId, String next);
	
	/**
	 * 删除任务
	 * @param taskId
	 */
	void deleteTask(String taskId);
	
	/**
	 * 设置流程实例变量
	 * @param processInstanceId 流程实例ID
	 * @param variables map type variable value
	 */
	void setInstanceVariables(String processInstanceId, Map<String, Object> variables);
	
	/**
	 * 设置流程实例变量
	 * @param processInstanceId 流程实例ID
	 * @param name 变量名称
	 * @param value 变量值
	 */
	void setInstanceVariables(String processInstanceId,String name,Object value);
	
	/**
	 * 根据流程实例获取指定变量值
	 * @param processInstanceId
	 * @param variableName
	 * @return Object
	 */
	Object getInstanceVariables(String processInstanceId,String variableName);
	
	/**
	 * 获取流程实例变量名
	 * @param processInstanceId
	 * @return Set<String>
	 */
	Set<String> getInstanceVariableNames(String processInstanceId);
	
	/**
	 * 获取流程实例全部变量
	 * @param processInstanceId
	 * @param variableNames Set<String>
	 * @return Set<String>
	 */
	Map<String,Object> getInstanceVariables(String processInstanceId, Set<String> variableNames);
	
	/**
	 * 设置任务变量
	 * @param taskId
	 * @param variables
	 */
	void setTaskVariables(String taskId,Map<String,Object> variables);
	
	/**
	 * 获取任务变量
	 * @param taskId
	 * @param variableName
	 * @return Object
	 */
	Object getTaskVariable(String taskId,String variableName);
	
	/**
	 * 获取任务变量名称
	 * @param taskId
	 * @return Set<String>
	 */
	Set<String> getTaskVariableNames(String taskId);
	
	/**
	 * 获取任务变量值
	 * @param taskId
	 * @param variableNames
	 * @return Map<String,Object>
	 */
	Map<String,Object> getTaskVariables(String taskId,Set<String> variableNames);
	
	/**
	 * 根据PID及当前活动获取下一节点
	 * @param processInstanceId
	 * @param activityName
	 * @param outcome
	 * @return String
	 */
	String getNextActivity(String processInstanceId , String activityName ,String outcome);
	
	/**
	 * 根据流程实例ID及任务名称获取相应的流程设置信息
	 * @param processInstanceId
	 * @param taskName
	 * @return FlowActivityDTO
	 */
	
	FlowActivityDTO findFlowActivityByPidTaskName(String processInstanceId , String taskName);
	/**
	 * 删除流程实例
	 * @param processInstanceId
	 */
	void deleteProcessInstance(String processInstanceId);
	
	/**
	 * 删除流程实例(级联)
	 * @param processInstanceId
	 */
	void deleteProcessInstanceCascade(String processInstanceId);
	
	/**
	 * 获取当前流程实例所有在任务ID
	 * @param processInstanceId
	 * @return List<String>
	 */
	 List<String> findAllTaskByPid(String processInstanceId);
	 
	 
	 
	 /**
	  * 流程开启
	  * @param obj
	  * @return 返回流程实例ID
	  */
	 String startFlowInstance(String Module ,String processDefinitionId , Object bean );
	 /**
	 * 获取流程定义
	 * @param processDefinitionId
	 * @param module
	 * @return
	 */
	 String getProcessDefinitionId(String processDefinitionId,String module);
	 /**
	  * 设置流程实例变量
	  * @param pid 流程实例ID
	  * @param obj 相应模块DTO对象
	  */
	 void setInstanceVariables(String pid , Object obj);
	 /**
	  * 查看流程任务指派人和组
	  * @param phDTO 流程处理DTO
	  * @param bean 相应模块DTO对象
	  * @param customAssign 自动指派接口
	  * @return 相应模块DTO对象
	  */
	 ProcessAssignDTO findProcessAssignDTO(ProcessHandleDTO phDTO, Object bean , ICustomAssign customAssign);
	 /**
	  * 流程任务指派
	  * @param phDTO 流程处理DTO
	  * @param dto 相应模块DTO对象
	  * @return 相应模块DTO对象
	  */
	 Object assignTask(ProcessHandleDTO phDTO,Object dto,ICustomAssign customAssign);
	 
	 /**
	  * 任务指派
	  * @param phDTO 流程处理DTO
	  * @param dto 相应模块DTO对象
	  * @return 相应模块DTO对象
	  */
	 Object assignTask(ProcessHandleDTO phDTO,Object dto);
	 /**
	  * 是否需要更新业务数据指派
	  * @param pid
	  * @return 是否需要指派结果
	  */
	 boolean isUpdateEventAssign(String pid);
	 
	 /**
	  * 查询下一活动类型
	  * @param pid 实例ID
	  * @param activityName 活动名
	  * @param outcome 出口
	  * @return 下一活动类型
	  */
	 String getNextActivityType(String pid , String activityName ,String outcome);
	 
	 
}
