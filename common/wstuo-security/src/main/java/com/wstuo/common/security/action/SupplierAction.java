package com.wstuo.common.security.action;

import java.io.InputStream;

import org.apache.log4j.Logger;

import com.opensymphony.xwork2.ActionSupport;

import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.exception.ApplicationException;
import com.wstuo.common.security.dto.SupplierDTO;
import com.wstuo.common.security.dto.SupplierQueryDTO;
import com.wstuo.common.security.service.ISupplierService;

/**
 * 供应商Action类
 * 
 * @author will
 * 
 */
@SuppressWarnings("serial")
public class SupplierAction extends ActionSupport {
	private static final Logger LOGGER = Logger.getLogger(SupplierAction.class);
	private ISupplierService supplierService;
	private PageDTO suppliers;
	private SupplierQueryDTO queryDto = new SupplierQueryDTO();
	private SupplierDTO supplierDto = new SupplierDTO();
	private int page = 1;
	private int rows = 10;
	private Long[] orgNos;
	private Long orgNo;
	private String sidx;
	private String sord;
	private String supplierFileName;
	private InputStream exportStream;
	private String effect = "";

	public String getEffect() {
		return effect;
	}

	public void setEffect(String effect) {
		this.effect = effect;
	}

	public String getSidx() {
		return sidx;
	}

	public void setSidx(String sidx) {
		this.sidx = sidx;
	}

	public String getSord() {
		return sord;
	}

	public void setSord(String sord) {
		this.sord = sord;
	}

	public ISupplierService getSupplierService() {
		return supplierService;
	}

	public void setSupplierService(ISupplierService supplierService) {
		this.supplierService = supplierService;
	}

	public PageDTO getSuppliers() {
		return suppliers;
	}

	public void setSuppliers(PageDTO suppliers) {
		this.suppliers = suppliers;
	}

	public SupplierQueryDTO getQueryDto() {
		return queryDto;
	}

	public void setQueryDto(SupplierQueryDTO queryDto) {
		this.queryDto = queryDto;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getRows() {
		return rows;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}

	public SupplierDTO getSupplierDto() {
		return supplierDto;
	}

	public void setSupplierDto(SupplierDTO supplierDto) {
		this.supplierDto = supplierDto;
	}

	public Long[] getOrgNos() {
		return orgNos;
	}

	public void setOrgNos(Long[] orgNos) {
		this.orgNos = orgNos;
	}

	public Long getOrgNo() {
		return orgNo;
	}

	public void setOrgNo(Long orgNo) {
		this.orgNo = orgNo;
	}

	public InputStream getExportStream() {
		return exportStream;
	}

	public void setExportStream(InputStream exportStream) {
		this.exportStream = exportStream;
	}

	public String getSupplierFileName() {
		return supplierFileName;
	}

	public void setSupplierFileName(String supplierFileName) {
		this.supplierFileName = supplierFileName;
	}

	/**
	 * Find customer by CompanyNo.
	 * 
	 * @return String
	 */
	public String findByCompany() {
		int start = (page - 1) * rows;

		queryDto.setStart(start);
		queryDto.setLimit(rows);

		suppliers = supplierService.findPagerSupplier(queryDto, sidx, sord);
		suppliers.setPage(page);
		suppliers.setRows(rows);

		return SUCCESS;
	}

	/**
	 * Add Customer.
	 * 
	 * @return String
	 */
	public String save() {
		supplierService.saveSupplier(supplierDto);

		return SUCCESS;
	}

	/**
	 * Edit Customer.
	 * 
	 * @return String
	 */
	public String merge() {
		try {
			supplierService.mergeSupplier(supplierDto);
		} catch (Exception ex) {
			throw new ApplicationException("ERROR_DATA_CAN_NOT_MERGE", ex.getMessage());
		}

		return SUCCESS;
	}

	/**
	 * deleteSuppliers
	 * 
	 * @return String
	 */
	public String deleteSuppliers() {
		try {
			supplierService.deleteSupplier(orgNos);
		} catch (Exception ex) {
			throw new ApplicationException("ERROR_DATA_CAN_NOT_DELETE", ex.getMessage());
		}

		return SUCCESS;
	}

	/**
	 * deleteSupplier
	 * 
	 * @return String
	 */
	public String deleteSupplier() {

		try {
			supplierService.deleteSupplier(orgNo);
		} catch (Exception ex) {
			throw new ApplicationException("ERROR_DATA_CAN_NOT_DELETE", ex.getMessage());
		}

		return SUCCESS;
	}

	/**
	 * 导出数据.
	 * 
	 * @return inputStream
	 */
	public String exportSupplier() {

		try {
			// 设置好要传入的groupNo
			exportStream = supplierService.exportSupplier(queryDto, sidx, sord);
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
		}
		return "exportSuccessful";
	}

	/**
	 * 导入数据
	 * 
	 * @return import result
	 */
	public String importSupplier() {
		effect = supplierService.importSupplier(supplierFileName) + "";
		return "importResult";
	}

}
