package com.wstuo.common.bpm.dao;

import com.wstuo.common.bpm.dao.IProcessUseDAO;
import com.wstuo.common.bpm.entity.ProcessUse;
import com.wstuo.common.dao.BaseDAOImplHibernate;
/**
 * ProcessUseDAO DAO Class
 * @author wstuo
 *
 */
public class ProcessUseDAO extends BaseDAOImplHibernate<ProcessUse> implements IProcessUseDAO {

}
