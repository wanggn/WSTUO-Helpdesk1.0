package com.wstuo.common.sso.util;

import java.io.IOException;
import java.util.Properties;

public class utilproper
{
  private Properties pro;
  private static final utilproper up = new utilproper();

  private utilproper() {
    init();
  }

  public static utilproper getInstance() {
    return up;
  }

  private void init() {
    this.pro = new Properties();
    try {
      this.pro.load(getClass().getResourceAsStream("/connCofig.properties"));
    } catch (IOException e) {
      System.out.println("连接失败");
      e.printStackTrace();
    }
  }

  public String getUrl() {
    return this.pro.getProperty("url");
  }
  public String getdriverName() {
    return this.pro.getProperty("diverName");
  }
  public String getusername() {
    return this.pro.getProperty("username");
  }
  public String getpassword() {
    return this.pro.getProperty("password");
  }
  public String getHql() {
	    return this.pro.getProperty("hql");
	  }
}