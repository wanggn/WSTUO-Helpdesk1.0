package com.wstuo.common.bpm.dto;

import com.wstuo.common.dto.BaseDTO;

/**
 * ParticipationDTO DTO Class(流程参与组、人员)
 * @author wstuo
 *
 */
@SuppressWarnings("serial")
public class ParticipationDTO extends BaseDTO {
	private String id;
	private String type;
	private String groupId;
	private String userId;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getGroupId() {
		return groupId;
	}
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
}
