package com.wstuo.common.Role.utils;

/**
 * 角色配置类
 * @author wing
 *
 */
public class RoleUtils {
	
	/**
	 * 系统管理员
	 */
    public final static String ROLE_SYSADMIN="ROLE_SYSADMIN";
    
    /**
     * 超级管理员代理
     */
    public final static String  ROLE_SUPER_ADMIN_DELEGAT=" ROLE_SUPER_ADMIN_DELEGAT";
    /**
     * 普通管理员,  
     */
    public final static String   ROLE_COMMON_ADMIN="ROLE_COMMON_ADMIN";
    /**
     * 外包客户管理员            
     */
    public final static String  ROLE_ITSOP_MANAGEMENT="ROLE_ITSOP_MANAGEMENT";
    /**
     * 审批成员</br> 
     * 不允许对其进行权限设置!
     */
    public final static String  ROLE_APPROVER="ROLE_APPROVER";
    /**
     * 终端用户</br> 
     * 当登记一条服务请求或IT故障的呼叫时，用户可以通过自助服务登记呼叫，或者通过直接在知识库内在线查找解决方法,终端用户,                
     */
    public final static String  ROLE_ENDUSER ="ROLE_ENDUSER ";
    /**
     * IT服务经理</br> 
     * 常规服务管理,监控登记的呼叫系统和相关的SLA报表。为了保证服务交付和质量，协调与管理其它角色。              
     */
    public final static String ROLE_ITSERVICEMANAGER="ROLE_ITSERVICEMANAGER";
    /**
     * 帮助台主管</br>
     * 这个角色是从用户那里升级请求,如果用户不满意的帮助台服务。它就是IT服务经理。              
     */
    public final static String  ROLE_HELPDESKTEAMLEADER ="ROLE_HELPDESKTEAMLEADER";
    /**
     * 帮助台工程师(一线工程师)</br>
     * 如果有可能，所有途经都通过登记呼叫,回答客户的呼叫并且快速提交解决方案。正常来讲，有80%的，甚至更多的呼叫都会通过这个角色来处理。"
     */
    public final static String  ROLE_HELPDESKENGINEER=" ROLE_HELPDESKENGINEER";
    /**
     * 二线工程师</br>
     * 做现场检查呼叫或者问题,或部署的变更。如果用户需要直接打电话给工程师，需要登记一条呼叫到帮助台               
     */
    public final static String ROLE_SECONDLINEENGINEER="ROLE_SECONDLINEENGINEER";
    /**
     * 三线工程师</br>
     * 做third-line支持,可能有更多些专家支持
     */
    public final static String ROLE_THIRDLINEENGINEER="ROLE_THIRDLINEENGINEER";
    /**
     * 四线工程师   </br>  
     * 从产品供应商可能有更多的专家。
     */
    public final static String  ROLE_FOURTHLINEENGINEER=" ROLE_FOURTHLINEENGINEER";
    /**
     * 问题经理 管理问题</br>  
     * 从日常呼叫中，检索或找到问题,接受问题。这个角色也联系专家提供解决方案
     */
    public final static String ROLE_PROBLEMMANAGER ="ROLE_PROBLEMMANAGER ";
    /**
     * 变更控制经理 </br> 
     *  控制所有的变更、协调和变更管理委员会成员。此角色也给呼叫会议和变更委员会成员
     */
    public final static String ROLE_CHANGECONTROLMANAGER="ROLE_CHANGECONTROLMANAGER";
    /**
     * CMDB管理员</br>  
     * CMDB主管是用来管理CMDB,并确保数据是最新的。CMDB主管也监控许可数
     */
    public final static String  ROLE_CMDBSUPERVISOR="ROLE_CMDBSUPERVISOR";
    /**
     * 知识库管理员</br>
     * 知识库管理是确保知识项在知识库中是最新的。对于某一标题这个角色也做一些培训,当用户查找知识库时非常有必要的             
     */
    public final static String ROLE_KNOWLEDGEBASEADMIN="ROLE_KNOWLEDGEBASEADMIN";
}
