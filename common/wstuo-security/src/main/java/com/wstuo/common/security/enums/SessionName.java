package com.wstuo.common.security.enums;

public enum SessionName {
	/**
	 * 用户账号
	 */
	LOGINNAME("loginUserName"),
	
	/**
	 * 用户ID
	 */
	USERID("userId"),
	
	/**
	 * 用户姓名
	 */
	FULLNAME("fullName"),
	
	/**
	 * 用户角色Code
	 */
	ROLECODES("roleCode"),
	
	/**
	 * 当前登录用户所属机构名称
	 */
	ORGNAME("orgName"),
	/**
	 * 当前登录用户所属机构ID
	 */
	ORGNO("orgNo"),
	
	/**
	 * 用户所属技术组
	 */
	TECHNOLOGYGROUP("technologyGroup"),
	/**
	 * 用户所属技术组No
	 */
	TECHNOLOGYGROUPNO("technologyGroupNo"),	
	/**
	 * 用户成本
	 */
	USERCOST("userCost"),

	/**
	 * 用户所属公司名称
	 */
	COMPANYNAME("companyName"),
	
	/**
	 * 用户所属公司ID
	 */
	COMPANYNO("companyNo"),
	
	/**
	 * 总公司ID
	 */
	TOPCOMPANYNO("topCompanyNo"),
	
	/**
	 * 呼叫中心工号
	 */
	CALLCENTERUSER("lcallCodeOper"),
	/**
	 * 呼叫中心服务器
	 */
	CALLCENTERHOST("remoteHost"),
	/**
	 * 呼叫中心端口
	 */
	CALLCENTERPORT("remotePort"),
	
	
	/**
	 * 验证码
	 */
	CAPTCHA("captcha"),
	
	/**
	 * TenantId
	 */
	TENANTID("tenantId"),
	/**
	 * version
	 */
	VERSION("version"),
	
	/**
	 * 终端用户风格
	 */
	ENDUSERSTYLE("endUserStyle"),
	
	/**
	 * 登录模式
	 */
	LOGINMODEL("loginModel");
	/**
	 * 属性名称
	 */
	private String name;

	private SessionName(String name){
		this.name = name;
	}
	public String getName(){
		return this.name;
	}
	
	@Override
	public String toString() {
		return "SessionName [name=" + name + "]";
	}
	
	
}
