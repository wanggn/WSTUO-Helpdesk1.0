package com.wstuo.common.security.dao;

import com.wstuo.common.dao.IEntityDAO;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.security.dto.RoleQueryDTO;
import com.wstuo.common.security.entity.Role;

import java.util.List;

/**
 * 角色DAO接口.
 * @author will
 */
public interface IRoleDAO
    extends IEntityDAO<Role>
{
    /**
     * 分页查找角色.
     * @param rqdto 查询DTO:RoleQueryDTO
     * @param sidx
     * @param sord
     * @return 分页数据:PageDTO
     */
    PageDTO findpager( final RoleQueryDTO rqdto, String sidx, String sord );

    /**
     * 根据相关属性查找角色列表.
     * @param propertyName 属性名称：String propertyName
     * @param value 属性值： Object value
     * @return 角色列表：List<Role>
     */
    List<Role> findBy( String propertyName, Object value );
    
    /**
     * 根据资源查找角色.
     * @param resourceCode
     * @return List<Role>
     */
    List<Role> findRolesByResource(String resourceCode);
    
	/**
	 * 根据用户编号，查询所有角色。
	 * @param userId
	 * @return List<Role>
	 */
	List<Role> findByUserId(Long userId);
	
	/**
	 * 根据资料URL，查询所有角色。
	 * @param resourceUrl
	 * @return List<Role>
	 */
	List<Role> findRolesByResourceByResUrl(String resourceUrl);
	/**
	 * 根据资源查找角色.
	 * @param roleCodes 角色Code
	 */
	List<Role> findRolesByRoleCodes(String[] roleCodes);
}
