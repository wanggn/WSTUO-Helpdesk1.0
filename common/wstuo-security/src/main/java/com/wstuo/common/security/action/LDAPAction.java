package com.wstuo.common.security.action;

import com.opensymphony.xwork2.ActionSupport;

import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.security.dto.LDAPAuthenticationSettingDTO;
import com.wstuo.common.security.dto.LDAPCheckDTO;
import com.wstuo.common.security.dto.LDAPDTO;
import com.wstuo.common.security.dto.LDAPQueryDTO;
import com.wstuo.common.security.dto.UserDTO;
import com.wstuo.common.security.service.ILDAPService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * LDAP Action类
 * 
 * @author will
 * 
 */
@SuppressWarnings("serial")
public class LDAPAction extends ActionSupport {
	@Autowired
	private ILDAPService ldapService;
	private PageDTO pageDto;
	private LDAPDTO ldapDto = new LDAPDTO();
	private LDAPQueryDTO queryDto = new LDAPQueryDTO();
	private LDAPCheckDTO ldapCheckDto = new LDAPCheckDTO();
	private UserDTO userDto = new UserDTO();
	private List<LDAPDTO> ldapDtos;
	private Long[] ids;
	private Boolean result = false;
	private int page = 1;
	private int rows = 10;
	private String sidx;
	private String sord;
	private LDAPAuthenticationSettingDTO ldapAuthenticationDTO;
	private String effect = "";

	public PageDTO getPageDto() {
		return pageDto;
	}

	public void setPageDto(PageDTO pageDto) {
		this.pageDto = pageDto;
	}

	public LDAPDTO getLdapDto() {
		return ldapDto;
	}

	public void setLdapDto(LDAPDTO ldapDto) {
		this.ldapDto = ldapDto;
	}

	public LDAPQueryDTO getQueryDto() {
		return queryDto;
	}

	public void setQueryDto(LDAPQueryDTO queryDto) {
		this.queryDto = queryDto;
	}

	public UserDTO getUserDto() {
		return userDto;
	}

	public void setUserDto(UserDTO userDto) {
		this.userDto = userDto;
	}

	public List<LDAPDTO> getLdapDtos() {
		return ldapDtos;
	}

	public void setLdapDtos(List<LDAPDTO> ldapDtos) {
		this.ldapDtos = ldapDtos;
	}

	public Long[] getIds() {
		return ids;
	}

	public void setIds(Long[] ids) {
		this.ids = ids;
	}

	public LDAPCheckDTO getLdapCheckDto() {
		return ldapCheckDto;
	}

	public void setLdapCheckDto(LDAPCheckDTO ldapCheckDto) {
		this.ldapCheckDto = ldapCheckDto;
	}

	public Boolean getResult() {
		return result;
	}

	public void setResult(Boolean result) {
		this.result = result;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getRows() {
		return rows;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}

	public String getSidx() {
		return sidx;
	}

	public void setSidx(String sidx) {
		this.sidx = sidx;
	}

	public String getSord() {
		return sord;
	}

	public void setSord(String sord) {
		this.sord = sord;
	}

	public LDAPAuthenticationSettingDTO getLdapAuthenticationDTO() {
		return ldapAuthenticationDTO;
	}

	public void setLdapAuthenticationDTO(LDAPAuthenticationSettingDTO ldapAuthenticationDTO) {
		this.ldapAuthenticationDTO = ldapAuthenticationDTO;
	}

	public String getEffect() {
		return effect;
	}

	public void setEffect(String effect) {
		this.effect = effect;
	}

	/**
	 * getADByLDAP()
	 * 
	 * @return String
	 */
	public String getADByLDAP() {
		pageDto = ldapService.getAllLdap(page, rows, queryDto.getLdapId());
		return SUCCESS;
	}

	/**
	 * ADUserImport
	 * 
	 * @return String
	 */
	public String adUserImport() {
		result = ldapService.adUserImport(userDto);
		return "result";
	}

	/**
	 * save
	 * 
	 * @return String
	 */
	public String save() {
		ldapService.saveLdap(ldapDto);

		return SUCCESS;
	}
	/**
	 * find
	 * 
	 * @return String
	 */
	public String find() {
		int start = (page - 1) * rows;

		queryDto.setLimit(rows);
		queryDto.setStart(start);
		pageDto = ldapService.findPagerLdap(queryDto, sidx, sord);
		pageDto.setPage(page);
		pageDto.setRows(rows);

		return SUCCESS;
	}

	/**
	 * merge
	 * 
	 * @return String
	 */
	public String mergeLdap() {
		ldapService.mergeLdap(ldapDto);

		return SUCCESS;
	}

	/**
	 * delete
	 * 
	 * @return String
	 */
	public String delete() {
		ldapService.deleteLdap(ids);

		return SUCCESS;
	}

	/**
	 * getLDAPAll
	 * 
	 * @return String
	 */
	public String getLDAPAll() {
		ldapDtos = ldapService.getLDAPAll();

		return "ldapDtos";
	}

	/**
	 * LDAPConfigCheck
	 * 
	 * @return String
	 */
	public String ldapConfigCheck() {
		result = ldapService.ldapConfigCheck(ldapDto);
		return "result";
	}

	/**
	 * LDAPConfigCheck
	 * 
	 * @return String
	 */
	public String ldapConnTest() {
		result = ldapService.ldapConnTest(ldapDto.getLdapId());
		return "result";
	}
	/**
	 * LDAP 验证配置连接测试
	 */
	public String LDAPAuthenticationSettingConnTest() {
		result = ldapService.LDAPAuthenticationSettingConnTest(ldapAuthenticationDTO);
		return "result";
	}

	/**
	 * connTestkerberos验证配置连接测试
	 */
	public String connTestkerberos() {
		result = ldapService.connTestkerberos(ldapAuthenticationDTO);
		return "result";
	}

	/**
	 * LDAP 连接测试
	 */
	public String connTestldap() {
		result = ldapService.connTestldap(ldapAuthenticationDTO);
		return "result";
	}

	/**
	 *  LDAP用户导入
	 */
	public String ldapUserImport() {
		effect = ldapService.ldapUserImport(userDto, queryDto.getLdapId());
		return "importResult";
	}
}
