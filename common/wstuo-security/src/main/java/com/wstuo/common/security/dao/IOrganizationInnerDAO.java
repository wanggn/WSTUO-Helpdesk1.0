package com.wstuo.common.security.dao;

import com.wstuo.common.dao.IEntityDAO;
import com.wstuo.common.security.entity.OrganizationInner;

import java.util.List;

/**
 * 内部机构DAO接口.
 * @author will 
 */
public interface IOrganizationInnerDAO
    extends IEntityDAO<OrganizationInner>
{
    /**
     * 根据公司编号查找内部机构列表.
     * @param companyNo 公司编号 Long
     * @return 内部机构列表 List<OrganizationInner>
     */
    List<OrganizationInner> findByCompany( Long companyNo );
}
