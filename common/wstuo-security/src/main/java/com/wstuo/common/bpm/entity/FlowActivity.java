package com.wstuo.common.bpm.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

import com.wstuo.common.bpm.entity.FlowActionProperty;
import com.wstuo.common.bpm.entity.FlowProperty;
import com.wstuo.common.bpm.entity.FlowTransition;

/**
 * Flow Activity Entity Class
 * @author wstuo
 *
 */
@Entity
public class FlowActivity extends FlowActionProperty {
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String activityName;//活动名
	private String activityType;//活动类型
	@ManyToMany
	private List<FlowTransition> flowTransition;//过渡
	@ManyToOne
	private FlowProperty flowProperty;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getActivityName() {
		return activityName;
	}
	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}
	public String getActivityType() {
		return activityType;
	}
	public void setActivityType(String activityType) {
		this.activityType = activityType;
	}
	public List<FlowTransition> getFlowTransition() {
		return flowTransition;
	}
	public void setFlowTransition(List<FlowTransition> flowTransition) {
		this.flowTransition = flowTransition;
	}
	public FlowProperty getFlowProperty() {
		return flowProperty;
	}
	public void setFlowProperty(FlowProperty flowProperty) {
		this.flowProperty = flowProperty;
	}
	
}
